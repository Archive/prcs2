#undef HAVE_HSTRERROR

/* #undef PACKAGE */
/* #undef VERSION */

#undef G_LOG_DOMAIN

#undef REPO_DEBUG
#undef HAVE_SSLEAY
#undef PRCS_DEVEL

#undef HAVE_DB2_INCLUDE_DIR
#undef HAVE_SSL_INCLUDE_DIR

#undef EXPLICIT_TEMPLATES
#undef NO_FD_SET
#undef SIGNAL_ARG_TYPE
#undef USE_SYS_FNMATCH
#undef SELECT_TYPE

#undef bool
#undef true
#undef false

#undef STRUCT_STAT_TIMESPEC
#undef STRUCT_STAT_TIME
#undef STRUCT_STAT_TIMENSEC
