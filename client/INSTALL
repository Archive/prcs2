-*- Text -*-

Sections in this file:

1. Things both binary and source distributions need.
2. How to install PRCS from a binary distribution.
3. How to install PRCS from a source distribution.
4. Running the first time.

Everyone should read the first and last section, read 2 or 3 depending
on whether you are compiling PRCS for yourself or using a precompiled
binary.

0. Remember to read the FAQ before asking questions if something fails!

1. Things both binary and source distributions need:

You need RCS version 5.7 installed.  If 'rcs -V' fails, then you don't
have the version 5.7.  PRCS is known to fail with previous versions of
RCS.  RCS must be properly configured!  If your version of RCS has been
configured to use the manufacturer's diff and diff3 programs, PRCS
and RCS will not be able to handle binary files.

You need the programs tar, gzip, and the GNU versions of diff and diff3.
These can be obtained in the GNU diffutils package, and are the same
that RCS needs to operate properly on binary files, so you might as
well insure that RCS is using them when configuring PRCS.

2. How to install PRCS from a binary distribution:

Simply install the binary somewhere, set RCS_PATH to a colon separated
list of directories containing the executables

************ NOTE ************

Run `prcs config' to see that the compiled-in command-paths are
correct.  If 'prcs config' reports an error, check RCS_PATH or the
version of RCS (rcs -V) or the version of diff (diff -v).

******************************

3. How to install PRCS from a source distribution:

A. Things you need to build the source distribution.

You need gcc version 2.7.2.  It does not compile on version 2.6.x due
to serious bugs in that version of g++.  It is not beleived to compile
with gcc 2.7.1 and 2.7.0, but this has not been verified.

(This may sound silly, but...) You need libg++ 2.7.1 or later.  It
will compile with an older version, but will not link.  On Digital
UNIX and SGI IRIX platforms, you need libg++ 2.7.2 or later due to
bugs in version 2.7.1 of the library.

B. Configuration and Compilation.

The configure argument --prefix lets you specify where PRCS is to be
installed.  The default installation prefix is /usr/local/lib.  If you
are using g++, you will need to tell `configure' where the libg++
libraries and include files are installed, using the environment
variable LIBGPLUSPLUS.  If it is not set, `configure' will assume that
g++'s default path is correct.  If the compiler's default path does
not include the lex or flex libraries, you should add it to the
LDFLAGS variable.  For example, if your shell is csh:

        setenv LIBGPLUSPLUS /usr/local/gcc-lib
	setenv LDFLAGS -L$HOME/lib
        setenv CC gcc
        setenv CXX g++
        ./configure --prefix=$HOME

or if your shell is a variant of sh (if your shell does not fall into
the category of "things that suck"):

        LIBGPLUSPLUS=$HOME/lib LDFLAGS=-L$HOME/lib CC=gcc CXX=g++ \
	  ./configure --prefix=$HOME

You may wish to supply several the additonal configure enable/disable
options:

        --disable-mmap	turns off all uses (and compilation of) mmap.
	--enable-debug	useful if you will be debugging PRCS.

Further, PRCS takes compile-time defaults for the various documented
environment variables when configure is run.  If set, each value is
used as a default when the users's environment does not have the
variable set.  The builder should take care that these settings are
appropriate for all users of the binary, or leave them unset at
compile-time.  'configure' prints out the variables it has detected,
ary is cfront based, and I'm not interested
in making this work.

4. Running the First Time.

Read at least the first several sections of the documentation.  It is
available from http://www.xcf.berkeley.edu/~jmacd/prcs.html or in the
info files contained in the source distribution.  Set your environment
varaible PRCS_REPOSITORY to the location you would like PRCS to use.
This directory will be created the first time you run PRCS, if it
doesn't already exist.  You probably want to set several other
environment variables, a complete list is contained at the end of the
documentation.
