;; PRCS minor mode
;; Notify Norbert Warmuth <k3190@fh-sw.de>

(defvar prcs-mode nil "The PRCS minor mode.")

(defvar prcs-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-c\C-c" 'prcs::prcs-checkin)
    map)
  "Keymap for `prcs-mode'.")

(make-variable-buffer-local 'prcs-minor-mode)

(or (assq 'prcs-mode minor-mode-alist)
    (setq minor-mode-alist
	  (cons '(prcs-mode " PRCS") minor-mode-alist)))

(or (assq 'prcs-mode minor-mode-map-alist)
    (setq minor-mode-map-alist (cons (cons 'prcs-mode
					   prcs-mode-map)
				     minor-mode-map-alist)))

(defun prcs-mode (&optional arg)
  "Toggle PRCS mode: C-c C-c runs 'prcs checkin'"
  (interactive "P")
  (if (setq prcs-mode
	    (if (null arg) (not prcs-mode)
	      (> (prefix-numeric-value arg) 0)))
      (run-hooks 'prcs-mode-hook)))

(provide 'prcs-minor)
