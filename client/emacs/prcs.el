;; -*-Mode: Emacs-Lisp;-*-
;; PRCS - The Project Revision Control System
;; Copyright (C) 1997  Josh MacDonald
;; Parts of this file by Jesse Glick <jglick@sig.bsh.com>; concepts
;; also due to Zack Weinberg <zack@rabi.phys.columbia.edu>, Dan
;; Nicolaescu <done@ece.arizona.edu>
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;; PRCS utility functionality: mainly, PRCS Major Mode (for project
;; files)

;; Reasonable .emacs fodder:
;;
;; (require 'cl)
;; (push '("\\.prj\\'" . prcs-mode) auto-mode-alist)
;; (autoload 'prcs-mode "prcs" "PRCS project-file mode." t)
;;
;; Avoid gratuitous backups:
;;
;; (setq backup-enable-predicate
;;       (lambda (name)
;;         (not (prcs-is-prcs-controlled (file-name-nondirectory name)))))
;; (autoload 'prcs-is-prcs-controlled "prcs" nil t)

;; $PrcsModeVersion: 1.28 on Wed, 08 Jul 1998 14:21:40 -0400 $
;;
;; TODO:
;;
;; List major features in a user-friendly fashion.
;;
;; An escaping parse which makes a change should offer to save the
;; buffer if it was unmodified previously.
;;
;; Caching seems to fail the first time after a parse which escaped
;; dots, though that should not make any difference since I am setting
;; the ticks after doing the parse!
;;
;; Honor `vc-make-backup-files'.
;;
;; Honor `vc-follow-symlinks' w.r.t. visit hook.
;;
;; `prcs-ask-about-save' <- `vc-suppress-confirm'. [But think about
;; effect on refreshes, which should be controllable separately.]
;;
;; `prcs-program-name' <- `vc-path'.
;;
;; Possibly replace uses of `cd-absolute' with a local binding for
;; `default-directory', which is a little nicer.
;;
;; Use `defined-derived-mode' if possible.
;;
;; XEmacs keymap compatibility.
;;
;; (Nonbinding) tab completion on major branch for checkin. Ditto for
;; diff and such things.
;;
;; Should be ways to turn various (slower) features off as
;; desired. [`prcs-check-if-file-modified' is at least faster now.]
;;
;; Refresh all after a `C-x v p'. [may be gratuitous]
;;
;; Search for `XXX' comments.
;;
;; ChangeLog name could be configurable. Or, could look for a file in
;; (Files) with :tag=changelog.
;;
;; ChangeLog message appearance leaves some things to be
;; desired. E.g., after inserting a message with embedded double
;; newlines, subsequent messages will be inserted in between
;; paragraphs because add-log gets confused about where it should
;; go. It may be necessary to either massage the message a fair bit
;; before insertion, or provide time for the user to do this. [See
;; discussion below.]
;;
;; Checkin should potentially see if it should merge against dest
;; branch first (if dest branch differs from current, or if acc. to
;; `prcs info -r.@ -f -q --plain-format' latest on current is not
;; this); if so, it should be able to check in onto scratch branch
;; instead (suggested: `user-login-name' + "_tmp"), then run
;; appropriate merge, then resume original checkin (unless things have
;; changed again since then...).
;;
;; What happens w/ nested projects? Presumably the subproject p-file
;; usually acts as if it belonged to the subproject, though refreshes
;; & the like may still work (modeline likely to become confused).

;; POSSIBLE VC-MODE PSEUDO-COMPATIBILITY SCHEME:
;;
;; Redefines of VC keys
;; --------------------
;; C-x C-q: default (read-only toggle) behavior
;; C-x v =: as currently in p-file, else single-file diff
;; C-x v a: need to think...
;; C-x v c: probably unimpl., or prompt for version deletion (p-file)
;; C-x v d: jump to p-file (currently C-x v p)
;; C-x v g: unimpl.
;; C-x v h: insert one of standard headers, or those mentioned in
;;          Project-Keywords (error from p-file), using completion; Format
;;          treated specially; just insert, no comment stuff
;; C-x v i: from p-file, create & populate new file; from other file,
;;          prompt to delete & depopulate
;; C-x v l: run info (details unclear)
;; C-x v r: checkout (file or project)
;; C-x v s: checkin (file or project)
;; C-x v u: revert to repository version (file or project); warn if mod
;; C-x v v: register (if called globally); else do a checkin
;; C-x v ~: ditto (this file, or whole project if in p-file)
;;
;; New keys in the same map
;; ------------------------
;; C-x v k: rekey (this file, or whole project)
;; C-x v n: rename (error if in p-file)
;; C-x v m: merge (just use interactive buffer, optionally with EMerge/EDiff hooks)
;; C-x v x: either release project/files (prompt if mod), or kill
;;          uncontrolled working files ("tidy", only from p-file), or
;;          both (acc. to prompt)
;; C-x v f: jump to file mentioned at point (only from p-file, else like C-x v d)
;;
;; Globally bound keys
;; -------------------
;; C-x v i: if in file buffer, and potent. PRCS controlled but not,
;;          prompt to do that; else run standard VC action
;; C-x v v: as for C-x v i (unless already in `vc-mode')
;; C-x v d: as for C-x v i (works in any buffers if potent. PRCS controlled)
;;
;; Log entry buffer
;; ----------------
;; C-c C-c: continue with check-in

;; POSSIBLE CHANGELOG UPGRADE:
;;
;; 1. Checkin inserts New-Version-Log into ChangeLog (if any)
;; immediately after date + author + mail + filename + splat as is
;; currently done, except:
;;
;; 1a. The filename is changed to `PROJECT:VERSION' (or somesuch),
;; rather than `PROJECT.prj'. Or, the version might be put in the
;; "defun" slot.
;;
;; 1b. \ and " are unescaped.
;;
;; 1c. The body is indented over by a TAB (except blank lines) and
;; filled.
;;
;; 1d. Examine code for `vc-comment-to-change-log' for hints.
;;
;; 1e. The insertion is skipped completely if the New-Version-Log
;; begins with a pound sign.
;;
;; 1f. A new entry is always created (i.e. a new splat-header, not
;; necessarily a new date + name + address header).
;;
;; 2. An alias (indirect) buffer is created upon aborted checkin or
;; upon `C-x v a' (see later) which spans the New-Version-Log string
;; (inside the quotes). Fill mode is maybe set.
;;
;; 2a. In the alias buffer, \ and " are bound to special commands
;; which insert a previous backslash, but make that backslash
;; invisible. (The invisibility should be a text property, not an
;; overlay; disable it in the p-file with `buffer-invisibility-spec'.)
;;
;; 2b. The alias buffer is created lazily, and destroyed upon
;; reversion of the project file or checkin.
;;
;; 2c. Wherever possible, key commands from within the alias buffer
;; work as they would if called from within the p-file.
;;
;; 3. `C-x v a' is bound to a command to add a partial log entry. It
;; always jumps into the alias buffer to do its work (other-window
;; display).
;;
;; 3a. If called from p-file, it just goes to the top of the buffer:
;; i.e. the message which will appear associated with the
;; project:version itself.
;;
;; 3b. If called from a controlled file, it jumps to (the beginning
;; of) that file's entry, creating it as necessary, of the form of a
;; new paragraph, splat, filename. There may be multiple filenames
;; there already, which is fine. If called from within a defun, that
;; is added in the usual place.
;;
;; 4. Aborted checkin sets a callback in the alias buffer to continue
;; same checkin upon `C-c C-c', to mimic VC-Mode behavior.

(require 'lisp-mode)
(require 'emerge)
(require 'cl)
(require 'add-log)

(defvar prcs-check-if-file-modified t
  "*If true, check visited PRCS files to see if they have been
modified since last checkout. The check is done in the background to
avoid overhead.")

(defvar prcs-use-toolbar t
  "*Whether to do things to the toolbar for PRCS support.")

(defvar prcs-auto-add-changelog t
  "*If non nil and the project contains a file matching named
ChangeLog in its root directory, prcs-mode will automatically insert
the contents of New-Version-Log into the ChangeLog.")

(defvar prcs-ask-about-save t
  "*If not nil, PRCS checkin asks which buffers to save before checking in.
Otherwise, it saves all modified buffers without asking.")

(defvar prcs-program-name "prcs"
  "*Name of the prcs executable.")

(defvar prcs-extra-checkin-args nil
  "*Extra arguments passed to checkin (list of strings).")

(defvar prcs-extra-diff-args '("-c")
  "*Extra arguments passed to diff (list of strings).")

(defvar prcs-display-path t
  "*If true, display the project path to a controlled file in its modeline.")

(defvar prcs-output-buffer-divider
  ;; Don't want to actually see this in prcs.el diffs!
  (concat "------------------%<------------------%<"
	  "------------------%<------------------")
  "*Divider between sections of PRCS output buffer.")

;; ----------------------------------------------------------

;; OK to redefine, need not make use of PROJECT:
(defun prcs-output-buffer-name (project)
  "Name of the prcs output buffer, possibly based on PROJECT."
  (concat "*prcs-output [" project "]*"))

(defconst prcs-descriptors
  '(Files Created-By-Prcs-Version Project-Description Project-Version Parent-Version
	  Version-Log New-Version-Log Checkin-Time Checkin-Login Populate-Ignore
	  Project-Keywords Merge-Parents New-Merge-Parents))
(defconst prcs-prj-descriptor-regex (mapconcat #'symbol-name prcs-descriptors "\\|")
  "Descriptors.")

(eval-after-load "font-lock"
  '(or (assq 'prcs-mode font-lock-defaults-alist)
       (setq font-lock-defaults-alist
	     (cons
	      (cons 'prcs-mode
		    '(prcs-font-lock-keywords
		      nil
		      t
		      (("+-*/.<>=!?$%_&~^:" . "w"))
		      beginning-of-defun
		      (font-lock-comment-start-regexp . ";")
		      (font-lock-mark-block-function . mark-defun)))
	      (if (boundp 'font-lock-defaults-alist) font-lock-defaults-alist nil)))))

(defconst prcs-font-lock-keywords
  (list
   (list (concat "(\\(" prcs-prj-descriptor-regex "\\)")
	 1 'font-lock-function-name-face)
   '("\\<:\\sw+\\>" 0 font-lock-reference-face prepend)
   )
  "PRCS keywords...")

(defun* prcs-check-that-file-is-ok (&optional (buffer (current-buffer)) force skip-modeline)
  "Handles background disk writes by PRCS and makes sure Emacs keeps
everything in synch for BUFFER.

If the file has been written on disk since last visit/save, but is
marked unmodified here, it refreshes the buffer and sets the modeline,
after confirming with the user (unless FORCE is non-nil).

If the file has been written on disk but is modified, does the same
thing, but only after a sterner warning (which is never turned off).

If the file is untouched on disk (whether the buffer is modified or
not), this does not try to revert.

In any case, the PRCS modeline setting will be recalculated, unless a
modified buffer was left as is, or SKIP-MODELINE is true."
  (save-excursion
    (set-buffer buffer)
    (when (and (not (verify-visited-file-modtime buffer))
	       (if (buffer-modified-p)
		   (yes-or-no-p (concat "Really revert " (buffer-file-name) " from disk, discarding modifications? "))
		 (or force (y-or-n-p (concat "Refresh " (buffer-file-name) " from disk? ")))))
      (revert-buffer t t t))
    (when (and (not (buffer-modified-p))
	       (not skip-modeline))
      (prcs-update-file-status))))

(defun* prcs-get-visited-buffers (&optional (buffer (current-buffer)))
  "Return a list of all currently visited buffers corresponding to
files in project file BUFFER (not including the project file buffer
itself)."
  ;; XXX This loops through file list and checks for the
  ;; buffer. Possibly it should actually loop through the buffers and
  ;; check for them in the file list, if that would be significantly
  ;; faster (probably unlikely to be too important unless you have a
  ;; huge project).
  ;; XXX this should skip over buffers with prcs-controlled-mode unset.
  (let ((base-dir (file-name-directory (buffer-file-name buffer))))
    (remove-if-not 'identity
		   (mapcar (lambda (desc)
			     (get-file-buffer (expand-file-name (prcs-coerce (car desc))
								base-dir)))
			   (cdr (find prcs-Files (prcs-parse-prj-file-cached buffer)
				      :key 'car))))))

(defun* prcs-prompt-for-saves (&optional (buffer (current-buffer)))
  "For all files mentioned in the project file BUFFER (default
current) which are also in memory buffers and modified, save them
(asking first if `prcs-ask-about-save' is non-nil). Includes project
file."
  (save-excursion
    (dolist (buf (cons buffer (prcs-get-visited-buffers buffer)))
      (set-buffer buf)
      (when (and (buffer-modified-p)
		 (or (not prcs-ask-about-save)
		     (y-or-n-p (concat "Save file " (buffer-file-name) "? "))))
	(save-buffer)))))

(defun* prcs-prompt-for-refreshes (&optional (buffer (current-buffer)) skip-modeline)
  "For all files mentioned in the project file BUFFER which are also
in memory buffers, make sure they are refreshed from disk as needed
with `prcs-check-that-file-is-ok', prompting in case of
`prcs-ask-about-save'. SKIP-MODELINE used as in
`prcs-check-that-file-is-ok'. Includes p-file."
  (dolist (buf (cons buffer (prcs-get-visited-buffers buffer)))
    (prcs-check-that-file-is-ok buf (not prcs-ask-about-save) skip-modeline)))

(defun* prcs-parse-prj-file-cached (&optional (buffer (current-buffer)))
  "Retrieve the parse for this buffer, from cache or fresh parse.
Use instead of `prcs-parse-prj-file'."
  (prcs-check-that-file-is-ok buffer nil t)
  (save-excursion
    (set-buffer buffer)
    (when (not (boundp 'parse-cache-tick))
      (when (not (string-match "\\.prj$" (buffer-file-name)))
	(error "Attempt to parse a non-p-file %S" buffer))
      (message "Warning--forcibly converting p-file to PRCS Mode")
      (prcs-mode))
    (when (> (buffer-modified-tick) parse-cache-tick)
      (setq parse-cache (prcs-parse-prj-file buffer))
      (setq parse-cache-tick (buffer-modified-tick)))
    parse-cache))

(defvar prcs-obarray-size 16383
  "*Suggested size for `prcs-obarray'. See Elisp manual for good
choices of numbers. You only need to change this if you have a huge
project, in which case you want something on order of 2-3 times the
number of files.")
(defvar prcs-obarray (make-vector prcs-obarray-size 0)
  "Obarray for symbols PRCS Mode creates during a parse.")

(defconst prcs-useful-symbols
  (mapcar 'symbol-name (append prcs-descriptors '(:symlink :no-keywords :directory)))
  "Symbols it is handy to refer to from the project file. The real
symbols are all interned privately, so simple quotes will not work.")
(eval-when (compile)
  (dolist (var prcs-useful-symbols)
    (set (intern (concat "prcs-" var)) nil)))
(dolist (var prcs-useful-symbols)
  (set (intern (concat "prcs-" var)) (intern var prcs-obarray)))

(defun prcs-coerce (object)
  "Coerce this number, string, or symbol into a string."
  (if object
      (typecase object
	(number (number-to-string object))
	(string object)
	(symbol (symbol-name object))
	(t (error "Weird object")))))

(defun* prcs-parse-prj-file (&optional (buffer (current-buffer)))
  "Return a simple list of all s-exps in project file, as symbols,
strings, numbers etc. (according to the superficial form). No
buffer-position info retained. Lists are natural.

Note that e.g. filenames are returned as symbols, so you probably want
to use `prcs-coerce' on them; they are interned in `prcs-obarray', so
you could find one by searching (via `eq') on `(intern \"...\"
prcs-obarray)'.

To compare against a standard keyword, e.g. `New-Version-Log', use the
variable `prcs-New-Version-Log' which will contain the properly
interned symbol.

`prcs-fixup-parse' is used to perform some postprocessing.

For speed, use `prcs-parse-prj-file-cached' instead.

This format is convenient for finding things: e.g. to get checkin time as
a string, try:

  (cadr (find prcs-Checkin-Time (prcs-parse-prj-file-cached BUFFER) :key 'car))"
  (message "Parsing project file...")
  (save-excursion
    (set-buffer buffer)
    (goto-char (point-min))
    (let (prj-sexps)
      (condition-case nil
	  ;; Optimistic first stab.
	  (while t
	    (push (let ((obarray prcs-obarray)) (read buffer)) prj-sexps))
	(end-of-file nil)
	(invalid-read-syntax
	 (prcs-escape-syntactic-nastiness)
	 ;; Try again.
	 (goto-char (point-min))
	 (setq prj-sexps nil)
	 (condition-case nil
	     ;; This had better work, else an error is really thrown.
	     (while t
	       (push (let ((obarray prcs-obarray)) (read buffer)) prj-sexps))
	   (end-of-file nil))))
      (let ((result (prcs-fixup-parse
                     (nreverse prj-sexps))))
        (message "Parsing project file...done")
        result))))

(defun prcs-escape-syntactic-nastiness ()
  "Escape symbols in current project buffers beginning with
dot. Operates heuristically, so is not foolproof, but mistakes
probably won't cause any harm. Skips over anything which font-lock
claims is a comment or string."
  (message "Escaping filenames...")
  (goto-char (point-min))
  (while (search-forward-regexp "(\\(\\.+\\)" nil t)
    (case (get-text-property (match-beginning 1) 'face)
      (font-lock-comment-face)
      (font-lock-string-face)
      (t (goto-char (match-beginning 1))
	 (dotimes (ignore (- (match-end 1) (match-beginning 1)))
	   (insert ?\\)
	   (forward-char 1))
	 (goto-char (match-end 0)))))
  (message "Escaping filenames...done"))

(defvar prcs-careful-parses nil
  "If true, actually insert copies of new-style scoped modifiers into
all the file descriptors they apply to. Else skip this step, but still
remove them from the Files list so as not to interfere.

You only need to turn this on if you are changing code so as to
actually depend on attributes of files in a specific fashion. Leaving
it off is more efficient.")

(defun prcs-fixup-parse (parse)
  "Given a basic parse of a project file, transform it to give
the illusion that it does not have multiple file lists. Thus the parse
of the following:

    (Files :tag=haha :tag=hoho
      (foo (...))
      (bar (...) :no-keywords))
    (Files
      (baz (quux) :symlink))

will come out as if it were:

    (Files
      (foo (...) :tag=haha :tag=hoho)
      (bar (...) :no-keywords :tag=haha :tag=hoho)
      (baz (quux) :symlink))

With `prcs-careful-parses' off, this would be:

    (Files
      (foo (...))
      (bar (...) :no-keywords)
      (baz (quux) :symlink))"
  (let (files-sexps other-sexps)
    ;; Pull out Files lists from all others.
    (dolist (sexp (reverse parse))
      (if (eq (car sexp) prcs-Files)
	  (push sexp files-sexps)
	(push sexp other-sexps)))
    ;; Unified Files list will go in front, arbitrarily. Otherwise
    ;; there is no rearrangement of sexps.
    (cons (cons prcs-Files
		;; Join together all lists after processing...
		(mapcan (lambda (files-sexp)
			  (let (modifiers
				(actuals (cdr files-sexp))) ; Skip prcs-Files
			    ;; Find modifier keys.
			    (while (and actuals
					(atom (car actuals)))
			      (push (pop actuals) modifiers))
			    (setq modifiers (nreverse modifiers))
			    ;; Put them on the end of each descriptor.
			    (if (and prcs-careful-parses modifiers)
				(mapcar (lambda (desc)
					  (append desc modifiers))
					actuals)
			      ;; Speed hack:
			      actuals)))
			files-sexps))
	  other-sexps)))

;; PRCS-Emerge
;;
;; These functions allow the script 'emerge', distributed with PRCS, to
;; invoke emerge during merge.  It uses a recursive edit.  Probably should
;; know what you're doing.

(defun prcs-emerge-files (file-A file-B file-out quit-hooks)
  "Run Emerge on two files."
  (emerge-files-internal file-A file-B nil quit-hooks file-out))

(defun prcs::emerge-files-with-ancestor (file-A file-B file-ancestor file-out quit-hooks)
  "Run Emerge on two files, giving another file as the ancestor."
  (emerge-files-with-ancestor-internal file-A file-B file-ancestor nil quit-hooks file-out))

(defun prcs::emerge(work com sel out)
  "Run Emerge on two or three files."
  (message "prcs::emerge with files %s %s %s %s" work com sel out)
  (let (quit-hooks)
    (add-hook 'quit-hooks (function (lambda () (exit-recursive-edit))))
    (add-hook 'quit-hooks (` (lambda () (emerge-files-exit (, out)))))
    (if (equal com "/dev/null")
	(prcs::emerge-files work sel out quit-hooks)
      (prcs::emerge-files-with-ancestor work sel com out quit-hooks))
    (recursive-edit)
    (message "prcs::emerge finished")
    )
  )

;; This is based on lisp-mode.el

;;; Code:

(defvar prcs-mode-syntax-table lisp-mode-syntax-table "")
(defvar prcs-mode-abbrev-table nil "")

(define-abbrev-table 'prcs-mode-abbrev-table ())

(defun prcs-mode-variables ()
  (setq local-abbrev-table prcs-mode-abbrev-table)
  (make-local-variable 'paragraph-start)
  (setq paragraph-start (concat page-delimiter "\\|$" ))
  (make-local-variable 'paragraph-separate)
  (setq paragraph-separate paragraph-start)
  (make-local-variable 'paragraph-ignore-fill-prefix)
  (setq paragraph-ignore-fill-prefix t)
  (make-local-variable 'fill-paragraph-function)
  (setq fill-paragraph-function 'lisp-fill-paragraph)
  ;; Adaptive fill mode gets in the way of auto-fill,
  ;; and should make no difference for explicit fill
  ;; because lisp-fill-paragraph should do the job.
  (make-local-variable 'adaptive-fill-mode)
  (setq adaptive-fill-mode nil)
  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'lisp-indent-line)
  (make-local-variable 'indent-region-function)
  (setq indent-region-function 'lisp-indent-region)
  (make-local-variable 'parse-sexp-ignore-comments)
  (setq parse-sexp-ignore-comments t)
  (make-local-variable 'outline-regexp)
  (setq outline-regexp ";;; \\|(....")
  (make-local-variable 'comment-start)
  (setq comment-start ";")
  (make-local-variable 'comment-start-skip)
  ;; Look within the line for a ; following an even number of backslashes
  ;; after either a non-backslash or the line beginning.
  (make-local-variable 'max-lisp-eval-depth)
  (setq max-lisp-eval-depth 10000)
  (make-local-variable 'max-specpdl-size)
  (setq max-specpdl-size 10000)
  (setq comment-start-skip "\\(\\(^\\|[^\\\\\n]\\)\\(\\\\\\\\\\)*\\);+ *")
  (make-local-variable 'comment-column)
  (setq comment-column 40)
  (make-local-variable 'comment-indent-function)
  (setq comment-indent-function 'lisp-comment-indent)
  (make-local-variable 'parse-cache)
  (setq parse-cache nil)		; don't parse until requested
  (make-local-variable 'parse-cache-tick)
  (setq parse-cache-tick 0))	; ticks start at 1

(defvar prcs-mode-map nil "Keymap for `prcs-mode'.")

(if prcs-mode-map
    ()
  (let ((map (make-sparse-keymap)))
    (define-key map "\C-c\C-c" 'prcs-checkin)
    (define-key map "\C-c\C-d" 'prcs-diff)
    (define-key map "\C-c\C-r" 'prcs-rekey)
    (when prcs-use-toolbar
      (define-key map [menu-bar prcs]
	(cons "PRCS" (make-sparse-keymap "PRCS")))
      (define-key map [menu-bar prcs rekey]
	'("Rekey" . prcs-rekey))
      (define-key map [menu-bar prcs complex-diff]
	'("Arbitrary diff" . prcs-diff-with-prefix))
      (define-key map [menu-bar prcs simple-diff]
	'("Working diff" . prcs-diff))
      (define-key map [menu-bar prcs checkin]
	'("Checkin" . prcs-checkin))
      )
    (setq prcs-mode-map (nconc map shared-lisp-mode-map))
    )
  )

(defvar prcs-mode-hook '() "Hooks for `prcs-mode'.")

(defun prcs-mode ()
  "Major mode for editing PRCS project files.
Commands:
Delete converts tabs to spaces as it moves back.
Blank lines separate paragraphs.  Semicolons start comments.
\\{prcs-mode-map}
Entry to this mode calls the value of `prcs-mode-hook'
if that value is non-nil.
\\[prcs-checkin] checks in the project as it stands.
\\[prcs-diff] looks for differences from last checkin/checkout;
C-u \\[prcs-diff] also prompts for version numbers.
\\[prcs-rekey] rekeys the project.
\\[prcs-jump-to-project-file] jumps here from other buffers."
  (interactive)
  (kill-all-local-variables)
  (use-local-map prcs-mode-map)
  (set-syntax-table prcs-mode-syntax-table)
  (setq major-mode 'prcs-mode)
  (setq mode-name "PRCS")
  (prcs-mode-variables)
  (run-hooks 'prcs-mode-hook))
;; XXX not if already there...
(push '("\\.prj\\'" . prcs-mode) auto-mode-alist)

(put 'Files 'lisp-indent-function 0)
(put 'Created-By-Prcs-Version 'lisp-indent-function 0)
(put 'Project-Description 'lisp-indent-function 0)
(put 'Project-Version 'lisp-indent-function 0)
(put 'Parent-Version 'lisp-indent-function 0)
(put 'Version-Log 'lisp-indent-function 0)
(put 'New-Version-Log 'lisp-indent-function 0)
(put 'Checkin-Time 'lisp-indent-function 0)
(put 'Checkin-Login 'lisp-indent-function 0)
(put 'Populate-Ignore 'lisp-indent-function 0)
(put 'Project-Keywords 'lisp-indent-function 0)
(put 'Merge-Parents 'lisp-indent-function 0)
(put 'New-Merge-Parents 'lisp-indent-function 0)

(defun* prcs-output-buffer (&optional (project-buffer (current-buffer)) divide switch-to)
  "Get a ready-to-use output buffer given PROJECT-BUFFER.
If DIVIDE is T, put in a divider at the end.
If it is BACK, go back to the last divider.
If nil, just retrieve the buffer.
If SWITCH-TO is non-nil, display the buffer too."
  (let* ((orig-buf (current-buffer))
	 (project-name (prcs-coerce
			(cadr (find prcs-Project-Version
				    (prcs-parse-prj-file-cached project-buffer)
				    :key 'car))))
	 (output-buffer (get-buffer-create (prcs-output-buffer-name project-name))))
    (if switch-to (switch-to-buffer-other-window output-buffer)
      (set-buffer output-buffer))
    (goto-char (point-max))
    (cond
     ((eq divide t)
      (insert prcs-output-buffer-divider "\n")
      (goto-char (point-max)))
     ((eq divide 'back)
      (let ((pos (search-backward prcs-output-buffer-divider nil t)))
	(if pos (goto-char pos)))))
    (when (not switch-to) (set-buffer orig-buf))
    output-buffer))

(defun* prcs-command (subcommand options &key
				 (buffer (current-buffer))
				 what
				 extra-args
				 (save 'as-needed)
				 (refresh 'as-needed)
				 (display t)
				 (error-on-fail t)
				 asynch
				 (killable t)
				 exclusive
				 sentinel
				 sentinel-data)
  "Run a PRCS command SUBCOMMAND with options OPTIONS (a list, `-f' is
implicit). BUFFER is a PRCS-controlled buffer which serves as the
reference point for everything. The command is always run from the
correct directory with the project file as first argument; WHAT
controls the second argument: none if `nil' (i.e. whole project),
project file if `t', or if `self', the correct relative path to BUFFER
(even if that is the project file); EXTRA-ARGS if any are also
appended (they are not options!).

SAVE determines whether to prompt to save buffers: `nil' means none,
`t' means all visited buffers for this project, `as-needed' (default)
means acc. to value of WHAT. Similar for REFRESH, after
command termination. DISPLAY (default on) means show the output buffer
(this is always written to in any case). ERROR-ON-FAIL signals an
error if PRCS completed in a failure condition; normally the exit
status is returned (1 does not signal an error).

If ASYNCH is true, the command is run asynchronously (and the process
object is returned instead); KILLABLE (default true) means it can be
killed off at Emacs exit; EXCLUSIVE if set to a symbol puts a kind of
lock on the BUFFER named by the symbol and will only run if no such
lock already exists (removed on exit); SENTINEL may be given as a
function accepting the process object, the BUFFER, and if desired
SENTINEL-DATA for state, to be run upon exit."
  ;; XXX implement
)

(defun prcs-checkin (&optional noact)
  "Checkin the current project from working dir.
Prompts for version log if necessary, and major version.
With prefix arg, takes no action, just shows what would happen."
  (interactive "P")
  (prcs-prompt-for-saves)
  (let* ((prj (prcs-parse-prj-file-cached (current-buffer)))
	 (log (cadr (find prcs-New-Version-Log prj :key 'car))))
    (when (not log)
      (goto-char (point-min))
      (error "Can't find New-Version-Log"))
    (when (and (zerop (length log))
	       (y-or-n-p "No version log entered, enter one first? "))
      (goto-char (point-min))
      ;; Best effort to find it:
      (search-forward-regexp "(New-Version-Log[ \t\n]+\"" nil t)
      (error "Checkin aborted."))
    (when (and prcs-auto-add-changelog (not noact) (not (zerop (length log))))
      (prcs-add-changelog (current-buffer) log))
    (cd-absolute (or (file-name-directory (buffer-file-name)) "."))
    (let* ((major (prcs-coerce (caddr (find prcs-Project-Version prj :key 'car))))
	   (new-major (read-string "Major version to checkin onto: " major))
	   (exited (apply 'call-process
			  (append (list prcs-program-name
					nil
					(prcs-output-buffer (current-buffer) t)
					nil
					"checkin"
					(concat "-r" new-major ".@")
					(if noact
					    "-fln"
					  "-fl"))
				  prcs-extra-checkin-args
				  (list (file-name-nondirectory (buffer-file-name)))))))
      ;; Always refresh p-file.
      (prcs-check-that-file-is-ok (current-buffer) t t)
      ;; Save time: we know what the modelines should be. In
      ;; particular, newly added files should now read "", not "+",
      ;; just like everything else.
      (prcs-prompt-for-refreshes (current-buffer) t)
      (let* ((new-prj (prcs-parse-prj-file-cached (current-buffer)))
	     (new-p-v (find prcs-Project-Version new-prj :key 'car))
	     (new-project (prcs-coerce (cadr new-p-v)))
	     (new-version (concat (prcs-coerce (caddr new-p-v)) "."
				  (prcs-coerce (cadddr new-p-v))))
	     (new-status ""))
	(save-excursion
	  (dolist (buf (cons (current-buffer) (prcs-get-visited-buffers)))
	    (set-buffer buf)
	    (setq prcs-controlled-project new-project)
	    (setq prcs-controlled-version new-version)
	    (setq prcs-controlled-status new-status))))
      (prcs-output-buffer (current-buffer) 'back t)
      (if (not (= exited 0))
	  (message "PRCS exited with non-zero status--failed"))
      )
    )
  )

(defun prcs-add-changelog (buffer message)
  (when (find (intern (file-name-nondirectory (change-log-name)) prcs-obarray)
	      (cdr (find prcs-Files (prcs-parse-prj-file-cached buffer) :key 'car))
	      :key 'car)
    (save-window-excursion
      (message "Adding a ChangeLog entry...")
      ;; XXX the "current defun" could plausibly be the project
      ;; version, perhaps, so you would get something like:
      ;;
      ;; foo.prj (release.17): ...
      ;;
      ;; Alternatively, this could be messily tacked onto the
      ;; name+address line above it.
      (let ((add-log-current-defun-function (lambda () nil)))
	;; XXX note that this will insert the name of the current
	;; buffer, i.e. the project file. There does not seem to be
	;; any good way to override that.
	(add-change-log-entry))
      ;; XXX should this trim outer newlines or somesuch?
      (insert message)
      ;; XXX might be nice to have it pause & wait for user to do
      ;; superficial edits, esp. as the indentation is probably
      ;; screwy.
      (save-buffer)
      (message "Adding a ChangeLog entry...done")
      )
    )
  )

(defun prcs-diff (&optional repo-versions)
  "List differences in the working version from the repository version.
With a prefix argument, prompts for versions to compare."
  (interactive "P")
  (prcs-prompt-for-saves)
  (cd-absolute (or (file-name-directory (buffer-file-name)) "."))
  (let* ((extra-dash-r
	  (if repo-versions
	      (let* ((prj (prcs-parse-prj-file-cached (current-buffer)))
		     (pv (find prcs-Parent-Version prj :key 'car))
		     (cv (find prcs-Project-Version prj :key 'car))
		     (pv-string (read-string "Older version: "
					     (concat (prcs-coerce (caddr pv)) "."
						     (prcs-coerce (cadddr pv)))))
		     (cv-string (read-string "Newer version (blank for working): "
					     (concat (prcs-coerce (caddr cv)) "."
						     (prcs-coerce (cadddr cv))))))
		(if (> (length cv-string) 0)
		    (list
		     (concat "-r" pv-string)
		     (concat "-r" cv-string))
		  (list (concat "-r" pv-string))))
	    nil))
	 (exited (apply 'call-process
			(append (list prcs-program-name
				      nil
				      (prcs-output-buffer (current-buffer) t)
				      nil
				      "diff"
				      "-f")
				extra-dash-r
				(list (file-name-nondirectory (buffer-file-name)))
				(if prcs-extra-diff-args
				    (append (list "--") prcs-extra-diff-args)
				  nil)))))
    (prcs-output-buffer (current-buffer) 'back t)
    (case exited
      (2 (error "PRCS exited with error status--failed"))
      (1 (message "There were differences."))
      (0 (message "No differences."))
      (t (error "Weird exit status")))
    )
  )
(defun prcs-diff-with-prefix ()
  "Like `prcs-diff', but with automatic prefix argument."
  (interactive)
  (prcs-diff t))

(defun prcs-rekey ()
  "Rekey the project."
  (interactive)
  (prcs-prompt-for-saves)
  (cd-absolute (or (file-name-directory (buffer-file-name)) "."))
  (case (call-process prcs-program-name nil (prcs-output-buffer (current-buffer) t)
		      nil "rekey" "-f")
    (2 (error "PRCS exited with error status--failed"))
    (0 t)
    (t (error "Weird exit status")))
  (prcs-prompt-for-refreshes (current-buffer) t)
  (prcs-output-buffer (current-buffer) 'back))

;;;;;; PRCS Minor Mode (for PRCS-controlled files, incl. the p-file)

(defvar prcs-controlled-mode nil
  "True when in a PRCS-controlled buffer.")
(make-variable-buffer-local 'prcs-controlled-mode)

;; Modeline display.
(make-variable-buffer-local 'prcs-controlled-project)
(make-variable-buffer-local 'prcs-controlled-version)
(make-variable-buffer-local 'prcs-controlled-status)
(make-variable-buffer-local 'prcs-controlled-path)
(or (assq 'prcs-controlled-mode minor-mode-alist)
    (push '(prcs-controlled-mode
	    (" " prcs-controlled-project ":" prcs-controlled-version
	     (prcs-controlled-status prcs-controlled-status "?")
	     prcs-controlled-path))
	  minor-mode-alist))

;; Generally already loaded into Emacs image; need `C-x v' keymap to
;; already exist:
(require 'vc-hooks)
;; Note that these bindings are global. This is actually kind of nice
;; since you can `C-x v p' from e.g. a directory listing to get to the
;; nearest applicable project file.
(global-set-key "\C-xvp" 'prcs-jump-to-project-file)
(when prcs-use-toolbar
  (define-key vc-menu-map [separator3] '("----"))
  (define-key global-map [menu-bar tools vc prcs-jump]
    '("Jump to PRCS project" . prcs-jump-to-project-file)))

(add-hook 'find-file-hooks 'prcs-maybe-put-into-controlled-mode)
(add-hook 'after-save-hook 'prcs-update-file-status)

(defun prcs-controlled-mode (&optional toggle info)
  "Toggle (or with prefix, turn on or off acc. to value) PRCS
Controlled Mode.

This minor mode is used for buffers that are to be considered under
PRCS control, including the project file itself (which is also in PRCS
Major Mode).

Currently just sets the modeline."
  (interactive "P")
  (let ((old-pcm prcs-controlled-mode))
    (setq prcs-controlled-mode
	  (if toggle (> (prefix-numeric-value toggle) 0) (not old-pcm)))
    (when (and (not prcs-controlled-mode) old-pcm)
      ;; Turning it off.
      ;; Don't want to kill process because that can leave locks around.
      (setq prcs-status-checker nil))
    (when (and prcs-controlled-mode (not old-pcm))
      ;; We are turning it on.
      (prcs-update-file-status (current-buffer) info))))

(make-variable-buffer-local 'prcs-status-checker)
(defun* prcs-update-file-status (&optional (buffer (current-buffer)) maybe-info)
  "Update modeline status acc. to PRCS. BUFFER is buffer to check;
INFO if any is existing result of `prcs-is-prcs-controlled' with
second arg true."
  (when prcs-controlled-mode
    (save-excursion
      (set-buffer buffer)
      (let* ((info (or maybe-info (prcs-is-prcs-controlled (buffer-file-name) t)))
	     (p-file (car info))
	     (desc (cadr info))
	     (pbuf (or (get-file-buffer p-file) (find-file-noselect p-file)))
	     (prj (prcs-parse-prj-file-cached pbuf))
	     (p-v (find prcs-Project-Version prj :key 'car)))
	(setq prcs-controlled-project (prcs-coerce (cadr p-v)))
	(setq prcs-controlled-version (concat (prcs-coerce (caddr p-v)) "."
					      (prcs-coerce (cadddr p-v))))
	(setq prcs-controlled-path
	      (if (and desc prcs-display-path)
		  (let* ((logical-path (concat "/" (prcs-coerce (car desc))))
			 (posn (search "/" logical-path :from-end t)))
		    (substring logical-path 0 (+ posn 1)))
		""))
	(setq prcs-controlled-status (if (and desc (not (cadr desc))) "+" nil))
	(when (and prcs-check-if-file-modified
		   (not prcs-controlled-status)
		   (not prcs-status-checker))
	  ;; Need to do an asynchronous check.
	  (let* ((default-directory (file-name-directory p-file))
		 (process-connection-type nil)
		 (checker (apply 'start-process
				 (append
				  (list "PRCS Controlled Status Checker"
					(prcs-output-buffer pbuf t)
					prcs-program-name "diff" "-f")
				  (if desc
				      ;; Controlled file.
				      (list p-file
					    (expand-file-name (buffer-file-name)))
				    ;; Project file.
				    (let ((base-name (file-name-nondirectory p-file)))
				      (list base-name base-name)))
				  (list "--" "-q")))))
	    (setq prcs-status-checker checker)
	    (process-kill-without-query checker)
	    (push (cons checker (buffer-file-name)) prcs-status-sentinel-queue)
	    (set-process-sentinel checker 'prcs-status-sentinel)
	    ;; Now in the background.
	    ))))))

(defvar prcs-status-sentinel-queue nil
  "A-list of processes running to determine PRCS status.")
(defun prcs-status-sentinel (process event)
  "Monitors a `prcs diff' and updates the modeline acc. to the result."
  (let ((file (cdr (assq process prcs-status-sentinel-queue))))
    (when (not file)
      (error "Process does not seem to correspond to a real PRCS sentinel"))
    (setq prcs-status-sentinel-queue
	  (delete* file prcs-status-sentinel-queue :key 'cdr :test 'string-equal))
    (let ((buffer (get-file-buffer file)))
      ;; May be nil if the buffer has already been killed, in which
      ;; case we do not care.
      (when buffer
	(save-excursion
	  (set-buffer buffer)
	  (setq prcs-controlled-status
		(case (process-status process)
		  (exit (case (process-exit-status process)
			  (0 "")
			  (1 "*")
			  (2 "???")
			  (t (error "Weird status"))))
		  (run (error "PRCS status process still running"))
		  (signal "???")
		  (t (error "Weird process result"))))
	  (force-mode-line-update)
	  (setq prcs-status-checker nil))))))

(defun prcs-is-potentially-prcs-controlled (name)
  "Provides list of potential names of project files that could be
controlling this file. Returns nil if none, of course, so can be used
as a predicate. More specific (lower-down) project files are listed
first. (No particular order if at same level.)

XXX: could become confused if you have some controlled files in a
directory symlinked to from the actual working dir, but not present
in it. This is pretty obscure."
  (let* ((dir (expand-file-name (file-name-directory name)))
	 (potential nil))
    ;; Probably not the most elegant algorithm!
    (do ((idx 0)
	 (stop nil))
	(stop nil)
      (if (string-match "/" dir idx)
	  (progn
	    (setq idx (match-end 0))
	    (let* ((test-dir (substring dir 0 (1+ (match-beginning 0))))
		   (test-files (save-excursion
				 ;; ICK!!!!!!!!! Bug in directory-files: sometimes,
				 ;; depending on what buffer you are currently in,
				 ;; running directory-files dies with "Wrong type
				 ;; argument: buffer-or-string-p, t" for no apparent
				 ;; reason. Edebug claims this occurs within an
				 ;; ange-ftp hook which calls directory-files again!
				 ;; The choice of buffers in which this occurs is
				 ;; reproducible but apparently random.
				 (set-buffer (get-buffer-create "*scratch*"))
				 (directory-files test-dir t "\\.prj$"))))
	      (setq potential (nconc potential test-files))))
	(setq stop t)))
    (reverse potential)))

(defun prcs-is-prcs-controlled (name &optional get-descriptor-too)
  "Is this file listed in the (Files) section of some enclosing project
file? If so, returns the name of that project file. Lower-down project
files take precedence.

If GET-DESCRIPTOR-TOO is non-nil, will return instead a list of the project
file name, then the file descriptor list: e.g.

	(.../foo.prj (bar/baz\\.c (foo/39_bar 1.2 666) :tag=mytag))

For the project file itself, just returns that name (or a list of that
name, if GET-DESCRIPTOR-TOO is set, since there is no actual
descriptor).

This makes a very useful addition to `backup-enable-predicate'.

XXX maybe should warn if multiple p-files contain it, to detect
possibly confusing use of subprojects."
  (if (string-match "\\.prj$" name)
      (if get-descriptor-too (list name) name)
    (save-excursion
      (block iterate
	(dolist (try (prcs-is-potentially-prcs-controlled name))
	  (let ((buf (get-file-buffer try)))
	    ;; Avoid refreshing font-lock whenever possible; it incs ticks!
	    (when (not buf) (setq buf (find-file-noselect try)))
	    ;; A little brutal, but hey...
	    (set-buffer buf)
	    (when (not (eq 'prcs-mode major-mode))
	      (prcs-mode))
	    (let ((found (find (intern
				(file-relative-name name (file-name-directory try))
				prcs-obarray)
			       (cdr (find prcs-Files
					  (prcs-parse-prj-file-cached buf)
					  :key 'car))
			       :key 'car)))
	      (when found
		(return-from iterate (if get-descriptor-too (list try found) try))))))
	nil))))

(defun prcs-maybe-put-into-controlled-mode ()
  "Hook to put buffers into PRCS Controlled mode where appropriate."
  (let ((info (prcs-is-prcs-controlled (buffer-file-name) t)))
    (when info
      (prcs-controlled-mode 1 info))))

(defun prcs-jump-to-project-file ()
  "Jump to the nearest applicable project file from here."
  (interactive)
  (let* ((here (or (buffer-file-name) default-directory))
	 (prj (or (prcs-is-prcs-controlled here)
		  (car (prcs-is-potentially-prcs-controlled here)))))
    (if prj
	(if (string-equal (buffer-file-name) prj)
	    (message "Already here!")
	  (switch-to-buffer-other-window (or (get-file-buffer prj)
					     (find-file-noselect prj))))
      (error "Could not find applicable project file!"))))

(provide 'prcs)
