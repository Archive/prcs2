<html> <head>
<title>PRCS Merge</title>
</head>

<body bgcolor="#ffffff" text="#000000">
<h1>PRCS Merge</h1>

Merge is probably the most difficult operation in PRCS to understand.
For that reason, I have prepared a fairly detailed explanation that
walks through several examples.  These examples should serve aside the
reference manual as a definition of the "correct" behavior.  If the
implementation produces results that do not agree with this document,
the implementation is incorrect.
<P>

In brief, this document covers the following topics:
<ul>
     <li> <A HREF="#SEC1">Preliminaries</A> </li>
     <li> <A HREF="#SEC2">Choosing the Common Version</A> </li>
     <li> <A HREF="#SEC3">Performing the Merge</A> </li>
     <li> <A HREF="#SEC4">Partial Merges</A> </li>
     <li> <A HREF="#SEC5">Subsequent Merges</A> </li>
     <li> <A HREF="#SEC6">Advanced Merges and Manual Intervention</A> </li>
     <li> <A HREF="#SEC8">Checkin After a Merge</A> </li>
</ul>

<h3><A NAME="SEC1">Preliminaries</A></h3>

A merge takes place between three project versions.  First, there is a
current version of the project in the repository that contains changes you
would like to incorporate.  This version is the <em>selected</em> version.
The set
of files that you have modified and wish to merge with the selected
version is called the <em>working</em> version.  In order to determine
the changes that have taken place in these two versions, a common
point of reference must be chosen.  This version is called the
<em>common</em> version.  The choice of a <em>common</em> version is
very important.  It allows PRCS to distinguish changes that have been made
in only one of the selected and working versions from conflicting changes that
have been made in both.  This distinction, in turn, determines appropriate
default dispositions for the changes.
A large part of this document describes the algorithm
for choosing a common version and justifies its correctness.  I
also discuss what a <em>vendor branch</em> is and why PRCS
needs no special support for such a thing.
<P>

Once a common version has been chosen, triplets of files from each of
the three versions are found and each triplet is individually
considered.  The next section of this document describes the choices
presented to the user at this time, possible error conditions, and
logic behind the default actions.
<P>

Sometimes you don't want to merge everything, resulting in what is called a
<em>partial merge</em>.  The third section describes the issues and
problems involved with partial merges, and how PRCS's default actions
help you deal with these situations.
<P>

After you've finished one merge, nothing is stopping you from merging
again.  This is, in fact, not an uncommon thing to do.  The fourth
section describes how the algorithm for choosing a common version is
altered for <em>subsequent merges</em>.
<P>

Sometimes PRCS just can't decide what to do for you, and sometimes
you've mixed things up so that merge either tells you to do something
by hand or asks you to make a choice where something is ambiguous.
Even worse, sometimes you want to do something PRCS thinks you
shouldn't.  In these situations, you may need to know how to override
certain default actions.  The fifth section describes these <em>merge
difficulties</em> and <em>skilled merging</em>.
<P>

Once you've merged to your heart's content and want to check in
another version, you've first got a few extra things to think about.
In fact, in most non-trivial cases you need to think about
what you're doing before you perform the merge.  The sixth section
describes issues involved with <em>checkin after merging</em>.  It
also describes the algorithm for choosing a parent version in general.
<P>

There's no way to make the most out of a version control system
without understanding these issues.  I hope that describing merge in a
semi-tutorial way will help people lose their fear of advanced
version-control features.

<h3><A NAME="SEC2">Choosing the Common Version</A></h3>

The number of different situations PRCS needs to deal with
is large, but most of these will result only from nonsensical
operations and don't need to be considered.  Let's first consider the
simplest practical case; where two developers have checked out the
same version of a project, one checks in a new version, and the second
merges.  Before proceeding, we'll introduce some notation for the
diagrams.
<P>

In each diagram, time runs from the bottom to the top.  Hexagons
denote versions in the repository and squares represent working
versions.  The arrows running between them indicate relationships
between these versions.  An arrow going up means that the upper
version was produced from the lower, either by the user's
modifications, or (for arrows running from a hexagon) by a checkout,
or (for arrows running to a hexagon) by a checkin, or by some
combination of these.  An arrow going down indicates that the lower
version is the parent of the upper version.  A horizontal arrow
indicates equivalence between the contents of a repository version and
a working version.

<P>
For example, if we check out version 0.3 to produce a working version,
edit it, and then check in version 0.4, we get the following diagram: <P>

<!-- merge2 -->
<center><IMG SRC="merge2.gif"></center>
<P>

To unclutter the diagrams, I'll often suppress intermediate working versions.
For the example above, this would result in the following, simpler picture:
<P>

<!-- merge3 -->
<center><IMG SRC="merge3.gif"></center>
<P>

With the diagrams explained, we need some concrete names.  Let's name
the two developers A and B.  They operate on a project named P, and
initially each developer has a copy of the initial version, 0.1.
Continuing with the simple scenario, A modifies his copy of 0.1 and
checks in 0.2.  B modifies his copy of 0.1 and then wants to reconcile
his changes with those made by A, and so must merge.  In this case, B
has a working version that is a <em>modified</em> set of working
files derived from 0.1.  The selected version is 0.2, the version B
wishes to reconcile changes with.  The common version is 0.1, since
both the working and selected versions were derived directly from it.
In this case, the common version was quite obvious.
<P>

When the merge is complete, B is left with a (possibly) modified set
of working files and a project file that has recorded in it the
events of the merge.  The repository <em>has not been modified</em>!
After the merge, B checks in version 0.3.  Version 0.3 has two parent
versions, 0.1 and 0.2.  The first parent records the original version
version 0.3 was derived from, since B started with 0.1.  The sequence
of events is shown below:
<P>

<!-- merge4 -->
<center><IMG SRC="merge4.gif"></center>
<P>

To be more precise requires a definition of the parent-version and the
algorithm PRCS uses.  Each time a version is checked in it has one or
more parent versions set.  The sequence of parent versions
establishes an ancestry graph that is directed and contains no
cycles (ancestry is a partial ordering).  A version is an ancestor of
another if it can be reached by following zero or more parent
versions.  In the simple example above, merge picks the common version
to be the only common ancestor of the both the working and selected
versions (the ancestors of a working version are the ancestors of the repository version from which it was checked out).
In the general case, the common ancestor is chosen from
the <em>set</em> of versions that are ancestors of both of the parent versions
of the working and selected versions.  If unique, the common version
is the element of this set of common ancestors that is not
an ancestor of any other element in the set: the <em>nearest</em> common
ancestor.  If the set of common ancestors is not empty, it must
contain at least one nearest ancestor since the graph is acyclic.  If
there is more than one nearest ancestor, merge will report an
ambiguity and ask you to choose a common version.  A situation where
there is more than one nearest ancestor is difficult to obtain, and
best avoided.  It can only arise when there is more than one source
in the graph, meaning that versions were imported into branches with
the empty version as their parent.  If the set of common ancestors is
empty, the common version is chosen as the empty version in the
selected branch (this choice over the working branch is arbitrary;
all branches have an empty version with minor version 0).
<P>

Merging directly into a modified working version (as in the previous
example) works, but I don't recommend it.  There are too many ways it
can go wrong.  Since the working files input by B are the only copy of
the files he wishes to merge, any accidents can cause him a lot of
trouble.  PRCS is careful not to delete anything, but accidents
happen.  It is prudent to for B to first check his working files into
a branch off of 0.1.  Many people I talk to about using PRCS seem to
be afraid of creating branches.  They are really quite simple--unlike,
perhaps, other version control systems.
Let's have B, as an alternative procedure, make several checkins onto another
branch named B.  He might, for example, type: <P>

<pre>
$ prcs checkin -l -r B P
</pre>

PRCS will confirm the creation of a new branch and check in version
B.1.  Afterwards, perhaps, B continues development and checks in two more
branches, leaving the tip of the B branch at minor version 3.
Since then, let's say that A has continued developing on major branch 0, and
has checked in version 0.3.  Now B would like to return his changes to
the main branch so that A can use his changes.  He merges against
major version 0.3 with the command:

<pre>
$ prcs merge -r 0
</pre>

The common version is obviously still the same, 0.1.  Nothing changed
except the number of points where B checkpointed a version.  Suppose B
completes the merge and checks in version 0.4, which will have
parent versions B.3 and 0.3.  A diagram follows:
<P>

<center><IMG SRC="merge5.gif"></center>
<P>

B can then continue development on the 0 branch and check in 0.5, or
he can create another branch at 0.4 similar to the B branch, for another
series of checkpoints before returning to the 0 branch again. <P>

B may also want to return to developing on the B branch, where
he left off at B.3.  This procedure is used for
<em>vendor branches</em>, so called because they track changes
to another person's (the vendor's) data.  PRCS has no special knowledge of
vendor branches.  Instead, its merge algorithm is powerful enough
to have any number of  vendor branches without special
consideration for what is and isn't a vendor branch.
<P>

***********DELETE************

The vendor
branch scenario describes the case where B continues development on
the B branch after B.3 has been merged with 0.3 because it's not
actually B doing the merge, B has sent the sources for B.3 and are
merged into the local changes made by A; B continues development at
B.3.
**********************

For clarity, let's examine the use of vendor branches with names
changed: the vendor branch will be called V; the local modifier and
local branch will be called L.  The vendor distributes release 1.0 of
a piece of software.  L gets a copy of this release and checks it in
as V.1.  L then checks out V.1, makes some local modifications and
checks the result in as L.1.  The vendor then releases 1.1, which L
obtains and checks in as V.2.  V.2 then gets merged against L.1,
producing L.2, and so forth.  L now modifies L.2 to produce L.3, which
he subsequently merges with V.3 to produce L.4, and so forth.
The important point is how the
common version is chosen at each merge.  The common versions are the
vendor releases.  For example, when V.3 is merged against L.3, V.2
is the common
version, and so on.  In the following diagram, the dashed lines run
from local versions produced by merges to the common ancestors used in
the merge.  <P>

<center><IMG SRC="merge6.gif"></center>
<P>

In short, there is nothing special about a vendor branch.  Another
developer could just as well treat the L branch as a vendor branch.
<P>

There is more to choosing a common version when multiple merges are
performed between checkins.  They will be covered
<A HREF="#SEC5">later</A>.
<P>

<h3><A NAME="SEC3">Performing the Merge</A></h3>

After a common version has been chosen, the merge proceeds by picking
tuples of files contained in each of the three versions.  The
files are matched up either by name and by internal-file family, as
discussed in the reference manual.
Roughly, an internal-file family sticks with a given file through
renamings of that file.  You can, for example, re-arrange a vendor's
files into a new directory structure with new naming conventions, and
yet still be able to merge your local files against the corresponding
vendor files.  In the usual case, however, files with the same name
have the same file family. <P>

Each file in each version is only considered once.  A file may be
present in all three versions, in only two versions (three possible
combinations), or possibly only in one version.  This makes seven
possibilities just for the existence/nonexistence of files.  When you
consider the number of situations where files may or may not differ
from one another, there are 14 cases.  For the case where all three
files exist: all three may be identical, two files may be identival
while the third differs, or all three may differ, a total of five
cases.  For the three cases where only two files exist, either the
files differ or they do not, for a total of six cases.  Add in the
three cases where a file exists only in one version, and you have a
grant total of 14 distinct situations. <P>

The general idea is that whenever a change has been made between the
selected version and the common version that hasn't been made to the
working version it should be incorporated into the working version.
Incorporating a change means either deleting a file, adding a file,
replacing a file, or merging two or three files.  When PRCS detects
that there may be a change you would like to incorporate into the
working version, it prompts you for the appropriate action and
continues.  PRCS does not prompt the user for three of the 14 cases,
listed listed as "no prompt" below.  These are the cases where there is no
change to be incorporated into the working version.  There are five
default values when PRCS prompts the user--the four actions listed above
plus <em>nothing</em>, this case is different from "no prompt" because
it indicates that there have been changes but PRCS can't recommend
what to do one way or another and that you probably want to ignore the
change.  <P>

First, the five cases where all files exist:
<ul>
     <li>All files are equal.  No prompt.<br>
	 No changes have been made.
     </li>
     <li>All files differ.  Prompt <em>merge</em>.<br>
	 There are no equivalent files among the three files in each version.
	 It is assumed that you are interested in merging the selected
	 version's changes with your changes.  Therefore, the default action is
	 merge.
     </li>
     <li>Selected file differs.  Prompt <em>replace</em>.<br>
	 Your version of the file is unchanged from the common version, but
	 the selected version has been modified.  PRCS assumes that the
	 selected version of the file is more up to date, and the default
	 action is to replace your file with the file from the selected
	 version.
     </li>
     <li>Working file differs.  No prompt.<br>
	 The selected version does not differ from the common version,
	 but the working file does.
     </li>
     <li>Common file differs.  No prompt.<br>
	 The working and selected version have both changed, but the
	 changes are identical, so there are no changes to incorporate
	 into the working version.
     </li>
</ul>

The three cases where only a pair of files exist but are <em>equal</em>:
<ul>
     <li>Common and Selected.  Prompt <em>nothing</em>.<br>
	 You deleted a file in the working version that was in the
	 common version and is still in the selected version.  The
	 selected version has not been modified, so PRCS assumes that
	 the file is obsolete.  Still, it prompts to make you
	 aware of the change.
     </li>
     <li>Common and Working.  Prompt <em>delete</em>.<br>
	 The selected version deleted a file that you have not
	 modified, so PRCS assumes that the file should be deleted.
     </li>
     <li>Working and Selected.  No action.<br>
	 Both versions have added an identical file that is not
	 present in the common version; there are no
	 changes to incorporate.
     </li>
</ul>

The 3 cases where only a pair of files exist but are
<em>different</em>:
<ul>
     <li>Common and Selected.  Prompt <em>nothing</em>.<br>
	 You deleted a file in the working version that was in the
	 common version and is still in the selected version.  The
	 selected version has been modified.  This type of conflict
	 cannot be merged, you must decide what to do.  The default
	 action assumes the deletion is correct.
     </li>
     <li>Common and Working.  Prompt <em>delete</em>.
	 The selected version deleted a file that you have not
	 modified, so PRCS assumes that the file should be deleted,
	 even though you modified the file.
     </li>
     <li>Working and Selected.  Prompt <em>merge</em>.<br>
	 Both versions added a file and the files differ.  The merge
	 will take place with an <em>empty</em> common file, and is
	 likely to produce serious changes.
     </li>
</ul>

The 3 cases where only a single file exists:
<ul>
     <li>Common.  No action.  <br>
	 Both working and selected versions deleted the file,
	 so both agree and there are no changes to
	 incorporate.
     </li>
     <li>Working.  No action.  <br>
	 Working version added a file not present in either of the
	 other two.  PRCS assumes the addition is valid and therefore
	 there is no change necessary to the working version.
     </li>
     <li>Selected.  Prompt <em>add</em>.<br>
	 The file exists only in the selected version and is assumed
	 to be valid.
     </li>
</ul>
<P>

Except for <em>merge</em>, the merge actions are self explanatory.
The merge action runs the diff3 program on the three files.  PRCS uses
GNU diff3, which is described in the info pages for GNU diff3.
Briefly, it finds the insertions and deletions between the common and
selected file and the common and working file.  It then outputs a file
containing both sets of insertions and deletions, placing markers
around the output when the two changes conflict.  To help deal with
conflicts, PRCS reports files containing conflicts.   You may arrange to edit
conflicted files immediately by setting the PRCS_CONFLICT_EDITOR
environment variable to your favorite text editor.
<P>

PRCS records which files you have merged in the project file.  If you
quit a merge in the middle, it will be marked incomplete in the
project file.  This way, when you re-issue the merge command, you do
not end up considering the same files twice.

<h3><A NAME="SEC4">Partial Merges</A></h3>

You can restrict the set of files that merge considers during a merge,
resulting in what is called a partial merge.  You achieve a similar
effect by terminating a merge before it is complete.  First, you
may wish to eventually finish a complete merge by running several
partial merges.  In this case, merge will notify you when the last
file has been merged and you can proceed.  This is the common use of a
partial merge.  <P>

Sometimes, you may wish to merge against certain files in another
version.  This is a somewhat tricky affair.  The problem is that as
far as PRCS is concerned the next time you attempt a checkin the
version is in an inconsistent state, halfway between one version and
the other.  <P>

In any case, if you attempt to check in a version that has not been
completely merged, checkin will ask you several questions.  First, it
asks if you really want to check in a partially merged version.  If
you respond yes, checkin asks whether to consider the version fully merged.
The question really asks whether or not the partially merged-against
version should be considered a parent of the new version.
<P>

Suppose there are two branches named L and M, where L.1 and M.1 share
a common parent.  All versions contain two files, named F and G.  A
developer with version L.3 does a partial merge against M.3, merging
only the file F, not G.  This might happen if the developer knew that
the file F had some changes that did not depend on G, and he wanted
those changes but not the changes in G.  He might type:

<pre>
$ prcs merge -rM.3 P F
# merge takes place...
$ prcs checkin P
</pre>

Checkin asks whether to consider M.3 as a parent.  If yes, then by
default PRCS checks in version M.4.  Suppose M.4 later gets merged
against L.4.  The common version chosen is L.3, so that the common
version chosen for merging the file G is incorrect.
<P>

If M.3 is not considered as a parent, then PRCS checks in L.4.
Later, if L.4 is merged against M.4 (or M.3), the common version
is the common parent of L.1 and M.1.  The file chosen for merging
the file F is incorrect.
<P>

The developer may also want to keep M.3 as a parent and checkin as
L.4.  To do this, he types:

<pre>
$ prcs checkin -rL P
</pre>

Supplying "-rL" forces PRCS to checkin as L.4 whether or not M.3 is
considered a parent.  If yes, then a later merge against the M
branch will produce a common version M.3.  The file chosen for merging
the file G is incorrect.
<P>

As you can see, checking in incomplete merges can create problems.  If
you don't intend to do later merges between the two branches, or if
you really pay attention to what you're doing, you can accomplish the
desired effect.

<h3><A NAME="SEC5">Subsequent Merges</A></h3>

Suppose you've just finished a merge.  Now what?  You can check in a
version or you can keep working.  If you keep working, then at some
point in the future you may wish to merge again.  This section
describes how subsequent merges work.
<P>

The common scenario is this: developers A and B check out the same
version, 0.1.  A checks in 0.2 and B merges.  A checks in 0.3 and B
merges again.  This repeats until B checks in a version.  Each version
B merges against is added to the list of parents.  For the second
merge, B's working files have two parents, 0.1 and 0.2.  When he
merges against 0.3, according to the algorithm defined above, it picks
the nearest element of the set of common versions, 0.2.  This is the
correct version, since B's files were most recently reconciled with
version 0.2.
<P>

In a less common scenario, an N-way merge is performed between
different branches.  First, some facts.  Order matters.  If it were
not for conflicts, it would be possible to say something about the
dependence on order.  Here, conflicts refers to more than just those
sometimes produced when a merge action is taken on a group of files,
it also refers to places where file additions or deletions conflict
with each other.  If there are no conflicts, however, order should not
matter.  I say "should not" because diff3 can get confused, so you
still have to be aware of what you're doing.  The important point is
that parents are updated after each merge, regardless of whether a
checkin is performed, so each subsequent merge takes place as
if it had been checked in first.  As an example, consider the
following diagram.
<P>

<center><IMG SRC="merge7.gif"></center>
<P>

Here M.4 is merged with B.3, producing W1.  The set of common
ancestors for this merge contains only M.1.  Now W1 is merged with
A.3.  The set of common ancestors contains both A.2 and M.1.  A.2 is
the nearest and is chosen as the common version.
<P>

When you attempt to merge against a new version following an
incomplete merge, PRCS asks the same question as it would at checkin:
should the partially merged version be considered a parent.

<h3><A NAME="SEC6">Advanced Merges and Manual Intervention</A></h3>

Once you have a good understanding of the merge process, you might
find the default decisions PRCS makes getting in your way.  A command
line option <tt>--skilled-merge</tt> has been created for telling PRCS
that you think you know what you're doing.  <P>

Specifically, PRCS will not let you merge a file twice.  If you want
to force a second merge, run merge with <tt>--skilled-merge</tt> and PRCS will
ask for each file that has already been considered whether it should
reconsider that file.
<P>

Sometimes PRCS can't help you.  When you mix partial merges where
files are selected by name or by file-family, it is possible to end up
in a situation where a file should to be added that already exists if
grouped by the other selection criteria.  In this case, PRCS will
print a message explaining the situation and tell you to deal with the
problem by hand.

<h3><A NAME="SEC7">Checkin After a Merge</A></h3>

After you have completed a series of merges you probably want to
check in a version.  Checkin proceeds as if the working version is the
last version merged against.  If the last merge is incomplete, it asks
whether to consider it a parent.

<hr>
<center>
<!-- hhmts start -->
Last modified: Sun Aug 31 16:16:38 PDT 1997
<!-- hhmts end --><br>
<a href="http://www.cs.berkeley.edu/~jmacd/">Josh MacDonald</a> /
<i><a HREF="mailto:jmacd@cs.berkeley.edu">jmacd@cs.berkeley.edu</a></i>
</center>

</body> </html>
