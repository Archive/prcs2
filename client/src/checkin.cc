/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: checkin.cc 1.22.1.2.1.9.1.23.1.1.1.6.1.15.1.2.1.55 Wed, 31 Mar 1999 02:17:22 -0800 jmacd $
 */

#include "prcs.h"
#include "prcs2.h"

#include "projdesc.h"
#include "fileent.h"
#include "misc.h"

static gboolean    client_checkin_start (Connection* conn, Peer* peer, void* server_data);

static TaskControl client_checkin_project_file     (Connection* conn, Task* task);
static TaskControl client_checkin_project_reply    (Connection* conn, Task* task);
static TaskControl client_checkin_project_reply_a  (Connection* conn, Task* task);
static TaskControl client_checkin_project_recv     (Connection* conn, Task* task);
static TaskControl client_checkin_project_recv_a   (Connection* conn, Task* task);
static TaskControl client_checkin_files_send_again (Connection* conn, Task* task);
static TaskControl client_checkin_files_send_one   (Connection* conn, Task* task);
static TaskControl client_checkin_files_send_reply (Connection* conn, Task* task, ReadyFunc ready, void* data);

static void client_checkin_success (Connection* conn, TaskManager* tm);
static void client_checkin_failure (TaskManager* tm);

static Transmition client_checkin_transmition[] =
{
  { & client_checkin_project_file,    NULL,                            ProtocolAsync },
  { NULL,                             & client_checkin_project_reply,  ProtocolAsync },
  { NULL,                             & client_checkin_project_recv,   ProtocolAsync },
  { & task_consume_ready,             client_checkin_files_send_again, ProtocolAsync }
};

TaskControl
client_checkin_files_send (Connection* conn, Task* task, gboolean send)
{
  if (send)
    {
      return task_consume (conn, task);
    }
  else
    {
      return client_checkin_files_send_again (conn, task);
    }
}


Request prcs_client_checkin_request = {
  "Client Checkin",
  PRCS_PROTOCOL_CHECKIN_REQUEST,
  STATIC_ARRAY (client_checkin_transmition),
  ST_PrcsReply,
  { (ServerStartFunc) & client_checkin_start },
  client_checkin_success,
  client_checkin_failure
};

#define CONN_DATA(conn) ((CheckinData *) conn_data (conn, CheckinData))

static PrVoidError maybe_query_user    (ProjectDescriptor *project);
static PrVoidError compute_new_project (ProjectDescriptor *project);

typedef struct {
  SerialPrcsReply *server;
  SerialPrcsReply  reply;
  Peer            *peer;
  SerialType       msg_type;
  SerialPrcsCheckinProjectReply *prj_reply;
  void*            msg;
} CheckinData;

static PrcsRepository* repo;
static ProjectDescriptor *project;

PrPrcsExitStatusError
checkin_command()
{
  SerialAliasedIdentifier pid;
  SerialVersionIdentifier vid;

  if (! (project = read_project_file (cmd_root_project_file, true)))
    throw prcserror << "@@@ this was already printed" << dotendl;

  /* remove files which were not named on the command line */
  eliminate_unnamed_files (project);

  /* warn the user about filenames on the command line which didn't match anything */
  Return_if_fail (warn_unused_files(true));

  /* warn the user about filenames on the command line that don't exist */
  Return_if_fail (eliminate_working_files(project, QueryUserRemoveFromCommandLine));

  /* compute everything */
  Return_if_fail (compute_new_project(project));

  /* if -n was specified */
  if (option_report_actions)
    return ExitSuccess;

  /* check if the user wants to enter a log. */
  Return_if_fail(maybe_query_user(project));

  pid = * project->project_id ();

  vid.minor = cmd_version_specifier_minor;

  if (cmd_version_specifier_major[0])
    {
      vid.major.alias = cmd_version_specifier_major;
      vid.major.id = "";
    }
  else
    {
      vid.major = * project->branch_id ();
    }

  /* @@@ make debugging easier for now. */ vid.major.id = "";
  /* @@@ make debugging easier for now. */ pid.id = "";

  repo = new PrcsRepository (& pid, & vid);

  Return_if_fail (repo->init ());

  return PrPrcsExitStatusError (repo->go (PRCS_PROTOCOL_CHECKIN_REQUEST));
}

/* This function will, in one pass, replace 4 of the old functions.
 * These were: check_project (which should have been named
 * check_empty_descriptors_and_missing_previous_rcs_version_data)
 * quick_elim_unmodified, slow_elim_unmodified, and compute_modified.
 * its much clearer now, the reason being that the
 * client/server/repository/norcs separation makes it make so much
 * more sense.  */
/* What it does now:
 *
 *   report all modifications to project
 *   compute md5 checksum for all regular files being checked in
 *   make sure that all files not being checked in are safe to ignore
 *
 */
static PrVoidError
compute_new_project (ProjectDescriptor *project)
{
  gboolean project_modified = FALSE;

  foreach_fileent (fe_ptr, project)
    {
      FileEntry *fe = *fe_ptr;

      const char* name = fe->working_name();

      guint8 *props;
      guint32 props_len;
      gboolean unmodified = FALSE;
      const guint8* data = NULL;
      guint len = 0, flags = 0;

      /* This checks whether empty descriptors are being excluded, or
       * whether their files are non-existant.  The first case is
       * queries the user to re-include the file (only if it exists),
       * and the second results in failure. (of course, they are
       * written in the reverse order).
       */
      if (! fe->on_cmd_line() && fe->has_new_family())
	{
	  if (! fe->working_file_exists ())
	    {
	      throw prcserror << "The file " << squote(name)
			      << " has an empty descriptor and does not exist.  You may not "
		"do a partial checkin excluding files with empty descriptors" << dotendl;
	    }
	  else
	    {
	      static BangFlag bang;

	      prcsquery << "The file " << squote(name)
			<< " has an empty descriptor and was excluded on the command line.  "
		"Partial checkins may not ignore files with empty descriptors.  "
			<< force("Continuing")
			<< report("Continue")
			<< allow_bang(bang)
			<< optfail('n')
			<< defopt('y', "Select this file, as if it were on the command line")
			<< query("Select this file");

	      Return_if_fail(prcsquery.result());

	      fe->set_on_cmd_line(true);
	    }
        }

      /* @@@ announce rename, typechange, add, delete */

      if (! fe->on_cmd_line())
	continue;

      /* See if stat() hasn't changed since last time prcs recorded it.
       */
      if (fe->type() != FT_Directory && fe->lookup_property ("WFP", &data, &len, &flags))
	unmodified = file_unmodified_since (fe->working_file (), data, len);

      /* Save the new properties.
       */
      if (! file_properties (fe->working_file (), & props, & props_len))
	throw prcserror << "@@ This was already reported" << dotendl;

      fe->define_property ("WFP", props, props_len, SFEF_WorkingProperty);

      g_free (props);

      /* Now any consistency errors have been reported.
       */
      switch (fe->type ())
	{
	case FT_Directory:
	  /* @@@ report modified? */
	  break;

	case FT_Symlink:

	  if (! unmodified)
	    {
	      guint old_link_len, old_link_flags, lname_len;
	      const guint8* old_link;
	      const char* lname;

	      if (! (lname = file_link (fe->working_file ())))
		throw prcserror << "@@@ this was already printed" << dotendl;

	      lname_len = strlen (lname);

	      if (! fe->has_new_family ())
		{
		  if (! fe->lookup_property ("LNK", & old_link, & old_link_len, & old_link_flags))
		    prcswarning << "Missing previous symlink for " << squote (name) << ", assuming modified" << dotendl;
		  else if (lname_len == old_link_len && strcmp (lname, (const char*) old_link) == 0)
		    {
		      unmodified = TRUE;
		    }
		}

	      fe->define_property ("LNK", (const guint8*) lname, lname_len + 1, SFEF_TypeProperty);
	    }

	  if (! fe->has_new_family ())
	    {
	      if (option_really_long_format && unmodified)
		prcsoutput << "Symlink " << squote (name) << " is unmodified" << dotendl;
	      else if (option_long_format && ! unmodified)
		prcsoutput << "Symlink " << squote (name) << " is modified" << dotendl;
	    }

	  break;

	case FT_Regular:

	  if (! unmodified)
	    {
	      /* stat reports a change in the inode, recompute its checksum */
	      guint md5_old_len, md5_old_flags;
	      const guint8 *md5, *md5_old;

	      if (! (md5 = file_checksum_md5 (fe->working_file ())))
		throw prcserror << "@@@ this was already printed" << dotendl;

	      if (! fe->has_new_family ())
		{
		  if (! fe->lookup_property ("MD5", & md5_old, & md5_old_len, &md5_old_flags))
		    prcswarning << "Missing previous checksum of file " << squote (name) << ", assuming modified" << dotendl;
		  else if (md5_old_len == 16 && memcmp (md5, md5_old, 16) == 0)
		    {
		      /* file was touched, not modified. */
		      unmodified = TRUE;
		    }
		}

	      fe->define_property ("MD5", md5, 16, SFEF_TypeProperty);
	    }

	  if (! fe->has_new_family ())
	    {
	      if (option_really_long_format && unmodified)
		prcsoutput << "File " << squote (name) << " is unmodified" << dotendl;
	      else if (option_long_format && ! unmodified)
		prcsoutput << "New version of file " << squote (name) << dotendl;
	    }

	  break;
	}

      if (! unmodified)
	project_modified = TRUE;
    }

  /* @@@ output the log. */

  /* @@@ That's everything?
   */
  if (! project_modified)
    {
      prcsquery << "No modifications were found.  "
		<< force("Aborting checkin")
		<< report("Abort checkin")
		<< option('n', "Continue, with no modified files in this version")
		<< deffail('y')
		<< query("Abort");

      Return_if_fail(prcsquery.result());
    }

  return NoError;
}

static PrVoidError
maybe_query_user (ProjectDescriptor* project)
{
  int tty, c, lc, llc;

  if (project->new_version_log()->length() > 0 ||
      ! get_environ_var ("PRCS_LOGQUERY") || /* either it has been edited or PRCS_LOGQUERY is not set */
      option_force_resolution)
    return NoError;

  tty = isatty (STDIN_FILENO);

  if (tty)
    {
      prcsquery << "Empty New-Version-Log.  "
		<< option('e', "Enter version log now")
		<< defopt('p', "Proceed with an empty version log")
		<< query("Enter a log, or proceed");

      char c;

      Return_if_fail (c << prcsquery.result());

      if(c == 'p')
	return NoError;

      prcsoutput << "Enter description terminated by a single '.' or EOF" << dotendl;

      fprintf (stdout, ">> ");
    }

  re_query_message = ">> ";
  re_query_len = 3;

  lc = '\n';
  llc = '\n';

  while (true)
    {
      If_fail (c << Err_fgetc(stdin))
	throw prcserror << "Read failure on stdin" << perror;

      if (c == EOF)
	{
	  fprintf (stdout, "\n");
	  break;
	}

      if (tty && c == '\n')
	{
	  if (lc == '.' && llc == '\n')
	    {
	      project->new_version_log()->truncate(project->new_version_log()->length() - 1);
	      break;
            }
	  fprintf(stdout, ">> ");
        }

      llc = lc;
      lc = c;

      project->new_version_log()->append(c);
    }

  if (tty)
    fprintf (stdout, "Done.\n");

  /* I don't think its possible that PRCS will ever need to query the
   * user after this, but I might as well reopen it. */
  if (freopen (ctermid (NULL), "r", stdin) == NULL)
    throw prcserror << "Couldn't reopen the standard input" << perror;

  return NoError;
}

gboolean
client_checkin_start (Connection* conn, Peer* peer, void* server_data)
{
  CheckinData* data = CONN_DATA (conn);

  data->peer = peer;

  data->server = (SerialPrcsReply*) server_data;

  if (! strtoui_checked (data->server->version.minor,
			 & data->server->version.minor_int,
			 "Invalid minor version (from server!)"))
    return FALSE;

  switch ((CheckoutReplyCode) data->server->code)
    {
    case CR_Continue:
      prcsinfo << "Checking in project " << squote (project->project_id()->alias)
	       << " version "
	       << data->server->version.major.alias << "." << data->server->version.minor
	       << dotendl;

      return TRUE;

    case CR_NoSuchProject:
      prcsinfo << "Project not found in repository" << dotendl;
      break;

    case CR_NoSuchBranch:
      prcsinfo << "Major version not found in repository" << dotendl;
      break;

    case CR_NoSuchVersion:
      prcserror << "Version not found: " << cmd_version_specifier_major
		<< "." << cmd_version_specifier_minor << prcsendl;
      break;

    default:
      prcserror << "Unknown server checkout reply code: " << data->server->code << prcsendl;
      break;
    }

  return FALSE;
}

TaskControl
client_checkin_project_file (Connection* conn, Task* task)
{
  CheckinData* data = CONN_DATA (conn);
  File* tfile = file_initialize_temp (_fs_repo);

  project->update_attributes_for_checkin (data->server->version.major.alias,
					  data->server->version.minor_int);

  if (! project->write_project_file (file_default_segment (tfile), SFEF_CheckinProperty | SFEF_TypeProperty))
    return repo->fail (task);

#ifndef NDEBUG
  ProjectDescriptor* nprj = read_project_file (tfile, false);

  if (! nprj)
    {
	  prcs_generate_void_event (EC_PrcsInvalidProjectFileWrite);
	  return repo->fail (task);
    }
#endif

  return task_segments_put (conn,
			    task,
			    & protocol_next,
			    file_default_segment (tfile),
			    NULL,
			    NULL);
}

TaskControl
client_checkin_project_reply (Connection* conn, Task* task)
{
  return task_finish_get (conn,
			  task,
			  client_checkin_project_reply_a,
			  ST_PrcsCheckinProjectReply,
			  NULL,
			  (void**) & CONN_DATA(conn)->prj_reply);
}

TaskControl
client_checkin_project_reply_a (Connection* conn, Task* task)
{
  /* this transmission isn't being used... yet */
  g_assert (CONN_DATA(conn)->prj_reply->code == 42);

  return protocol_next (conn, task);
}

TaskControl
client_checkin_project_recv (Connection* conn, Task* task)
{
  return task_segments_get (conn,
			    task,
			    client_checkin_project_recv_a,
			    file_default_segment (cmd_root_project_file),
			    0,
			    NULL,
			    0,
			    NULL,
			    0);
}

TaskControl
client_checkin_project_recv_a (Connection* conn, Task* task)
{
  ProjectDescriptor* nproject;

  if (! (nproject = read_project_file (cmd_root_project_file, false)))
    return reponet_kill_task (task);

  if (! nproject->mix_working_data (project))
    return reponet_kill_task (task);

  delete (project);

  project = nproject;

  return protocol_next (conn, task);
}

TaskControl
client_checkin_files_send_again (Connection* conn, Task* task)
{
  return task_finish_get (conn,
			  task,
			  client_checkin_files_send_one,
			  ST_PrcsOneFile | ST_PrcsDone,
			  & CONN_DATA(conn)->msg_type,
			  & CONN_DATA(conn)->msg);
}

TaskControl
client_checkin_files_send_one (Connection* conn, Task* task)
{
  if (CONN_DATA(conn)->msg_type == ST_PrcsDone)
    {
      task_produce_done (conn, task);

      return protocol_next (conn, task);
    }
  else
    {
      SerialPrcsOneFile* one = (SerialPrcsOneFile*) CONN_DATA(conn)->msg;
      FileEntry* fe = project->file_entry_by_family (one->family);

      if (! fe || ! fe->on_cmd_line_type (FT_Regular))
	{
	  prcs_generate_string_event (EC_PrcsServerConfused, one->family);
	  return repo->fail (task);
	}

      /* @@@ verify that one->md5 == fe's checksum */

      task_produce (conn, task, client_checkin_files_send_reply, fe);

      return client_checkin_files_send_again (conn, task);
    }
}

TaskControl
client_checkin_files_send_reply (Connection* conn, Task* task, ReadyFunc ready, void* data)
{
  FileEntry* fe = (FileEntry*) data;
  File* file = fe->working_file ();

  return task_segments_put (conn,
			    task,
			    ready,
			    file_default_segment (file),
			    NULL,
			    NULL);
}

void client_checkin_success (Connection* /*conn*/, TaskManager* /*tm*/) { }
void client_checkin_failure (TaskManager* /*tm*/) { }
