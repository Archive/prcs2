/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: checkout.cc 1.16.1.24.1.3.1.8.1.1.1.41 Wed, 31 Mar 1999 02:17:22 -0800 jmacd $
 */

#include "prcs.h"
#include "prcs2.h"

#include "projdesc.h"
#include "fileent.h"
#include "misc.h"

static gboolean    client_checkout_start (Connection* conn, Peer* peer, void* server_data);
static TaskControl client_checkout_project_file (Connection *conn, Task* task);
static TaskControl client_checkout_barrier (Connection *conn, Task* task);

static TaskControl client_checkout_file_request (Connection* conn, Task* task);
static TaskControl client_checkout_file_receive (Connection* conn, Task* task, ReadyFunc ready, void* data);
static TaskControl client_checkout_file_receive_a (Connection* conn, Task* task);

static void client_checkout_success (Connection* conn, TaskManager* tm);
static void client_checkout_failure (TaskManager* tm);

#define CONN_DATA(conn) ((CheckoutData *) conn_data (conn, CheckoutData))

typedef struct {
  int index;
} CheckoutData;

static PrcsRepository* repo;
static ProjectDescriptor* project;
static File* temp_prj_file;

static Transmition prcs_client_checkout_transmition[] =
{
  { NULL,                           & client_checkout_project_file, ProtocolAsync },
  { & client_checkout_barrier,      NULL,                           ProtocolBarrier },
  { & client_checkout_file_request, task_consume_ready,             ProtocolAsync }
};

Request prcs_client_checkout_request = {
  "Client Checkout",
  PRCS_PROTOCOL_CHECKOUT_REQUEST,
  STATIC_ARRAY (prcs_client_checkout_transmition),
  ST_PrcsReply,
  { (ServerStartFunc) & client_checkout_start },
  client_checkout_success,
  client_checkout_failure
};

PrPrcsExitStatusError
checkout_command()
{
  SerialAliasedIdentifier project;
  SerialVersionIdentifier version;

  project.alias = cmd_root_project_name;
  project.id = "";

  version.minor = cmd_version_specifier_minor;
  version.major.alias = cmd_version_specifier_major;
  version.major.id = "";

  repo = new PrcsRepository (& project, & version);

  Return_if_fail (repo->init ());

  return PrPrcsExitStatusError (repo->go (PRCS_PROTOCOL_CHECKOUT_REQUEST));
}

gboolean
client_checkout_start (Connection* /*conn*/, Peer* /*peer*/, void* server_data)
{
  SerialPrcsReply *server = (SerialPrcsReply*) server_data;

  switch ((CheckoutReplyCode) server->code)
    {
    case CT_Continue:
      return TRUE;

    case CT_NoSuchProject:
      prcsinfo << "Project not found in repository" << dotendl;
      break;

    case CT_NoSuchBranch:
      prcsinfo << "Major version not found in repository" << dotendl;
      break;

    case CT_NoSuchVersion:
      prcserror << "Version not found: " << cmd_version_specifier_major
		<< "." << cmd_version_specifier_minor << prcsendl;
      break;

    default:
      prcserror << "Unknown server checkout reply code: " << server->code << prcsendl;
      break;
    }

  return FALSE;
}

TaskControl
client_checkout_project_file (Connection *conn, Task* task)
{
  temp_prj_file = file_initialize_temp (_fs_repo);

  return task_segments_get (conn, task, & protocol_next,
			    file_default_segment (temp_prj_file), 0, NULL, 0, NULL, 0);
}

TaskControl
client_checkout_barrier (Connection *conn, Task* task)
{
  if (! (project = read_project_file (temp_prj_file, false)))
    return repo->fail (task);

  prcsinfo << "Checkout project " << squote(cmd_root_project_name)
	   << " version " << project->project_full_version ()
	   << dotendl;

  eliminate_unnamed_files (project);

  If_fail (warn_unused_files (true))
    return repo->fail (task);

  return protocol_next (conn, task);
}

TaskControl
client_checkout_file_request (Connection* conn, Task* task)
{
  CheckoutData* data = CONN_DATA(conn);

  while (data->index < project->file_entries()->length())
    {
      FileEntry* fe = project->file_entries()->index (data->index++);

      /* @@@ can't checkout irregular files yet (do it here) */
      if (! fe->on_cmd_line_type (FT_Regular))
	continue;

      if (file_is_type_noerr (fe->working_file (), FV_Regular))
	{
	  const guint8* wfp;
	  guint wfp_len, wfp_flags;

	  if (fe->lookup_property ("WFP", &wfp, &wfp_len, &wfp_flags))
	    {
	      if (file_unmodified_since (fe->working_file (), wfp, wfp_len))
		{
		  if (option_long_format)
		    prcs_generate_string_event (EC_PrcsFileUnmodified, fe->working_name ());

		  continue;
		}
	    }
	}

      const guint8* md5, *act_md5;
      guint md5_len, md5_flags;

      if (! fe->lookup_property ("MD5", & md5, & md5_len, & md5_flags))
	{
	  prcs_generate_stringstring_event (EC_PrcsMissingProperty, fe->working_name (), "MD5");
	  continue;
	}

      g_assert (md5_len == 16);

      if (file_is_type_noerr (fe->working_file (), FV_Regular))
	{
	  if (! (act_md5 = file_checksum_md5 (fe->working_file ())))
	    continue;

	  if (memcmp (act_md5, md5, 16) == 0)
	    {
	      if (option_long_format)
		prcs_generate_string_event (EC_PrcsFileUnmodified, fe->working_name ());

	      continue;
	    }

	  static BangFlag bang;

	  prcsquery << "File " << squote (fe->working_name ())
		    << " differs.  "
		    << report("Replace") /* no force */
		    << force("Replacing")
		    << allow_bang(bang)
		    << option('n', "Leave old file and ignore this checkout")
		    << defopt('y', "Replace with new file")
		    << query("Replace");

	  char c;

	  If_fail (c << prcsquery.result())
	    return repo->exitwith (task, prcsquery.result());

	  if (c == 'n')
	    continue;
	}

      task_produce (conn, task, client_checkout_file_receive, fe);

      return task_finish_put (conn,
			      task,
			      client_checkout_file_request,
			      serialize_prcsonefile (conn_sink (conn),
						     fe->descriptor (),
						     md5));
    }

  task_produce_done (conn, task);

  return task_finish_put (conn,
			  task,
			  protocol_next,
			  serialize_prcsdone (conn_sink (conn)));
}

typedef struct {
  FileEntry *fe;
  ReadyFunc ready;
} CheckoutOneData;

TaskControl
client_checkout_file_receive (Connection* conn, Task* task, ReadyFunc ready, void* vdata)
{
  FileEntry* fe = (FileEntry*) vdata;

  CheckoutOneData *data = task_stack_push (task, CheckoutOneData);

  data->ready = ready;
  data->fe = fe;

  return task_segments_get (conn,
			    task,
			    client_checkout_file_receive_a,
			    file_default_segment (fe->working_file ()),
			    0,
			    NULL,
			    0,
			    NULL,
			    0);
}

TaskControl
client_checkout_file_receive_a (Connection* conn, Task* task)
{
  CheckoutOneData *data = task_stack_pop (task, CheckoutOneData);
  guint8* props;
  guint32 props_len;

  if (! file_properties (data->fe->working_file (), & props, & props_len))
    return reponet_kill_task (task);

  data->fe->define_property ("WFP", props, props_len, SFEF_WorkingProperty);

  g_free (props);

  return data->ready (conn, task);
}

void
client_checkout_success (Connection* /*conn*/, TaskManager* /*tm*/)
{
  if (! project->write_project_file (file_default_segment (cmd_root_project_file), -1))
    /* @@@ */ abort ();
}

void client_checkout_failure (TaskManager* /*tm*/) { }
