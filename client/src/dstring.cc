/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: dstring.cc 1.2.1.3.1.1.1.5 Tue, 09 Mar 1999 03:05:26 -0800 jmacd $
 */

#include "prcs.h"
#include "dstring.h"
#include <stdarg.h>

void Dstring::assign_int(int x)
{
  sprintf("%d", x);
}

void Dstring::append_int(int x)
{
  sprintfa("%d", x);
}

void Dstring::sprintfa(const char* fmt, ...)
{
  char* buf;

  va_list args;

  va_start(args, fmt);

  buf = g_strdup_vprintf (fmt, args);

  append (buf);

  g_free (buf);

  va_end(args);
}

void Dstring::sprintf(const char* fmt, ...)
{
  char* buf;

  va_list args;

  va_start(args, fmt);

  truncate(0);

  buf = g_strdup_vprintf (fmt, args);

  append (buf);

  g_free (buf);

  va_end(args);
}

bool Dstring::is_dstring() { return true; }
