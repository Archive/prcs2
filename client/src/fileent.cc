/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: fileent.cc 1.9.1.1.1.7.1.13.1.6.1.1.1.25 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */

#include "prcs.h"
#include "prcs2.h"

#include "projdesc.h"
#include "fileent.h"

/* constructor */
FileEntry::FileEntry (Path             *working_path,
		      const char       *working_name,
		      const ListMarker &ent_marker,
		      const PrcsAttrs  *attrs,
		      Dstring          *descriptor)
  :_working_path   (working_path),
   _working_name   (working_name),
   _ent_marker     (ent_marker),
   _attrs          (attrs),
   _descriptor     (descriptor),
   _on_cmd_line    (true),
   _has_new_family (false)
{
  _working_file = NULL;
  _props = NULL;
}

SymlinkFileEntry::SymlinkFileEntry (Path             *working_path,
				    const char       *working_name,
				    const ListMarker &ent_marker,
				    const PrcsAttrs  *attrs,
				    Dstring          *descriptor)
  :FileEntry (working_path,
	      working_name,
	      ent_marker,
	      attrs,
	      descriptor)
{ }

RegularFileEntry::RegularFileEntry (Path             *working_path,
				    const char       *working_name,
				    const ListMarker &ent_marker,
				    const PrcsAttrs  *attrs,
				    Dstring          *descriptor)
  :FileEntry (working_path,
	      working_name,
	      ent_marker,
	      attrs,
	      descriptor)
{ }

DirectoryFileEntry::DirectoryFileEntry (Path             *working_path,
					const char       *working_name,
					const ListMarker &ent_marker,
					const PrcsAttrs  *attrs,
					Dstring          *descriptor)
  :FileEntry (working_path,
	      working_name,
	      ent_marker,
	      attrs,
	      descriptor)
{ }

/* destructor */
FileEntry::~FileEntry (void) { }

Path*              FileEntry::working_path (void) const { return _working_path; }
const char*        FileEntry::working_name (void) const { return _working_name; }
const ListMarker*  FileEntry::ent_marker   (void) const { return & _ent_marker; }
const PrcsAttrs*   FileEntry::attrs        (void) const { return _attrs; }
const char*        FileEntry::descriptor   (void) const { return _descriptor != NULL > 0 ? _descriptor->cast() : (const char*) 0; }
ProjectDescriptor* FileEntry::project      (void) const { return _attrs->_project; }
bool               FileEntry::has_new_family (void) const { return _has_new_family; }

bool FileEntry::on_cmd_line      (void) const { return _on_cmd_line; }
void FileEntry::set_on_cmd_line  (bool on)    { _on_cmd_line = on; }

File* FileEntry::working_file (void)
{
  if (! _working_file)
    _working_file = file_initialize (_fs_repo, _working_path);

  return _working_file;
}

FileType SymlinkFileEntry::type (void) const { return FT_Symlink; }
FileType RegularFileEntry::type (void) const { return FT_Regular; }
FileType DirectoryFileEntry::type (void) const { return FT_Directory; }

bool FileEntry::on_cmd_line_type (FileType type0) const
{
  return on_cmd_line () && type0 == type ();
}

gboolean
SymlinkFileEntry::working_file_exists (void)
{
  /* @@@ what about returning failure? */
  if (! file_is_type_noerr (working_file (), FV_SymbolicLink))
    return false;

  return true;
}

gboolean
DirectoryFileEntry::working_file_exists (void)
{
  /* @@@ what about returning failure? */
  if (! file_is_type_noerr (working_file (), FV_Directory))
    return false;

  return true;
}

gboolean
RegularFileEntry::working_file_exists (void)
{
  /* @@@ what about returning failure? */
  if (! file_is_type_noerr (working_file (), FV_Regular))
    return false;

  return true;
}

void
FileEntry::set_descriptor (const char* ndesc)
{
  g_assert (! _descriptor);

  _has_new_family = true;

  _descriptor = new Dstring (ndesc);
}

bool
FileEntry::lookup_property (const char* name, const guint8** val_ptr, guint* len_ptr, guint* flags_ptr)
{
  if (! _props)
    return false;

  for (guint i = 0; i < _props->len; i += 1)
    {
      SerialFileEntryProperty *oprop = & g_array_index (_props, SerialFileEntryProperty, i);

      if (strcmp (oprop->name, name) == 0)
	{
	  if (val_ptr)   (*val_ptr) = oprop->val;
	  if (len_ptr)   (*len_ptr) = oprop->val_len;
	  if (flags_ptr) (*flags_ptr) = oprop->flags;

	  return true;
	}
    }

  return false;
}

void
FileEntry::define_property (const char* name, const guint8* val, guint len, guint flags)
{
  SerialFileEntryProperty prop;

  if (! _props)
    _props = g_array_new (FALSE, FALSE, sizeof (SerialFileEntryProperty));

  for (guint i = 0; i < _props->len; i += 1)
    {
      SerialFileEntryProperty *oprop = & g_array_index (_props, SerialFileEntryProperty, i);

      if (strcmp (oprop->name, name) == 0)
	{
	  g_free ((void*) oprop->val);

	  oprop->name = name;
	  oprop->val = (guint8*) g_memdup (val, len);
	  oprop->val_len = len;
	  oprop->flags = flags;

	  return;
	}
    }

  prop.name = name;
  prop.val = (guint8*) g_memdup (val, len);
  prop.val_len = len;
  prop.flags = flags;

  g_array_append_val (_props, prop);
}

void
FileEntry::rename_descriptor (const char* ndesc)
{
  /* @@@ rename everywhere a descriptor name appars (in the logs) */

  _descriptor->assign (ndesc);
}

void
FileEntry::transfer_properties_with_mask (FileEntry* other, int mask)
{
  if (! other->_props)
    return;

  for (guint i = 0; i < other->_props->len; i += 1)
    {
      SerialFileEntryProperty *oprop = & g_array_index (other->_props, SerialFileEntryProperty, i);

      if (oprop->flags & mask)
	define_property (oprop->name, oprop->val, oprop->val_len, oprop->flags);
    }
}

GPtrArray*
FileEntry::all_properties (int mask) const
{
  GPtrArray* ret = g_ptr_array_new ();

  if (_props)
    {
      for (guint i = 0; i < _props->len; i += 1)
	{
	  SerialFileEntryProperty *prop = & g_array_index (_props, SerialFileEntryProperty, i);

	  if (prop->flags & mask)
	    g_ptr_array_add (ret, prop);
	}
    }

  return ret;
}
