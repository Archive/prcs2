/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: fileent.h 1.3.1.2.1.12.1.2.1.22 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */


#ifndef _FILEENT_H_
#define _FILEENT_H_

class FileEntry {

public:

  FileEntry  (Path             *working_path,
	      const char       *working_name,
	      const ListMarker &ent_marker,
	      const PrcsAttrs  *attrs,
	      Dstring          *descriptor);

  virtual ~FileEntry (void);

  /* Intrinsic properties and a virtual type */

  Path*              working_path   (void) const;
  const char*        working_name   (void) const;
  const ListMarker*  ent_marker     (void) const;
  const PrcsAttrs*   attrs          (void) const;
  const char*        descriptor     (void) const;
  ProjectDescriptor* project        (void) const;
  bool               has_new_family (void) const;

  virtual FileType type                (void) const = 0;
  virtual gboolean working_file_exists (void) = 0;

  /* session properties: getter/setter */

  bool               on_cmd_line      (void) const;
  bool               on_cmd_line_type (FileType type) const;
  void               set_on_cmd_line  (bool);

  void               set_descriptor    (const char* ndesc);
  void               rename_descriptor (const char* ndesc);

  bool               lookup_property (const char* name, const guint8** val_ptr, guint* len_ptr, guint* flags_ptr);
  void               define_property (const char* name, const guint8* val, guint len, guint flags);

  GPtrArray*         all_properties (int mask) const;

  void               transfer_properties_with_mask (FileEntry* other, int mask);

  File*              working_file   (void);

protected:

  Path            *_working_path;
  const char      *_working_name;
  File            *_working_file;
  ListMarker       _ent_marker;
  const PrcsAttrs *_attrs;
  Dstring         *_descriptor;
  GArray          *_props;

  bool             _on_cmd_line;
  bool             _has_new_family;
};

class SymlinkFileEntry : public FileEntry
{
public:
  SymlinkFileEntry (Path             *working_path,
		    const char       *working_name,
		    const ListMarker &ent_marker,
		    const PrcsAttrs  *attrs,
		    Dstring          *descriptor);

  virtual FileType    type                (void) const;
  virtual gboolean working_file_exists (void);
};

class RegularFileEntry : public FileEntry
{
public:
  RegularFileEntry (Path             *working_path,
		    const char       *working_name,
		    const ListMarker &ent_marker,
		    const PrcsAttrs  *attrs,
		    Dstring          *descriptor);

  virtual FileType    type                (void) const;
  virtual gboolean working_file_exists (void) ;
};

class DirectoryFileEntry : public FileEntry
{
public:
  DirectoryFileEntry (Path             *working_path,
		      const char       *working_name,
		      const ListMarker &ent_marker,
		      const PrcsAttrs  *attrs,
		      Dstring          *descriptor);

  virtual FileType    type                (void) const;
  virtual gboolean working_file_exists (void) ;
};

#endif /* FILEENT_H */
