/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: misc.h 1.10.1.1.1.11.1.12.1.5.1.2.1.18 Tue, 16 Mar 1999 00:13:24 -0800 jmacd $
 */


#ifndef _MISC_H_
#define _MISC_H_

#include <unistd.h>

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

extern const char* get_host_name();
extern const char* get_login();
extern gid_t get_user_id();
extern mode_t get_umask();
extern char* guess_prj_name (File* dir);

NprIntError  Err_fgetc(FILE*);
NprVoidError read_string(FILE* f, Dstring* s);

extern gboolean check_create_subdir (Path* path);

/* pathname_hash, pathname_equal --
 *
 *     used by the hash table.  these both ignore duplicated slashes
 *     in a path name.  */
extern int  pathname_hash(const char* const&, int);
extern bool pathname_equal(const char* const&, const char* const&);

extern const char* major_version_of(const char* maj_dot_min);
extern const char* minor_version_of(const char* maj_dot_min);

extern const char* format_type(FileType, bool cap = false);

extern char get_user_char();

const char* get_environ_var (const char* var);

PrVoidError bug(void);
const char* bug_name (void);

class Umask {
public:
    Umask (mode_t mode) { _old = umask(mode); }
    ~Umask () { umask (_old); }
private:
    Umask (const Umask&);
    Umask& operator=(const Umask&);
    mode_t _old;
};

#endif
