/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: prcs.h 1.16.1.15.1.6.1.12.1.32 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */


#ifndef _PRCS_H_
#define _PRCS_H_

extern "C" {

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>

/* S_ISLNK() should be defined in sys/stat.h, but it isn't in Unixware-1.1.x
 * according to Thanh Ma <tma@encore.com>. */
#if !defined(S_ISLNK) && defined(S_IFLNK)
#define S_ISLNK(mode) (((mode) & S_IFMT) == S_IFLNK)
#endif

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <repo.h>
#include "prcs_edsio.h"
}

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

/* EGCS doesn't have this header. */
#ifdef HAVE_STD_H
#include <std.h>
#endif

#include <iostream.h>
#include <fstream.h>

#ifdef __GNUG__
/* This gets defined in config.h now. */
/*#define PRCS_DEVEL*/
#endif

#ifdef NULL
#undef NULL
#endif
#define NULL 0 /* Some systems define NULL as (void*)0, but that messes
		* uses of the ?: operator with NULL. */

#include "typedefs.h"
#include "prcserror.h"

/* all variables beginning with 'cmd_' are derived from information
 * present after the options part of the command line.  */

/* for command line project name path1/path2/prcs */
extern const char  *cmd_program_name;              /* basename (argv[0]) */

extern const char  *cmd_root_project_name;         /* prcs */

extern Path        *cmd_root_project_file_path;    /* /absolute/path1/path2/prcs.prj */
extern Path        *cmd_root_project_path;         /* /absolute/path1/path2/ */

extern File        *cmd_root_project_file;
extern File        *cmd_root_project_dir;

extern int          cmd_repository_port;
extern const char  *cmd_repository_host;

extern PathUsed    *cmd_paths_given;
extern GHashTable  *cmd_paths_given_table;
extern int          cmd_paths_count;

extern char       **cmd_filenames_given;           /* from command line@@@obsolete */
extern int          cmd_filenames_count;           /* number of filenames */

extern char       **cmd_diff_options_given;
extern int          cmd_diff_options_count;

extern const char  *cmd_version_specifier_major;
extern const char  *cmd_version_specifier_minor;
extern int          cmd_version_specifier_minor_int;

extern const char  *cmd_alt_version_specifier_major;
extern const char  *cmd_alt_version_specifier_minor;

extern bool         cmd_prj_given_as_file;

/* Since PRCS is a disconnected system, it must occasionally produce a
 * name which is can "strongly hope" will not conflict with any other.
 * It uses 168 bits of randomness for this purpose.  This yields a
 * (1/2^84) chance of collision for each name, and takes 21 bytes raw
 * or 28 bytes using base64.  It _MUST_ correspond with the size to
 * the number of bytes used in ../prcs.ser.  Rationale: as many bits
 * as SHA-1, which people seem to trust, plus some more to make the
 * base64 encoding not require padding.
 */
#define PRCS_UNIQUE_STRING_BYTES (168/8)

extern const char   prcs_version_string[];

/* all variables beginning with 'option_' are derived from information
 * in the options part of the command line.  */
extern int option_force_resolution;        /* -f */
extern int option_long_format;             /* -l */
extern int option_really_long_format;      /* -L */
extern int option_report_actions;          /* -n */
extern int option_version_present;         /* -r */
extern int option_diff_keywords;           /* -k */
extern int option_diff_new_file;           /* -N */
extern int option_populate_delete;         /* -d */
extern int option_be_silent;               /* -q */
extern int option_preserve_permissions;    /* -p */
extern int option_exclude_project_file;    /* -P */
extern int option_unlink;                  /* -u */
extern int option_match_file;              /* --match */
extern int option_not_match_file;          /* --not */
extern int option_all_files;               /* --all */
extern int option_preorder;                /* --pre */
extern int option_pipe;                    /* --pipe */
extern int option_skilled_merge;           /* -s */
extern int option_plain_format;            /* --plain-format */
extern int option_sort;                    /* --sort */
#ifdef PRCS_DEVEL
#define option_debug (! option_n_debug)    /* --debug */
extern int option_n_debug;                 /* ! --debug */
extern int option_tune;                    /* --tune */
#endif

extern const char *option_match_file_pattern;
extern const char *option_not_match_file_pattern;
extern const char *option_sort_type;

extern const int prcs_version_number[3];

extern PrPrcsExitStatusError checkin_command();
extern PrPrcsExitStatusError checkout_command();
extern PrPrcsExitStatusError populate_command();

#define foreach(P, INIT, TYPE) \
    for (TYPE P(INIT); ! (P).finished (); (P).next ())

#endif
