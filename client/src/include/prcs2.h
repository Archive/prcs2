/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: prcs2.h 1.13 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */


#ifndef _PRCS2_H_
#define _PRCS2_H_

#include "reponet.h"

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN extern
#endif

EXTERN int prjlex();
EXTERN int prjinput(void* buf, int max_size);

enum PrjTokenType {
  PrjString,
  PrjName,
  PrjOpen,
  PrjClose,
  PrjNull,
  PrjBadString,
  PrjEof
};

#ifdef __cplusplus

class PrcsRepository {
 public:
  PrcsRepository (SerialAliasedIdentifier* project, SerialVersionIdentifier* version);

  PrVoidError init (void);

  PrcsExitStatus go (int request);

  TaskControl fail (Task* task);
  TaskControl exitwith (Task* task, PrPrcsExitStatusError err);

 private:

  SerialPrcsConnect       _connect;
  PrcsExitStatus          _result;
};

extern PrProjectDescriptorPtrError checkout_empty_prj_file (const char* fullname, const char* maj);
extern PrProjectDescriptorPtrError checkout_create_empty_prj_file (const char* fullname, File* file, const char* maj);
extern void                        eliminate_unnamed_files(ProjectDescriptor*);
extern PrVoidError                 eliminate_working_files(ProjectDescriptor* project, MissingFileAction action);
extern PrVoidError                 warn_unused_files(bool prompt_abort);

#define PRCS_PROTOCOL_MAJOR_VERSION   1
#define PRCS_PROTOCOL_MINOR_VERSION   0
#define PRCS_PROTOCOL_DEFAULT_PORT    2699
#define PRCS_PROTOCOL_CHECKIN_REQUEST 1
#define PRCS_PROTOCOL_CHECKOUT_REQUEST 2

extern Request prcs_client_checkin_request;
extern Request prcs_client_checkout_request;

enum CheckinReplyCode {
  CR_Continue,
  CR_NoSuchProject,
  CR_NoSuchBranch,
  CR_NoSuchVersion
};

enum CheckoutReplyCode {
  CT_Continue,
  CT_NoSuchProject,
  CT_NoSuchBranch,
  CT_NoSuchVersion
};

extern gboolean append_project_data_to_handle (SerialProjectData* pdata, FileHandle* handle);

class PrcsRegex;

PrcsRegex* prcs_compile_regex (const char* exp);
gboolean   prcs_regex_matches (PrcsRegex* re, const char* str, guint len);

#endif

#endif
