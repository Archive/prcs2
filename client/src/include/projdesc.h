/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: projdesc.h 1.3.1.1.1.2.1.10.1.1.1.6.3.8.1.31 Wed, 31 Mar 1999 02:17:22 -0800 jmacd $
 */


#ifndef _PROJDESC_H_
#define _PROJDESC_H_

#include <repo.h>
#include "prcs2.h"

#ifdef __cplusplus

#define foreach_fileent(name, project) \
    foreach(name, (project)->file_entries(), FileEntryPtrArray::ArrayIterator)

class PrjFields {
public:
    const char *name;
    gboolean (ProjectDescriptor:: *parse_func)(const Sexp* list);
    gboolean (ProjectDescriptor:: *read_func) (int offset);
    bool must_have;
};

class Estring : public Dstring {

public:
    Estring (ProjectDescriptor* project,
	     const char* s,
	     size_t off,
	     size_t len,
	     EstringType type);
    Estring (ProjectDescriptor* project, size_t position); /* type = EsUnProtected */

    virtual bool is_dstring();

    void append_protected(const char*); /* To insert a NameLiteral into an EsUnProtected string */
    void append_string(const char*); /* To insert a StringLiteral into an EsUnProtected string */

    EstringType type() const;

    /* A special Dstring that remembers where it was in the project
     * file, allows itself to be modified, adds itself to the
     * descriptors list of strings, and will update the project file
     * when it needs to get written out.  Modify is called before the
     * string is modified.  the alloc field will be set to -1 until
     * it is actually modified. */
    virtual void modify();

    size_t offset()        const;
    size_t offset_length() const;

protected:

    size_t _offset;
    size_t _len;
    EstringType _type;
};

class ListMarker {
public:
    ListMarker (size_t start0, size_t stop0) :start(start0), stop(stop0) { }
    ListMarker () :start(0), stop(0) { }
    size_t start;
    size_t stop;
};

class AttrDesc { public: const char* name; AttrType type; };

class PrcsAttrs {
    friend class ProjectDescriptor;
    friend class FileEntry;
    friend bool attrs_equal(const PrcsAttrs*const& x, const PrcsAttrs*const& y);
    friend int attrs_hash(const PrcsAttrs*const& s, int M);
public:

    FileType type() const;
    bool keyword_sub() const;
    ProjectDescriptor* project() const;

    void print_to_string (Dstring* str, bool lead_if) const;
    gboolean print (FileHandle* handle, bool lead_if) const;
  /*bool regex_matches (reg2ex2_t *re) const;*/

  /*MergeAction merge_action (int rule) const;*/
  /*const char* merge_desc (int rule) const;*/

  /*const char* merge_tool () const;*/
    const char* diff_tool () const;

private:
    PrcsAttrs (const DstringPtrArray *vals0, int ngroup);

    /* used to test equality in attrs_equal */
    DstringPtrArray _vals;
    int             _ngroup;

    /* set by ProjectDescriptor::init_attr */
    FileType        _type;
    ProjectDescriptor *_project;
    bool            _keyword_sub;
  /*Dstring         _mergetool;*/
    Dstring         _difftool;
  /*MergeAction     _merge_actions[14];*/
  /*const char*     _merge_descs[14];*/
};

ProjectDescriptor* read_project_file (File* file, bool is_working);

class ProjectDescriptor {

  friend ProjectDescriptor* read_project_file (File* file, bool is_working);

  friend class Estring;

public:

  ~ProjectDescriptor();

  gboolean write_project_file (FileSegment* seg, int fe_prop_mask);

  Dstring* project_version_name()  const;
  Dstring* project_version_major() const;
  Dstring* project_version_minor() const;

  Dstring* parent_version_name()  const;
  Dstring* parent_version_major() const;
  Dstring* parent_version_minor() const;

  Dstring* project_description() const;
  Dstring* version_log()         const;
  Dstring* new_version_log()     const;
  Dstring* checkin_time()        const;
  Dstring* checkin_login()       const;

  KeywordTable*       project_keywords       (FileEntry* fe, bool setting);
  OrderedStringTable* project_keywords_extra () const;

  FileEntryPtrArray* file_entries() const;

  const SerialAliasedIdentifier* project_id()           const;
  const SerialAliasedIdentifier* branch_id()            const;
  const char*                    project_full_version() const;

  void     update_attributes_for_checkin (const char* new_major, int new_minor);
  gboolean mix_working_data (ProjectDescriptor* working_copy);

  /* Attributes */
  PrcsAttrs* intern_attrs (const DstringPtrArray*, int ngroup, const char* name, bool validate);
  gboolean validate_attrs (const PrcsAttrs *attrs, const char* name);
  void init_attrs (PrcsAttrs *attrs);

  /* Keyword and Populate ignore manipulation. */
  void add_keyword (const char* key, const char* val);
  void rem_keyword (const char* key);
  void set_keyword (const char* key, const char* val);

  FileEntry*  file_entry_by_family (const char* family);
  FileEntry*  file_entry_by_name   (const char* name);

  GHashTable* filenames_table      (void);
  GHashTable* descriptors_table    (void);
  GHashTable* working_paths_table  (void);
  GHashTable* working_dest_paths_table  (void);

  gboolean    populate_ignore_compile (void);
  gboolean    populate_ignore_matches (const char* str, const char** ignore_exp);

  void append_new_regular_file (const char* working_name);

private:

  gboolean project_ver_prj_entry_func       (const Sexp* s);
  gboolean parent_ver_prj_entry_func        (const Sexp* s);
  gboolean project_desc_prj_entry_func      (const Sexp* s);
  gboolean version_log_prj_entry_func       (const Sexp* s);
  gboolean new_version_log_prj_entry_func   (const Sexp* s);
  gboolean checkin_time_prj_entry_func      (const Sexp* s);
  gboolean checkin_login_prj_entry_func     (const Sexp* s);
  gboolean populate_ignore_prj_entry_func   (const Sexp* s);
  gboolean project_keywords_prj_entry_func  (const Sexp* s);

  /* These 3 entries in the project file grow with project
   * file count, so reading the entire list is a waste of
   * memory. */
  gboolean files_prj_entry_read             (int offset);
  gboolean idata_prj_entry_read             (int offset);

  /* Read helper functions. */
  gboolean individual_insert_fileent        (const Sexp* s, const DstringPtrArray *gtags);

  /* Friendly constructors. */
  ProjectDescriptor ();
  gboolean init_from_file (File* file, bool is_working);

  /* Illegal. */
  ProjectDescriptor (const ProjectDescriptor&);
  int operator= (const ProjectDescriptor&);

  /* Parsing. */
  int prj_lookup_hash (const char *str, int len);
  const PrjFields *prj_lookup_func (const char *str, int len);
  const Sexp* read_list(int offset);
  const Sexp* read_list_token(PrjTokenType type, int offset);
  const Sexp* read_list_elt ();
  const Sexp* read_new_estring(PrjTokenType type);
  void read_parse_bad_token(PrjTokenType type);
  gboolean read_list_checked(size_t offset);
  gboolean parse_prj_file ();
  void register_estring(Estring*);

  /* State manipulation. */
  gboolean mix_idata (void);
  void set_project_full_version (void);
  void idata_log_file_add (FileEntry* fe);
  void idata_log_file_rename (FileEntry* fe, const char* old_name);
  void idata_log_file_retype (FileEntry* fe, int old_type);
  void idata_log_file_regroup (FileEntry* fe, int old_group);
  void idata_log_file_delete (SerialFileEntry* sfe);

  /* Writing. */
  gboolean really_write_project_file (FileHandle* handle);
  gboolean append_project_idata (FileHandle* handle, int fe_prop_mask);

  /* Cleanup */

  /* Static data. */
  static const int       _prj_entries;
  static const PrjFields _pftable[];
  static bool            _prj_entry_found[];

  /* Parsing data. */
  File* _prj_source;
  FileHandle* _prj_source_backing;

  /* Names */
  Dstring  _project_full_version;

  /* Insertion points and the original buffer. */
  guint32          _segment_length;
  DstringPtrArray *_all_estrings;
  Estring         *_files_insertion_point;
  Estring         *_project_version_point;
  Estring         *_end_of_buffer_point;
  Estring         *_project_keywords_point;
  Estring         *_populate_ignore_point;
  ListMarker       _project_keywords_marker;
  ListMarker       _populate_ignore_marker;
  ListMarker       _idata_marker;
  ConstListMarkerPtrArray *_deleted_markers;

  /* The files. */
  FileEntryPtrArray *_file_entries;
  GHashTable        *_descriptors_table;
  GHashTable        *_filenames_table;
  GHashTable        *_working_paths_table;
  GHashTable        *_working_dest_paths_table;
  GPtrArray         *_populate_ignore;

  /* Project file strings. */
  Dstring*      _project_version_name;
  Dstring*      _project_version_major;
  Dstring*      _project_version_minor;

  Dstring*      _parent_version_name;
  Dstring*      _parent_version_major;
  Dstring*      _parent_version_minor;

  Dstring*      _project_description;
  Dstring*      _version_log;
  Dstring*      _new_version_log;
  Dstring*      _checkin_time;
  Dstring*      _checkin_login;

  /* Keyword stuff */
  KeywordTable       *_project_keywords;
  OrderedStringTable *_project_keywords_extra;
  Dstring            *_keyword_id;
  Dstring            *_keyword_pheader;
  Dstring            *_keyword_pversion;
  bool                _alter_popkey;

  /* Attributes */
  AttrsTable *_attrs_table;

  /* PRCS2 */
  SerialProjectData        *_idata;

  GArray                   *_idata_files;
  GArray                   *_idata_files_log;
  GArray                   *_idata_invokation_log;

  bool _is_working;
  Path *_root_path;
};

#endif

#endif /* PROJDESC_H */
