/* -*-Mode: C++;-*-
 * $Id: servside.h 1.7 Wed, 31 Mar 1999 02:17:22 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _SERVSIDE_H_
#define _SERVSIDE_H_

#include "server.h"

extern Request     prcs_server_checkin_request;
extern Request     prcs_server_checkout_request;
extern Repository *cp_repo;

NameSpace* servside_prcs_families_namespace (AliasedIdentifier* id);
NameSpace* servside_prcs_branches_namespace (AliasedIdentifier* id);

gboolean servside_check_client_authorization (Connection* conn);

FileSegment* servside_create_empty_project_file (const AliasedIdentifier* project_id,
						 const AliasedIdentifier* branch_id,
						 const char* login);

#endif
