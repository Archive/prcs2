/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: typedefs.h 1.8.1.10.1.6.2.1.1.6.1.18 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */


#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_

enum AttrType {
  RealFileAttr,
  NoKeywordAttr,
  DirectoryAttr,
  SymlinkAttr,
  ImplicitDirectoryAttr,
  ProjectFileAttr,
  TagAttr,
  MergetoolAttr,
  DifftoolAttr,
  Mergerule1Attr,
  Mergerule2Attr,
  Mergerule3Attr,
  Mergerule4Attr,
  Mergerule5Attr,
  Mergerule6Attr,
  Mergerule7Attr,
  Mergerule8Attr,
  Mergerule9Attr,
  Mergerule10Attr,
  Mergerule11Attr,
  Mergerule12Attr,
  Mergerule13Attr,
  Mergerule14Attr
};

enum EstringType {
  EsStringLiteral,
  EsNameLiteral,
  EsUnProtected
};

enum SetkeysAction {
    Setkeys,
    Unsetkeys
};

enum ErrorToken {
    NonFatalError           = 1 << 0,
    FatalError              = 1 << 1,
    InternalError           = 1 << 2,
    InternalRepositoryError = 1 << 3,
    RepositoryError         = 1 << 4,
    WriteFailure            = 1 << 5,
    ReadFailure             = 1 << 6,
    StatFailure             = 1 << 7,
    UserAbort               = 1 << 8
};

enum MissingFileAction {
    QueryUserRemoveFromCommandLine,
    NoQueryUserRemoveFromCommandLine
};

enum OverwriteStatus { DoesntExist, IgnoreMe, SameType };

enum NonErrorToken { NoError };

enum PrcsExitStatus {
    ExitSuccess = 0,
    ExitDiffs = 1,
    ExitFailure = 2
};

enum FileType {
  FT_Symlink = FV_SymbolicLink,
  FT_Directory = FV_Directory,
  FT_Regular = FV_Regular
};

enum SerialFileEntryFlags {
  SFEF_None =            1<<0,
  SFEF_WorkingProperty = 1<<1, /* property logically belongs to the working copy, not in the repository */
  SFEF_CheckinProperty = 1<<2, /* property logically belongs to the project until the next checkin */
  SFEF_TypeProperty =    1<<3  /* property logically belongs to the project so long as the type does not change */
};

#define SFEF_All ((SerialFileEntryFlags) (SFEF_TypeProperty | SFEF_CheckinProperty | SFEF_WorkingProperty))

struct PathUsed
{
  Path* path;
  bool  used;
};

#include "dstring.h"
#include "dynarray.h"
#include "hash.h"
#include "prcserror.h"

class PrVoidError;                    /* include/prcserror.h */
template <class Type> class PrError;  /* include/prcserror.h */

#ifdef PRCS_DEVEL
class NprVoidError;                   /* include/prcserror.h */
template <class Type> class NprError; /* include/prcserror.h */
#else
typedef PrVoidError NprVoidError;
#endif

class Dstring;            /* include/dstring.h */
class Estring;            /* include/projdesc.h */
class ProjectDescriptor;  /* include/projdesc.h */
class FileEntry;          /* include/fileent.h */
class PrettyOstream;      /* include/prcserror.h */
class PrettyStreambuf;    /* include/prcserror.h */
class Sexp;               /* include/sexp.h */
class PipeRec;            /* include/syscmd.h */
class SystemCommand;      /* include/syscmd.h */
class PrjFields;
class ListMarker;
class QuickElimEntry;
class PrcsAttrs;

template <class T, int DefaultSize, bool ZeroTerm> class Dynarray; /* include/dynarray.h */
template <class Key, class Data> class HashTable; /* include/hash.h */
template <class Key, class Data> class OrderedTable; /* include/hash.h */
template <class X, class Y> class Pair; /* include/hash.h */
template <class T> class List;          /* include/hash.h */

typedef Dynarray< char const*, 64, true> ArgList;
typedef Dynarray< char const*, 64, true> CharPtrArray;
typedef Dynarray< Dstring*, 64, false > DstringPtrArray;
typedef Dynarray< FileEntry*, 64, false > FileEntryPtrArray;
typedef Dynarray< const ListMarker*, 4, false > ConstListMarkerPtrArray;
typedef Dynarray< int, 4, false > IntArray;

typedef HashTable< const char* , FileType > PathTable;
typedef HashTable< ino_t , FileType > InoTable;
typedef HashTable< const char*, Sexp* > SexpTable;
typedef HashTable< const char* , FileEntry* > FileTable;
typedef HashTable< const char*, const char* > KeywordTable;
typedef HashTable< FileEntry*, FileEntry* > EntryTable;
typedef HashTable< const PrcsAttrs*, PrcsAttrs* > AttrsTable;
typedef HashTable< const char*, SystemCommand* > CommandTable;

typedef OrderedTable< const char*, const char* > OrderedStringTable;

typedef List< void* > VoidPtrList;
typedef List< const ListMarker* > ListMarkerList;

typedef PrError< ArgList* > PrArgListPtrError;
typedef PrError< int > PrExitStatusError;
typedef PrError< int > PrIntError;
typedef PrError< pid_t > PrPidTError;
typedef PrError< PrcsExitStatus > PrPrcsExitStatusError;
typedef PrError< time_t > PrTimeTError;
typedef PrError< bool > PrBoolError;
typedef PrError< const char* > PrConstCharPtrError;
typedef PrError< Dstring* > PrDstringPtrError;
typedef PrError< ProjectDescriptor* > PrProjectDescriptorPtrError;
typedef PrError< PipeRec* > PrPipeRecPtrError;
typedef PrError< const Sexp* > PrConstSexpPtrError;
typedef PrError< OverwriteStatus > PrOverwriteStatusError;
typedef PrError< FileEntry* > PrFileEntryPtrError;
typedef PrError< const PrcsAttrs* > PrPrcsAttrsPtrError;

#ifdef PRCS_DEVEL
typedef NprError< ArgList* > NprArgListPtrError;
typedef NprError< int > NprExitStatusError;
typedef NprError< int > NprIntError;
typedef NprError< PrcsExitStatus > NprPrcsExitStatusError;
typedef NprError< time_t > NprTimeTError;
typedef NprError< bool > NprBoolError;
typedef NprError< const char* > NprConstCharPtrError;
typedef NprError< Dstring* > NprDstringPtrError;
typedef NprError< ProjectDescriptor* > NprProjectDescriptorPtrError;
typedef NprError< PipeRec* > NprPipeRecPtrError;
typedef NprError< OverwriteStatus > NprOverwriteStatusError;
typedef NprError< char > NprCharError;
#else
typedef PrArgListPtrError NprArgListPtrError;
typedef PrExitStatusError NprExitStatusError;
typedef PrIntError NprIntError;
typedef PrPrcsExitStatusError NprPrcsExitStatusError;
typedef PrTimeTError NprTimeTError;
typedef PrBoolError NprBoolError;
typedef PrConstCharPtrError NprConstCharPtrError;
typedef PrDstringPtrError NprDstringPtrError;
typedef PrProjectDescriptorPtrError NprProjectDescriptorPtrError;
typedef PrPipeRecPtrError NprPipeRecPtrError;
typedef PrOverwriteStatusError NprOverwriteStatusError;
#endif

#endif
