/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: misc.cc 1.30.1.3.1.17.1.11.1.7.1.18.1.42 Thu, 25 Mar 1999 05:04:57 -0800 jmacd $
 */

#include "prcs.h"
#include "prcs2.h"

#include "docs.h"
#include "misc.h"

const char  *cmd_program_name;

const char  *cmd_root_project_name;
Path        *cmd_root_project_file_path;
Path        *cmd_root_project_path;
File        *cmd_root_project_file;
File        *cmd_root_project_dir;

int          cmd_repository_port;
const char  *cmd_repository_host;

PathUsed    *cmd_paths_given;
GHashTable  *cmd_paths_given_table;
int          cmd_paths_count;
char       **cmd_filenames_given;
int          cmd_filenames_count;

char       **cmd_diff_options_given;
int          cmd_diff_options_count;

const char  *cmd_version_specifier_major;
const char  *cmd_version_specifier_minor;
int          cmd_version_specifier_minor_int = -1;
const char  *cmd_alt_version_specifier_major;

const char  *cmd_alt_version_specifier_minor;

bool         cmd_prj_given_as_file;

int option_force_resolution;
int option_long_format;
int option_really_long_format;
int option_report_actions;
int option_version_present;
int option_diff_keywords;
int option_diff_new_file;
int option_populate_delete;
int option_be_silent;
int option_preserve_permissions;
int option_exclude_project_file;
int option_unlink;
int option_match_file;
int option_not_match_file;
int option_all_files;
int option_preorder;
int option_pipe;
#ifdef PRCS_DEVEL
int option_n_debug = 1;
int option_tune = 10;
#endif
int option_skilled_merge;
int option_plain_format;
int option_sort;

const char *option_match_file_pattern;
const char *option_not_match_file_pattern;
const char *option_sort_type;

PrVoidError bug (void)
{
  static const char maintainer[] = "prcs-bugs@XCF.Berkeley.EDU";

  throw prcserror << "Please report this to " << maintainer << dotendl
		  << "When sending bug reports, always include:" << prcsendl
		  << "-- a complete description of the problem encountered" << prcsendl
		  << "-- the output of `prcs config'" << prcsendl
		  << "-- the operating system and version" << prcsendl
		  << "-- the architecture" << dotendl
		  << "If possible, include:" << prcsendl
		  << "-- the file " << squote (bug_name()) << ", if it exists" << prcsendl
		  << "-- the working project file, if one was in use" << prcsendl
		  << "-- the output of `ls -lR' in the current directory" << dotendl
		  << "Disk space permitting, retain the following:" << prcsendl
		  << "-- any relevant working project files" << prcsendl
		  << "-- a repository package created with `prcs package PROJECT'" << dotendl
		  << "Disk space not permitting, retain just:" << prcsendl
		  << "-- the repository file PRCS/PROJECT/PROJECT.prj,v" << prcsendl
		  << "-- the repository log PRCS/PROJECT/prcs_log" << prcsendl
		  << "-- the repository data file PRCS/PROJECT/prcs_data" << prcsendl
		  << "-- the output of `rlog' on each repository file under PRCS/PROJECT" << dotendl
		  << "These steps will help diagnose the problem" << dotendl;
}

const char* bug_name (void)
{
  const char* tmp = get_environ_var ("TMPDIR");

  if (!tmp) tmp = "/tmp";

  static Dstring *it;

  if (!it)
    it = new Dstring;

  it->assign (tmp);
  it->append ("/");
  it->append ("prcs_bug");

  return it->cast();
}

char*
guess_prj_name (File* dir)
{
  bool found = false;
  DirectoryHandle* dh;
  Path* ent_long;
  Dstring buf;

  if (! (dh = file_opendir (dir)))
    return NULL;

  while ((ent_long = file_readdir (dh)))
    {
      const char* ent = path_basename (ent_long);
      int len = strlen (ent);

      if (len > 4 && strcmp(".prj", ent + len - 4) == 0)
	{
	  if (found)
	    {
	      return NULL;
            }
	  else
	    {
	      buf.assign (ent);
	      found = true;
            }
        }
    }

  if (! file_closedir (dh))
    return NULL;

  if (found)
    {
      char* ret = g_strdup (buf.cast ());

      ret[buf.length() - 4] = 0;

      return ret;
    }
  else
    {
      return NULL;
    }
}

char
get_user_char()
    /* 0 is error */
{
    int ans, c;

    If_fail(ans << Err_fgetc(stdin))
	return 0;

    if(ans == EOF)
        return 0;
    else if(ans == '\n')
        return '\n';

    while(true) {
	If_fail(c << Err_fgetc(stdin))
	    return 0;

	if (c == EOF)
	    return 0;
	else if (c == '\n')
	    break;
    }

    return (char)ans;
}

NprVoidError read_string(FILE* f, Dstring* s)
{
    while(true) {
	int c;

	Return_if_fail(c << Err_fgetc(f));

	if (c == EOF)
	  return ReadFailure;

	if (c == '\n')
	    break;

	s->append((char)c);
    }

    return NoError;
}

const char*
format_type (FileType t, bool cap)
{
  switch (t)
    {
    case FT_Symlink:
      return cap ? "Symlink" : "symlink";
    case FT_Directory:
      return cap ? "Directory" : "directory";
    case FT_Regular:
      return cap ? "File" : "file";
    default:
      abort ();
    }
}

const char*
get_environ_var (const char* var)
{
    static KeywordTable env_table;
    static bool once = false;
    const char* env;

    if (!once) {
	once = true;

	for (int i = 0; i < env_names_count; i += 1) {
	    env = getenv (env_names[i].var);

	    if (!env)
		env = env_names[i].defval;

	    if (env && !env[0])
	      env = NULL;

	    env_table.insert (env_names[i].var, env);
	}
    }

    const char** lu = env_table.lookup (var);

    if (lu)
	return *lu;

    env = getenv (var);

    if (env && *env) {
	env_table.insert (var, env);

	return env;
    }

    return NULL;
}

NprIntError Err_fgetc(FILE* f)
{
    int c;

    while ((c = fgetc(f)) == EOF && errno == EINTR) { }

    if (c == EOF && ferror(f))
	return ReadFailure;
    else
	return c;
}
