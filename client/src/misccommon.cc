/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: misccommon.cc 1.9 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */

#include "prcs.h"
#include "prcs2.h"

extern "C" {
#include <pwd.h>
#include <pcre.h>
}

#include "docs.h"
#include "misc.h"

gboolean
append_project_data_to_handle (SerialProjectData* pdata, FileHandle* handle)
{
  SerialSink *sink   = NULL;
  GByteArray *idata  = NULL;
  gboolean    result = FALSE;

  const char idata_header[] =
    "(Internal-Data\n"
    "\n;; ------ Do not modify below this line ------\n\n";

  sink = edsio_simple_sink (NULL, SBF_Checksum | SBF_Base64, TRUE, NULL, & idata);

  if (! serialize_projectdata_obj (sink, pdata))
    goto bail;

  if (! sink->sink_close (sink))
    goto bail;

  if (! handle_printf (handle, "%s", idata_header))
    goto bail;

  for (guint pos = 0; pos < idata->len;)
    {
      guint now = MIN (idata->len - pos, 76);

      if (! handle_printf (handle, "  \""))
	goto bail;

      if (! handle_write (handle, idata->data + pos, now))
	goto bail;

      if (! handle_printf (handle, "\"\n"))
	goto bail;

      pos += now;
    }

  if (! handle_printf (handle, ")\n"))
    goto bail;

  result = TRUE;;

 bail:

  if (sink)
    sink->sink_free (sink);

  return result;
}

extern "C" void
lex_fatal_error(const char* msg)
{
  prcs_generate_string_event (EC_PrcsLexFatalError, msg);
  abort ();
}

mode_t get_umask()
{
    mode_t mask = umask(0);

    umask(mask);

    return mask;
}

const char* get_login()
{
    static char buf[32]; /* L_cuserid */
    static bool do_once = false;

    if(do_once) return buf;

    struct passwd *user = getpwuid(get_user_id());

    if(!user)
	strcpy(buf, "unknown");
    else
	strcpy(buf, user->pw_name);

    do_once = true;

    return buf;
}

uid_t get_user_id()
{
    static bool do_once = false;
    static uid_t uid;

    if (do_once) return uid;

    do_once = true;

    uid = getuid();

    return uid;
}

PrcsRegex*
prcs_compile_regex (const char* exp)
{
  int error_offset;
  const char* error;
  pcre* re;

  if (! (re = pcre_compile (exp, 0, & error, & error_offset, NULL)))
    {
      prcs_generate_stringintstring_event (EC_PrcsRegularExpressionError, exp, error_offset, error);
      return NULL;
    }

  return (PrcsRegex*) re;
}

gboolean
prcs_regex_matches (PrcsRegex* pre, const char* str, guint len)
{
  pcre* re = (pcre*) pre;
  int match[3];

  return (pcre_exec (re, NULL, str, len, 0, match, 3) == 1);
}
