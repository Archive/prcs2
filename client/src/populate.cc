/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id$
 */

#include "prcs.h"
#include "prcs2.h"
#include "projdesc.h"

static gboolean populate_path               (ProjectDescriptor* project, Path* path);

PrPrcsExitStatusError
populate_command()
{
  ProjectDescriptor* project;

  if (! (project = read_project_file (cmd_root_project_file, true)))
    throw prcserror << "@@@ this was already printed" << dotendl;

  if (! project->populate_ignore_compile ())
    throw prcserror << "@@@ this was already printed" << dotendl;

  if (cmd_filenames_count < 1)
    {
      if (! populate_path (project, cmd_root_project_path))
	throw prcserror << "@@@ this was already printed" << dotendl;
    }
  else
    {
      for (int i = 0; i < cmd_paths_count; i += 1)
	{
	  if (! populate_path (project, cmd_paths_given[i].path))
	    throw prcserror << "@@@ this was already printed" << dotendl;
	}
    }

  if (! (project->write_project_file (file_default_segment (cmd_root_project_file), SFEF_All)))
    throw prcserror << "@@@ this was already printed" << dotendl;

  return ExitSuccess;
}

gboolean
populate_path (ProjectDescriptor* project, Path* path)
{
  GHashTable* table = project->working_paths_table ();
  GHashTable* dest_table = project->working_dest_paths_table ();

  if (g_hash_table_lookup (table, path))
    return TRUE;

  File* file = file_initialize (_fs_repo, path);

  if (g_hash_table_lookup (dest_table, file_dest_path (file)))
    return TRUE;

  g_assert (path_contains_equiv (cmd_root_project_path, path));

  Path* rel_path = path_suffix (path, path_length (path) - path_length (cmd_root_project_path));

  const char* rel_path_str = path_to_string_simple (rel_path, '/') + 1; /* +1 to skip leading / */
  const char* ignore_exp;

  if (project->populate_ignore_matches (rel_path_str, & ignore_exp))
    {
      prcs_generate_stringstring_event (EC_PrcsPopulateIgnored, rel_path_str, ignore_exp);
      return TRUE;
    }

  switch (file_type (file))
    {
    case FV_Regular:
      project->append_new_regular_file (rel_path_str);
      g_hash_table_insert (table, file_access_path (file), /* @@@ */table);
      g_hash_table_insert (dest_table, file_dest_path (file), /* @@@ */table);
      prcs_generate_string_event (EC_PrcsPopulateFile, rel_path_str);
      break;

    case FV_Directory:
      {
	DirectoryHandle* dh;
	Path* ent;

	if (! (dh = file_opendir (file)))
	  return FALSE;

	while ((ent = file_readdir (dh)))
	  {
	    if (! populate_path (project, ent))
	      return FALSE;
	  }

	if (! file_closedir (dh))
	  return FALSE;
      }
      break;

    case FV_SymbolicLink:
      prcswarning << "@@@ Ignoring symlink: " << squote (path) << dotendl;
      break;
    case FV_NotPresent:
    case FV_Invalid:
    default:
      prcswarning << "Ignoring special file: " << squote (path)
		  << ", continuing" << dotendl;
      break;
    }

  return TRUE;
}
