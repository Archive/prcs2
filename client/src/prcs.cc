/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: prcs.cc 1.23.1.1.1.14.1.18.1.8.1.16.1.1.1.7.1.43 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */

/* $Format: "static const char prcs_version_id[] = \"$ProjectVersion$ $ProjectAuthor$ $ProjectDate$\";"$ */
static const char prcs_version_id[] = "2.0-devel.30 jmacd Fri, 09 Apr 1999 15:27:06 -0700";

#include "prcs.h"
#include "prcs2.h"

#include "docs.h"
#include "misc.h"

extern "C" {
#include <signal.h>
#include <sys/time.h>
#include "getopt.h"
}

/* $Format: "const char prcs_version_string[] = \"$ReleaseVersion$\";"$ */
const char prcs_version_string[] = "1.2.9";

/* The following classes are only used inside this file, and oraganize
 * several arrays of information used to select the command and
 * configuration. */
enum ProjectArgType {
    InsureProjectName,
    NoInsureProjectName,
    NoProjectRequired,
    OptionalProjectName
};

enum RevisionSpecifierArgType {
    NoRevisionArg,
    OneRevisionArg,
    TwoRevisionArgs
};

enum RevisionFlags {
    NoFlags        = 0,
    AllowWildCards = 1 << 1,
    NoDefault      = 1 << 2
};

enum DiffArgType {
    ReallyDiffArgs,
    ReallyExecuteArgs,
    NoDiffArgs
};

struct CommandOptionPair {
    char opt;
    const char* desc;
    int *addr;
};

struct CommandNamePair {
    SystemCommand* command;
    const char* name;
};

struct PrcsCommand {
    const char *command_name;
    const char *defmaj;
    const char *defmin;
    const char *defmin_maj_only;
    int revision_flags;
    PrPrcsExitStatusError (*command_function)();
    const char *help_string;
    RevisionSpecifierArgType revision_arg_type;
    ProjectArgType project_arg_type;
    const char* optstring;
    DiffArgType diff_arg_type;
    int n_files_allowed;
};

/* Static data */

static PrcsCommand *command = NULL;
static PrcsCommand *subcommand = NULL;
static bool illegal_option = false;

static const char check_diff_arg[] = "-v";
static const char check_diff_expected[] = "diff - GNU diffutils version";

#if 0
static const CommandNamePair check_diff = { &gdiff_command, "gdiff" };

static CommandNamePair const command_names[] = {
    { &gdiff_command, "diff" },
    { &gdiff3_command, "diff3" },
    { &gzip_command, "gzip" },
    { &ls_command, "ls" },
    { &tar_command, "tar" },
    { NULL, NULL }
};
#endif

#define MATCH_CHAR       '\300'
#define NOTMATCH_CHAR    '\301'
#define ALL_CHAR         '\302'
#define PRE_CHAR         '\303'
#define PIPE_CHAR        '\304'
#define PLAINFORMAT_CHAR '\305'
#define SORT_CHAR        '\306'
#define EXECUTE_STR      "\300\301\302\303\304"
#define PLAINFORMAT_STR  "\305"
#define SORT_STR         "\306"

static CommandOptionPair const command_options[] = {
    { 'k', "--keywords", &option_diff_keywords },
    { 'N', "--new", &option_diff_new_file },
    { 'd', "--delete", &option_populate_delete },
    /*{ 'z', "--compress", &option_package_compress },*/
    { 'p', "--preserve", &option_preserve_permissions },
    { 'P', "--exclude-project-file", &option_exclude_project_file },
    { 'u', "--unlink", &option_unlink },
    { 's', "--skilled-merge", &option_skilled_merge },
    { SORT_CHAR, "--sort", &option_sort },
    { MATCH_CHAR, "--match", &option_match_file },
    { NOTMATCH_CHAR, "--not", &option_not_match_file },
    { ALL_CHAR, "--all", &option_all_files },
    { PRE_CHAR, "--pre", &option_preorder },
    { PIPE_CHAR, "--pipe", &option_pipe },
    { 0, NULL }
};

static struct option const long_options[] = {
  /* standard operands */
  {"version",                 no_argument, 0, 'v'},
  {"help",                    no_argument, 0, 'h'},
  {"long-long-format",        no_argument, 0, 'L'},
  {"long-format",             no_argument, 0, 'l'},
  {"no-action",               no_argument, 0, 'n'},
  {"quiet",                   no_argument, 0, 'q'},
  {"force",                   no_argument, 0, 'f'},
  {"repository",              required_argument, 0, 'R'},
  {"plain-format",            no_argument, 0, PLAINFORMAT_CHAR },
#ifdef PRCS_DEVEL
  {"debug",                   no_argument, 0, 'D'},
  {"tune",                    required_argument, 0, 't'},
#endif

  /* non-standard operands */
  {"new",                     no_argument, 0, 'N'},
  {"keywords",                no_argument, 0, 'k'},
  {"delete",                  no_argument, 0, 'd'},
  {"compress",                no_argument, 0, 'z'},
  {"family",                  no_argument, 0, 'F'},
  {"preserve-permissions",    no_argument, 0, 'p'},
  {"preserve",                no_argument, 0, 'p'},
  {"permissions",             no_argument, 0, 'p'},
  {"exclude-project-file",    no_argument, 0, 'P'},
  {"revision",                required_argument, 0, 'r'},
  {"unlink",                  no_argument, 0, 'u'},
  {"skilled-merge",           no_argument, 0, 's'},
  {"sort",                    required_argument, 0, SORT_CHAR},
  {"match",                   required_argument, 0, MATCH_CHAR},
  {"not",                     required_argument, 0, NOTMATCH_CHAR},
  {"all",                     no_argument, 0, ALL_CHAR},
  {"pre",                     no_argument, 0, PRE_CHAR},
  {"pipe",                    no_argument, 0, PIPE_CHAR},
  {0,0,0,0}};

static const char prcs_optstring[] = "+vhHLlnqfR:r:NkdFpPrus"
                                     EXECUTE_STR PLAINFORMAT_STR SORT_STR ":"
#ifdef PRCS_DEVEL
"Dt:"
#endif
;

static PrcsCommand commands[] = {
    /* commands accepting -r */
    {"checkin", "", "@", "@", NoFlags,
     checkin_command, checkin_help_string,
     OneRevisionArg, InsureProjectName, "uj", NoDiffArgs, -1 },

    {"checkout", "@", "@", "@", NoFlags,
     checkout_command, checkout_help_string,
     OneRevisionArg, InsureProjectName, "upP", NoDiffArgs, -1 },

#if DEAD
    {"diff", "", "", "@", NoFlags,
     diff_command, diff_help_string,
     TwoRevisionArgs, InsureProjectName, "NkFP", ReallyDiffArgs, -1 },

    {"merge", "", "@", "@", NoFlags,
     merge_command, merge_help_string,
     OneRevisionArg, InsureProjectName, "Fus", ReallyDiffArgs, -1 },

    {"delete", "", "", "@", NoDefault,
     delete_command, delete_help_string,
     OneRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 },

    {"info", "*", "*", "*", AllowWildCards,
     info_command, info_help_string,
     OneRevisionArg, InsureProjectName, SORT_STR, NoDiffArgs, -1 },

    {"execute", "", "", "@", NoFlags,
     execute_command, execute_help_string,
     OneRevisionArg, InsureProjectName, EXECUTE_STR, ReallyExecuteArgs, -1 },
#endif

    /* commands not accepting -r */
    {"populate", NULL, NULL, NULL, NoFlags,
     populate_command, populate_help_string,
     NoRevisionArg, InsureProjectName, "du", NoDiffArgs, -1 },

#if DEAD
    {"depopulate", NULL, NULL, NULL, NoFlags,
     depopulate_command, depopulate_help_string,
     NoRevisionArg, InsureProjectName, "u", NoDiffArgs, -1 },

    {"package", NULL, NULL, NULL, NoFlags,
     package_command, package_help_string,
     NoRevisionArg, InsureProjectName, "z", NoDiffArgs, 1 },

    {"unpackage", NULL, NULL, NULL, NoFlags,
     unpackage_command, unpackage_help_string,
     NoRevisionArg, NoInsureProjectName, NULL, NoDiffArgs, -2 },

    {"rekey", NULL, NULL, NULL, NoFlags,
     rekey_command, rekey_help_string,
     NoRevisionArg, InsureProjectName, "u", NoDiffArgs, -1 },

    {"config", NULL, NULL, NULL, NoFlags,
     config_command, config_help_string,
     NoRevisionArg, NoProjectRequired, NULL, NoDiffArgs, 0 },
#endif /* DEAD */

    /* fake command entries */
    {"admin", NULL, NULL, NULL, NoFlags,
     NULL, NULL, NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 },

    {NULL, NULL, NULL, NULL, NoFlags,
     NULL, NULL, NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 }
};

static PrcsCommand subcommands[] = {
#if 0
    {"compress", NULL, NULL, NULL, NoFlags,
     admin_compress_command, admin_compress_help_string,
     NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 },

    {"uncompress", NULL, NULL, NULL, NoFlags,
     admin_uncompress_command, admin_uncompress_help_string,
     NoRevisionArg, InsureProjectName, "i", NoDiffArgs, 0 },

    {"rebuild",  NULL,  NULL, NULL, NoFlags,
     admin_rebuild_command, admin_rebuild_help_string,
     NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 },

    {"init",  NULL,  NULL, NULL, NoFlags,
     admin_init_command, admin_init_help_string,
     NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 },

    {"access", NULL,  NULL, NULL, NoFlags,
     admin_access_command, admin_access_help_string,
     NoRevisionArg, OptionalProjectName, NULL, NoDiffArgs, 0 },

    {"pdelete", NULL, NULL, NULL, NoFlags,
     admin_pdelete_command, admin_pdelete_help_string,
     NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 },

    {"pinfo", NULL, NULL, NULL, NoFlags,
     admin_pinfo_command, admin_pinfo_help_string,
     NoRevisionArg, NoProjectRequired, NULL, NoDiffArgs, 0 },

    {"prename", NULL, NULL, NULL, NoFlags,
     admin_prename_command, admin_prename_help_string,
     NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 1 },
#endif

    {NULL, NULL, NULL, NULL, NoFlags,
     NULL, NULL, NoRevisionArg, InsureProjectName, NULL, NoDiffArgs, 0 }
};

static const int friendly_signals[] = {
    SIGINT, SIGTERM, SIGHUP,
#ifdef SIGXCPU
    SIGXCPU,
#endif
#ifdef SIGXFSZ
    SIGXFSZ,
#endif
    0
};

static const int unfriendly_signals[] = {
    SIGBUS, SIGFPE, SIGILL, SIGQUIT, SIGSEGV, 0
};

/* End static data */

#if DEAD
static PrVoidError check_system_command(CommandNamePair com,
					const char* arg,
					const char* expected)
{
    Dstring output;

    ArgList *args;

    Return_if_fail(args << com.command->new_arg_list());

    args->append(arg);

    Return_if_fail(com.command->open(true, false));

    If_fail(read_string(com.command->standard_out(), &output))
	throw prcserror << "Read failed from " << com.name << perror;

    Return_if_fail_if_ne(com.command->close(), 0) {
	throw prcserror << com.name << ' ' << arg
			<< " failed, " << com.name
			<< " is probably too old for PRCS to use"
			<< dotendl;
    }

    if(strncmp(output.cast(), expected, strlen (expected)) != 0) {
	throw prcserror << com.name
			 << " installation is too old, please install "
			 << expected << dotendl;
    }

    return NoError;
}

static PrVoidError check_system_commands()
{
    Return_if_fail(check_system_command(check_rcs,
					check_rcs_arg,
					check_rcs_expected));

    Return_if_fail(check_system_command(check_diff,
					check_diff_arg,
					check_diff_expected));

    return NoError;
}

/* Config checks and reports the paths of all the programs upon which
 * PRCS depends, and exits according to whether everything checked
 * out. */
PrPrcsExitStatusError config_command()
{
    const int width = 40;
    const CommandNamePair* command = command_names;
    PrPrcsExitStatusError ret(ExitSuccess);

    prcsoutput << "Version " << prcs_version_string << prcsendl;

    prcsoutput << prcs_version_id << prcsendl;

    if(get_environ_var("RCS_PATH"))
	prcsinfo << "Using $RCS_PATH to find binaries" << dotendl;

    for(;command->command;command += 1) {
	If_fail(command->command->init()) {
	    ret = PrPrcsExitStatusError (FatalError);
	    continue;
	}

	prcsoutput << "System path for " << squote(command->name)
		   << " is: " << setcol(width) << command->command->path() << prcsendl;
    }

    const char* rep;

    If_fail(rep << Rep_guess_repository_path())
	rep = "-*- unavailable -*-";

    prcsoutput << "Repository path is: " << setcol(width) << rep << prcsendl;

    const char* log = get_environ_var ("PRCS_LOGQUERY");

    prcsoutput << "Log querying is: " << setcol(width)
	       << (log ? "on" : "off") << prcsendl;

    const char* editor = get_environ_var ("PRCS_CONFLICT_EDITOR");

    if (editor)
	prcsoutput << "Conflict editor is: " << setcol(width) << editor << prcsendl;

    const char* merge = get_environ_var ("PRCS_MERGE_COMMAND");

    if (merge)
	prcsoutput << "Merge command is: " << setcol(width) << merge << prcsendl;

    const char* diff = get_environ_var ("PRCS_DIFF_COMMAND");

    if (diff)
	prcsoutput << "Diff command is: " << setcol(width) << diff << prcsendl;

    diff = get_environ_var ("PRCS_DIFF_OPTIONS");

    if (diff)
	prcsoutput << "Diff options are: " << setcol(width) << diff << prcsendl;

    const char* tmpdir = get_environ_var ("TMPDIR");
    if (!tmpdir) tmpdir = "/tmp";
    prcsoutput << "Temp directory is: " << setcol(width) << tmpdir << prcsendl;

    prcsoutput << "Plain formatting is: " << setcol(width)
	       << (option_plain_format ? "on" : "off") << prcsendl;

    Return_if_fail(check_system_commands());

    return ret;
}
#endif

static bool check_ignored_options(const char* optstring)
{
    const CommandOptionPair *p = command_options;
    bool warning = false;

    for(; p->opt; p += 1) {
	if(*p->addr && !strchr(optstring, p->opt)) {
	    prcsinfo << "Ignoring command line switch "
		     << p->desc << dotendl;
	    warning = true;
	    *p->addr = 0;
	}
    }

    if (option_all_files && option_pipe) {
	prcsinfo << "Ignoring --pipe with --all specified" << dotendl;
	option_pipe = 0;
	warning = true;
    }

    return warning;
}

#ifndef DEAD
#define VC_check_token_match(a,b) 1
#endif

static PrVoidError check_major_version_name(const char* name)
{
    if (name[0] && strcmp(name, "@") != 0 &&
	VC_check_token_match(name, "label") <= 0)
	throw prcserror << "Illegal major version name "
			 << squote(name)
			 << dotendl;

    return NoError;
}

static PrVoidError check_minor_version_name(const char* name)
{
    if (name[0] && strcmp(name, "@") != 0 &&
	VC_check_token_match(name, "number") <= 0)
	throw prcserror << "Illegal minor version name "
			 << squote(name)
			 << dotendl;

    return NoError;
}

static PrPrcsExitStatusError invoke_command(PrcsCommand *c)
{
    bool warning = false;

    if (c->n_files_allowed >= 0 && cmd_filenames_count != c->n_files_allowed) {
	throw prcserror << "This command requires " << c->n_files_allowed
			<< " file-or-dir arguments" << dotendl;
    }

    if(c->project_arg_type == InsureProjectName ||
       (c->project_arg_type == OptionalProjectName && cmd_root_project_name)) {
	if(VC_check_token_match(cmd_root_project_name, "label") <= 0 ||
	   /* this length is arbitrary */
	   strlen(cmd_root_project_name) > 200) {
	    prcserror << "Illegal project name " << squote(cmd_root_project_name)
		       << dotendl;
	    exit(2);
	}
    }

    if (c->revision_arg_type != NoRevisionArg) {
	if (c->revision_flags & NoDefault &&
	    !cmd_version_specifier_major &&
	    !cmd_version_specifier_minor)
	    throw prcserror << "You must specify a revision argument with "
			    << squote ("-r") << " for this command" << dotendl;

	if (!cmd_version_specifier_major && !cmd_version_specifier_minor) {
	    cmd_version_specifier_major = c->defmaj;
	    cmd_version_specifier_minor = c->defmin;
	}

	if (!cmd_version_specifier_minor)
	    cmd_version_specifier_minor = c->defmin_maj_only;

	if (!cmd_version_specifier_minor[0] && cmd_version_specifier_major[0])
	    throw prcserror << "It is illegal to specify a null minor "
		"version without a null major version" << dotendl;

	if (!(c->revision_flags & AllowWildCards)) {
	    Return_if_fail(check_major_version_name(cmd_version_specifier_major));
	    Return_if_fail(check_minor_version_name(cmd_version_specifier_minor));
	}

	if (c->revision_arg_type == TwoRevisionArgs &&
	    cmd_alt_version_specifier_major) {
	    if (!cmd_alt_version_specifier_minor)
		cmd_alt_version_specifier_minor = c->defmin_maj_only;

	    if (!(c->revision_flags & AllowWildCards)) {
		Return_if_fail(check_major_version_name(cmd_alt_version_specifier_major));
		Return_if_fail(check_minor_version_name(cmd_alt_version_specifier_minor));
	    }
	}
    }

    warning |= check_ignored_options(c->optstring ? c->optstring : "");

    if(warning) {
	prcsquery << "Command line options are being ignored.  "
		  << force("Continuing")
		  << report("Continue")
		  << optfail('n')
		  << defopt('y', "Continue, ignoring these options")
		  << query("Continue");

	Return_if_fail(prcsquery.result());
    }

    if (cmd_version_specifier_minor &&
	cmd_version_specifier_minor[0] &&
	strcmp(cmd_version_specifier_minor, "@") != 0) {
	cmd_version_specifier_minor_int = atoi(cmd_version_specifier_minor);
    }

    return c->command_function();
}

static PrVoidError
set_project_name (const char* specifier0)
{
  /* There is ambiguity between interpreting "prcs" as a directory
   * or as a project name.  It will only be interpreted as a dir
   * if the directory is really a directory. */
  Path* spec_path = path_canonicalize (_fs_repo, specifier0);
  File* spec_file = file_initialize (_fs_repo, spec_path);

  int   spec_len  = strlen (specifier0);

  Path    *project_path = NULL;
  Dstring *project_name = new Dstring;

  if (spec_len > 4 && strcmp (specifier0 + spec_len - 4, ".prj") == 0)
    {
      project_path = path_dirname (spec_path);

      project_name->assign (path_basename (spec_path));
      project_name->truncate (project_name->length() - 4);
    }
  else if (file_is_type_noerr (spec_file, FV_Directory))
    {
      const char* prj;

      project_path = spec_path;

      if (! (prj = guess_prj_name (file_initialize (_fs_repo, project_path))))
	{
	  throw prcserror << "Cannot determine project name, there must be a single "
	    "project file in the directory " << squote (spec_path) << dotendl;
	}

      project_name->assign (prj);

      g_free ((void*) prj);
    }
  else if (specifier0[spec_len - 1] == '/' /* @@@ UNIX-ism, but unneccessary */)
    {
      throw prcserror << "Directory " << squote(specifier0)
		      << " does not exist, cannot determine project name" << dotendl;
    }
  else
    {
      project_path = path_dirname (spec_path);

      project_name->assign (path_basename (spec_path));
    }

  cmd_root_project_name      = project_name->cast(); /* project_name leaks */

  cmd_root_project_file_path = path_append_format (project_path, "%s.prj", cmd_root_project_name);
  cmd_root_project_path      = project_path;

  cmd_root_project_file      = file_initialize (_fs_repo, cmd_root_project_file_path);
  cmd_root_project_dir       = file_initialize (_fs_repo, cmd_root_project_path);

  return NoError;
}

static PrIntError
determine_project (PrcsCommand* command, int argc, char** argv)
{
  if (command->project_arg_type != NoProjectRequired)
    {
      if (argc > 0 && strcmp(argv[0], "--") != 0)
	{
	  Return_if_fail(set_project_name(argv[0]));

	  return 1;
	}
      else if (command->project_arg_type == InsureProjectName)
	{
	  Return_if_fail (set_project_name("./"));
	}
      else if (command->project_arg_type != OptionalProjectName)
	{
	  throw prcserror << "Missing operand" << dotendl;
	}
    }

  return 0;
}

static gboolean
set_repository (const char* str)
{
  if (! cmd_repository_host)
    {
      /* host [:port] */

      char* colon = strchr (str, ':');
      char* host;
      guint port;

      if (colon)
	{
	  if (! strtoui_checked (colon + 1, & port, "Invalid port"))
	    return FALSE;

	  host = g_new (char, colon - str + 1);
	  strncpy (host, str, colon - str);
	  host[colon-str] = 0;
	}
      else
	{
	  port = PRCS_PROTOCOL_DEFAULT_PORT;
	  host = g_strdup (str);
	}

      if (! host[0])
	{
	  prcs_generate_string_event (EC_PrcsInvalidHostString, "(null)");
	  return FALSE;
	}

      cmd_repository_port = port;
      cmd_repository_host = host;

      DEBUG ("Repository host: " << cmd_repository_host);
      DEBUG ("Repository port: " << cmd_repository_port);
    }

  return TRUE;
}

static gboolean
set_repository_env (const char* env)
{
  return set_repository (getenv (env));
}

static PrVoidError
set_cmd_filenames (void)
{
  if (cmd_filenames_count > 0)
    {
      cmd_paths_given       = g_new0 (PathUsed, cmd_filenames_count);
      cmd_paths_given_table = g_hash_table_new (g_direct_hash, g_direct_equal);

      int j = 0;
      bool warn = false;

      for (int i = 0; i < cmd_filenames_count; i += 1)
	{
	  Path* p = path_canonicalize (_fs_repo, cmd_filenames_given[i]);

	  if (! p)
	    {
	      warn = true;
	      continue;
	    }

	  if (! path_contains (cmd_root_project_path, p))
	    {
	      prcswarning << "Path " << squote(p) << " not contained in project" << dotendl;
	      warn = true;
	      continue;
	    }

	  cmd_paths_given[j].path = p;
	  cmd_filenames_given[j] = cmd_filenames_given[i];

	  g_hash_table_insert (cmd_paths_given_table, p, cmd_paths_given + j);

	  j += 1;
	}

      cmd_paths_count = cmd_filenames_count = j;

       if (warn)
	 {
	   prcsquery << "Command line file-or-dirs are being ignored do to prior warnings.  "
		     << force ("Continuing")
		     << report ("Continue")
		     << optfail ('n')
		     << defopt ('y', "Proceed, ignore these filenames")
		     << query ("Proceed");

	   char c;

	   Return_if_fail(c << prcsquery.result());
	 }
    }

  return NoError;
}

static bool read_command_line(int argc, char** argv)
{
    int c;
    PrcsCommand *pc = commands;
    int num_revs = 0, i;
    int longind;

    if(argc > 1)
	for(; pc->command_name != NULL; pc += 1)
	    if (strcmp(pc->command_name, argv[1]) == 0) {
		argv += 1;
		argc -= 1;
		command = pc;
		break;
	    }

    if(command && strcmp(command->command_name, "admin") == 0) {
	if(argc <= 1)
	    return false;

	for(pc = subcommands; pc->command_name != NULL; pc += 1)
	    if (strcmp(pc->command_name, argv[1]) == 0) {
		argv += 1;
		argc -= 1;
		subcommand = pc;
		break;
	    }

	if(subcommand == NULL)
	    return false;
    }

    while ((c = getopt_long(argc, argv, prcs_optstring, long_options, &longind)) != EOF)
    {
	char *min;
	switch (c) {
	case 'v':
	    prcsoutput << "Version " << prcs_version_string << endl;
	    exit(0);
	    break;
	case 'h': case 'H': return false; break;
	case 'f': option_force_resolution = 1; break;
	case 'k': option_diff_keywords = 1; break;
	case 'l': option_long_format = 1; break;
	  /*case 'z': option_package_compress = 1; break;*/
	case 'd': option_populate_delete = 1; break;
	case 'q': option_be_silent = 1; break;
	case 'N': option_diff_new_file = 1; break;
	case 'L': option_really_long_format = 1; break;
	case 'n': option_report_actions = 1; break;
	case 'p': option_preserve_permissions = 1; break;
	case 'P': option_exclude_project_file = 1; break;
	case 'u': option_unlink = 1; break;
	case 'R':
	  {
	    if (! set_repository (optarg))
	      exit(2);
	  }
	  break;

	break;
	case MATCH_CHAR:
	    option_match_file = 1;
	    option_match_file_pattern = optarg;
	    break;
	case NOTMATCH_CHAR:
	    option_not_match_file = 1;
	    option_not_match_file_pattern = optarg;
	    break;
	case ALL_CHAR: option_all_files = 1; break;
	case PRE_CHAR: option_preorder = 1; break;
	case PIPE_CHAR: option_pipe = 1; break;
	case PLAINFORMAT_CHAR: option_plain_format = 1; break;
	case SORT_CHAR: option_sort = 1; option_sort_type = optarg; break;
#ifdef PRCS_DEVEL
	case 'D': option_n_debug = 0; break;
	case 't': option_tune = atoi (optarg); break;
#endif
	case 's': option_skilled_merge = 1; break;
	case 'r':
	    option_version_present = 1;

	    if(!command) break;

	    if (num_revs < 1 && !command->revision_arg_type == NoRevisionArg) {
		min = strrchr(optarg, '.'); /* last occurance of a period */
		if(min == NULL){
		    cmd_version_specifier_major = optarg;
		} else {
		    *min = '\0';
		    cmd_version_specifier_minor = min + 1;
		    cmd_version_specifier_major = optarg;
		}
		num_revs += 1;
	    } else if(num_revs < 2 && command->revision_arg_type == TwoRevisionArgs) {
		min = strrchr(optarg, '.'); /* last occurance of a period */
		if(min == NULL){
		    cmd_alt_version_specifier_major = optarg;
		} else {
		    *min = '\0';
		    cmd_alt_version_specifier_minor = min + 1;
		    cmd_alt_version_specifier_major = optarg;
		}
		num_revs += 1;
	    } else {
		prcserror << "Too many -r options given on command line" << dotendl;
		exit(2);
	    }

	    break;
	case '?':
	default:
	    illegal_option = true;
	    return false;
	    break;
	}
    }

    if (!option_sort)
	option_sort_type = "version";

    if (strcmp(option_sort_type, "version") != 0 &&
	strcmp(option_sort_type, "date") != 0) {
	prcserror << "Illegal sort type " << squote (option_sort_type) << dotendl;
	exit(2);
    }

    if(get_environ_var("PRCS_PLAIN_FORMAT"))
	option_plain_format = 1;

    if(option_report_actions)
	option_long_format = 1;

    if(subcommand)
	command = subcommand;

    if(command == NULL)
	return false;

    argc -= optind;
    argv += optind;

    int used;

    If_fail(used << determine_project(command, argc, argv))
	exit(2);

    argv += used;
    argc -= used;

    cmd_filenames_given = argv;
    cmd_filenames_count = argc;

    i = 0;
    cmd_diff_options_given = 0;
    cmd_diff_options_count = -1;

    while(i < cmd_filenames_count) {
	if(strcmp(cmd_filenames_given[i], "--") == 0) {
	    cmd_filenames_count = i;
	    cmd_diff_options_count = argc - i - 1;
	    cmd_diff_options_given = argv + i + 1;
	}
	i += 1;
    }

    if (command->diff_arg_type == NoDiffArgs && cmd_diff_options_count >= 0) {
	prcserror << "This command does not accept arguments following "
		  << squote ("--") << dotendl;
	exit(2);
    }

    if (cmd_diff_options_count < 0 && command->diff_arg_type == ReallyDiffArgs) {
	const char* pdo = get_environ_var ("PRCS_DIFF_OPTIONS");

	if (pdo) {
	    CharPtrArray *env_diff_options = new CharPtrArray;
	    char *p;

	    p = strtok (g_strdup (pdo), " \t\n");

	    while (p) {
		env_diff_options->append (p);
		p = strtok(NULL, " \t\n");
	    }

	    cmd_diff_options_given = (char**)env_diff_options->cast();
	    cmd_diff_options_count = env_diff_options->length();
	    /* @@@ leak */
	}
    }

    if (command->n_files_allowed == -1) {
	If_fail(set_cmd_filenames())
	    exit(2);
    }

    return true;
}

static void usage()
{
  if (illegal_option)
    {
      cerr << options_summary << endl;
    }
  else if (command == NULL)
    {
      cerr << "Usage: " << cmd_program_name
	   << " command [subcommand] [option ...] [project [file-or-dir]]\n";
      cerr << general_help_string;
    }
  else if (strcmp (command->command_name, "admin") == 0)
    {
      if (subcommand == NULL)
	{
	  cerr << admin_help_string;
	}
      else
	{
	  cerr << subcommand->help_string;
	}
    }
  else
    {
      cerr << command->help_string;
    }

  exit(2);
}

/* Cleanup */

static void
bad_clean_up_handler(SIGNAL_ARG_TYPE)
{
  prcserror << "Internal error, attempting to drop core" << dotendl;

  bug ();

  if (! repository_rollback (_fs_repo))
    prcserror << "warning: rollback failed" << dotendl;

  if (! repository_system_close ())
    ; /* @@@ DTRT here */

  abort();
}

static void
clean_up_handler(SIGNAL_ARG_TYPE)
{
  if (! repository_rollback (_fs_repo))
    prcserror << "warning: rollback failed" << dotendl;

  if (! repository_system_close ())
    ; /* @@@ DTRT here */

  exit(2);
}

static void
sigpipe_handler(SIGNAL_ARG_TYPE)
{
  /* Hi! */
}

static void
handle_signals()
{
  struct sigaction act;
  sigset_t signal_mask;
  int i;

  sigfillset(&signal_mask);
  sigdelset(&signal_mask, SIGABRT);

  act.sa_handler = clean_up_handler;
  act.sa_mask = signal_mask;
  act.sa_flags = 0;

  for (i = 0; friendly_signals[i]; i += 1)
    sigaction(friendly_signals[i], &act, NULL);

  act.sa_handler = bad_clean_up_handler;

  for (i = 0; unfriendly_signals[i]; i += 1)
    sigaction(unfriendly_signals[i], &act, NULL);

  sigemptyset(&signal_mask);

  /* SIGCONT */
  act.sa_handler = continue_handler;
  act.sa_mask = signal_mask;
  sigaction(SIGCONT, &act, NULL);

  /* SIGPIPE */
  act.sa_handler = sigpipe_handler;
  sigaction(SIGPIPE, &act, NULL);
}

static void
check_umask()
{
  mode_t user_file_creation_mask = get_umask();

  if (user_file_creation_mask & 0700)
    {
      prcswarning << "Your umask will prevent PRCS from functioning properly.  "
	"Proceeding with user RWX permissions" << dotendl;

      umask(user_file_creation_mask & 0077);
    }
}

int
main (int argc, char** argv)
{
  PrPrcsExitStatusError exitval = ExitSuccess;
  const char* home;
  Path *home_path;
  File *prcs_dir, *privkey_file;
  Path* p;

  if (! repository_system_init ())
    exit (2);

  p = path_canonicalize (_fs_repo, argv[0]);

  cmd_program_name = (p ? path_basename (p) : argv[0]);

  prcs_edsio_init ();

  if (! edsio_time_of_day (NULL))
    exit (2);

  setup_streams ();

  handle_signals ();

  check_umask();

  if (read_command_line(argc, argv) == false)
    usage ();

  if (! set_repository_env ("PRCS2_REPOSITORY"))
    exit (2);

  if (! set_repository_env ("PRCS_REPOSITORY"))
    exit (2);

  adjust_streams();

  if (!option_force_resolution && !isatty(STDIN_FILENO) )
    {
      prcserror << "Please specify -f to run PRCS without a controlling terminal"
		<< dotendl;
      exit(2);
    }

  home = g_get_home_dir ();

  if (! home || ! (home_path = path_canonicalize (_fs_repo, home)))
    {
      prcserror << "No home directory available, set HOME in your environment" << dotendl;
      exit (2);
    }

  prcs_dir = file_initialize (_fs_repo, path_append (home_path, ".prcs"));

  if (file_is_type_noerr (prcs_dir, FV_Directory))
    ;
  else if (file_is_type_noerr (prcs_dir, FV_NotPresent))
    {
      prcsinfo << "Creating the directory " << squote (prcs_dir) << dotendl;

      if (! file_mkdir (prcs_dir))
	exit (2);
    }
  else
    {
      prcserror << "The file " << squote (prcs_dir) << " is not a directory, cannot continue" << dotendl;
      exit (2);
    }

  privkey_file = file_initialize (_fs_repo, path_append2 (home_path, ".prcs", "identity"));

  if (! reponet_init (privkey_file))
    exit (2);

  if(subcommand == NULL)
    exitval = invoke_command(command);
  else
    exitval = invoke_command(subcommand);

  if (exitval.error())
    {
      if (! repository_rollback (_fs_repo))
	prcserror << "warning: rollback failed" << dotendl;
    }

  if (! repository_system_close ())
    ; /* @@@ DTRT here */

  if (Failure (exitval))
    {
      if (exitval.error_val() != UserAbort)
	prcserror << "Command failed" << dotendl;

      return 2;
    }

  return exitval.non_error_val();
}
