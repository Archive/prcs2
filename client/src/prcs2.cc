/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: prcs2.cc 1.16 Sat, 27 Mar 1999 03:16:08 -0800 jmacd $
 */

#include "prcs.h"
#include "prcs2.h"

#include "projdesc.h"
#include "misc.h"
#include "fileent.h"


/* Protocol Declaration
 */

static Request* prcs_client_protocol_requests[] =
{
  & prcs_client_checkin_request,
  & prcs_client_checkout_request
};

Protocol prcs_client_protocol =
{
  "PRCS",
  PRCS_PROTOCOL_DEFAULT_PORT,
  NULL,
  NULL,
  NULL,
  PRCS_PROTOCOL_MAJOR_VERSION,
  PRCS_PROTOCOL_MINOR_VERSION,
  STATIC_ARRAY (prcs_client_protocol_requests)
};

/* General functions
 */

/*
 * eliminate_unnamed_files --
 *
 *     this marks files that were not given on the command line so
 *     that they can be ignored in certain stages of the checkin (and
 *     other) commands.  if no files were given, it leaves them all
 *     marked, otherwise, it unmarks them all and marks all files (and
 *     recursively marks directories).  */

static bool
path_supplied (Path* p)
{
  PathUsed* pu = NULL;

  for (;;)
    {
      pu = (PathUsed*) g_hash_table_lookup (cmd_paths_given_table, p);

      if (pu)
	{
	  pu->used = true;
	  break;
	}

      g_assert (p != path_root ());

      p = path_dirname (p);

      if (p == cmd_root_project_path)
	break;
    }

  return pu != NULL;
}

void
eliminate_unnamed_files (ProjectDescriptor* project)
{
  if (cmd_filenames_count < 1)
    {
      if (! option_exclude_project_file)
	cmd_prj_given_as_file = true;
      return;
    }

  if (path_supplied (cmd_root_project_file_path))
    cmd_prj_given_as_file = true;

  foreach_fileent (fe_ptr, project)
    {
      FileEntry *fe = *fe_ptr;

      if (! path_supplied (fe->working_path ()))
	fe->set_on_cmd_line (false);
    }
}

/*
 * warn_unused_files --
 *
 *     this optionally prompts the user to continue and warns if any files
 *     were not matched by a call to eliminate_unnamed_files.
 *
 *     if prompt_abort is true, returns an error if the user does not
 *     confirm ignoring unmatched filenames.
 *
 *     if prompt_abort is false, it will return an error if files are unmatched.
 */
PrVoidError
warn_unused_files (bool prompt_abort)
{
  bool any = false, ret = true, need_prompt = false;

  if (cmd_paths_count < 1)
    return NoError;

  for (int j = 0; j < cmd_filenames_count; j += 1)
    {
      any |= cmd_paths_given[j].used;

      if (! cmd_paths_given[j].used)
	{
	  if (prompt_abort)
	    {
	      need_prompt = true;
	      prcswarning << "File or directory " << squote(cmd_filenames_given[j])
			  << " on command line does not match any working files" << dotendl;
	    }
	  else
	    {
	      ret = false;
	      prcswarning << "File or directory " << squote(cmd_filenames_given[j])
			  << " on command line did not match any working files" << dotendl;
	    }
	}
    }

  if (! any)
    throw prcserror << "No files on command line match working files" << dotendl;

  if (need_prompt)
    {
      prcsquery << "Files on the command line did not match files in the project.  "
		<< force("Continuing")
		<< report("Continue")
		<< optfail('n')
		<< defopt('y', "Continue, ignoring these files")
		<< query("Continue");

      Return_if_fail(prcsquery.result());
    }

  if (ret)
    return NoError;
  else
    return PrVoidError(NonFatalError);
}

/* returns true if the file should be skipped */
static PrBoolError
try_elim_subdir (FileEntry* fe, MissingFileAction action)
{
  static GHashTable *already_eliminated = NULL;

  Path *dir_path = path_dirname (fe->working_path());

  if (! already_eliminated)
    already_eliminated = g_hash_table_new (g_direct_hash, g_direct_equal);

  if (g_hash_table_lookup (already_eliminated, dir_path))
    {
      switch (action)
	{
	case QueryUserRemoveFromCommandLine:
	case NoQueryUserRemoveFromCommandLine:
	  fe->set_on_cmd_line (false);
	  break;
	}

      return true;
    }

  do
    {
      File* dir = file_initialize (_fs_repo, dir_path);

      if (dir && file_is_type_noerr (dir, FV_Directory))
	{
	  return false;
	}
      else
	{
	  static BangFlag bang;

	  switch (action)
	    {
	    case QueryUserRemoveFromCommandLine:
	      prcsquery << "The directory " << squote (dir_path)
			<< " does not exist.  "
			<< force("Ignoring all entries")
			<< report("Ignore all entries")
			<< optfail('n')
			<< defopt('y', "Ignore all entries of this directory")
			<< allow_bang (bang)
			<< query("Ignore it and descendants");

	      Return_if_fail(prcsquery.result());

	      fe->set_on_cmd_line(false);
	      break;
	    case NoQueryUserRemoveFromCommandLine:
	      prcswarning << "The directory " << squote (dir_path)
			  << " does not exist.  Ignoring all entries" << dotendl;

	      fe->set_on_cmd_line(false);
	      break;
	    }

	  g_hash_table_insert (already_eliminated, dir, dir);

	  return true;
	}
    }
  while (dir_path != cmd_root_project_path);

  return false;
}

PrVoidError
eliminate_working_files (ProjectDescriptor* project, MissingFileAction action)
{
  foreach_fileent (fe_ptr, project)
    {
      FileEntry* fe = *fe_ptr;

      if (fe->on_cmd_line())
	{
	  static BangFlag bang;

	  if (fe->working_file_exists ())
	    continue;

	  bool ignore_subdir;

	  Return_if_fail (ignore_subdir << try_elim_subdir(fe, action));

	  if (ignore_subdir)
	    continue;

	  switch (action)
	    {
	    case QueryUserRemoveFromCommandLine:
	      prcsquery << "File " << squote(fe->working_name()) << " is unavailable.  "
			<< force("Continuing")
			<< report("Continue")
			<< allow_bang(bang)
			<< defopt('y', "Ignore this file, as if it were not on the command line")
			<< optfail('n')
			<< query("Ignore this file");

	      Return_if_fail(prcsquery.result());
	      fe->set_on_cmd_line(false);
	      break;
	    case NoQueryUserRemoveFromCommandLine:
	      prcswarning << "File " << squote(fe->working_name())
			  << " is unavailable.  Continuing" << dotendl;
	      fe->set_on_cmd_line(false);
	      break;
	    }
	}
    }

  return NoError;
}

/* New Repository
 */

TaskControl
PrcsRepository::fail (Task* task)
{
  _result = ExitFailure;

  return reponet_done_task (task);
}

TaskControl
PrcsRepository::exitwith (Task* task, PrPrcsExitStatusError err)
{
  _result = err.non_error_val ();

  return reponet_done_task (task);
}

PrcsRepository::PrcsRepository (SerialAliasedIdentifier* project0, SerialVersionIdentifier* version0)
  :_result (ExitSuccess)
{
  _connect.project = *project0;
  _connect.version = *version0;
}

PrVoidError
PrcsRepository::init (void)
{
  return NoError;
}

PrcsExitStatus
PrcsRepository::go (int request)
{
  TaskManager* tm = task_manager_new ();
  Peer peer;

  peer.port = cmd_repository_port;
  peer.address_name = cmd_repository_host;

  if (! (peer.address = sock_addr_init (cmd_repository_host)))
    return ExitFailure;

  if (! protocol_connect_init (& peer,
			       & _connect,
			       ST_PrcsConnect,
			       & prcs_client_protocol,
			       request,
			       NULL,
			       NULL,
			       NULL,
			       tm))
    return ExitFailure;

  if (! reponet_event_loop (tm))
    return ExitFailure;

  return _result;
}
