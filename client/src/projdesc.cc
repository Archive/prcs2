/* -*-Mode: C++;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: projdesc.cc 1.9.1.1.1.4.1.17.1.5.1.17.3.9.1.45 Wed, 31 Mar 1999 02:17:22 -0800 jmacd $
 */

extern "C" {
#include <stdarg.h>
}

#include "prcs.h"
#include "prcs2.h"

#include "projdesc.h"
#include "sexp.h"
#include "fileent.h"
#include "misc.h"

class PopulateIgnoreEntry {

public:
  PopulateIgnoreEntry (Estring* str) { _str = str; }

  Estring*   _str;
  PrcsRegex* _regex;
};

/* in attrs.cc */
extern const struct AttrDesc*
is_file_attribute (register const char *str, register int len);

#if DEAD
static const char bad_match_help_string[] =
"When trying to determine how files are related between versions, PRCS "
"first attempts to find a file with the same file family, then looks for "
"a file with the same name.  If both exist, and are not the same file, "
"the match is ambiguous.  This can happen if you rename a file and then "
"recreate a file with the original name.  You may do this, but it is "
"problematic when performing merge and diff operations";

extern const char *default_merge_descs[14];
extern const MergeAction default_merge_actions[14];
#endif

#define prj_lex_val prjtext
#define prj_lex_len prjleng

EXTERN int         prjleng;
EXTERN const char* prjtext;
EXTERN int         prj_lex_this_index;
EXTERN int         prj_lex_cur_index;
EXTERN int         prj_lineno;

#include "prj-names.h"

#define ENTRIES sizeof(ProjectDescriptor::_pftable)/sizeof(ProjectDescriptor::_pftable[0])

const int ProjectDescriptor::_prj_entries = ENTRIES;
bool ProjectDescriptor::_prj_entry_found [ENTRIES];

static FileHandle* _prj_handle;

EXTERN int is_builtin_keyword(const char* s, int len);

Dstring* ProjectDescriptor::project_version_name()  const { return _project_version_name;}
Dstring* ProjectDescriptor::project_version_major() const { return _project_version_major; }
Dstring* ProjectDescriptor::project_version_minor() const { return _project_version_minor; }
Dstring* ProjectDescriptor::parent_version_name()  const { return _parent_version_name; }
Dstring* ProjectDescriptor::parent_version_major() const { return _parent_version_major; }
Dstring* ProjectDescriptor::parent_version_minor() const { return _parent_version_minor; }
Dstring* ProjectDescriptor::project_description() const { return _project_description;}
Dstring* ProjectDescriptor::version_log()         const { return _version_log; }
Dstring* ProjectDescriptor::new_version_log()     const { return _new_version_log; }
Dstring* ProjectDescriptor::checkin_time()        const { return _checkin_time; }
Dstring* ProjectDescriptor::checkin_login()       const { return _checkin_login; }

const SerialAliasedIdentifier* ProjectDescriptor::project_id() const { return & _idata->project_id; }
const SerialAliasedIdentifier* ProjectDescriptor::branch_id() const { return & _idata->project_branch_id; }

const char*         ProjectDescriptor::project_full_version() const { return _project_full_version; }
FileEntryPtrArray*  ProjectDescriptor::file_entries() const { return _file_entries; }
GHashTable*         ProjectDescriptor::filenames_table (void) { return _filenames_table; }
GHashTable*         ProjectDescriptor::descriptors_table (void) { return _descriptors_table; }
GHashTable*         ProjectDescriptor::working_paths_table (void) { return _working_paths_table; }
GHashTable*         ProjectDescriptor::working_dest_paths_table (void) { return _working_dest_paths_table; }

#define bad_project_file(sexp,msg) \
    (prcs_generate_fileintstring_event(EC_PrcsInvalidProjectFile,_prj_source,(sexp)->line_number(),(msg)),FALSE)

static void
remove_syntax_from_string (Dstring* d)
{
  Dstring copy (*d);
  const char* p = copy.cast();

  d->truncate (0);

  while (*p)
    {
      if (*p == '\\')
	p += 1;

      d->append(*p);
      p += 1;
    }
}

static void
append_syntax_label (const char* str, Dstring *d)
{
  while (*str)
    {
      char c = *str++;

      switch (c)
	{
	case '\"': case '\\': case ')':  case '(':  case ' ': /*case '.':*/
	case '#': case '\t': case '\n': case '\v': case '\r': case '\f':
	case ';':
	  d->append ('\\');
	default:
	  d->append (c);
	}
    }
}

static void
append_syntax_string (const char* str, Dstring* d)
{
  while (*str)
    {
      char c = *str++;

      switch (c)
	{
	case '\"':
	case '\\':
	  d->append ('\\');
	default:
	  d->append (c);
	}
    }
}

static gboolean
write_syntax_string (const char* str, FileHandle* handle)
{
  Dstring d;

  append_syntax_string (str, &d);

  return handle_write (handle, (const guint8*) d.cast(), d.length());
}

static gboolean
write_syntax_label (const char* str, FileHandle* handle)
{
  Dstring d;

  append_syntax_label (str, &d);

  return handle_write (handle, (const guint8*) d.cast (), d.length());
}

ProjectDescriptor::~ProjectDescriptor()
{
  if (_file_entries)
    {
      foreach_fileent (fe_ptr, this)
	delete (*fe_ptr);

      delete (_file_entries);
    }

  if (_all_estrings)
    {
      foreach (es_ptr, _all_estrings, DstringPtrArray::ArrayIterator)
	delete (*es_ptr);

      delete _all_estrings;
    }

  delete _deleted_markers;

  if (_populate_ignore)  g_ptr_array_free (_populate_ignore, TRUE); /* @@@ free entries */

  if (_project_keywords) delete _project_keywords;
  if (_project_keywords_extra) delete _project_keywords_extra;

  if (_keyword_id)       delete _keyword_id;
  if (_keyword_pheader)  delete _keyword_pheader;
  if (_keyword_pversion) delete _keyword_pversion;

  if (_prj_source_backing)
    handle_close (_prj_source_backing);
}

ProjectDescriptor::ProjectDescriptor()
  :_prj_source(NULL),
   _all_estrings(NULL),
   _files_insertion_point(NULL),
   _project_version_point(NULL),
   _end_of_buffer_point(NULL),
   _project_keywords_point(NULL),
   _populate_ignore_point(NULL),
   _project_keywords_marker(0, 0),
   _populate_ignore_marker(0, 0),
   _idata_marker(0, 0),
   _deleted_markers(NULL),
   _file_entries(NULL),
   _project_version_name(NULL),
   _project_version_major(NULL),
   _project_version_minor(NULL),
   _parent_version_name(NULL),
   _parent_version_major(NULL),
   _parent_version_minor(NULL),
   _project_description(NULL),
   _version_log(NULL),
   _new_version_log(NULL),
   _checkin_time(NULL),
   _checkin_login(NULL),
   _project_keywords(NULL),
   _project_keywords_extra(NULL),
   _keyword_id(NULL),
   _keyword_pheader(NULL),
   _keyword_pversion(NULL),
   _alter_popkey(false),
   _idata(NULL),
   _idata_files(g_array_new (FALSE, FALSE, sizeof (SerialFileEntry))),
   _idata_files_log(g_array_new (FALSE, FALSE, sizeof (SerialFileEntryLogEntry))),
   _idata_invokation_log(g_array_new (FALSE, FALSE, sizeof (SerialInvokationLogEntry)))
{ }

ProjectDescriptor*
read_project_file (File *file, bool is_working)
{
  ProjectDescriptor* new_project = new ProjectDescriptor ();

  if (! new_project->init_from_file (file, is_working))
    {
      delete new_project;
      return NULL;
    }

  return new_project;
}

gboolean
ProjectDescriptor::init_from_file (File *file, bool is_working)
{
  gboolean result = FALSE;

  _is_working = is_working;
  _root_path = path_dirname (file_access_path (file));

  _populate_ignore          = g_ptr_array_new ();
  _project_keywords         = new KeywordTable;
  _project_keywords_extra   = new OrderedStringTable;
  _file_entries             = new FileEntryPtrArray;
  _deleted_markers          = new ConstListMarkerPtrArray;
  _attrs_table              = new AttrsTable (attrs_hash, attrs_equal);
  _filenames_table          = g_hash_table_new (g_str_hash, g_str_equal);
  _descriptors_table        = g_hash_table_new (g_str_hash, g_str_equal);

  if (is_working)
    {
      _working_paths_table      = g_hash_table_new (g_direct_hash, g_direct_equal);
      _working_dest_paths_table = g_hash_table_new (g_direct_hash, g_direct_equal);

      g_hash_table_insert (_working_paths_table, file_access_path (file), file/*@@@*/);
      g_hash_table_insert (_working_dest_paths_table, file_dest_path (file), file/*@@@*/);
    }

  _prj_source = file;

  if ((result = parse_prj_file ()))
    {
      _deleted_markers->append (& _idata_marker);

      set_project_full_version ();

      /* @@@ check that the project file is not included in the project. */

      if (! mix_idata ())
	return FALSE;

#if 0
      SerialInvokationLogEntry ent;

      ent.opcode = opcode; /* @@@ what is this all about? */
      ent.file_log_low = _idata_files_log->len;
      ent.file_log_high = _idata_files_log->len;
      get_generic_time_of_day (& ent.time);
      g_array_append_val (_idata_invokation_log, ent);
#endif
    }

  return result;
}

void
ProjectDescriptor::set_project_full_version ()
{
  _project_full_version.assign (*project_version_major());
  _project_full_version.append ('.');
  _project_full_version.append (*project_version_minor());

  if (_is_working)
    _project_full_version.append ("(w)");
}

/* format should be a string consisting of 'N', 'L', 'S', and
 * 'A', for user, number, label, string and any.  returns false if a
 * match fails.  arguments following format are the addresses of
 * Dstring*s to fill in. an example is:
 *
 *     sexp_scan(s, "LLL", &d1, &d2, &d3);
 *
 * where dN will get the Nth label of three.
 *
 * if the first character of format is an asterisk, then "-*-" is
 * accepted as a label.  if the first cahracter of format is an 'e',
 * then empty lists are allowed.  */

static bool
sexp_scan (const Sexp* lst, const char* format, ...)
{
  va_list args;
  char c;
  int len = lst->length();
  const char* type = NULL;
  bool accept_ast = false;
  bool accept_empty = false;
  bool loop_done = false;

  while (!loop_done)
    {
      switch (*format)
	{
	case '*':
	  accept_ast = true;
	  format += 1;
	  break;
	case 'e':
	  accept_empty = true;
	  format += 1;
	  break;
	default:
	  loop_done = true;
	  break;
	}
    }

  va_start(args, format);

  if (! lst->is_pair())
    goto error;

  while((c = *format++))
    {
      switch(c)
	{
	case 'N': type = "number"; break;
	case 'S': type = "any"; break;
	case 'L': type = "label"; break;
	case 'A': type = "any"; break;
	default:
	  abort ();
	}

      /* If there are no characters left, and we are expecting one, bail */
      if (len == 0)
	goto error;

      bool okay_empty = false;

      if (lst->car()->is_pair())
	{
	  if (accept_empty && !lst->car())
	    okay_empty = true;
	  else
	    goto error;
	}

      /* @@@ now check the token type */
#if DEAD
      if (!okay_empty && VC_check_token_match(*lst->car()->key(), type) <= 0)
	{
	  if(!(accept_ast && strcmp(*lst->car()->key(), "-*-") == 0))
	    goto error;
	}
#endif

      if (c == 'S' && lst->car()->key()->type())
	goto error;

      Estring** dptr = va_arg (args, Estring**);

      if (dptr)
	{
	  if (okay_empty)
	    (*dptr) = NULL;
	  else
	    (*dptr) = lst->car()->key();
	}

      lst = lst->cdr();
      len -= 1;
    }

  va_end (args);

  return len == 0;

 error:

  va_end (args);

  return false;
}

/* Project read functions
 */

gboolean
ProjectDescriptor::project_ver_prj_entry_func (const Sexp* s)
{
  if (! sexp_scan (s, "LLLN", NULL, &_project_version_name,
		   &_project_version_major, &_project_version_minor ))
    return bad_project_file (s, "badly formed Project-Version entry");

  _project_version_point = new Estring (this, s->end_index());

  return TRUE;
}

gboolean
ProjectDescriptor::parent_ver_prj_entry_func (const Sexp* s)
{
  if (! sexp_scan(s, "*LLLN", NULL, &_parent_version_name,
		  &_parent_version_major, &_parent_version_minor ))
    return bad_project_file (s, "badly formed Parent-Version entry");
  else
    {
      bool major_absent = strcmp(*_parent_version_name, "-*-") == 0;
      bool minor_absent = strcmp(*_parent_version_major, "-*-") == 0;
      bool name_absent = strcmp(*_parent_version_minor, "-*-") == 0;

      if (major_absent ^ minor_absent ||
	  major_absent ^ name_absent ||
	  name_absent ^ minor_absent)
	return bad_project_file (s, "illegal use of -*-");
    }

  return TRUE;
}

gboolean
ProjectDescriptor::project_desc_prj_entry_func(const Sexp* s)
{
  if (! sexp_scan(s, "LS", NULL, &_project_description))
    return bad_project_file (s, "badly formed Project-Description entry");

  return TRUE;
}

gboolean
ProjectDescriptor::version_log_prj_entry_func(const Sexp* s)
{
  if (! sexp_scan(s, "LS", NULL, &_version_log))
    return bad_project_file (s, "badly formed Version-Log entry");

  return TRUE;
}

gboolean
ProjectDescriptor::new_version_log_prj_entry_func(const Sexp* s)
{
  if (! sexp_scan(s, "LS", NULL, &_new_version_log))
    return bad_project_file (s, "badly formed New-Version-Log entry");

  return TRUE;
}

gboolean
ProjectDescriptor::checkin_time_prj_entry_func(const Sexp* s)
{
  if (! sexp_scan(s, "LS", NULL, &_checkin_time))
    return bad_project_file (s, "badly formed Checkin-Time entry");

  return TRUE;
}

gboolean
ProjectDescriptor::checkin_login_prj_entry_func(const Sexp* s)
{
  if (! sexp_scan(s, "LS", NULL, &_checkin_login))
    return bad_project_file (s, "badly formed Checkin-Login entry");

  return TRUE;
}

gboolean
ProjectDescriptor::populate_ignore_prj_entry_func(const Sexp* s)
{
  _populate_ignore_marker = ListMarker (s->start_index(), s->end_index());
  _populate_ignore_point = new Estring (this, s->end_index());

  if (s->length() != 2 || !s->cadr()->is_pair())
    return bad_project_file (s, "malformed Populate-Ignore entry");

  foreach_sexp (t_ptr, s->cadr())
    {
      const Sexp *t = *t_ptr;

      if (t->is_pair() || t->key()->type() != EsStringLiteral)
	return bad_project_file (t, "expected a string");
    }

  foreach_sexp (t_ptr, s->cadr())
    {
      Estring* str = (*t_ptr)->key();

      PopulateIgnoreEntry* ent = new PopulateIgnoreEntry (str);

      g_ptr_array_add (_populate_ignore, ent);
    }

  return TRUE;
}

gboolean
ProjectDescriptor::project_keywords_prj_entry_func (const Sexp* s)
{
  _project_keywords_marker = ListMarker (s->start_index(), s->end_index());
  _project_keywords_point = new Estring (this, s->end_index());

  foreach_sexp (t_ptr, s->cdr())
    {
      const Sexp *t = *t_ptr;

      if (!t->is_pair() ||
	  t->length() != 2 ||
	  t->car()->is_pair() ||
	  t->cadr()->is_pair() ||
	  t->car()->key()->type() != EsNameLiteral)
	return bad_project_file (t, "malformed Project-Keywords entry");

      if (is_builtin_keyword (t->car()->key()->cast(), t->car()->key()->length()))
	return bad_project_file (t, "Project-Keywords may not contain a builtin keyword");

      if (_project_keywords->isdefined (t->car()->key()->cast()))
	return bad_project_file (t, "duplicate keyword");

      if (strchr (t->cadr()->key()->cast(), '\n'))
	return bad_project_file (t, "keyword value may not contain a `$' character");

      _project_keywords->insert (t->car()->key()->cast(), t->cadr()->key()->cast());
      _project_keywords_extra->insert (t->car()->key()->cast(), t->cadr()->key()->cast());
    }

  return TRUE;
}

static ListMarker *finish_me = NULL;

static PrjTokenType
prj_lex (void)
{
  int val = prjlex ();

  if (finish_me)
    {
      finish_me->stop = prj_lex_this_index + (val == PrjEof);
      finish_me = NULL;
    }

  return (PrjTokenType) val;
}

gboolean
ProjectDescriptor::files_prj_entry_read(int /*offset*/)
{
  const Sexp* s;
  DstringPtrArray gtags;
  bool allow_tags = true;

  if (! (s = read_list_elt()))
    return FALSE;

  while (! s->is_empty())
    {
      if (s->is_pair())
	{
	  if (! individual_insert_fileent (s, &gtags))
	    return FALSE;

	  allow_tags = false;
	}
      else if (allow_tags && s->key()->index(0) == ':')
	{
	  gtags.append (s->key());
	}
      else
	{
	  return bad_project_file (s, "unexpected or illegal file attribute");
	}

      delete s;

      if (! (s = read_list_elt ()))
	return FALSE;
    }

  _files_insertion_point = new Estring (this, prj_lex_this_index);

  return TRUE;
}

gboolean
ProjectDescriptor::idata_prj_entry_read(int offset)
{
  const Sexp* s;
  GByteArray* idata_buf = g_byte_array_new (); /* @@@ cleanup */
  SerialSource* idata_source;

  if (! (s = read_list_elt ()))
    return FALSE;

  while (! s->is_empty())
    {
      if (s->is_pair())
	{
	  return bad_project_file (s, "expected a string");
	}

      Estring *line = s->key();

      if (line->type () != EsStringLiteral)
	{
	  return bad_project_file (s, "expected a string of characters, unquoted");
	}

      g_byte_array_append (idata_buf, (const guint8*) line->cast (), line->length ());

      delete s;

      if (! (s = read_list_elt ()))
	return FALSE;
    }

  idata_source = edsio_simple_source (idata_buf->data, idata_buf->len, SBF_Checksum | SBF_Base64);

  if (! unserialize_projectdata (idata_source, & _idata))
    return FALSE;

  if (! idata_source->source_close (idata_source))
    return FALSE;

  idata_source->source_free (idata_source);

  g_byte_array_free (idata_buf, TRUE);

  _idata_marker.start = offset;

  finish_me = & _idata_marker;

  return TRUE;
}

gboolean
ProjectDescriptor::mix_idata ()
{
  /* Connect the two data structures, and validate a few things.
   */
  GHashTable* idesc_table = g_hash_table_new (g_str_hash, g_str_equal);
  GHashTable* all_table = g_hash_table_new (g_str_hash, g_str_equal);

  g_assert (_idata);

  /* @@@ verify immutable prj fields */

  for (guint i = 0; i < _idata->files_len; i += 1)
    {
      SerialFileEntry* wfe = & _idata->files[i];

      switch (wfe->type)
	{
	case FT_Regular:
	case FT_Symlink:
	case FT_Directory: break;
	default: abort ();
	}

      g_assert (wfe->working_name[0]);
      g_assert (wfe->descriptor[0]);

      g_hash_table_insert (idesc_table, (void*) wfe->descriptor, wfe);
      g_hash_table_insert (all_table, (void*) wfe->descriptor, wfe);
    }

  /* @@@ compute & log changes in group attributes */

  foreach_fileent (fe_ptr, this)
    {
      FileEntry *fe = *fe_ptr;

      const char* name = fe->working_name();

      if (fe->descriptor())
	{
	  SerialFileEntry *prev_record = (SerialFileEntry*) g_hash_table_lookup (idesc_table, (void*) fe->descriptor ());

	  if (! prev_record)
	    {
	      prcs_generate_filestringstring_event (EC_PrcsInvalidFileFamily, _prj_source, name, fe->descriptor ());
	      return FALSE;
	    }

	  prev_record->found = TRUE;

	  if (strcmp (name, prev_record->working_name) != 0)
	    idata_log_file_rename (fe, prev_record->working_name);

	  if (fe->type() != prev_record->type)
	    idata_log_file_retype (fe, prev_record->type);

	  /* @@@ compute and log changes in attributes, group membership */

	  for (guint i = 0; i < prev_record->props_len; i += 1)
	    {
	      SerialFileEntryProperty* prop = prev_record->props[i];

	      fe->define_property (prop->name, prop->val, prop->val_len, prop->flags);

	      /*g_print ("file %s: read property with name %s\n", fe->working_name (), prop->name);*/
	    }
	}
      else
	{
	  while (true)
	    {
	      guint8 buf[PRCS_UNIQUE_STRING_BYTES];
	      guint8 base64[PRCS_UNIQUE_STRING_BYTES*2];
	      guint  base64_len = PRCS_UNIQUE_STRING_BYTES*2;

	      RAND_bytes (buf, PRCS_UNIQUE_STRING_BYTES);

	      if (! edsio_base64_encode_region_into (buf, PRCS_UNIQUE_STRING_BYTES, base64, &base64_len))
		abort ();

	      g_assert (base64_len == (PRCS_UNIQUE_STRING_BYTES*4/3));

	      base64[base64_len++] = 0;

	      if (! g_hash_table_lookup (all_table, base64))
		{
		  fe->set_descriptor ((const char*) base64);
		  fe->define_property ("NEW", NULL, 0, SFEF_CheckinProperty);

		  g_hash_table_insert (all_table, (void*) fe->descriptor (), fe);
		  g_hash_table_insert (_descriptors_table, (void*) fe->descriptor (), fe);

		  break;
		}
	    }

	  idata_log_file_add (fe);
	}
    }

  for (guint i = 0; i < _idata->files_len; i += 1)
    {
      if (! _idata->files[i].found)
	{
	  idata_log_file_delete (& _idata->files[i]);
	}

      _idata->files[i].found = FALSE;
    }

  g_hash_table_destroy (idesc_table);
  g_hash_table_destroy (all_table);

  return TRUE;
}

void
ProjectDescriptor::idata_log_file_add (FileEntry* fe)
{
  SerialFileEntryLogAdd *log = g_new0 (SerialFileEntryLogAdd, 1);
  SerialFileEntryLogEntry ent;

  log->working_name = fe->working_name ();
  log->desc = fe->descriptor ();
  log->type = fe->type ();

  ent.entry = log;
  ent.entry_type = ST_FileEntryLogAdd;

  g_array_append_val (_idata_files_log, ent);
}

void
ProjectDescriptor::idata_log_file_rename (FileEntry* fe, const char* old_name)
{
  SerialFileEntryLogRename *log = g_new0 (SerialFileEntryLogRename, 1);
  SerialFileEntryLogEntry ent;

  log->working_to = fe->working_name ();
  log->working_from = old_name;
  log->desc = fe->descriptor ();

  ent.entry = log;
  ent.entry_type = ST_FileEntryLogRename;

  g_array_append_val (_idata_files_log, ent);
}

void
ProjectDescriptor::idata_log_file_retype (FileEntry* fe, int old_type)
{
  SerialFileEntryLogRetype *log = g_new0 (SerialFileEntryLogRetype, 1);
  SerialFileEntryLogEntry ent;

  log->from_type = old_type;
  log->to_type = fe->type ();
  log->desc = fe->descriptor ();

  ent.entry = log;
  ent.entry_type = ST_FileEntryLogRetype;

  g_array_append_val (_idata_files_log, ent);
}

#if DEAD
void
ProjectDescriptor::idata_log_file_regroup (FileEntry* fe, int old_group)
{
}
#endif

void
ProjectDescriptor::idata_log_file_delete (SerialFileEntry* sfe)
{
  SerialFileEntryLogDelete *log = g_new0 (SerialFileEntryLogDelete, 1);
  SerialFileEntryLogEntry ent;

  log->desc = sfe->descriptor;

  ent.entry = log;
  ent.entry_type = ST_FileEntryLogDelete;

  g_array_append_val (_idata_files_log, ent);
}

gboolean
ProjectDescriptor::mix_working_data (ProjectDescriptor* working_copy)
{
  /* can assume both projects have identical file lists, but not
   * identical descriptors, since the server renames all new
   * descriptors to avoid conflicts.  working_copy is the copy the
   * client sent.  THIS is the copy the server returned after checking
   * it in. */

  foreach_fileent (sfe_ptr, this)
    {
      FileEntry* sfe = *sfe_ptr;
      FileEntry* cfe = working_copy->file_entry_by_name (sfe->working_name ());

      g_assert (cfe);

      sfe->transfer_properties_with_mask (cfe, SFEF_WorkingProperty);
    }

  return TRUE;
}

gboolean
ProjectDescriptor::populate_ignore_compile (void)
{
  for (guint i = 0; i < _populate_ignore->len; i += 1)
    {
      PopulateIgnoreEntry *ent = (PopulateIgnoreEntry*) _populate_ignore->pdata[i];

      if (! (ent->_regex = prcs_compile_regex (ent->_str->cast ())))
	return FALSE;
    }

  return TRUE;
}

gboolean
ProjectDescriptor::populate_ignore_matches (const char* str, const char** ignore_exp)
{
  for (guint i = 0; i < _populate_ignore->len; i += 1)
    {
      PopulateIgnoreEntry *ent = (PopulateIgnoreEntry*) _populate_ignore->pdata[i];

      if (prcs_regex_matches (ent->_regex, str, strlen (str)))
	{
	  (*ignore_exp) = ent->_str->cast ();
	  return TRUE;
	}
    }

  return FALSE;
}

void
ProjectDescriptor::append_new_regular_file (const char* working_name)
{
  static bool once = false;

  if (! once)
    {
      _files_insertion_point->append ("\n\n;; Files inserted by populate\n\n");
      once = true;
    }

  _files_insertion_point->append ("  (");
  _files_insertion_point->append_protected (working_name);
  _files_insertion_point->append (" ())\n");
}

FileEntry*
ProjectDescriptor::file_entry_by_family (const char* family)
{
  return (FileEntry*) g_hash_table_lookup (_descriptors_table, family);
}

FileEntry*
ProjectDescriptor::file_entry_by_name (const char* name)
{
  return (FileEntry*) g_hash_table_lookup (_filenames_table, name);
}

void
ProjectDescriptor::update_attributes_for_checkin (const char* new_major, int new_minor)
{
  SerialGenericTime t;

  /* The Versions. */
  if (strcmp (*project_version_minor(), "0") != 0)
    {
      parent_version_major()->assign(*project_version_major());
      parent_version_minor()->assign(*project_version_minor());
    }

  project_version_major()->assign (new_major);
  project_version_minor()->assign_int (new_minor);

  /* the Checkin-Time attribute */
  if (edsio_time_of_day (& t))
    checkin_time()->sprintf ("%s", edsio_time_to_iso8601 (& t));

  /* the Logs */
  version_log()->assign (*new_version_log ());
  new_version_log()->truncate (0);

  /* the Checkin-Login attribute */
  checkin_login()->assign (get_login ());

  set_project_full_version ();
}

/* Read Helpers.
 */

static bool
weird_pathname (const char* N)
{
  /* @@@ also don't allow //, projdesc.cc relies on it, because
   * g_str_{hash,equal} */
    /* don't allow anything outside the repository */
    if (strncmp("../", N, 3) == 0)
        return true;
    else if (strncmp("./", N, 2) == 0)
	return true;

    if (N[0] == '/')
	return true;

    /* don't allow any '/../'s in the path */
    while ((N = strchr(N, '/')) != NULL) {
        if (strncmp(N, "/../", 4) == 0)
            return true;
	if (strncmp(N, "/./", 3) == 0)
	    return true;
        N += 1;
    }

    if (N) {
	int len = strlen(N);

	if (len > 0 && N[len - 1] == '/')
	    return true;
    }

    return false;
}

gboolean
ProjectDescriptor::individual_insert_fileent (const Sexp* file, const DstringPtrArray* gtags)
{
  Path *working_path    = NULL;
  Path *relative_path   = NULL;
  FileEntry *fe;

  const char* working_name;
  const Sexp *descriptor_sexp = NULL;
  ListMarker ent_marker;
  DstringPtrArray tags;
  const PrcsAttrs* attrs;
  Dstring *descriptor = NULL;

  if (! file->is_pair())
    return bad_project_file (file, "expected a list");

  if (file->length() < 2)
    return bad_project_file (file, "file entry too short");

  if (file->car()->is_pair ())
    return bad_project_file (file, "expected a filename");

  if (! file->cadr()->is_pair ())
    return bad_project_file (file, "malformed internal file identifier");

  if (weird_pathname (*file->car()->key()))
    return bad_project_file (file, "illegal pathname");

  working_name  = file->car()->key()->cast();
  relative_path = path_canonicalize_simple (path_root (), '/', working_name);

  working_path  = path_append_path (_root_path, relative_path);

  g_assert (relative_path && working_path);

  if (g_hash_table_lookup (_filenames_table, working_name))
    {
      prcs_generate_fileint_event (EC_PrcsDuplicateFileName, _prj_source, file->line_number ());
      return FALSE;
    }

  ent_marker = file->marker();
  descriptor_sexp = file->cadr();

  if (descriptor_sexp->length () > 1)
    return bad_project_file (file, "malformed internal file descriptor");

  if (descriptor_sexp->length() == 1)
    {
      descriptor = descriptor_sexp->car()->key();

      if (g_hash_table_lookup (_descriptors_table, descriptor->cast ()))
	{
	  prcs_generate_fileint_event (EC_PrcsDuplicateFileFamily, _prj_source, file->line_number ());
	  return FALSE;
	}
    }

  tags.append (*gtags);

  foreach_sexp (tag_ptr, file->cdr()->cdr())
    {
      const Sexp* tag = *tag_ptr;

      if (tag->is_pair() || tag->key()->index(0) != ':')
	return bad_project_file(file, "invalid file attribute");

      tags.append ((*tag_ptr)->key());
    }

  if (! (attrs = intern_attrs (&tags, gtags->length(), working_name, true)))
    return FALSE;

  switch (attrs->type())
    {
    case FT_Directory:
      {
	fe = new DirectoryFileEntry (working_path,
				     working_name,
				     ent_marker,
				     attrs,
				     descriptor);
      }
      break;
    case FT_Symlink:
      {
	fe = new SymlinkFileEntry (working_path,
				   working_name,
				   ent_marker,
				   attrs,
				   descriptor);
      }
      break;
    case FT_Regular:
      {
	fe = new RegularFileEntry (working_path,
				   working_name,
				   ent_marker,
				   attrs,
				   descriptor);

      }
      break;
    default:
      abort ();
    }

  _file_entries->append (fe);

  /* @@@ This was removed, add it here */
  /* This code insures:
   * no filename is a prefix of another unless it has :directory
   */

  g_hash_table_insert (_filenames_table, (void*) fe->working_name (), fe);

  if (_is_working)
    {
      g_hash_table_insert (_working_paths_table, working_path, fe);
      g_hash_table_insert (_working_dest_paths_table, file_dest_path (fe->working_file()), fe);
    }

  if (fe->descriptor ())
    g_hash_table_insert (_descriptors_table, (void*) fe->descriptor (), fe);

  return TRUE;
}

/* Keywords
 */

void
ProjectDescriptor::add_keyword (const char* key, const char* val)
{
  _alter_popkey = true;

  g_assert (! _project_keywords_extra->lookup (key));

  _project_keywords->insert (key, val);
  _project_keywords_extra->insert (key, val);
}

void
ProjectDescriptor::rem_keyword (const char* key)
{
  _alter_popkey = true;

  g_assert (_project_keywords_extra->lookup (key));

  _project_keywords->remove (key);
  _project_keywords_extra->remove (key);
}

void
ProjectDescriptor::set_keyword (const char* key, const char* val)
{
  _alter_popkey = true;

  g_assert (_project_keywords_extra->lookup (key));

  _project_keywords->insert (key, val);
  _project_keywords_extra->insert (key, val);
}

OrderedStringTable*
ProjectDescriptor::project_keywords_extra() const
{
  return _project_keywords_extra;
}

#if DEAD
KeywordTable*
ProjectDescriptor::project_keywords(FileEntry* fe, bool setting)
{
}
#endif

/* File Insertion
 */

#if DEAd
void ProjectDescriptor::append_files_data (const char* data)
{
    _files_insertion_point->append (data);
}

void ProjectDescriptor::append_file (const char* working_path,
				     bool keywords)
{
    _files_insertion_point->append ("\n  (");
    _files_insertion_point->append_protected (working_path);
    _files_insertion_point->append (" ()");

    if (keywords)
	_files_insertion_point->append (" :no-keywords");

    _files_insertion_point->append (")");
}

void ProjectDescriptor::append_link (const char* working_path,
				     const char* link_name)
{
    _files_insertion_point->append ("\n  (");
    _files_insertion_point->append_protected (working_path);
    _files_insertion_point->append (" (");
    _files_insertion_point->append_protected (link_name);
    _files_insertion_point->append (") :symlink)");
}

void ProjectDescriptor::append_directory (const char* working_path)
{
    _files_insertion_point->append ("\n  (");
    _files_insertion_point->append_protected (working_path);
    _files_insertion_point->append (" () :directory)");
}

void ProjectDescriptor::append_file_deletion (FileEntry* fe)
{
    _files_insertion_point->append ("\n  ; (");
    _files_insertion_point->append_protected (fe->working_path());
    _files_insertion_point->append (" ()");

    fe->file_attrs()->print_to_string (_files_insertion_point, true);

    _files_insertion_point->append (")");
}

void ProjectDescriptor::append_file (FileEntry* fe)
{
    _files_insertion_point->append ("\n  (");
    _files_insertion_point->append_protected (fe->working_path());
    if (fe->file_type() == RealFile && fe->descriptor_name()) {
	_files_insertion_point->append (" (");
	_files_insertion_point->append_protected (fe->descriptor_name());
	_files_insertion_point->append (" ");
	_files_insertion_point->append_protected (fe->descriptor_cksum_pretty());
	_files_insertion_point->sprintfa (" %03o)", fe->file_mode());
    } else {
	_files_insertion_point->append (" ()");
    }

    fe->file_attrs()->print_to_string (_files_insertion_point, true);

    _files_insertion_point->append (")");
}

void
ProjectDescriptor::delete_file (FileEntry* fe)
{
  _deleted_markers->append(fe->ent_marker());
}
#endif

/* Attributes */

PrcsAttrs::PrcsAttrs (const DstringPtrArray *vals0, int ngroup)
  :_vals(*vals0),
   _ngroup(ngroup),
   _type(FT_Regular),
   _project(NULL),
   _keyword_sub(true)
{
#if DEAD
    memcpy (_merge_actions, default_merge_actions, sizeof (_merge_actions));
    memcpy (_merge_descs,   default_merge_descs  , sizeof (_merge_descs));

    const char* env;

    env = get_environ_var ("PRCS_MERGE_COMMAND");

    if (env)
	_mergetool.assign (env);

    env = get_environ_var ("PRCS_DIFF_COMMAND");

    if (env)
	_difftool.assign (env);
#endif
}

FileType           PrcsAttrs::type() const { return _type; }
bool               PrcsAttrs::keyword_sub() const { return _keyword_sub; }
ProjectDescriptor* PrcsAttrs::project() const { return _project; };
#if DEAD
MergeAction        PrcsAttrs::merge_action (int rule) const { return _merge_actions[rule]; };
const char*        PrcsAttrs::merge_desc (int rule) const { return _merge_descs[rule]; };
const char*        PrcsAttrs::merge_tool () const { return _mergetool.length() > 0 ? _mergetool.cast() : NULL; }
#endif
const char*        PrcsAttrs::diff_tool () const { return _difftool.length() > 0 ? _difftool.cast() : NULL; }

void
PrcsAttrs::print_to_string (Dstring* str, bool lead_if) const
{
  if (lead_if && _vals.length() > _ngroup)
    str->append (' ');

  for (int i = _ngroup; i < _vals.length(); i += 1)
    {
      str->append (_vals.index(i)->cast());

      if (i < (_vals.length() - 1))
	str->append (' ');
    }
}

gboolean
PrcsAttrs::print (FileHandle *handle, bool lead_if) const
{
  if (lead_if && _vals.length() > _ngroup)
    {
      if (! handle_putc (handle, ' '))
	return FALSE;
    }

  for (int i = _ngroup; i < _vals.length(); i += 1)
    {
      if (! handle_write (handle, (const guint8*) _vals.index(i)->cast (), _vals.index(i)->length ()))
	return FALSE;

      if (i < (_vals.length() - 1))
	{
	  if (! handle_putc (handle, ' '))
	    return FALSE;
	}
    }

  return TRUE;
}

#if DEAD
bool
PrcsAttrs::regex_matches (reg2ex2_t *re) const
{
  foreach (ds_ptr, &_vals, DstringPtrArray::ArrayIterator)
    {
      if (prcs_regex_matches ((*ds_ptr)->cast(), re))
	return true;
    }

  return false;
}
#endif

gboolean
ProjectDescriptor::validate_attrs (const PrcsAttrs */*attrs*/, const char* /*name*/)
{
#if 0
  bool seen_type = false;

  foreach (ds_ptr, &attrs->_vals, DstringPtrArray::ArrayIterator)
    {
      Dstring an = (*ds_ptr)->cast();
      Dstring av;

      const char* equal = strchr (an.cast(), '=');

      if (equal)
	{
	  av.assign (equal + 1);
	  an.truncate (equal - an.cast());
	}

      const AttrDesc* ad = is_file_attribute (an.cast(), an.length());

      if (!ad)
	throw prcserror  << "Unrecognized attribute " << an << " for file " << squote(name) << dotendl;

      switch (ad->type)
	{
	case RealFileAttr:
	case DirectoryAttr:
	case SymlinkAttr:
	  if (seen_type)
	    throw prcserror << "Duplicate file type attribute for file " << squote(name) << dotendl;
	  seen_type = true;
	  break;
	case Mergerule1Attr:
	case Mergerule2Attr:
	case Mergerule3Attr:
	case Mergerule4Attr:
	case Mergerule5Attr:
	case Mergerule6Attr:
	case Mergerule7Attr:
	case Mergerule8Attr:
	case Mergerule9Attr:
	case Mergerule10Attr:
	case Mergerule11Attr:
	case Mergerule12Attr:
	case Mergerule13Attr:
	case Mergerule14Attr:
	  if (av.length() != 1 || strchr("andrm", av.index(0)) == NULL)
	    throw prcserror << "Illegal merge action " << av << dotendl;
	  break;
	case MergetoolAttr:
	case DifftoolAttr:
	  if (av.length() < 1)
	    throw prcserror << "Illegal tool name " << av << dotendl;
	default:
	  break;
	}
    }
#endif
  return TRUE;
}

void
ProjectDescriptor::init_attrs (PrcsAttrs *attrs)
{
  attrs->_project = this;

  foreach (ds_ptr, &attrs->_vals, DstringPtrArray::ArrayIterator)
    {
      Dstring an = (*ds_ptr)->cast();
      Dstring av;

      const char* equal = strchr (an.cast(), '=');

      if (equal)
	{
	  av.assign (equal + 1);
	  an.truncate (equal - an.cast());
	}

      const AttrDesc* ad = is_file_attribute (an.cast(), an.length());
      int mergerule = 0;

      switch (ad->type)
	{
	case RealFileAttr:
	case ProjectFileAttr:
	  attrs->_type = FT_Regular;
	  break;
	case DirectoryAttr:
	case ImplicitDirectoryAttr:
	  attrs->_type = FT_Directory;
	  break;
	case SymlinkAttr:
	  attrs->_type = FT_Symlink;
	  break;
	case NoKeywordAttr:
	  attrs->_keyword_sub = false;
	  break;
	case MergetoolAttr:
#if DEAD
	  attrs->_mergetool.assign (av);
#endif
	  break;
	case DifftoolAttr:
	  attrs->_difftool.assign (av);
	  break;
	case TagAttr:
	  break;
	case Mergerule14Attr: mergerule += 1;
	case Mergerule13Attr: mergerule += 1;
	case Mergerule12Attr: mergerule += 1;
	case Mergerule11Attr: mergerule += 1;
	case Mergerule10Attr: mergerule += 1;
	case Mergerule9Attr: mergerule += 1;
	case Mergerule8Attr: mergerule += 1;
	case Mergerule7Attr: mergerule += 1;
	case Mergerule6Attr: mergerule += 1;
	case Mergerule5Attr: mergerule += 1;
	case Mergerule4Attr: mergerule += 1;
	case Mergerule3Attr: mergerule += 1;
	case Mergerule2Attr: mergerule += 1;
	case Mergerule1Attr: /* notice zero origin */
#if DEAD
	  attrs->_merge_descs[mergerule] = "User supplied action, no help available";
	  attrs->_merge_actions[mergerule] = (MergeAction)av.index(0);
#endif
	  break;
	}
    }
}

PrcsAttrs*
ProjectDescriptor::intern_attrs (const DstringPtrArray* tagvals,
				 int ngroup,
				 const char* name,
				 bool validate)
{
  PrcsAttrs *it = new PrcsAttrs (tagvals, ngroup);
  PrcsAttrs **lu;

  lu = _attrs_table->lookup (it);

  if (lu)
    {
      delete it;
      return *lu;
    }
  else
    {
      if (validate && ! (validate_attrs (it, name)))
	return NULL;

      init_attrs (it);

      _attrs_table->insert (it, it);

      return it;
    }
}

bool
attrs_equal(const PrcsAttrs*const& x, const PrcsAttrs*const& y)
{
  if (x->_ngroup != y->_ngroup || x->_vals.length() != y->_vals.length())
    return false;

  for (int i = 0; i < x->_vals.length(); i += 1)
    {
      if (strcmp (x->_vals.index(i)->cast(), y->_vals.index(i)->cast()) != 0)
	return false;
    }

  return true;
}

extern int hash(const char *const& s, int M);

int
attrs_hash(const PrcsAttrs*const & s, int M)
{
  int h = 0;
  const char* p;

  for (int i = 0; i < s->_vals.length(); i += 1)
    {
      p = s->_vals.index(i)->cast();
      h += hash (p, M);
    }

  return h % M;
}

/* Updates and Deletions.
 */

gboolean
ProjectDescriptor::write_project_file (FileSegment* seg, int fe_prop_mask)
{
  project_version_name()->assign(project_id()->alias);

  if (strcmp(parent_version_minor()->cast(), "-*-") != 0)
    parent_version_name()->assign(*project_version_name());

#if DEAD
  if (_alter_popkey)
    {
      _deleted_markers->append (& _populate_ignore_marker);
      _deleted_markers->append (& _project_keywords_marker);

      int first = 0;

      _project_keywords_point->append ("(Project-Keywords");
      _populate_ignore_point->append  ("(Populate-Ignore (");

      int ks = _project_keywords_extra->key_array()->length();

      for (int i = 0; i < ks; i += 1)
	{
	  int nspaces = (first * strlen ("(Project-Keywords")) + 1;

	  for (int j = 0; j < nspaces; j += 1)
	    _project_keywords_point->append (" ");

	  _project_keywords_point->append ("(");
	  _project_keywords_point->append_protected (_project_keywords_extra->key_array()->index(i));
	  _project_keywords_point->append (" ");
	  _project_keywords_point->append_string (_project_keywords_extra->data_array()->index(i));
	  _project_keywords_point->append (")");

	  if (i < (ks - 1))
	    _project_keywords_point->append ("\n");

	  first = 1;
	}

      first = 0;

      ks = _populate_ignore->key_array()->length();

      for (int i = 0; i < ks; i += 1)
	{
	  int nspaces = (first * strlen ("(PopulateIgnore ( "));

	  for (int j = 0; j < nspaces; j += 1)
	    _populate_ignore_point->append (" ");

	  _populate_ignore_point->append_string (_populate_ignore->key_array()->index(i));

	  if (i < (ks - 1))
	    _populate_ignore_point->append ("\n");

	  first = 1;
	}

      _project_keywords_point->append (")");
      _populate_ignore_point->append ("))");
    }
#endif

  FileHandle* handle;

  if (! (handle = segment_open (seg, HV_Replace)))
    return FALSE;

  if (! really_write_project_file (handle))
    return FALSE;

  if (! append_project_idata (handle, fe_prop_mask))
    return FALSE;

  if (! handle_close (handle))
    return FALSE;

  return TRUE;
}

static int
sort_marker (const void* a, const void* b)
{
  return (*((const ListMarker**)a ))->start - (*((const ListMarker**)b ))->start;
}

gboolean
ProjectDescriptor::really_write_project_file (FileHandle* handle)
{
  size_t buffer_index   = 0;
  size_t buffer_length  = _segment_length;

  int file_number    = 0;
  int marker_number  = 0;
  int estring_number = 0;

  int file_number_max = _file_entries->length();
  int marker_number_max = 0;
  int estring_number_max = _all_estrings->length();

  size_t next_file_index;
  size_t next_marker_index;
  size_t next_estring_index;
  size_t next_index;

  if (_deleted_markers)
    {
      marker_number_max = _deleted_markers->length();

      /* File entries and Estrings are in sorted order, they are read that way. */
      /* Markers must be sorted. */
      _deleted_markers->sort(sort_marker);
    }

  while (buffer_index <= buffer_length)
    {
      /* markers are never skipped, so get the next index. */
      if (marker_number < marker_number_max)
	next_marker_index = _deleted_markers->index(marker_number)->start;
      else
	next_marker_index = buffer_length + 1;

      /* estrings and files may be skipped by markers, so if the index
       * found is less than the buffer_index, then its been skipped and
       * we incr their number until one ahead of the point is found. */
      do
	{
	  if (file_number < file_number_max)
	    next_file_index = _file_entries->index(file_number)->ent_marker()->start;
	  else
	    next_file_index = buffer_length + 1;
	}
      while (next_file_index < buffer_index && (file_number += 1, true));

      do
	{
	  if (estring_number < estring_number_max)
	    next_estring_index = ((Estring*)_all_estrings->index(estring_number))->offset();
	  else
	    next_estring_index = buffer_length + 1;
	}
      while (next_estring_index < buffer_index && (estring_number += 1, true));

      next_index = next_estring_index < next_file_index   ? next_estring_index : next_file_index;
      next_index = next_index         < next_marker_index ? next_index         : next_marker_index;

      g_assert (buffer_index <= next_index);

      if (next_index > buffer_length)
	break;

      if (buffer_index < next_index)
	{
	  int len = next_index - buffer_index;
	  guint8* buf = g_new (guint8, len); /* @@@ alloca */

	  if (handle_seek (_prj_source_backing, buffer_index, HANDLE_SEEK_SET) < 0)
	    return FALSE;

	  if (handle_read (_prj_source_backing, buf, len) != len)
	    return FALSE;

	  if (! handle_write (handle, buf, len))
	    return FALSE;

	  buffer_index = next_index;

	  g_free (buf);
	}
      else if (next_index == next_marker_index)
	{
	  const ListMarker* marker = _deleted_markers->index(marker_number++);

	  buffer_index = marker->stop;
	}
      else if (next_index == next_estring_index)
	{
	  Estring* str = (Estring*) _all_estrings->index (estring_number++);

	  g_assert (buffer_index == str->offset());

	  switch (str->type()) {
	  case EsStringLiteral:

	    if (! write_syntax_string (str->cast(), handle))
	      return FALSE;

	    buffer_index += str->offset_length();

	    break;
	  case EsNameLiteral:
	    if (! write_syntax_label (str->cast(), handle))
	      return FALSE;

	    buffer_index += str->offset_length();

	    break;
	  case EsUnProtected:
	    if (! handle_write (handle, (const guint8*) str->cast(), str->length()))
	      return FALSE;

	    break;
	  }

	}
      else /* (next_index == next_file_index) */
	{
	  FileEntry *fe = _file_entries->index (file_number++);

	  Dstring entry ("(");

	  append_syntax_label (fe->working_name(), &entry);

	  entry.append (" (");

	  if (fe->descriptor ())
	    append_syntax_label (fe->descriptor(), &entry);

	  entry.append (")");

	  fe->attrs()->print_to_string (&entry, true);

	  entry.append (")");

	  if (! handle_write (handle, (const guint8*) entry.cast(), entry.length()))
	    return FALSE;

	  buffer_index = fe->ent_marker()->stop;
	}
    }

  return TRUE;
}

gboolean
ProjectDescriptor::append_project_idata (FileHandle* handle, int fe_prop_mask)
{
  GPtrArray* free_these = g_ptr_array_new ();

  g_array_set_size (_idata_files, 0);

  foreach_fileent (fe_ptr, this)
    {
      FileEntry* fe = *fe_ptr;
      SerialFileEntry sfe;

      sfe.working_name = fe->working_name ();
      sfe.descriptor   = fe->descriptor ();
      sfe.type         = fe->type ();
      sfe.flags        = 0;

      GPtrArray* props = fe->all_properties (fe_prop_mask);

      g_ptr_array_add (free_these, props);

      sfe.props        = (SerialFileEntryProperty**) props->pdata;
      sfe.props_len    = props->len;

      g_array_append_val (_idata_files, sfe);
    }

  _idata->files          = (SerialFileEntry*)          _idata_files->data;
  _idata->files_log      = (SerialFileEntryLogEntry*)  _idata_files_log->data;
  _idata->invokation_log = (SerialInvokationLogEntry*) _idata_invokation_log->data;

  _idata->files_len          = _idata_files->len;
  _idata->files_log_len      = _idata_files_log->len;
  _idata->invokation_log_len = _idata_invokation_log->len;

  if (! (append_project_data_to_handle (_idata, handle)))
    return FALSE;

  for (guint i = 0; i < free_these->len; i += 1)
    g_ptr_array_free ((GPtrArray*) free_these->pdata[i], TRUE);

  g_ptr_array_free (free_these, TRUE);

  return TRUE;
}

/* Estring
 */

Estring::Estring (ProjectDescriptor* project, const char* s, size_t off, size_t len, EstringType type)
  :Dstring (s, len), _offset(off), _len (len), _type(type)
{
  project->register_estring (this);
}

Estring::Estring (ProjectDescriptor* project, size_t position)
  :Dstring(), _offset(position), _len (0), _type(EsUnProtected)
{
  project->register_estring (this);
}

void
Estring::modify()
{
  char* oldvec = _vec;

  if (_alloc > 0 || _filled == 0)
    return;

  _alloc = 1;

  while (_alloc < _filled + 1)
    _alloc <<= 1;

  _vec = g_new0 (char, _alloc);

  strcpy (_vec, oldvec);
}

void
Estring::append_protected (const char* string)
{
  append_syntax_label (string, this);
}

void
Estring::append_string (const char* string)
{
  append ("\"");
  append_syntax_string (string, this);
  append ("\"");
}

void
ProjectDescriptor::register_estring(Estring* estring)
{
  if (!_all_estrings)
    _all_estrings = new DstringPtrArray;

  _all_estrings->append (estring);
}

bool        Estring::is_dstring() { return false; }
EstringType Estring::type() const { return _type; };
size_t      Estring::offset() const { return _offset; }
size_t      Estring::offset_length() const { return _len; }

/* Parser
 */

extern "C" int
prjinput (void* buf, int max_size)
{
  if (_prj_handle)
    return handle_read (_prj_handle, (guint8*) buf, max_size);
  else
    return 0;
}

gboolean
ProjectDescriptor::parse_prj_file (void)
{
  PrjTokenType tok;

  while (prj_lex () != PrjEof) { }

  prj_lineno = 1;
  prj_lex_cur_index = 0;
  prj_lex_this_index = 0;

  _prj_source_backing = _prj_handle = file_open (_prj_source, HV_Read);

  if (! _prj_handle)
    return FALSE;

  memset (_prj_entry_found, 0, sizeof(_prj_entry_found));

  while (true)
    {
      tok = prj_lex();

      switch (tok)
	{
	case PrjNull:
	case PrjBadString:
	case PrjString:
	case PrjName:
	case PrjClose:
	  {
	    read_parse_bad_token (tok);
	    return FALSE;
	  }

	case PrjOpen:
	  if (! read_list_checked (prj_lex_this_index))
	    return FALSE;

	  break;
	case PrjEof:

	  _segment_length = handle_length (_prj_handle);

	  for (int i = 0; i < _prj_entries; i += 1)
	    if (_pftable[i].must_have && !_prj_entry_found[i])
	      {
		prcs_generate_filestring_event (EC_PrcsMissingProjectFileEntry, _prj_source, _pftable[i].name);
		return FALSE;
	      }

	  _end_of_buffer_point = new Estring (this, prj_lex_cur_index);

	  return TRUE;
	}
    }
}

const Sexp*
ProjectDescriptor::read_list_elt()
{
  PrjTokenType tok = prj_lex();

  switch (tok)
    {
    case PrjEof:
    case PrjNull:
    case PrjBadString:
      {
	read_parse_bad_token (tok);
	return NULL;
      }
    case PrjString:
    case PrjName:
      return read_new_estring(tok);
    case PrjClose:
      return new Sexp (prj_lineno);
    case PrjOpen:
      break;
    }
  return read_list(prj_lex_this_index);
}

const Sexp*
ProjectDescriptor::read_new_estring(PrjTokenType type)
{
  int start_index = prj_lex_this_index + (type == PrjString);
  int stop_index = prj_lex_cur_index - (type == PrjString);

  Estring *estring = new Estring (this,
				  prjtext + (type == PrjString),
				  start_index,
				  stop_index - start_index,
				  type == PrjName ? EsNameLiteral : EsStringLiteral);

  if (strchr (estring->cast(), '\\'))
    /* All names/strings are deslashified when read. */
    remove_syntax_from_string (estring); /* this is destructive. */

  return new Sexp (estring, prj_lineno);
}

void
ProjectDescriptor::read_parse_bad_token(PrjTokenType tok)
{
  switch (tok)
    {
    case PrjEof:
      prcs_generate_file_event (EC_PrcsUnexpectedEof, _prj_source);
      break;
    case PrjNull:
      prcs_generate_fileint_event (EC_PrcsIllegalNull, _prj_source, prj_lineno);
      break;
    case PrjBadString:
      prcs_generate_fileint_event (EC_PrcsUnterminatedString, _prj_source, prj_lineno);
      break;
    case PrjString:
    case PrjName:
      prcs_generate_fileint_event (EC_PrcsUnexpectedToken, _prj_source, prj_lineno);
      break;
    case PrjClose:
      prcs_generate_fileint_event (EC_PrcsUnexpectedClose, _prj_source, prj_lineno);
      break;
    case PrjOpen:
      prcs_generate_fileint_event (EC_PrcsUnexpectedOpen, _prj_source, prj_lineno);
      break;
    }
}

const Sexp*
ProjectDescriptor::read_list_token(PrjTokenType tok, int start_index)
{
  const Sexp *s1, *s2;
  Sexp *s;
  int start_line = prj_lineno;

  switch (tok)
    {
    case PrjEof: case PrjNull: case PrjBadString:
      {
	read_parse_bad_token (tok);
	return NULL;
      }

    case PrjString:
    case PrjName:
      if (! (s2 = read_new_estring (tok)))
	return NULL;

      if (! (s1 = read_list(start_index)))
	return NULL;

      s = new Sexp(s2, s1, prj_lineno);

      s->set_start (start_index);
      s->set_end (prj_lex_cur_index);

      return s;

    case PrjClose:
      s = new Sexp (prj_lineno);

      s->set_start (start_index);
      s->set_end (prj_lex_cur_index);

      return s;

    case PrjOpen:
      break;
    }

  if (! (s1 = read_list(prj_lex_this_index)))
    return NULL;

  if (! (s2 = read_list(start_index)))
    return NULL;

  s = new Sexp(s1, s2, start_line);

  s->set_start (start_index);
  s->set_end (prj_lex_cur_index);

  return s;
}

const Sexp*
ProjectDescriptor::read_list(int offset)
{
  return read_list_token (prj_lex(), offset);
}

gboolean
ProjectDescriptor::read_list_checked(size_t offset)
{
  const PrjFields* pf;
  PrjTokenType tok;
  const Sexp* s;
  int h;

  tok = prj_lex();

  if (tok != PrjName)
    {
      prcs_generate_fileint_event (EC_PrcsExpectedLabel, _prj_source, prj_lineno);
      return FALSE;
    }

  pf = prj_lookup_func(prj_lex_val, prj_lex_len);
  h  = prj_lookup_hash(prj_lex_val, prj_lex_len);

  if (! pf)
    {
      prcs_generate_fileintstring_event (EC_PrcsUnrecognizedLabel, _prj_source, prj_lineno, prj_lex_val);
      return FALSE;
    }

  if (_prj_entry_found[h] && h != prj_lookup_hash ("Files", strlen ("Files")))
    {
      prcs_generate_fileintstring_event (EC_PrcsDuplicateLabel, _prj_source, prj_lineno, prj_lex_val);
      return FALSE;
    }

  _prj_entry_found[h] = true;

  if (pf->parse_func)
    {
      if (! (s = read_list_token (tok, offset)))
	return FALSE;

      /* I finally found a use for the ->* operator, fear it. */
      if (! ( (this ->* (pf->parse_func)) (s) ))
	return FALSE;

      delete s;
    }
  else
    {
      if (! ((this ->* (pf->read_func)) (offset)))
	return FALSE;
    }

  return TRUE;
}
