/* -*-Mode: C++;-*-
 * $Id: servcheckin.cc 1.12 Sat, 03 Apr 1999 20:12:02 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "prcs.h"
#include "prcs2.h"
#include "projdesc.h"
#include "fileent.h"
#include "servside.h"

#define CONN_DATA(conn) ((CheckinData *) conn_data (conn, CheckinData))

typedef struct {
  GlobalNameSpace        *branches_space;
  GlobalNameSpace        *families_space;
  ControlledNameSpace    *branch_space;
  SerialPrcsConnect      *client;
  SerialPrcsReply         reply;
  SerialVersionIdentifier version;
  gint                    checkin_file_pos;
} CheckinData;

static ReponetServerReply servside_checkin_start       (Connection* conn, void* client_data);

static TaskControl servside_checkin_project_file       (Connection *conn, Task* task);
static TaskControl servside_checkin_project_analyze    (Connection *conn, Task* task);
static TaskControl servside_checkin_project_reply      (Connection *conn, Task* task);
static TaskControl servside_checkin_project_send       (Connection *conn, Task* task);
static TaskControl servside_checkin_files_recv_request (Connection *conn, Task* task);
static TaskControl servside_checkin_files_recv_one     (Connection* conn, Task* task, ReadyFunc ready, void* data);
static TaskControl servside_checkin_files_recv_one_a   (Connection* conn, Task* task);

static void servside_checkin_success (Connection* conn, TaskManager* tm);
static void servside_checkin_failure (TaskManager* tm);

static CheckinReplyCode servside_resolve_client_checkin     (CheckinData* data);

static Transmition servside_checkin_transmition[] =
{
  { NULL,                                  & servside_checkin_project_file, ProtocolAsync },
  { & servside_checkin_project_analyze,    NULL,                            ProtocolBarrier },
  { & servside_checkin_project_reply,      NULL,                            ProtocolAsync },
  { & servside_checkin_project_send,       NULL,                            ProtocolAsync },
  { & servside_checkin_files_recv_request, task_consume_ready,              ProtocolAsync }
};

Request prcs_server_checkin_request = {
  "Server Checkin",
  PRCS_PROTOCOL_CHECKIN_REQUEST,
  STATIC_ARRAY (servside_checkin_transmition),
  ST_PrcsConnect,
  { & servside_checkin_start },
  & servside_checkin_success,
  & servside_checkin_failure
};

CheckinReplyCode
servside_resolve_client_checkin (CheckinData* data)
{
  if (! (data->branches_space = servside_prcs_branches_namespace (& data->client->project)))
    return CR_NoSuchProject;

  if (! (data->families_space = servside_prcs_families_namespace (& data->client->project)))
    return CR_NoSuchProject;

  if (! (data->branch_space = server_controlled_namespace_attach (data->branches_space,
								  & data->client->version.major,
								  NSAF_AllowWildCard)))
    return CR_NoSuchBranch;

  data->version.major = data->client->version.major;
  data->version.minor_int = server_controlled_namespace_peek (data->branch_space);
  data->version.minor = g_strdup_printf ("%d", data->version.minor_int);

  data->client->version = data->version;
  data->reply.version = data->version;

  return CR_Continue;
}

ReponetServerReply
servside_checkin_start (Connection* conn, void* client_data)
{
  CheckinData* data = CONN_DATA (conn);

  data->client = (SerialPrcsConnect*) client_data;

  data->reply.code = servside_resolve_client_checkin (data);

  return reponet_server_reply (conn, & data->reply, ST_PrcsReply);
}

static FileSegment* checkin_prj_file;
static File* checkin_prj_recv;

TaskControl
servside_checkin_project_file (Connection *conn, Task* task)
{
  checkin_prj_recv = file_initialize_temp (cp_repo);

  return task_segments_get (conn,
			    task,
			    & protocol_next,
			    file_default_segment (checkin_prj_recv),
			    0 /* flags1 */,
			    NULL,
			    0,
			    NULL,
			    0);
}

static ProjectDescriptor* checkin_project;

TaskControl
servside_checkin_project_analyze (Connection *conn, Task* task)
{
  CheckinData* data = CONN_DATA(conn);

  if (! (checkin_project = read_project_file (checkin_prj_recv, FALSE)))
    return reponet_kill_task (task);

  GlobalNameSpace *families_space = CONN_DATA(conn)->families_space;

  foreach_fileent (fe_ptr, checkin_project)
    {
      FileEntry* fe = *fe_ptr;

      if (fe->lookup_property ("NEW", NULL, NULL, NULL))
	{
	  AbsoluteNameSpace* ans = server_absolute_namespace_create (families_space);

	  /* @@@ important: must also verify that the created id is not
	   * one of the irregular descriptors used in the project file?
	   * actually... they should be distinguished by name somehow,
	   * so that its unneccesary.  (it won't work, otherwise).
	   */

	  fe->rename_descriptor (server_namespace_id ((NameSpace*) ans)->id);
	}
    }

  /* @@@ free checkin_prj_recv */

  checkin_prj_file = server_controlled_namespace_next (data->branch_space);

  if (! checkin_project->write_project_file (checkin_prj_file, SFEF_TypeProperty))
    return reponet_kill_task (task);

  return protocol_next (conn, task);
}

TaskControl
servside_checkin_project_reply (Connection *conn, Task* task)
{
  return task_finish_put (conn,
			  task,
			  protocol_next,
			  serialize_prcscheckinprojectreply (conn_sink (conn), 42 /* @@@ reserved for later */));
}

TaskControl
servside_checkin_project_send (Connection *conn, Task* task)
{
  return task_segments_put (conn,
			    task,
			    protocol_next,
			    checkin_prj_file,
			    NULL,
			    NULL);
}

typedef struct {
  AbsoluteNameSpace *family_space;
  const guint8* md5;
  FileSegment* seg;
  FileEntry* fe;
} RecvFile;

TaskControl
servside_checkin_files_recv_request (Connection *conn, Task* task)
{
  /* Here we send requests for the files we don't already have. */
  while (CONN_DATA(conn)->checkin_file_pos < checkin_project->file_entries()->length())
    {
      FileEntry* fe = checkin_project->file_entries()->index (CONN_DATA(conn)->checkin_file_pos++);

      if (fe->type() != FT_Regular)
	continue;

      GlobalNameSpace   *families_space = CONN_DATA(conn)->families_space;
      RecvFile* rf = g_new (RecvFile, 1);

      rf->fe = fe;

      if (! (rf->family_space = server_absolute_namespace_attach (families_space, fe->descriptor (), NSAF_None)))
	return reponet_kill_task (task);

      guint md5_len, md5_flags;

      if (! fe->lookup_property ("MD5", & rf->md5, & md5_len, & md5_flags))
	{
	  prcs_generate_stringstring_event (EC_PrcsMissingProperty, fe->working_name (), "MD5");
	  return reponet_kill_task (task);
	}

      g_assert (md5_len == 16);

      if (! (rf->seg = server_absolute_namespace_find (rf->family_space, rf->md5)))
	return reponet_kill_task (task);

      if (segment_is_type_noerr (rf->seg, SV_Regular))
	continue;

      task_produce (conn, task, servside_checkin_files_recv_one, rf);

      return task_finish_put (conn,
			      task,
			      servside_checkin_files_recv_request,
			      serialize_prcsonefile (conn_sink (conn), fe->descriptor (), rf->md5));
    }

  task_produce_done (conn, task);

  return task_finish_put (conn,
			  task,
			  protocol_next,
			  serialize_prcsdone (conn_sink (conn)));
}

typedef struct {
  ReadyFunc ready;
  RecvFile *rf;
} ReadyData;

TaskControl
servside_checkin_files_recv_one (Connection* conn, Task* task, ReadyFunc ready, void* data)
{
  RecvFile* rf = (RecvFile*) data;

  ReadyData *rd = task_stack_push (task, ReadyData);

  rd->ready = ready;
  rd->rf = rf;

  return task_segments_get (conn,
			    task,
			    servside_checkin_files_recv_one_a,
			    rf->seg,
			    HV_ComputeMD5,
			    NULL,
			    0,
			    NULL,
			    0);
}

TaskControl
servside_checkin_files_recv_one_a (Connection* conn, Task* task)
{
  ReadyData *data = task_stack_pop (task, ReadyData);

  g_free (data->rf);

  return data->ready (conn, task);
}

void servside_checkin_success (Connection* /*conn*/, TaskManager* tm)
{
  if (! repository_commit (cp_repo))
    reponet_exit_event_loop (tm);
}

void servside_checkin_failure (TaskManager* /*tm*/) { }
