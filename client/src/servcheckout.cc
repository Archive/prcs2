/* -*-Mode: C++;-*-
 * $Id: servcheckout.cc 1.12 Sat, 03 Apr 1999 20:12:02 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "prcs.h"
#include "prcs2.h"
#include "servside.h"

#define CONN_DATA(conn) ((CheckoutData *) conn_data (conn, CheckoutData))

typedef struct {
  GlobalNameSpace        *branches_space;
  GlobalNameSpace        *families_space;
  ControlledNameSpace    *branch_space;
  SerialPrcsConnect      *client;
  SerialPrcsReply reply;
} CheckoutData;

static ReponetServerReply servside_checkout_start (Connection* conn, void* client_data);

static TaskControl servside_checkout_project_file (Connection* conn, Task* task);

static TaskControl servside_checkout_file_request (Connection* conn, Task* task);
static TaskControl servside_checkout_file_respond (Connection* conn, Task* task);
static TaskControl servside_checkout_file_send    (Connection* conn, Task* task, ReadyFunc ready, void* vdata);

static void servside_checkout_success (Connection* conn, TaskManager* tm);
static void servside_checkout_failure (TaskManager* tm);

static gint        servside_compute_checkout_version (CheckoutData* data);

static Transmition servside_checkout_transmition[] =
{
  { servside_checkout_project_file, NULL,                           ProtocolAsync },
  { task_consume_ready,             servside_checkout_file_request, ProtocolAsync }
};

Request prcs_server_checkout_request = {
  "Server Checkout",
  PRCS_PROTOCOL_CHECKOUT_REQUEST,
  STATIC_ARRAY (servside_checkout_transmition),
  ST_PrcsConnect,
  { & servside_checkout_start },
  & servside_checkout_success,
  & servside_checkout_failure
};

gint
servside_compute_checkout_version (CheckoutData* data)
{
  if (! (data->branches_space = servside_prcs_branches_namespace (& data->client->project)))
    return CT_NoSuchProject;

  if (! (data->families_space = servside_prcs_families_namespace (& data->client->project)))
    return CT_NoSuchProject;

  if (! (data->branch_space = server_controlled_namespace_attach (data->branches_space,
								  & data->client->version.major,
								  NSAF_AllowWildCard)))
    return CT_NoSuchBranch;

  if (! server_namespace_expand ((NameSpace*) data->branch_space, & data->client->version.minor))
    return CT_NoSuchVersion;

  if (! strtoui_checked (data->client->version.minor, & data->client->version.minor_int, "Invalid minor version"))
    return CT_NoSuchVersion;

  data->reply.version = data->client->version;

  return CT_Continue;
}

ReponetServerReply
servside_checkout_start (Connection* conn, void* client_data)
{
  CheckoutData* data = CONN_DATA (conn);

  data->client = (SerialPrcsConnect*) client_data;

  data->reply.code = servside_compute_checkout_version (data);

  return reponet_server_reply (conn, & data->reply, ST_PrcsReply);
}

TaskControl
servside_checkout_project_file (Connection* conn, Task* task)
{
  if (CONN_DATA (conn)->reply.code != CT_Continue)
    return reponet_done_task (task);

  CheckoutData* data = CONN_DATA(conn);
  FileSegment* prj;

  if (data->client->version.minor_int == 0)
    {
      const char *login = reponet_get_conn_common_name (conn);

      if (! (prj = servside_create_empty_project_file (& data->client->project,
						       server_namespace_id ((NameSpace*) data->branch_space),
						       login)))
	return reponet_kill_task (task);
    }
  else
    {
      if (! (prj = server_controlled_namespace_find (data->branch_space, data->client->version.minor_int)))
	return reponet_kill_task (task);
    }

  return task_segments_put (conn,
			    task,
			    protocol_next,
			    prj,
			    NULL,
			    NULL);
}

typedef struct {
  void* msg;
  SerialType msg_type;
} CheckoutFileData;

TaskControl
servside_checkout_file_request (Connection* conn, Task* task)
{
  CheckoutFileData *data = task_stack_push (task, CheckoutFileData);

  return task_finish_get (conn,
			  task,
			  & servside_checkout_file_respond,
			  ST_PrcsOneFile | ST_PrcsDone,
			  & data->msg_type,
			  (void**) & data->msg);
}

TaskControl
servside_checkout_file_respond (Connection* conn, Task* task)
{
  CheckoutFileData *data = task_stack_pop (task, CheckoutFileData);

  if (data->msg_type == ST_PrcsOneFile)
    {
      task_produce (conn, task, servside_checkout_file_send, data->msg);

      return servside_checkout_file_request (conn, task);
    }
  else
    {
      task_produce_done (conn, task);

      return protocol_next (conn, task);
    }
}

TaskControl
servside_checkout_file_send (Connection* conn, Task* task, ReadyFunc ready, void* vdata)
{
  SerialPrcsOneFile* msg = (SerialPrcsOneFile*) vdata;
  CheckoutData* data = CONN_DATA(conn);

  FileSegment* seg;
  AbsoluteNameSpace *family_space;

  if (! (family_space = server_absolute_namespace_attach (data->families_space, msg->family, NSAF_None)))
    return reponet_kill_task (task);

  if (! (seg = server_absolute_namespace_find (family_space, msg->md5)))
    return reponet_kill_task (task);

  if (segment_is_not_type (seg, SV_Regular))
    return reponet_kill_task (task);

  return task_segments_put (conn,
			    task,
			    ready,
			    seg,
			    NULL,
			    NULL);
}

void
servside_checkout_success (Connection* /*conn*/, TaskManager* tm)
{
  if (! repository_commit (cp_repo))
    reponet_exit_event_loop (tm);
}

void servside_checkout_failure (TaskManager* /*tm*/) { }
