/* -*-Mode: C++;-*-
 * $Id: servside.cc 1.12 Wed, 31 Mar 1999 02:17:22 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "prcs.h"
#include "prcs2.h"
#include "servside.h"

/* Protocol Declarations
 */


static gboolean createproject_command (gint argc, gchar** argv);
static gboolean createbranch_command (gint argc, gchar** argv);
static gboolean prcs_server_protocol_init   (Repository* repo, Protocol* /*protocol*/) ;
static gboolean prcs_server_protocol_create (Repository* repo, Protocol* /*protocol*/) ;

Repository *cp_repo;

static Request*
prcs_server_protocol_requests[] =
{
  & prcs_server_checkin_request,
  & prcs_server_checkout_request
};

static Protocol prcs_server_protocol =
{
  "PRCS",
  PRCS_PROTOCOL_DEFAULT_PORT,
  NULL,
  & prcs_server_protocol_create,
  & prcs_server_protocol_init,
  PRCS_PROTOCOL_MAJOR_VERSION,
  PRCS_PROTOCOL_MINOR_VERSION, /* @@@ make sure you have adequate version forward capabilities
				  * w/ protocols before you release. */
  STATIC_ARRAY (prcs_server_protocol_requests)
};

static NameSpace* _prcs_namespace;

static gboolean
initialize_namespaces (void)
{
  SerialAliasedIdentifier id;

  id.alias   = "prcs";
  id.id      = "";

  if (! (_prcs_namespace = server_global_namespace_attach (server_root_namespace (), & id, NSAF_None)))
    return FALSE;

  return TRUE;
}

gboolean
prcs_server_protocol_create (Repository* repo, Protocol* /*protocol*/)
{
  GlobalNameSpace* prcs;

  cp_repo = repo;

  if (! (prcs = server_global_namespace_create (server_root_namespace (), "prcs", FALSE)))
    return FALSE;

  return TRUE;
}

gboolean
prcs_server_protocol_init (Repository* repo, Protocol* /*protocol*/)
{
  cp_repo = repo;

  if (! prcs_edsio_init ())
    return FALSE;

  if (! initialize_namespaces ())
    return FALSE;

  return TRUE;
}

gboolean
createproject_command (gint argc, gchar** argv)
{
  GlobalNameSpace* project;

  g_assert (argc == 1);

  if (! (project = server_global_namespace_create (_prcs_namespace, argv[0], TRUE)))
    return FALSE;

  if (! server_global_namespace_create (project, "families", FALSE))
    return FALSE;

  if (! server_global_namespace_create (project, "branches", FALSE))
    return FALSE;

  prcs_generate_stringstring_event (EC_PrcsProjectCreated, argv[0], server_namespace_id ((NameSpace*) project)->id);

  if (! repository_commit (cp_repo))
    return FALSE;

  return TRUE;
}

gboolean
createbranch_command (gint argc, gchar** argv)
{
  GlobalNameSpace* branches;
  ControlledNameSpace *branch;
  SerialAliasedIdentifier id;

  g_assert (argc == 2);

  id.id = "";
  id.alias = argv[0];

  if (! (branches = servside_prcs_branches_namespace (& id)))
    return FALSE;

  if (! (branch = server_controlled_namespace_create (branches, argv[1])))
    return FALSE;

  prcs_generate_stringstring_event (EC_PrcsBranchCreated, argv[1], server_namespace_id ((NameSpace*) branch)->id);

  if (! repository_commit (cp_repo))
    return FALSE;

  return TRUE;
}

GlobalNameSpace*
servside_prcs_branches_namespace (SerialAliasedIdentifier* id)
{
  GlobalNameSpace* gns;
  SerialAliasedIdentifier pid;

  if (! (gns = server_global_namespace_attach (_prcs_namespace, id, NSAF_None)))
    return FALSE;

  pid.id = "";
  pid.alias = "branches";

  return server_global_namespace_attach (gns, &pid, NSAF_None);
}

GlobalNameSpace*
servside_prcs_families_namespace (SerialAliasedIdentifier* id)
{
  GlobalNameSpace* gns;
  SerialAliasedIdentifier pid;

  if (! (gns = server_global_namespace_attach (_prcs_namespace, id, NSAF_None)))
    return FALSE;

  pid.id = "";
  pid.alias = "families";

  return server_global_namespace_attach (gns, &pid, NSAF_None);
}

FileSegment*
servside_create_empty_project_file (const SerialAliasedIdentifier* project_id,
				    const SerialAliasedIdentifier* branch_id,
				    const char* login)
{
  FileHandle* handle;
  SerialProjectData pdata;
  File* file;

  const char new_project_format[] =
    ";; -*- Prcs2 -*-\n"
    "(Project-Description \"\")\n"
    "(Project-Version %s %s 0)\n"
    "(Parent-Version -*- -*- -*-)\n"
    "(Version-Log \"Empty project.\")\n"
    "(New-Version-Log \"\")\n"
    "(Checkin-Time \"%s\")\n"
    "(Checkin-Login \"%s\")\n"
    "(Populate-Ignore ())\n"
    "(Project-Keywords)\n"
    "(Files\n"
    ";; This is a comment.  Fill in files here.\n"
    ";; For example:  (prcs/checkout.cc ())\n"
    ")\n";

  file = file_initialize_temp (cp_repo);

  if (! (handle = file_open (file, HV_Replace)))
    return NULL;

  if (! handle_printf (handle,
		       new_project_format,
		       project_id->alias,
		       branch_id->alias,
		       edsio_time_t_to_iso8601 (time (NULL)),
		       login))
    return NULL;

  pdata.project_id = *project_id;
  pdata.project_branch_id = *branch_id;
  pdata.project_repository = "localhost:2700"; /* @@@ not */
  pdata.version_number = 0;

  pdata.files_len = 0;
  pdata.files = NULL;

  pdata.files_log_len = 0;
  pdata.files_log = NULL;

  pdata.invokation_log_len = 0;
  pdata.invokation_log = NULL;

  if (! append_project_data_to_handle (& pdata, handle))
    return NULL;

  if (! handle_close (handle))
    return NULL;

  return file_default_segment (file);
}

int
main (gint argc, gchar** argv)
{
  server_add_command ("createproject", createproject_command, 2);
  server_add_command ("createbranch", createbranch_command, 3);
  server_add_protocol (& prcs_server_protocol);
  return server_main (argc, argv);
}
