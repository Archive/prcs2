/* -*-Mode: C;-*-
 * $Id$
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <xdelta.h>
#include <edsiostdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

typedef struct _ArchiveTest ArchiveTest;

struct _ArchiveTest {
  const char *name;
  gboolean  (*initialize1) (void);
  gboolean  (*initialize2) (void);
  gboolean  (*close1) (void);
  gboolean  (*close2) (void);
  void      (*abort) (void);
  gboolean  (*onefile) (RcsFile *rcs, RcsStats* set, void* data);
  gboolean  (*dateorder) (RcsFile* rcs, RcsVersion *v, void* data);
  gboolean  (*retrieve) (RcsFile* rcs, RcsVersion *v, void* data);
  gboolean    copydir;
  XdfsPolicy  policy;
  int         flags;
};

#define AT_FLAG_RCS_LINEAR  (1 << 0)
#define AT_FLAG_RCS_TREE    (1 << 1)
#define AT_FLAG_RCS_NAME    (1 << 2)
#define AT_FLAG_TXN_ONE     (1 << 3)
#define AT_FLAG_TXN_NEST    (1 << 4)

static char *tmp_file;

ArchiveTest *current_test;
char        *archive_out_base_dir;

DBFS        *dbfs;
RepoTxn     *xdfs_txn;
Path        *xdfs_loc;

double       insert_pass_time;
double       retrieve_pass_time;
GTimer      *timer;
BinCounter  *insert_single_times;
BinCounter  *retrieve_single_times;
long long    encoded_size;
long long    unencoded_size;
gboolean     retrieve_verify;
long long    disk_blocks;

gboolean
disk_usage (char* file, long long *dblocks)
{
  struct stat buf;
  DIR* thisdir = NULL;
  struct dirent* ent = NULL;

  if (stat (file, & buf) < 0)
    return TRUE;

  (* dblocks) += buf.st_blocks;

  if (S_ISDIR (buf.st_mode))
    {
      if (! (thisdir = opendir (file)))
	{
	  g_warning ("opendir failed: %s", file);
	  return FALSE;
	}

      while ((ent = readdir (thisdir)))
	{
	  char* name = ent->d_name;
	  char* fullname;

	  if (strcmp (name, ".") == 0)
	    continue;

	  if (strcmp (name, "..") == 0)
	    continue;

	  /* Special cases here: I'm NOT counting Berkeley DB logs and shared
	   * memory regions. */
	  if (strncmp (name, "log.", 4) == 0)
	    continue;

	  if (strncmp (name, "__db", 4) == 0)
	    continue;

	  fullname = g_strdup_printf ("%s/%s", file, name);

	  if (! disk_usage (fullname, dblocks))
	    goto abort;

	  g_free (fullname);
	}
    }

  if (thisdir && closedir (thisdir) < 0)
    {
      g_warning ("closedir failed: %s", file);
      return FALSE;
    }

  return TRUE;

 abort:

  if (thisdir)
    closedir (thisdir);

  return FALSE;
}

void
report_stats ()
{
  char* timefile = g_strdup_printf ("%s/%s.data", _rcswalk_output_dir, current_test->name);
  FILE* tf = fopen (timefile, "w");
  long long real_size = disk_blocks * 512;

  g_assert (tf);
  g_free (timefile);

  fprintf (tf, "----------------\n");
  fprintf (tf, "Name:               %s\n", current_test->name);
  fprintf (tf, "Insert time:        %0.2f seconds\n", insert_pass_time);
  fprintf (tf, "Unencoded:          %qd bytes\n", unencoded_size);
  fprintf (tf, "Storage:            %qd bytes\n", real_size);
  fprintf (tf, "Ideal:              %qd bytes\n", encoded_size);
  fprintf (tf, "Storage overhead:   %0.2f%%\n", 100.0 * ((double) (real_size - encoded_size) / (double) encoded_size));
  fprintf (tf, "Actual compression: %0.2f%%\n", 100.0 * (1.0 - ((double) real_size    / (double) unencoded_size)));
  fprintf (tf, "Ideal compression:  %0.2f%%\n", 100.0 * (1.0 - ((double) encoded_size / (double) unencoded_size)));

  fprintf (tf, "Retrieve time:      %0.2f seconds\n", retrieve_pass_time);

  if (fclose (tf) < 0)
    g_error ("fclose failed\n");
}

gboolean
atest_finalize (RcsStats* set, void* data)
{
  if (current_test->close1)
    {
      g_timer_start (timer);
      if (! current_test->close1 ())
	return FALSE;
      g_timer_stop (timer);

      insert_pass_time += g_timer_elapsed (timer, NULL);
    }

  rcswalk_report (set);

  if (! disk_usage (archive_out_base_dir, & disk_blocks))
    return FALSE;

  return TRUE;
}

char*
atest_writeto (RcsFile *rcs)
{
  if (current_test->flags & AT_FLAG_RCS_NAME)
    {
      int len = strlen (rcs->copyname);
      char* res = g_strdup (rcs->copyname);

      res[len-2] = 0;

      return res;
    }

  return NULL;
}

gboolean
atest_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  if (current_test->onefile)
    {
      return current_test->onefile (rcs, set, data);
    }

  return TRUE;
}

gboolean
atest_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  gboolean res = TRUE;
  double stime;
  static int progress = 0;

  if ((++progress % 1000) == 0)
    fprintf (stdout, "%d\n", progress);

  unencoded_size += v->size;

  g_timer_start (timer);

  res = current_test->dateorder (rcs, v, data);

  g_timer_stop (timer);

  stime = g_timer_elapsed (timer, NULL);

  insert_pass_time += stime;

  stat_bincount_add_item (insert_single_times, v->dateseq, stime);

  return res;
}

gboolean
xdfs_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  XdfsState xstat;

  if (! xdfs_stat (xdfs_txn, xdfs_loc, & xstat))
    return FALSE;

  encoded_size += xstat.literal_size + xstat.control_size + xstat.patch_size;

  if (xdfs_txn)
    {
      g_timer_start (timer);

      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;

      g_timer_stop (timer);

      insert_pass_time += g_timer_elapsed (timer, NULL);
    }

  xdfs_txn = NULL;
  xdfs_loc = NULL;

  return TRUE;
}

gboolean
xdfs_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle* fh;

  if (! xdfs_txn)
    {
      if (! (xdfs_txn = dbfs_txn_begin (dbfs, (current_test->flags & AT_FLAG_TXN_NEST) ? 0 : DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      Inode ino;
      Inode root;
      XdfsParams params;

      xdfs_loc = path_append_bytes (xdfs_txn->alloc, path_root (xdfs_txn->alloc), PP_String, rcs->filename, strlen (rcs->filename));

      if (! dbfs_inode_find_root (xdfs_txn, path_root (xdfs_txn->alloc), FALSE, FT_Directory, & root))
	return FALSE;

      if (! dbfs_inode_new (xdfs_txn, & ino))
	return FALSE;

      if (! dbfs_make_directory (xdfs_txn, & ino, FALSE))
	return FALSE;

      if (! dbfs_link_create (xdfs_txn, & root, & ino, path_basename (xdfs_loc), FALSE))
	return FALSE;

      memset (& params, 0, sizeof (params));

      params.policy = current_test->policy;

      if (! xdfs_location_create (xdfs_txn, xdfs_loc, & params))
	return FALSE;
    }

  if (! (fh = handle_read_file (v->filename)))
    return FALSE;

  if (! xdfs_insert_version (xdfs_txn, xdfs_loc, fh))
    return FALSE;

  handle_free (fh);

  if (current_test->flags & AT_FLAG_TXN_ONE)
    {
      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;

      xdfs_txn = NULL;
    }

  return TRUE;
}

gboolean
xdfs_init1 ()
{
  Path *archive_path;

  if (! dbfs_library_init ())
    return FALSE;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, archive_out_base_dir)))
    return FALSE;

  if (! (dbfs = dbfs_create (archive_path)))
    return FALSE;

  if (! xdfs_library_init ())
    return FALSE;

  return TRUE;
}

gboolean
xdfs_init2 ()
{
  Path *archive_path;

  if (! (archive_path = path_from_host_string (NULL, _fs_pthn, archive_out_base_dir)))
    return FALSE;

  if (! (dbfs = dbfs_initialize (archive_path)))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_close1 ()
{
  if (! dbfs_close (dbfs))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_close2 ()
{
  if (! dbfs_close (dbfs))
    return FALSE;

  if (! dbfs_library_close ())
    return FALSE;

  return TRUE;
}

void
xdfs_abort ()
{
  if (xdfs_txn)
    dbfs_txn_abort (xdfs_txn);

  if (dbfs)
    dbfs_close (dbfs);

  dbfs_library_close ();
}

gboolean
rcs_register (RcsFile *rcs)
{
  int pid;
  static GPtrArray *a = NULL;

  if (! a)
    a = g_ptr_array_new ();

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      /* child */
      g_ptr_array_set_size (a, 0);

      g_ptr_array_add (a, "rcs");

      g_ptr_array_add (a, "-i");
      g_ptr_array_add (a, "-U");
      g_ptr_array_add (a, "-t-no");
      g_ptr_array_add (a, "-q");
      g_ptr_array_add (a, (char*) rcs->copyname);
      g_ptr_array_add (a, NULL);

      /* exec */
      execv ("/usr/bin/rcs", (char**) a->pdata);
      g_warning ("exec failed: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("rcs did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("rcs did not execute\n");
    }

  return TRUE;
}

gboolean
rcs_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  int pid;
  static GPtrArray *a = NULL;
  static GString   *varg = NULL;

  if (! a)
    {
      a = g_ptr_array_new ();
      varg = g_string_new (NULL);
    }

  if (! v->parent && ! rcs_register (rcs))
    return FALSE;

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      /* child */
      g_ptr_array_set_size (a, 0);

      if (current_test->flags & AT_FLAG_RCS_TREE)
	g_string_sprintf (varg, "-q%s", v->vname);
      else if (current_test->flags & AT_FLAG_RCS_LINEAR)
	g_string_sprintf (varg, "-q");
      else
	abort ();

      g_ptr_array_add (a, "ci");

      g_ptr_array_add (a, "-f");
      g_ptr_array_add (a, "-mno");
      g_ptr_array_add (a, "-t-no");
      g_ptr_array_add (a, varg->str);
      g_ptr_array_add (a, (char*) v->filename);
      g_ptr_array_add (a, (char*) rcs->copyname);
      g_ptr_array_add (a, NULL);

      /* exec */
      execv ("/export/spin/jmacd/prcs2/librepo+/utils/bin/ci", (char**) a->pdata);
      g_warning ("exec failed: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("ci did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("ci did not execute\n");
    }

  return TRUE;
}

/* Retrieval code
 */

gboolean
rtest_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  if (xdfs_txn)
    {
      if (! dbfs_txn_commit (xdfs_txn))
	return FALSE;

      xdfs_txn = NULL;
      xdfs_loc = NULL;
    }

  return TRUE;
}

gboolean
xdfs_retrieve (RcsFile* rcs, RcsVersion *v, void* data)
{
  FileHandle *in_fh, *out_fh;
  Inode seq;
  Inode ino;

  if (! xdfs_txn)
    {
      if (! (xdfs_txn = dbfs_txn_begin (dbfs, (current_test->flags & AT_FLAG_TXN_NEST) ? 0 : DBFS_TXN_FLAT)))
 	return FALSE;
    }

  if (! xdfs_loc)
    {
      xdfs_loc = path_append_bytes (xdfs_txn->alloc, path_root (xdfs_txn->alloc), PP_String, rcs->filename, strlen (rcs->filename));
    }

  if (! xdfs_container_sequence (xdfs_txn, xdfs_loc, & seq))
    return FALSE;

  if (! dbfs_inode_find_seqno (xdfs_txn, & seq, v->dateseq+1, DbfsNoFollowLinks, FT_CanRead, & ino))
    return FALSE;

  if (! (in_fh = dbfs_inode_open_read (xdfs_txn, & ino)))
    return FALSE;

  if (! (out_fh = handle_write_file (tmp_file)))
    return FALSE;

  if (! handle_drain (in_fh, out_fh))
    return FALSE;

  if (! handle_close (out_fh))
    return FALSE;

  handle_close (in_fh);
  handle_free (out_fh);
  handle_free (in_fh);

  return TRUE;
}

gboolean
rcs_retrieve (RcsFile* rcs, RcsVersion *v, void* data)
{
  int pid;
  static GPtrArray *a = NULL;
  static GString   *varg = NULL;

  if (! a)
    {
      a = g_ptr_array_new ();
      varg = g_string_new (NULL);
    }

  if ((pid = vfork ()) < 0)
    g_error ("vfork failed\n");

  if (pid == 0)
    {
      int fd;

      if ((fd = open (tmp_file, O_WRONLY | O_TRUNC | O_CREAT, 0666)) < 0)
	g_error ("open tmp failed\n");

      dup2(fd, STDOUT_FILENO);

      /* child */
      g_ptr_array_set_size (a, 0);

      g_string_sprintf (varg, "-p%s", v->vname);

      g_ptr_array_add (a, "co");

      g_ptr_array_add (a, "-ko");
      g_ptr_array_add (a, "-q");
      g_ptr_array_add (a, varg->str);
      g_ptr_array_add (a, (char*) rcs->copyname);
      g_ptr_array_add (a, NULL);

      /* exec */
      execv ("/usr/bin/co", (char**) a->pdata);
      g_warning ("exec failed: %s", g_strerror (errno));
      _exit (127);
    }
  else
    {
      /* parent */
      int status;

      if (waitpid (pid, &status, 0) < 0)
	g_error ("waitpid failed\n");

      if (! WIFEXITED (status))
	g_error ("co did not exit\n");

      if (WEXITSTATUS (status) == 127)
	g_error ("co did not execute\n");
    }

  return TRUE;
}

gboolean
verify_version (RcsFile* rcs, RcsVersion *v)
{
  FileHandle *orig, *retr;
  guint8 obuf[1024], rbuf[1024];
  int offset = 0;
  int oc, rc;

  if (! (orig = handle_read_file (v->filename)))
    return FALSE;

  if (! (retr = handle_read_file (tmp_file)))
    return FALSE;

  for (;;)
    {
      oc = handle_read (orig, obuf, 1024);
      rc = handle_read (retr, rbuf, 1024);

      if (oc < 0 || rc < 0)
	g_error ("read failed: verify\n");

      if (oc != rc)
	{
	  fprintf (stderr, "verify failed: different lengths: %d != %d\n", oc, rc);
	  return FALSE;
	}

      if (oc == 0)
	break;

      if (memcmp (obuf, rbuf, oc) != 0)
	{
	  fprintf (stderr, "verify failed: different content near offset: %d\n", offset);
	  return FALSE;
	}

      offset += oc;
    }

  handle_close (orig);
  handle_close (retr);

  handle_free (orig);
  handle_free (retr);

  return TRUE;
}

gboolean
rtest_dateorder (RcsFile* rcs, RcsVersion *v, void* data)
{
  gboolean res;
  double stime;
  int bin;

  fprintf (stderr, "%s %s\n", rcs->filename, v->vname);

  g_timer_start (timer);
  res = current_test->retrieve (rcs, v, data);
  g_timer_stop (timer);

  if (! res)
    return FALSE;

  stime = g_timer_elapsed (timer, NULL);

  retrieve_pass_time += stime;

  /* Decide how to place this single time.  For the RCS tree case, use delta
   * chain length, otherwise use sequence number.
   */
  if (current_test->flags & AT_FLAG_RCS_TREE)
    bin = v->chain_length;
  else
    bin = rcs->version_count - v->dateseq - 1;

  stat_bincount_add_item (retrieve_single_times, bin, stime);

  if (retrieve_verify && ! verify_version (rcs, v))
    return FALSE;

  return TRUE;
}

gboolean
rtest_finalize (RcsStats* set, void* data)
{
  if (current_test->close2 && ! current_test->close2 ())
    return FALSE;

  report_stats ();

  stat_bincount_report (insert_single_times);
  stat_bincount_report (retrieve_single_times);

  return TRUE;
}

RcsWalker atest_walker = {
  NULL,
  & atest_finalize,
  & atest_onefile,
  & atest_dateorder,
  NULL,
  & atest_writeto,
  TRUE
};

ArchiveTest all_tests[] = {
  { "XDFS-none",    xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_NoCompress, 0 },
  { "XDFS-rj",      xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_ReverseJump, 0 },
  { "XDFS-fj",      xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_ForwardJump, 0 },
  { "XDFS-f",       xdfs_init1, xdfs_init2, xdfs_close1, xdfs_close2, xdfs_abort, xdfs_onefile, xdfs_dateorder, xdfs_retrieve, FALSE, XP_Forward, 0 },
  { "RCS-tree",     NULL, NULL, NULL, NULL, NULL, NULL, rcs_dateorder, rcs_retrieve, TRUE, 0, AT_FLAG_RCS_TREE   | AT_FLAG_RCS_NAME },
  { "RCS-linear",   NULL, NULL, NULL, NULL, NULL, NULL, rcs_dateorder, rcs_retrieve, TRUE, 0, AT_FLAG_RCS_LINEAR | AT_FLAG_RCS_NAME }
};

RcsWalker rtest_walker = {
  NULL,
  & rtest_finalize,
  & rtest_onefile,
  & rtest_dateorder,
  NULL,
  NULL,
  FALSE
};

int
main (int argc, char** argv)
{
  char *stat_output_dir;
  char *test;
  int   i;
  char  buf[1024];

  if (argc > 1 && strcmp (argv[1], "-v") == 0)
    {
      retrieve_verify = TRUE;
      rtest_walker.write_files = TRUE;
      argc -= 1;
      argv += 1;
    }

  if (argc < 5)
    g_error ("usage: %s TEST ARCHIVE_OUT_BASE_DIR STAT_OUTPUT_DIR RCS_DIR ...\n", argv[0]);

  timer = g_timer_new ();
  insert_single_times = stat_bincount_new ("InsertTime");
  retrieve_single_times = stat_bincount_new ("RetrieveTime");

  tmp_file = g_strdup_printf ("%s/at.%d", g_get_tmp_dir (), (int) getpid ());

  test                 = argv[1];
  archive_out_base_dir = argv[2];
  stat_output_dir      = argv[3];

  argc -= 4;
  argv += 4;

  for (i = 0; i < ARRAY_SIZE (all_tests); i += 1)
    {
      if (strcmp (test, all_tests[i].name) == 0)
	{
	  current_test = all_tests + i;
	  break;
	}
    }

  if (! current_test)
    g_error ("No such test\n");

  sprintf (buf, "rm -rf %s %s /cheetah1/log/*", archive_out_base_dir, stat_output_dir);
  system (buf);

  if (! rcswalk_output_dir (stat_output_dir))
    goto bail;

  if (current_test->initialize1 && ! current_test->initialize1 ())
    goto bail;

  if (! rcswalk (& atest_walker, argc, argv, current_test->copydir ? archive_out_base_dir : NULL))
    goto bail;

  if (current_test->initialize2 && ! current_test->initialize2 ())
    goto bail;

  if (! rcswalk (& rtest_walker, argc, argv, current_test->copydir ? archive_out_base_dir : NULL))
    goto bail;

  unlink (tmp_file);

  return 0;

 bail:

  if (current_test && current_test->abort)
    current_test->abort ();

  return 2;
}
