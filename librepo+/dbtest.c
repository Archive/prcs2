#include <db.h>

int main()
{
  DB_ENV *env;
  int err;
  DB *dbp;
  DB_TXN *db_txn;
  DBT key, data;
  db_recno_t rn;
  int testval = 21342;

  if ((err = db_env_create (& env, 0)))
    abort ();

  env->set_errfile (env, stderr);

  if ((err = env->open (env,
			".",
			NULL,
			DB_CREATE | DB_INIT_LOCK | DB_INIT_LOG | DB_INIT_MPOOL | DB_INIT_TXN | DB_THREAD | DB_PRIVATE,
			0666)))
    abort ();

  if ((err = db_create (& dbp, env, 0)))
    abort ();

  if ((err = dbp->set_re_len (dbp, 4)))
    abort ();

  if ((err = dbp->open (dbp,
			"testqueue",
			NULL,
			DB_QUEUE,
			DB_THREAD | DB_CREATE | DB_EXCL,
			0666)))
    abort ();

  memset (& key, 0, sizeof (key));
  memset (& data, 0, sizeof (data));

  if ((err = txn_begin (env, NULL, & db_txn, DB_TXN_NOSYNC)))
    abort ();

  rn = 1;

  key.data = & rn;
  key.size = sizeof (db_recno_t); /* @@@ Try it with and without this line. */

  data.data = (void*) & testval;
  data.size = sizeof (testval);

  if ((err = dbp->put (dbp, db_txn, & key, & data, 0)))
    abort ();

  if ((err = txn_commit (db_txn, 0)))
    abort ();


}
