/* -*-Mode: C;-*-
 * $Id: dbfs.c,v 1.1 1999/08/27 04:03:50 jmacd Exp $
 *
 * Copyright (C) 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"

#include <ctype.h>

struct _Path {
  Path     *dirname;
  BaseName *basename;
  PathType  type;
  guint32   uplinks;
  PathPrinter pp;
};

struct _Allocator {
  GSList *ptrs;
};

PathToHostNative *_fs_pthn;
PathToHostNative _fs_pthn_s;

PathToHostNative *_print_pthn;
PathToHostNative _print_pthn_s;

Allocator*
allocator_new  (void)
{
  return g_new0 (Allocator, 1);
}

void
allocator_free (Allocator* alloc)
{
  GSList *p;

  for (p = alloc->ptrs; p; p = p->next)
    g_free (p->data);

  g_slist_free (alloc->ptrs);
  g_free (alloc);
}

void*
allocator_malloc (Allocator* alloc, guint size)
{
  void* it = g_malloc (size);
  if (alloc) alloc->ptrs = g_slist_prepend (alloc->ptrs, it);
  return it;
}

void*
allocator_calloc (Allocator* alloc, guint size)
{
  void* it = g_malloc0 (size);
  if (alloc) alloc->ptrs = g_slist_prepend (alloc->ptrs, it);
  return it;
}

void*
allocator_memdup (Allocator* alloc, const void* val, guint size)
{
  void* it = g_memdup (val, size);
  if (alloc) alloc->ptrs = g_slist_prepend (alloc->ptrs, it);
  return it;
}

/* These are three methods that return path terminators, these are
 * always the root element of a Path object.
 */

Path*
path_root (Allocator* alloc)
{
  static Path root;

  root.type = PT_PathAbsolute;

  return & root;
}

Path*
path_current (Allocator* alloc)
{
  static Path root;

  root.type = PT_PathRelative;

  return & root;
}

Path*
path_parent (Allocator* alloc, guint32 uplinks)
{
  Path* par = alc_new0 (alloc, Path);

  par->type = PT_PathRelative;
  par->uplinks = uplinks;

  return par;
}

/* These are the constructors for non-terminators.  The first four all
 * append a single element.  All results are translated into an
 * equivalent byte array.  Strings do not include their nul, and there
 * are assumed to be no out-of-band characters, such as the path
 * terminator character, zero characters, or any others that may need
 * escaping.
 */

Path*
path_append_format (Allocator* alloc, Path* path, PathPrinter pp, const char* fmt, ...)
{
  va_list args;
  gchar* copy;
  Path* path2;

  va_start (args, fmt);

  copy = g_strdup_vprintf (fmt, args);

  path2 = path_append_string (alloc, path, pp, copy);

  g_free (copy);

  va_end (args);

  return path2;
}

Path*
path_append_string (Allocator* alloc, Path* path, PathPrinter pp, const char* s)
{
  return path_append_bytes (alloc, path, pp, s, strlen (s));
}

Path*
path_append_basename (Allocator* alloc, Path* path, PathPrinter pp, BaseName *bn)
{
  Path* npath;

  npath = alc_new0 (alloc, Path);

  npath->dirname = path;
  npath->basename = bn;
  npath->type = PT_PathName;
  npath->pp = pp;

  return npath;
}

BaseName*
basename_new (Allocator* alloc, const guint8* val, guint len)
{
  BaseName *bn;

  bn = alc_new0 (alloc, BaseName);

  bn->data = allocator_memdup (alloc, val, len);
  bn->len = len;

  return bn;
}

Path*
path_append_bytes (Allocator* alloc, Path* path, PathPrinter pp, const guint8* val, guint32 len)
{
  return path_append_basename (alloc, path, pp, basename_new (alloc, val, len));
}

/* This constructor appends two paths.  If APPEND has uplinks, they
 * are evaluated, meaning that each uplink consumes one element of the
 * base path.  Otherwise, the elements of each without regard to their
 * type, and especially their terminator type--absolute paths are
 * treated as if they were relative in this operation.
 */

Path*
path_append_path (Allocator* alloc, Path* base, Path* append)
{
  if (append->type != PT_PathName)
    {
      int i;

      for (i = 0; i < append->uplinks; i += 1)
	{
	  /* @@@ This could avoid a few allocations when base is also
	   * relative, but solve the memory management problem
	   * first.
	   */
	  base = path_dirname (alloc, base);
	}

      return base;
    }

  return path_append_basename (alloc,
			       path_append_path (alloc, base, path_dirname (alloc, append)),
			       append->pp,
			       path_basename (append));
}

/* This constructor evaluates one path relative to another.  This
 * operation implements the logic used to evaluate a symbolic link.
 * If LINK is a relative path, then the result is the same as
 * path_append_path, otherwise LINK is simply returned.
 */

Path*
path_evaluate_path (Allocator* alloc, Path* base, Path* link)
{
  switch (link->type)
    {
    case PT_PathName:
      return path_evaluate_path (alloc, base, path_dirname (alloc, link));
    case PT_PathAbsolute:
      return link;
    case PT_PathRelative:
      return path_append_path (alloc, base, link);
    }
  abort ();
}

/* These are four selectors.
 */

PathType
path_type (Path* path)
{
  return path->type;
}

BaseName*
path_basename (Path* path)
{
  g_assert (path->type == PT_PathName);

  return path->basename;
}

guint32
path_parent_count (Path* path)
{
  g_assert (path->type == PT_PathRelative);

  return path->uplinks;
}

/* This operation returns PATH's parent.  For relative path terminators, one
 * uplink is added.  Absolute path terminators simply return themselves.
 */
Path*
path_dirname (Allocator* alloc, Path* path)
{
  switch (path->type)
    {
    case PT_PathName:
      return path->dirname;
    case PT_PathAbsolute:
      return path;
    case PT_PathRelative:
      return path_parent (alloc, path->uplinks - 1);
    }

  abort ();
}

gboolean
path_term (Path* path)
{
  switch (path->type)
    {
    case PT_PathName:
      return FALSE;
    default:
      return TRUE;
    }
}

gboolean
path_equal (Path* path, Path* comp)
{
  if (path->type != comp->type)
    return FALSE;

  switch (path->type)
    {
    case PT_PathName:

      if (path->basename->len != comp->basename->len)
	return FALSE;

      if (memcmp (path->basename->data, comp->basename->data, path->basename->len) != 0)
	return FALSE;

      return path_equal (path->dirname, comp->dirname);
    case PT_PathAbsolute:
      return TRUE;
    case PT_PathRelative:
      return path->uplinks == comp->uplinks;
    }

  abort ();
}

/* These methods deal with host-specific and canonical path name
 * conversions.  There are two representations currently.  These are
 * both canonical and string representations.  The canonical
 * representation uses a compact somewhat-ordered encoding.  It
 * appends the value to RESULT.  It uses these encodings:
 *
 *   '_' -> '_' 0
 *   sep -> '_' 1
 *   abs -> '_' 2
 *   rel -> '_' 3 [4 bytes uplinks big endian]
 */

void
path_basename_to_canonical (BaseName *bn, GByteArray* result)
{
  int i;

  for (i = 0; i < bn->len; i += 1)
    {
      guint8 c = bn->data[i];

      switch (c)
	{
	case '_':
	  {
	    const guint8 *v = "_";
	    g_byte_array_append (result, v, 2);
	  }
	  break;

	default:
	  g_byte_array_append (result, & c, 1);
	  break;
	}
    }
}

void
path_to_canonical (Path *path, GByteArray* result)
{
  switch (path->type)
    {
    case PT_PathName:
      {
	path_to_canonical (path->dirname, result);

	if (path->dirname->type == PT_PathName)
	  {
	    const guint8* v = "_\001";
	    g_byte_array_append (result, v, 2);
	  }

	path_basename_to_canonical (path->basename, result);
      }
      break;

    case PT_PathAbsolute:
      {
	const guint8* v = "_\002";
	g_byte_array_append (result, v, 2);
      }
      break;

    case PT_PathRelative:
      {
	const guint8* v = "_\003";
	guint32 uplinks_be = GUINT32_TO_BE (path->uplinks);

	g_byte_array_append (result, v, 2);
	g_byte_array_append (result, (guint8*) & uplinks_be, sizeof (guint32));
      }
      break;
    }
}

Path*
path_from_canonical (Allocator* alloc, const guint8* buf, guint32 len)
{
  int i;
  Path* p = NULL;
  GByteArray* b = g_byte_array_new ();
  gboolean in_name = FALSE;

  for (i = 0; i < len; i += 1)
    {
      guint8 c = buf[i];

      switch (c)
	{
	case '_':

	  if (i == (len - 1))
	    goto badpath;

	  c = buf[i++];

	  switch (c)
	    {
	    case 0:
	      /* '_' character */
	      {
		const guint8 v = '_';

		if (! in_name)
		  goto badpath;

		g_byte_array_append (b, & v, 1);
	      }

	      break;
	    case 1:
	      /* sep */

	      if (! in_name)
		goto badpath;

	      p = path_append_bytes (alloc, p, PP_Hex, b->data, b->len);

	      g_byte_array_set_size (b, 0);

	      break;
	    case 2:
	      /* abs */

	      if (in_name)
		goto badpath;

	      p = path_root (alloc);
	      in_name = TRUE;

	      break;
	    case 3:
	      /* rel */

	      {
		guint32 uplinks, uplinks_be;

		if (in_name)
		  goto badpath;

		if (i > (len - sizeof (guint32)))
		  goto badpath;

		memcpy (& uplinks_be, buf + i, sizeof (guint32));
		i += sizeof (guint32);
		uplinks = GUINT32_FROM_BE (uplinks_be);
		p = path_parent (alloc, uplinks);
		in_name = TRUE;
	      }

	      break;
	    default:
	      goto badpath;
	    }

	  break;
	default:

	  if (! in_name)
	    goto badpath;

	  g_byte_array_append (b, & c, 1);

	  break;
	}
    }

  p = path_append_bytes (alloc, p, PP_Hex, b->data, b->len);

  g_byte_array_free (b, TRUE);

  return p;

 badpath:

  dbfs_generate_void_event (EC_DbfsMalformedCanonicalPath);

  g_byte_array_free (b, TRUE);

  return NULL;
}

/* The string encoding must accomodate for characters that are illegal
 * by the host's standards.  In the UNIX FS case, there are two illegal
 * characters, these are encoded using the same convention used by
 * Java for encoding greater-than-8-bit values in the file system
 * using '@' as an escape for hex-coded values.
 */

void
path_to_host_string (PathToHostNative* pthn, Path* path, GString* result)
{
  switch (path->type)
    {
    case PT_PathName:
      path_to_host_string (pthn, path->dirname, result);
      if (path->dirname->type == PT_PathName)
	g_string_append (result, pthn->sep);
      path_basename_to_host_string (pthn, path->pp, path->basename, result);
      break;
    case PT_PathAbsolute:
      g_string_append (result, pthn->root_sep);
      break;
    case PT_PathRelative:
      if (path->uplinks == 0)
	{
	  g_string_append (result, pthn->this_dir);
	  g_string_append (result, pthn->sep);
	}
      else
	{
	  int i;

	  for (i = 0; i < path->uplinks; i += 1)
	    {
	      g_string_append (result, pthn->parent_dir);
	      g_string_append (result, pthn->sep);
	    }
	}
      break;
    }
}

void
path_fs_bths (PathToHostNative* pthn, PathPrinter pp, BaseName* bn, GString* result)
{
  int i;

  for (i = 0; i < bn->len; i += 1)
    {
      guint8 c = bn->data[i];

      if (c != '@' && isprint (c))
	{
	  g_string_append_c (result, c);
	}
      else
	{
	  g_string_sprintfa (result, "@%x%x", (c & 0xf0) >> 4, c & 0xf);
	}
    }
}

static Path*
path_fs_add_segment (Allocator* alloc, PathToHostNative* pthn, Path* p, const char* s, int start, int stop)
{
  int len = stop - start;
  GByteArray *a;

  if (len == 0)
    return p;

  if (len == pthn->this_dir_len && strncmp (s + start, pthn->this_dir, len) == 0)
    return p;

  if (len == pthn->parent_dir_len && strncmp (s + start, pthn->parent_dir, len) == 0)
    return path_dirname (alloc, p);

  a = g_byte_array_new ();

  while (start < stop)
    {
      guint8 c = s[start++];

      if (c == '@' && (stop - start) > 1)
	{
	  char buf[3];
	  int x;

	  buf[0] = s[start++];
	  buf[1] = s[start++];
	  buf[2] = 0;

	  sscanf (buf, "%2x", & x);

	  c = x;
	}

      g_byte_array_append (a, & c, 1);
    }

  p = path_append_bytes (alloc, p, PP_Auto, a->data, a->len);

  g_byte_array_free (a, TRUE);

  return p;
}

Path*
path_fs_fhs (Allocator* alloc, PathToHostNative* pthn, const char* s)
{
  Path* p;
  const char *l;
  int pos = 0, len = strlen (s);

  if (strncmp (s, pthn->root_sep, pthn->root_sep_len) == 0)
    {
      p = path_root (alloc);
      pos += pthn->root_sep_len;
    }
  else
    p = path_current (alloc);

  while ((l = strstr (s + pos, pthn->sep)))
    {
      int stop = l - s;

      p = path_fs_add_segment (alloc, pthn, p, s, pos, stop);

      pos = stop + pthn->sep_len;
    }

  return path_fs_add_segment (alloc, pthn, p, s, pos, len);
}

const char*
path_to_string (Path* p)
{
  GString *s = g_string_new (NULL);

  path_to_host_string (_print_pthn, p, s);

  return s->str;
}

void
path_print_bths (PathToHostNative* pthn, PathPrinter pp, BaseName* bn, GString* result)
{
  int i;

  switch (pp)
    {
    case PP_Auto:
      for (i = 0; i < bn->len; i += 1)
	{
	  char c = bn->data[i];
	  if (! isprint (c))
	    goto hex;
	}
      /* fallthrough */
    case PP_String:
      path_fs_bths (pthn, pp, bn, result);
      break;
    case PP_Hex:
    hex:
      g_string_append (result, "#<");
      for (i = 0; i < bn->len; i += 1)
	{
	  guint8 c = bn->data[i];
	  g_string_sprintfa (result, "%x%x", c >> 4, c & 0xf);
	}
      g_string_append (result, ">");
      break;
    }
}

Path*
path_print_fhs (Allocator* alloc, PathToHostNative* pthn, const char* s)
{
  /* Only for output. */
  abort ();
}

void
path_basename_to_host_string (PathToHostNative* pthn, PathPrinter pp, BaseName* bn, GString* result)
{
  pthn->bths (pthn, pp, bn, result);
}

Path*
path_from_host_string (Allocator* alloc, PathToHostNative* pthn, const char* s)
{
  return pthn->fhs (alloc, pthn, s);
}

void
path_library_init (void)
{
  _fs_pthn    = & _fs_pthn_s;
  _print_pthn = & _print_pthn_s;

  _fs_pthn->root_sep = "/";
  _fs_pthn->sep = "/";
  _fs_pthn->this_dir = ".";
  _fs_pthn->parent_dir = "..";

  _fs_pthn->root_sep_len = 1;
  _fs_pthn->sep_len = 1;
  _fs_pthn->this_dir_len = 1;
  _fs_pthn->parent_dir_len = 2;

  _fs_pthn->bths = path_fs_bths;
  _fs_pthn->fhs = path_fs_fhs;

  _print_pthn->root_sep = "/";
  _print_pthn->sep = "/";
  _print_pthn->this_dir = ".";
  _print_pthn->parent_dir = "..";

  _print_pthn->root_sep_len = 1;
  _print_pthn->sep_len = 1;
  _print_pthn->this_dir_len = 1;
  _print_pthn->parent_dir_len = 2;

  _print_pthn->bths = path_print_bths;
  _print_pthn->fhs = path_print_fhs;
}
