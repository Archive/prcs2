/* -*-Mode: C;-*-
 * $Id: rcsinfo.c,v 1.3 2000/01/27 20:54:28 jmacd Exp jmacd $
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"

gboolean
info_onefile (RcsFile *rcs, RcsStats* set, void* data)
{
  g_print ("%d: ******************************\n", set->version_stat->count);
  g_print ("RCS file: %s\n", rcs->filename);
  g_print ("versions: %d\n", rcs->version_count);
  g_print ("reverse: %d\n", rcs->reverse_count);
  g_print ("forward: %d\n", rcs->forward_count);
  g_print ("branches: %d\n", rcs->branch_count);
  g_print ("totalsize: %qd\n", (long long) rcs->total_size);
  g_print ("head: %s\n", rcs->head_version->vname);
  g_print ("root: %s\n", rcs->root_version->vname);
  return TRUE;
}

gboolean
info_finalize (RcsStats* set, void* data)
{
  rcswalk_report (set);

  return TRUE;
}

gboolean
info_dateorder (RcsFile* rcs, RcsVersion* v, void* data)
{
  /*g_print ("version: %s; children: %d; date: %d; size: %qd\n", v->vname, g_slist_length (v->children), (int) v->date, v->size);*/
  return TRUE;
}

gboolean
info_delta (RcsFile* rcs, RcsVersion* from, RcsVersion *to, void* data)
{
  /*g_print ("delta: %s->%s\n", from->vname, to->vname);*/
  return TRUE;
}

RcsWalker info_walker = {
  NULL,
  & info_finalize,
  & info_onefile,
  NULL,
  NULL,
  NULL,
  FALSE
};

RcsWalker infodate_walker = {
  NULL,
  & info_finalize,
  & info_onefile,
  & info_dateorder,
  NULL,
  NULL,
  FALSE
};

RcsWalker infodelta_walker = {
  NULL,
  & info_finalize,
  & info_onefile,
  NULL,
  & info_delta,
  NULL,
  FALSE
};

int
main (int argc, char** argv)
{
  char *op;
  RcsWalker *walker;

  if (argc < 3)
    g_error ("usage: %s OPERATION OUTPUT_DIR RCS_DIR ...\n", argv[0]);

  op = argv[1];

  if (strcmp (op, "info") == 0)
    walker = & info_walker;
  else if (strcmp (op, "infodate") == 0)
    walker = & infodate_walker;
  else if (strcmp (op, "infodelta") == 0)
    walker = & infodelta_walker;
  else
    g_error ("unrecognized operation");

  if (! rcswalk_output_dir (argv[2]))
    return 2;

  argc -= 3;
  argv += 3;

  if (! rcswalk (walker, argc, argv, NULL))
    return 2;

  return 0;
}
