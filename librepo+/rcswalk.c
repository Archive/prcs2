/* -*-Mode: C;-*-
 * $Id$
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>
#include <math.h>

#undef BUFSIZE
#define BUFSIZE (1<<14)

char       *tmp_file_1;
gboolean    tmp_file_1_free = TRUE;
char       *tmp_file_2;
gboolean    tmp_file_2_free = TRUE;
int         skip_count;

extern time_t str2time (char const *, time_t, long);

static guint8 readbuf[BUFSIZE];

void
rcswalk_free_segment (RcsVersion *v)
{
  if (v->segment)
    g_free (v->segment);

  if (v->filename == tmp_file_1)
    tmp_file_1_free = TRUE;
  else if (v->filename == tmp_file_2)
    tmp_file_2_free = TRUE;
  else
    g_free (v->filename);

  v->segment = NULL;
  v->filename = NULL;
}

gboolean
rcswalk_checkout (RcsFile* rcs, RcsWalker* walker, RcsVersion *v)
{
  FILE* out;
  char cmdbuf[1024];
  int nread;
  int alloc = BUFSIZE;
  int pos = 0;

  sprintf (cmdbuf, "co -ko -p%s %s 2>/dev/null\n", v->vname, rcs->filename);

  g_assert (! v->segment);

  v->segment = g_malloc (alloc);

  if (! (out = popen (cmdbuf, "r")))
    {
      g_warning ("popen failed: %s: %s", cmdbuf, g_strerror (errno));
      return FALSE;
    }

  for (;;)
    {
      nread = fread (readbuf, 1, BUFSIZE, out);

      if (nread == 0)
	break;

      if (nread < 0)
	{
	  g_warning ("fread failed: %s", g_strerror (errno));
	  return FALSE;
	}

      if (pos + nread > alloc)
	{
	  alloc *= 2;
	  v->segment = g_realloc (v->segment, alloc);
	}

      memcpy (v->segment + pos, readbuf, nread);

      pos += nread;
    }

  if (pclose (out) < 0)
    {
      g_warning ("pclose failed");
      return FALSE;
    }

  v->size = pos;

  if (walker->write_files)
    {
      char* file = NULL;

      if (walker->writeto)
	file = walker->writeto (rcs);

      if (! file && tmp_file_1_free)
	{
	  file = tmp_file_1;
	  tmp_file_1_free = FALSE;
	}

      if (! file && tmp_file_2_free)
	{
	  file = tmp_file_2;
	  tmp_file_2_free = FALSE;
	}

      g_assert (file);

      v->filename = file;

      if (! (out = fopen (file, "w")))
	{
	  g_warning ("fopen failed: %s\n", file);
	  return FALSE;
	}

      if (fwrite (v->segment, v->size, 1, out) != 1)
	{
	  g_warning ("fwrite failed: %s\n", file);
	  return FALSE;
	}

      if (fclose (out) < 0)
	{
	  g_warning ("fclose failed: %s\n", file);
	  return FALSE;
	}
    }

  return TRUE;
}

gboolean
rcswalk_delta (RcsFile* rcs, RcsWalker* walker, RcsVersion* version, int *count, void* data)
{
  GSList *c;
  RcsVersion *child;

  for (c = version->children; c; c = c->next)
    {
      gboolean reverse;

      child = c->data;

      if (! version->segment)
	{
	  if (! rcswalk_checkout (rcs, walker, version))
	    return FALSE;
	}

      if (! rcswalk_checkout (rcs, walker, child))
	return FALSE;

      reverse = version->on_trunk && child->on_trunk;

      (* count) += 1;

      if (! walker->delta (rcs, reverse ? child : version, reverse ? version : child, data))
	return FALSE;

      rcswalk_free_segment (version);

      if (! rcswalk_delta (rcs, walker, child, count, data))
	return FALSE;
    }

  rcswalk_free_segment (version);
  return TRUE;
}

gboolean
rcswalk_dateorder (RcsFile* rcs, RcsWalker *walker, RcsStats *stats, void* data)
{
  int i;

  for (i = 0; i < rcs->version_count; i += 1)
    {
      RcsVersion *v = rcs->versions_date[i];

      if (! rcswalk_checkout (rcs, walker, v))
	return FALSE;

      stat_bincount_add_item (stats->avg_version_size, i, v->size);

      if (! walker->dateorder (rcs, v, data))
	return FALSE;

      rcswalk_free_segment (v);
    }

  return TRUE;
}

gboolean
rcswalk_match (char** line_p, char* str)
{
  int len = strlen (str);

  if (strncmp (*line_p, str, len) == 0)
    {
      (*line_p) += len;
      return TRUE;
    }

  return FALSE;
}

void
rcswalk_find_parent (RcsFile *rcs, GHashTable* hash, RcsVersion *v)
{
  char *lastdot;
  char  mbuf[1024];
  int   lastn;
  RcsVersion *p;

  strcpy (mbuf, v->vname);

  if (! (lastdot = strchr (mbuf, '.')))
    abort ();

  if (! (lastdot = strchr (lastdot+1, '.')))
    v->on_trunk = TRUE;

  lastdot = strrchr (mbuf, '.');
  lastn = atoi (lastdot + 1);

  do
    {
      if (lastn == 1)
	{
	  (*lastdot) = 0;

	  if (strcmp (mbuf, "1") == 0)
	    {
	      /* Assuming the first version is always "1.1".
	       */
	      rcs->root_version = v;
	      return;
	    }
	  else if (! (lastdot = strrchr (mbuf, '.')))
	    {
	      int i = 1;
	      int br = atoi (mbuf) - 1;
	      RcsVersion *p2 = NULL;

	      /* Now we have something like "2.1" and need to
	       * search for the highest "1.x" version.
	       */

	      do
		{
		  sprintf (mbuf, "%d.%d", br, i++);
		  p = p2;
		}
	      while ((p2 = g_hash_table_lookup (hash, mbuf)));

	      if (p == NULL)
		{
		  rcs->root_version = v;
		  return;
		}

	      break;
	    }
	  else
	    {
	      /* 1.2.3.1 => 1.2 */
	      (*lastdot) = 0;
	      lastdot = strrchr (mbuf, '.');
	      lastn = atoi (lastdot + 1);
	    }
	}
      else
	{
	  lastn -= 1;
	  sprintf (lastdot, ".%d", lastn);
	}
    }
  while (! (p = g_hash_table_lookup (hash, mbuf)));

  g_assert (p);

  v->parent = p;

  p->children = g_slist_prepend (p->children, v);
}

int
rcswalk_traverse_graph (RcsFile* rcs, RcsVersion* version, RcsVersion *parent)
{
  GSList *c;
  int distance = -1;

  version->cc = g_slist_length (version->children);

  if (version->cc > 1)
    rcs->branch_count += (version->cc - 1);

  if (parent)
    {
      /* Insure that there is proper date ordering. */
      if (version->date <= parent->date)
	version->date = parent->date + 1;

      if (parent->on_trunk && version->on_trunk)
	rcs->reverse_count += 1;
      else
	rcs->forward_count += 1;
    }

  for (c = version->children; c; c = c->next)
    {
      int c_dist = rcswalk_traverse_graph (rcs, c->data, version);

      distance = MAX (distance, c_dist);
    }

  if (version == rcs->head_version)
    distance = 0;

  if (distance >= 0)
    {
      version->chain_length = distance;

      return distance + 1;
    }

  return -1;
}

void
rcswalk_compute_chain_length (RcsFile* rcs, RcsVersion* version, RcsVersion *parent)
{
  GSList *c;

  if (! parent)
    {
      g_assert (version->chain_length >= 0);
    }
  else if (version->chain_length < 0)
    {
      version->chain_length = parent->chain_length + 1;
    }

  for (c = version->children; c; c = c->next)
    {
      rcswalk_compute_chain_length (rcs, c->data, version);
    }
}

int
rcswalk_date_compare (const void* a, const void* b)
{
  RcsVersion **ra = (void*) a;
  RcsVersion **rb = (void*) b;

  return (*ra)->date - (*rb)->date;
}

gboolean
rcswalk_build_graph (RcsFile* rcs)
{
  GHashTable* hash = g_hash_table_new (g_str_hash, g_str_equal);
  int i;

  for (i = 0; i < rcs->version_count; i += 1)
    g_hash_table_insert (hash, rcs->versions[i].vname, rcs->versions + i);

  for (i = 0; i < rcs->version_count; i += 1)
    {
      RcsVersion *v = rcs->versions + i;

      v->chain_length = -1;

      rcswalk_find_parent (rcs, hash, v);
    }

  rcs->head_version = g_hash_table_lookup (hash, rcs->headname);

  rcswalk_traverse_graph (rcs, rcs->root_version, NULL);

  rcswalk_compute_chain_length (rcs, rcs->root_version, NULL);

  for (i = 0; i < rcs->version_count; i += 1)
    rcs->versions_date[i] = rcs->versions + i;

  qsort (rcs->versions_date, rcs->version_count, sizeof (RcsVersion*), & rcswalk_date_compare);

  for (i = 0; i < rcs->version_count; i += 1)
    {
      RcsVersion *v = rcs->versions_date[i];

      v->dateseq = i;
    }

  g_hash_table_destroy (hash);

  return TRUE;
}

#define HEAD_STATE 0
#define BAR_STATE 1
#define REV_STATE 2
#define DATE_STATE 3

gboolean
rcswalk_load (RcsFile *rcs, gboolean *skip)
{
  FILE* rlog;
  char cmdbuf[1024];
  char oneline[1024], *oneline_p;
  char rbuf[1024];
  int version_i = 0;
  int read_state = HEAD_STATE;

  sprintf (cmdbuf, "rlog %s", rcs->filename);

  if (! (rlog = popen (cmdbuf, "r")))
    {
      g_warning ("popen failed: %s", cmdbuf);
      return FALSE;
    }

  while (fgets (oneline, 1024, rlog))
    {
      oneline_p = oneline;

      if (read_state == HEAD_STATE && rcswalk_match (& oneline_p, "total revisions: "))
	{
	  if (sscanf (oneline_p, "%d", & rcs->version_count) != 1)
	    goto badscan;

	  rcs->versions = g_new0 (RcsVersion, rcs->version_count);
	  rcs->versions_date = g_new (RcsVersion*, rcs->version_count);
	  read_state = BAR_STATE;
	}
      else if (read_state == HEAD_STATE && rcswalk_match (& oneline_p, "head: "))
	{
	  if (sscanf (oneline_p, "%s", rbuf) != 1)
	    goto badscan;

	  rcs->headname = g_strdup (rbuf);
	  read_state = HEAD_STATE; /* no change */
	}
      else if (read_state == BAR_STATE && rcswalk_match (& oneline_p, "----------------------------"))
	{
	  read_state = REV_STATE;
	}
      else if (read_state == REV_STATE && rcswalk_match (& oneline_p, "revision "))
	{
	  if (version_i >= rcs->version_count)
	    {
	      /* Jordan likes to insert the rlog of one RCS file into the log
	       * message of another, and this can confuse things.  Why, oh why,
	       * doesn't rlog have an option to not print the log?
	       */
	      fprintf (stderr, "rcswalk: too many versions: skipping file %s\n", rcs->filename);
	      *skip = TRUE;
	      skip_count += 1;
	      return TRUE;
	    }

	  if (sscanf (oneline_p, "%s", rbuf) != 1)
	    goto badscan;

	  rcs->versions[version_i].vname = g_strdup (rbuf);
	  read_state = DATE_STATE;

	  g_assert (rcs->versions[version_i].vname);
	}
      else if (read_state == DATE_STATE && rcswalk_match (& oneline_p, "date: "))
	{
	  char* semi = strchr (oneline_p, ';');

	  if (! semi)
	    goto badscan;

	  strncpy (rbuf, oneline_p, semi - oneline_p);

	  rbuf[semi - oneline_p] = 0;

	  rcs->versions[version_i].date = str2time (rbuf, 0, 0);

	  version_i += 1;
	  read_state = BAR_STATE;
	}
    }

  if (pclose (rlog) < 0)
    {
      g_warning ("pclose failed: %s", cmdbuf);
      return FALSE;
    }

  if (! rcswalk_build_graph (rcs))
    return FALSE;

  return TRUE;

 badscan:

  g_warning ("rlog syntax error");
  return FALSE;
}

void
rcswalk_free (RcsFile* rcs)
{
  int i;

  for (i = 0; i < rcs->version_count; i += 1)
    {
      g_free (rcs->versions[i].vname);
      g_slist_free (rcs->versions[i].children);
    }

  g_free (rcs->filename);
  g_free (rcs->headname);
  g_free (rcs->versions);
  g_free (rcs->versions_date);
  g_free (rcs);
}

gboolean
rcswalk_one (char* rcsfile, char* copyfile, RcsWalker* walker, RcsStats* stats, void* data)
{
  RcsFile* rcs;
  int i;
  long long maxsize = 0;
  gboolean skip = FALSE;

  rcs = g_new0 (RcsFile, 1);

  rcs->filename = g_strdup (rcsfile);
  rcs->copyname = copyfile;

  if (! rcswalk_load (rcs, & skip))
    return FALSE;

  if (! skip)
    {
      if (walker->dateorder && ! rcswalk_dateorder (rcs, walker, stats, data))
	return FALSE;

      if (walker->delta)
	{
	  int count = 0;

	  if (! rcswalk_delta (rcs, walker, rcs->root_version, & count, data))
	    return FALSE;

	  g_assert (count == (rcs->version_count - 1));
	}

      for (i = 0; i < rcs->version_count; i += 1)
	{
	  rcs->total_size += rcs->versions[i].size;
	  maxsize = MAX (rcs->versions[i].size, maxsize);
	}

      stat_int_add_item (stats->version_stat, rcs->version_count);
      stat_int_add_item (stats->forward_stat, rcs->forward_count);
      stat_int_add_item (stats->reverse_stat, rcs->reverse_count);
      stat_int_add_item (stats->branch_stat, rcs->branch_count);
      stat_int_add_item (stats->unencoded_stat, rcs->total_size);
      stat_int_add_item (stats->literal_stat, maxsize);

      if (walker->onefile && ! walker->onefile (rcs, stats, data))
	return FALSE;
    }

  rcswalk_free (rcs);

  return TRUE;
}

gboolean
rcswalk_create_dir (const char* dirname)
{
  struct stat buf;

  if (stat (dirname, & buf) < 0)
    {
      if (mkdir (dirname, 0777) < 0)
	{
	  fprintf (stderr, "mkdir failed: %s\n", dirname);
	  return FALSE;
	}
    }
  else
    {
      if (! S_ISDIR (buf.st_mode))
	{
	  fprintf (stderr, "not a directory: %s\n", dirname);
	  return FALSE;
	}
    }

  return TRUE;
}

gboolean
rcswalk_dir (char* dir, RcsWalker* walker, RcsStats* stats, void* data, char* copy_dir)
{
  DIR* thisdir;
  struct dirent* ent;

  if (copy_dir && ! rcswalk_create_dir (copy_dir))
    return FALSE;

  if (! (thisdir = opendir (dir)))
    {
      g_warning ("opendir failed: %s", dir);
      return FALSE;
    }

  while ((ent = readdir (thisdir)))
    {
      char* name = ent->d_name;
      int len;
      struct stat buf;
      char* fullname;
      char* copyname = NULL;

      if (strcmp (name, ".") == 0)
	continue;

      if (strcmp (name, "..") == 0)
	continue;

      len = strlen (name);

      fullname = g_strdup_printf ("%s/%s", dir, name);

      if (copy_dir)
	copyname = g_strdup_printf ("%s/%s", copy_dir, name);

      if (len > 2 && strcmp (name + len - 2, ",v") == 0)
	{
	  if (! rcswalk_one (fullname, copyname, walker, stats, data))
	    goto abort;
	}
      else
	{
	  if (stat (fullname, & buf) < 0)
	    {
	      g_warning ("stat failed: %s\n", fullname);
	      goto abort;
	    }

	  if (S_ISDIR (buf.st_mode))
	    {
	      if (! rcswalk_dir (fullname, walker, stats, data, copyname))
		goto abort;
	    }
	}

      g_free (fullname);

      if (copyname)
	g_free (copyname);
    }

  if (closedir (thisdir) < 0)
    {
      g_warning ("closedir failed: %s", dir);
      return FALSE;
    }

  return TRUE;

 abort:

  if (thisdir)
    closedir (thisdir);

  return FALSE;
}

gboolean
rcswalk (RcsWalker *walker, int argc, char** argv, char* copy_base)
{
  int i;
  void* data = NULL;
  RcsStats stats;

  skip_count = 0;

  memset (& stats, 0, sizeof (stats));

  stats.avg_version_size = stat_bincount_new ("AvgVersionSize"); /* @@@ leak */
  stats.version_stat = stat_int_new ("Version", 10, "files", "versions"); /* @@@ leak */
  stats.forward_stat = stat_int_new ("Forward", 10, "files", "versions"); /* @@@ leak */
  stats.reverse_stat = stat_int_new ("Reverse", 10, "files", "versions"); /* @@@ leak */
  stats.branch_stat  = stat_int_new ("Branch",  10, "files", "branches"); /* @@@ leak */
  stats.unencoded_stat = stat_int_new ("Unencoded", 100000, "files", "bytes"); /* @@@ leak */
  stats.literal_stat   = stat_int_new ("Literal",   10000, "files", "bytes"); /* @@@ leak */

  tmp_file_1 = g_strdup_printf ("%s/dt1.%d", g_get_tmp_dir (), (int) getpid ());
  tmp_file_2 = g_strdup_printf ("%s/dt2.%d", g_get_tmp_dir (), (int) getpid ());

  if (walker->initialize)
    data = walker->initialize ();

  for (i = 0; i < argc; i += 1)
    {
      if (! rcswalk_dir (argv[i], walker, & stats, data, copy_base))
	return FALSE;
    }

  if (walker->finalize)
    {
      if (! walker->finalize (& stats, data))
	return FALSE;
    }

  unlink (tmp_file_1);
  unlink (tmp_file_2);

  fprintf (stderr, "rcswalk: skipped %d files\n", skip_count);

  return TRUE;
}

/* Statistics
 */

void
rcswalk_report (RcsStats* set)
{
  stat_bincount_report (set->avg_version_size);
  stat_int_report (set->version_stat);
  stat_int_report (set->forward_stat);
  stat_int_report (set->reverse_stat);
  stat_int_report (set->branch_stat);
  stat_int_report (set->unencoded_stat);
  stat_int_report (set->literal_stat);
}

IntStat*
stat_int_new (const char* name, int binsize, const char* popunit, const char* eltunit)
{
  IntStat* s = g_new0 (IntStat, 1);

  s->name = name;
  s->popunit = popunit;
  s->eltunit = eltunit;
  s->binsize = binsize;
  s->values = g_array_new (FALSE, FALSE, sizeof (long long));

  return s;
}

void
stat_int_add_item (IntStat* stat, long long v)
{
  if (! stat->count)
    stat->min = v;
  stat->count += 1;
  stat->min = MIN (v, stat->min);
  stat->max = MAX (v, stat->max);
  stat->sum += v;

  g_array_append_val (stat->values, v);
}

double
stat_int_stddev (IntStat *stat)
{
  double f = 0;
  double m = (double) stat->sum / (double) stat->count;
  double v;
  int i;

  for (i = 0; i < stat->count; i += 1)
    {
      long long x = g_array_index (stat->values, long long, i);

      f += (m - (double) x) * (m - (double) x);
    }

  v = f / (double) stat->count;

  return sqrt (v);
}

int
ll_comp (const void* a, const void* b)
{
  const long long* lla = a;
  const long long* llb = b;
  return (*lla) - (*llb);
}

const char* _rcswalk_output_dir = ".";

void
stat_int_histogram (IntStat *stat)
{
  int i, consec;
  long long cum = 0;

  char* ecdf_p = g_strdup_printf ("%s/%s.pop.hist", _rcswalk_output_dir, stat->name); /* weighted by population */
  char* ecdf_s = g_strdup_printf ("%s/%s.sum.hist", _rcswalk_output_dir, stat->name); /* weighted by sum */

  FILE* p_out = fopen (ecdf_p, "w");
  FILE* s_out = fopen (ecdf_s, "w");

  g_free (ecdf_p);
  g_free (ecdf_s);

  g_assert (p_out && s_out);

  qsort (stat->values->data, stat->count, sizeof (long long), ll_comp);

  for (i = 0; i < stat->count; i += consec)
    {
      long long ix = g_array_index (stat->values, long long, i);

      for (consec = 1; (i+consec) < stat->count; consec += 1)
	{
	  long long jx = g_array_index (stat->values, long long, i+consec);

	  if (ix != jx)
	    break;
	}

      cum += consec * g_array_index (stat->values, long long, i);

      fprintf (p_out, "%qd, %0.3f\n", g_array_index (stat->values, long long, i), (double) i / (double) stat->count);
      fprintf (s_out, "%qd, %0.3f\n", g_array_index (stat->values, long long, i), (double) cum / (double) stat->sum);
    }

  if (fclose (p_out) < 0 ||
      fclose (s_out) < 0)
    {
      g_error ("fclose failed\n");
    }
}

void
stat_int_report (IntStat* stat)
{
  static int once = 0;
  char* outfile = g_strdup_printf ("%s/%s.stat", _rcswalk_output_dir, stat->name);
  FILE *out = fopen (outfile, "w");

  if (! once)
    {
      char* keyfile = g_strdup_printf ("%s/key", _rcswalk_output_dir);
      FILE* key = fopen (keyfile, "w");

      once = 1;

      g_free (keyfile);
      g_assert (key);

      fprintf (key, "Name Count Min Max Sum Mean Stddev\n");

      if (fclose (key) < 0)
	g_error ("fclose failed\n");
    }

  g_assert (out);
  g_free (outfile);

  fprintf (out, "%s %d %qd %qd %qd %0.2f %0.2f\n",
	   stat->name,
	   stat->count,
	   stat->min,
	   stat->max,
	   stat->sum,
	   (double) stat->sum / (double) stat->count,
	   stat_int_stddev (stat));

  if (fclose (out) < 0)
    g_error ("fclose failed");

  stat_int_histogram (stat);
}

gboolean
rcswalk_output_dir (const char* dirname)
{
  if (! rcswalk_create_dir (dirname))
    return FALSE;

  _rcswalk_output_dir = g_strdup (dirname);

  return TRUE;
}

BinCounter*
stat_bincount_new (const char* name)
{
  BinCounter* bc = g_new0 (BinCounter, 1);

  bc->name = name;
  bc->sums   = g_array_new (FALSE, TRUE, sizeof (double));
  bc->counts = g_array_new (FALSE, TRUE, sizeof (int));

  return bc;
}

void
stat_bincount_add_item (BinCounter* bc, int bin, double val)
{
  if (bin >= bc->max_bin)
    {
      bc->max_bin = bin + 1;
      g_array_set_size (bc->sums, bin+1);
      g_array_set_size (bc->counts, bin+1);
    }

  g_array_index (bc->sums, double, bin) += val;
  g_array_index (bc->counts, int, bin) += 1;
}

void
stat_bincount_report (BinCounter* bc)
{
  char* outfile = g_strdup_printf ("%s/%s.avg", _rcswalk_output_dir, bc->name);
  FILE *out = fopen (outfile, "w");
  int i;

  g_assert (out);
  g_free (outfile);

  for (i = 0; i < bc->max_bin; i += 1)
    {
      fprintf (out, "%f %d\n",
	       g_array_index (bc->sums, double, i) / (double) g_array_index (bc->counts, int, i),
	       g_array_index (bc->counts, int, i));
    }

  if (fclose (out) < 0)
    g_error ("fclose failed");
}
