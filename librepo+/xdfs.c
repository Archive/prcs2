/* -*-Mode: C;-*-
 * $Id: dbfs.c,v 1.1 1999/08/27 04:03:50 jmacd Exp $
 *
 * Copyright (C) 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include "xdelta.h"

#define NO_INVERSE_CODE

#define MAX_XDFS_SOURCES 128

static Path* xidx_path;
static Path* xstate_path;
static Path* xseq_path;
static Path* xcur_path;
static Path* xsrcdir_path;

static const MessageDigest *xdfs_message_digest;
static ViewDef             *xdfs_reconstruct_view;
static guint64              xdfs_reconstruct_view_id = 67;

#ifndef NO_INVERSE_CODE
static BaseName      *xdfs_inverse_name;
static BaseName      *xdfs_inverse_ptr_name;
#endif

typedef struct _XdfsLocation            XdfsLocation;
typedef struct _XdfsSourceStats         XdfsSourceStats;
typedef struct _SerialXdfsInstruction   XdfsInstruction;
typedef struct _XdfsLocalIndex          XdfsLocalIndex;

struct _XdfsLocalIndex {
  XdfsInstruction *inst;
  guint            inst_len;
  guint            length;
  Inode           *inop;
  GSList          *free_list;
  GArray          *inst_array;
};

struct _XdfsLocation {
  Inode  seq_ino;
  Inode  state_ino;
  Inode  idx_ino;
  Inode  dir_ino;
  Inode  srcdir_ino;

  XdfsState *state;
};

static gboolean xdfs_insert_not_found  (RepoTxn         *txn,
				        XdfsLocation    *loc,
				        Path            *copy_digest_path,
				        Inode           *copy_ino);
static gboolean xdfs_source_reverse_segments (RepoTxn         *txn,
					      XdfsLocation    *loc,
					      XdeltaGenerator *gen);
static gboolean xdfs_source_segment    (RepoTxn         *txn,
					XdfsLocation    *loc,
				        XdeltaGenerator *gen,
					gboolean         reverse,
				        Inode           *from);
static gboolean xdfs_source_evolve     (RepoTxn         *txn,
					XdfsLocation    *loc,
					Inode           *patch_ino);
static gboolean xdfs_reject_delta      (RepoTxn         *txn,
				        XdfsLocation    *loc,
				        XdeltaControl   *control,
				        Inode           *patch_ino,
				        gboolean        *reject_delta);
static gboolean xdfs_update_inverses   (RepoTxn         *txn,
				        XdeltaControl   *control);
static gboolean xdfs_control_read      (RepoTxn         *txn,
					Inode           *inode,
					XdeltaControl  **control_out);
static gboolean xdfs_control_write     (RepoTxn         *txn,
					Inode           *inode,
					XdeltaControl   *control,
					Inode           *cino_out);
static gboolean xdfs_location_init     (RepoTxn         *txn,
					Path            *xdfs,
					XdfsLocation    *loc,
					gboolean         read_settings);

static ViewPgin  xdfs_view_page_in;
static ViewBegin xdfs_view_begin;
static ViewEnd   xdfs_view_end;

gboolean
xdfs_library_init (void)
{
  if (! xd_edsio_init ())
    return FALSE;

  xidx_path    = path_append_string (NULL, path_current (NULL), PP_String, "xidx");
  xseq_path    = path_append_string (NULL, path_current (NULL), PP_String, "xseq");
  xstate_path  = path_append_string (NULL, path_current (NULL), PP_String, "xsta");
  xcur_path    = path_append_string (NULL, path_current (NULL), PP_String, "xcur");
  xsrcdir_path = path_append_string (NULL, path_current (NULL), PP_String, "xsrc");

  xdfs_message_digest = edsio_message_digest_md5 ();

  xdfs_reconstruct_view = dbfs_view_definition (xdfs_reconstruct_view_id,
						sizeof (XdfsLocalIndex),
						xdfs_view_begin,
						xdfs_view_page_in,
						xdfs_view_end);

  return TRUE;
}

gboolean
xdfs_container_sequence (RepoTxn    *txn,
			 Path       *xdfs,
			 Inode      *inop)
{
  XdfsLocation loc;

  if (! xdfs_location_init (txn, xdfs, & loc, FALSE))
    return FALSE;

  (* inop) = loc.seq_ino;

  return TRUE;
}

gboolean
xdfs_state_read (RepoTxn      *txn,
		 XdfsLocation *loc)
{
  SerialSource *src;
  FileHandle   *set_in;

  if (! (set_in = dbfs_inode_open_read (txn, & loc->state_ino)))
    return FALSE;

  src = handle_source (set_in);

  if (! unserialize_xdfsstate (src, & loc->state))
    return FALSE;

  if (! handle_close (set_in))
    return FALSE;

  handle_free (set_in);
  src->source_free (src);

  return TRUE;
}

gboolean
xdfs_state_write (RepoTxn      *txn,
		  XdfsLocation *loc)
{
  SerialSink *sink;
  FileHandle *set_out;

  if (! (set_out = dbfs_inode_open_replace (txn, & loc->state_ino)))
    return FALSE;

  sink = handle_sink (set_out, NULL, NULL, NULL, NULL);

  if (! serialize_xdfsstate_obj (sink, loc->state))
    return FALSE;

  if (! handle_close (set_out))
    return FALSE;

  handle_free (set_out);
  sink->sink_free (sink);

  return TRUE;
}

gboolean
xdfs_stat (RepoTxn    *txn,
	   Path       *xdfs,
	   XdfsState  *state)
{
  XdfsLocation loc;

  if (! xdfs_location_init (txn, xdfs, & loc, TRUE))
    return FALSE;

  memcpy (state, loc.state, sizeof (*state));

  g_free (loc.state);

  return TRUE;
}

const char*
xdfs_policy_to_string (XdfsPolicy policy)
{
  switch (policy)
    {
    case XP_ReverseJump:
      return "Reverse Jump";
    case XP_ForwardJump:
      return "Forward Jump";
    case XP_Forward:
      return "Forward";
    case XP_NoCompress:
      return "Forward";
    }

  return "Error";
}

gboolean
xdfs_state_print (FileHandle *handle,
		  Path       *xdfs,
		  XdfsState  *state)
{
  GString *ps = g_string_new (NULL);

  guint32 encoded_size = state->literal_size + state->control_size + state->patch_size;

  path_to_host_string (_fs_pthn, xdfs, ps);

  if (! handle_printf (handle, "XDFS stats for file:         %s\n", ps->str) ||
      ! handle_printf (handle, "Version count:               %d\n", state->version_count) ||
      ! handle_printf (handle, "Average file size:           %d\n", state->unencoded_size / state->version_count) ||
      ! handle_printf (handle, "Unencoded size:              %d\n", state->unencoded_size) ||
      ! handle_printf (handle, "Encoded size:                %d\n", encoded_size) ||
      ! handle_printf (handle, "Literal size:                %d\n", state->literal_size) ||
      ! handle_printf (handle, "Control size:                %d\n", state->control_size) ||
      ! handle_printf (handle, "Patch size:                  %d\n", state->patch_size) ||
      ! handle_printf (handle, "Ideal compression:           %0.2f%%\n",
		       100.0 * (1.0 - (double) encoded_size / (double) state->unencoded_size)) ||
      ! handle_printf (handle, "Policy:                      %s\n", xdfs_policy_to_string (state->policy)) ||
      ! handle_printf (handle, "Delta reject threshhold:     %d%%\n", state->delta_reject_percent) ||
      ! handle_printf (handle, "Patch files:                 %d\n", state->patches) ||
      ! handle_printf (handle, "Literal files:               %d\n", state->literals) ||
      ! handle_printf (handle, "Source buffer files:         %d\n", state->src_buffer_files) ||
      ! handle_printf (handle, "Source buffer size:          %d\n", state->src_buffer_size) ||
      ! handle_printf (handle, "Source buffer max files:     %d\n", state->src_buffer_max_files) ||
      ! handle_printf (handle, "Source buffer max size:      %d\n", state->src_buffer_max_size) ||
      ! handle_printf (handle, "Source buffer max size/file: %d\n", state->src_buffer_max_size_per_file) ||
      ! handle_printf (handle, "Source buffer min size/file: %d\n", state->src_buffer_min_size_per_file))
    return FALSE;

  g_string_free (ps, TRUE);

  return TRUE;
}

gboolean
xdfs_state_init (XdfsLocation *loc,
		 XdfsState    *state,
		 XdfsParams   *params)
{
  loc->state = state;

  memset (state, 0, sizeof (*state));

  /* Default values */
  state->policy                       = XP_ReverseJump;
  state->delta_reject_percent         = 50;
  state->src_buffer_max_files         = 4;
  state->src_buffer_max_size          = (1 << 26);
  state->src_buffer_max_size_per_file = (1 << 24);
  state->src_buffer_min_size_per_file = 256;

  /* User inputs */
  if (params)
    {
      if (params->policy != 0)
	state->policy = params->policy;

      if (params->delta_reject_percent != 0)
	state->delta_reject_percent = params->delta_reject_percent;

      if (params->src_buffer_max_files != 0)
	state->src_buffer_max_files = params->src_buffer_max_files;

      if (params->src_buffer_max_size != 0)
	state->src_buffer_max_size = params->src_buffer_max_size;

      if (params->src_buffer_max_size_per_file != 0)
	state->src_buffer_max_size_per_file = params->src_buffer_max_size_per_file;

      if (params->src_buffer_min_size_per_file != 0)
	state->src_buffer_min_size_per_file = params->src_buffer_min_size_per_file;
    }

  return TRUE;
}

gboolean
xdfs_location_create (RepoTxn    *txn,
		      Path       *xdfs,
		      XdfsParams *params)
{
  XdfsLocation loc;
  XdfsState    state;

  if (! dbfs_inode_find_root (txn, xdfs, DbfsFollowLinks, FT_Directory, & loc.dir_ino))
    return FALSE;

  if (! dbfs_inode_new (txn, & loc.seq_ino))
    return FALSE;

  if (! dbfs_inode_new (txn, & loc.state_ino))
    return FALSE;

  if (! dbfs_inode_new (txn, & loc.idx_ino))
    return FALSE;

  if (! dbfs_inode_new (txn, & loc.srcdir_ino))
    return FALSE;

  if (! dbfs_make_sequence (txn, & loc.seq_ino))
    return FALSE;

  if (! dbfs_make_sequence (txn, & loc.srcdir_ino))
    return FALSE;

  if (! dbfs_make_index (txn, & loc.idx_ino, FALSE))
    return FALSE;

  if (! dbfs_link_create (txn, & loc.dir_ino, & loc.seq_ino, path_basename (xseq_path), DbfsNoOverwrite))
    return FALSE;

  if (! dbfs_link_create (txn, & loc.dir_ino, & loc.state_ino, path_basename (xstate_path), DbfsNoOverwrite))
    return FALSE;

  if (! dbfs_link_create (txn, & loc.dir_ino, & loc.idx_ino, path_basename (xidx_path), DbfsNoOverwrite))
    return FALSE;

  if (! dbfs_link_create (txn, & loc.dir_ino, & loc.srcdir_ino, path_basename (xsrcdir_path), DbfsNoOverwrite))
    return FALSE;

  if (! xdfs_state_init (& loc, & state, params))
    return FALSE;

  if (! xdfs_state_write (txn, & loc))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_location_init (RepoTxn      *txn,
		    Path         *xdfs,
		    XdfsLocation *loc,
		    gboolean      read_settings)
{
  memset (loc, 0, sizeof (* loc));

  if (! dbfs_inode_find_root (txn, xdfs, DbfsFollowLinks, FT_Directory, & loc->dir_ino))
    return FALSE;

  if (! dbfs_inode_find (txn, & loc->dir_ino, xseq_path, DbfsNoFollowLinks, FT_Sequence, & loc->seq_ino))
    return FALSE;

  if (! dbfs_inode_find (txn, & loc->dir_ino, xidx_path, DbfsNoFollowLinks, FT_Index, & loc->idx_ino))
    return FALSE;

  if (! dbfs_inode_find (txn, & loc->dir_ino, xsrcdir_path, DbfsNoFollowLinks, FT_Sequence, & loc->srcdir_ino))
    return FALSE;

  if (read_settings)
    {
      if (! dbfs_inode_find (txn, & loc->dir_ino, xstate_path, DbfsNoFollowLinks, FT_Segment, & loc->state_ino))
	return FALSE;

      if (! xdfs_state_read (txn, loc))
	return FALSE;
    }

  return TRUE;
}

gboolean
xdfs_insert_version (RepoTxn *txn, Path *xdfs, FileHandle *in)
{
  FileHandle   *copy;
  XdfsLocation  loc;
  Inode         old_ino;
  Inode         copy_ino;
  const guint8 *ins_digest;
  Path         *digest_path;
  gboolean      res = TRUE;

  /* Initialize the XDFS directories
   */
  if (! xdfs_location_init (txn, xdfs, & loc, TRUE))
    goto exit;

  /* Create the new file entry by draining IN, meanwhile compute digest.
   */
  if (! dbfs_inode_new (txn, & copy_ino))
    goto exit;

  if (! (copy = dbfs_inode_open_replace (txn, & copy_ino)))
    goto exit;

  if (! handle_digest_compute (copy, xdfs_message_digest))
    goto exit;

  if (! handle_drain (in, copy))
    goto exit;

  if (! handle_close (in))
    goto exit;

  if (! handle_close (copy))
    goto exit;

  if (! (ins_digest = handle_digest (copy, xdfs_message_digest)))
    goto exit;

  /* Look for the digest entry in the index, to see if it already exists.
   */
  digest_path = path_append_bytes (txn->alloc, path_current (txn->alloc), PP_Hex, ins_digest, xdfs_message_digest->cksum_len);

  handle_free (copy);

  if (! dbfs_inode_find (txn, & loc.idx_ino, digest_path, DbfsNoFollowLinks, FT_IsAny, & old_ino))
    goto exit;

  /* Link new version into the sequence
   */
  if (! dbfs_link_create_next (txn, & loc.seq_ino, & copy_ino))
    return FALSE;

  switch (old_ino.minor.ino_type)
    {
    case FT_NotFound:

      res = xdfs_insert_not_found (txn, & loc, digest_path, & copy_ino);
      break;

    case FT_Segment:
    case FT_View:

      /* Copy is not referenced, will delete at commit.  Could delete now.
       */
      break;

    default:
      /*dbfs_generate_inode_event (EC_XdfsUnexpectedFileType, & old_ino);*/
      /* @@@ */
      abort ();
    }

  if (0)
    {
    exit:
      res = FALSE;
    }

  if (loc.state)
    g_free (loc.state);

  return res;
}

gboolean
xdfs_insert_not_found (RepoTxn      *txn,
		       XdfsLocation *loc,
		       Path         *copy_digest_path,
		       Inode        *copy_ino)
{
  Inode            cur_ino;
  Inode            patch_ino;
  Inode            from_ino;
  Inode            to_ino;
  Inode            cont_ino;
  XdeltaGenerator *gen;
  FileHandle      *delta_to;
  FileHandle      *patch_out;
  XdeltaControl   *control;
  const guint8    *patch_digest;
  Path            *patch_digest_path;
  gboolean         reject_delta;
  XdfsPolicy       policy = loc->state->policy;

  loc->state->literals += 1;
  loc->state->version_count  += 1;
  loc->state->unencoded_size += copy_ino->minor.ino_segment_len;
  loc->state->literal_size += copy_ino->minor.ino_segment_len;

  /* Insert the link into the index.
   */
  if (! dbfs_link_create (txn, & loc->idx_ino, copy_ino, path_basename (copy_digest_path), DbfsNoOverwrite))
    return FALSE;

  /* Lookup current version in xdfsdir.
   */
  if (! dbfs_inode_find (txn, & loc->dir_ino, xcur_path, DbfsNoFollowLinks, FT_IsAny, & cur_ino))
    return FALSE;

  if (policy == XP_NoCompress)
    goto skipcompress;

  switch (cur_ino.minor.ino_type)
    {
    case FT_NotFound:
      /* If the current link isn't there, then this is the first version.
       */
      if (! dbfs_link_create (txn, & loc->dir_ino, copy_ino, path_basename (xcur_path), DbfsNoOverwrite))
	return FALSE;

      goto skipcompress;

    case FT_Segment:
      /* Continue.
       */
      break;

    default:
      dbfs_generate_int_event (EC_DbfsInvalidFileType, cur_ino.key.inum);
      return FALSE;
    }

  /* Prepare to compute the delta
   */
  gen = xdp_generator_new (xdfs_message_digest);

  if (policy & XP_IsReverse)
    {
      /* Current is being replaced by reverse delta.
       */
      if (! xdfs_source_reverse_segments (txn, loc, gen))
	return FALSE;

      from_ino = *copy_ino;
      to_ino = cur_ino;
    }
  else
    {
      /* Current stays the same, compute a forward delta.
       */
      from_ino = cur_ino;
      to_ino = *copy_ino;
    }

  if (! xdfs_source_segment (txn, loc, gen, FALSE, & from_ino))
    return FALSE;

  if (! (delta_to = dbfs_inode_open_read (txn, & to_ino)))
    return FALSE;

  /* Now compute the delta.
   */
  if (! dbfs_inode_new (txn, & patch_ino))
    return FALSE;

  if (! (patch_out = dbfs_inode_open_replace (txn, & patch_ino)))
    return FALSE;

  if (! (control = xdp_generate_delta (gen, delta_to, NULL, patch_out)))
    return FALSE;

  if (! handle_close (delta_to))
    return FALSE;

  handle_free (delta_to);

  if (! (patch_digest = handle_digest (patch_out, xdfs_message_digest)))
    return FALSE;

  patch_digest_path = path_append_bytes (txn->alloc, path_current (txn->alloc), PP_Hex, patch_digest, xdfs_message_digest->cksum_len);

  handle_free (patch_out);

  xdp_generator_free (gen);

  /* Decide whether to reject this delta.
   */
  if (! xdfs_reject_delta (txn, loc, control, & patch_ino, & reject_delta))
    return FALSE;

  if (reject_delta)
    {
      /* Delete the patch... Well it has no references so it will be
       * deleted on commit.  Could delete now.
       */
    }
  else
    {
      int i;

      /* Create view in to_ino, replacing old contents.
       */
      loc->state->literals -= 1;
      loc->state->literal_size -= to_ino.minor.ino_segment_len;
      loc->state->patch_size   += patch_ino.minor.ino_segment_len;

      if (! dbfs_inode_delete (txn, & to_ino))
	return FALSE;

      if (! dbfs_make_view (txn, & to_ino, to_ino.minor.ino_segment_len, xdfs_reconstruct_view))
	return FALSE;

      /* Create links to its sources (one may be the patch)
       */
      for (i = 0; i < xdp_control_source_count (control); i += 1)
	{
	  Inode   *src_ino = xdp_control_source_data (control, i);
	  BaseName si;
	  guint16  si_be = GUINT16_TO_BE (i);

	  if (control->has_data && i == 0)
	    {
	      g_assert (src_ino == NULL);
	      src_ino = & patch_ino;
	    }
	  else
	    {
	      g_assert (src_ino);
	    }

	  si.data = (void*) & si_be;
	  si.len  = sizeof (si_be);

	  if (! dbfs_make_indirect_link_stacked_named (txn, & to_ino, to_ino.minor.ino_stack_id, & si, src_ino))
	    return FALSE;
	}

      /* Write its control.
       */
      if (! xdfs_control_write (txn, & to_ino, control, & cont_ino))
	return FALSE;

      loc->state->control_size += cont_ino.minor.ino_segment_len;

      if (control->has_data)
	loc->state->patches += 1;

      /* Decide whether to place patch in source set, what to remove,
       * etc.  This only happens if the delta generated a patch and if
       * the file is presumed to be evolving.
       */
      if (control->has_data && (policy & XP_IsEvolving))
	{
	  if (! xdfs_source_evolve (txn, loc, & patch_ino))
	    return FALSE;
	}

      /* Update inverses.
       */
      if (! xdfs_update_inverses (txn, control))
	return FALSE;
    }

  if ((policy & XP_IsReverse) || ((policy & XP_IsEvolving) && reject_delta))
    {
      /* If Reverse is true or ForwardJump indicates JUMP, replace the
       * current version (CUR) with the new file (COPY).
       */
      if (! dbfs_link_create (txn, & loc->dir_ino, copy_ino, path_basename (xcur_path), DbfsOverwrite))
	return FALSE;
    }

  xdp_control_free (control);

 skipcompress:

  if (! xdfs_state_write (txn, loc))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_source_reverse_segments (RepoTxn         *txn,
			      XdfsLocation    *loc,
			      XdeltaGenerator *gen)
{
  /* Open the SRC sequence, where a history of recent patches is
   * stored and later used in the computation of reverse deltas.
   */

  Cursor *c = NULL;

  if (! (c = dbfs_cursor_open (txn, & loc->srcdir_ino, CT_Container)))
    goto exit;

  while (dbfs_cursor_next (c))
    {
      Inode *one_ino = alc_new (txn->alloc, Inode);

      if (! dbfs_cursor_inode (c, FT_Segment, one_ino))
	goto exit;

      if (! xdfs_source_segment (txn, loc, gen, TRUE, one_ino))
	goto exit;
    }

  if (! dbfs_cursor_close (c))
    goto exit;

  return TRUE;

 exit:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

gboolean
xdfs_source_segment (RepoTxn         *txn,
		     XdfsLocation    *loc,
		     XdeltaGenerator *gen,
		     gboolean         reverse,
		     Inode           *src)
{
  XdeltaSource *xs;
  FileHandle *fh;

  guint32 this_len = src->minor.ino_segment_len;

  if (this_len > loc->state->src_buffer_max_size_per_file ||
      this_len < loc->state->src_buffer_min_size_per_file)
    return TRUE;

  if (reverse)
    {
      if (loc->state->src_buffer_files >= loc->state->src_buffer_max_files ||
	  loc->state->src_buffer_size + this_len >= loc->state->src_buffer_max_size)
	return TRUE;

      loc->state->src_buffer_files += 1;
      loc->state->src_buffer_size += this_len;
    }

  if (! (fh = dbfs_inode_open_read (txn, src)))
    goto exit;

  if (! (xs = xdp_source_new ("", fh, src)))
    goto exit;

  xdp_source_add (gen, xs);

  return TRUE;

 exit:

  if (fh)
    handle_close (fh);

  return FALSE;
}

gboolean
xdfs_source_evolve (RepoTxn         *txn,
		    XdfsLocation    *loc,
		    Inode           *patch_ino)
{
  guint32 this_len = patch_ino->minor.ino_segment_len;
  gboolean once = TRUE;
  Cursor *c = NULL;

  if (this_len > loc->state->src_buffer_max_size_per_file ||
      this_len < loc->state->src_buffer_min_size_per_file ||
      this_len > loc->state->src_buffer_max_size)
    return TRUE;

  loc->state->src_buffer_files += 1;
  loc->state->src_buffer_size += this_len;

  if (! dbfs_link_create_next (txn, & loc->srcdir_ino, patch_ino))
    goto exit;

  while (loc->state->src_buffer_files >= loc->state->src_buffer_max_files ||
	 loc->state->src_buffer_size >= loc->state->src_buffer_max_size)
    {
      Inode one_ino;

      if (once)
	{
	  once = FALSE;

	  if (! (c = dbfs_cursor_open (txn, & loc->srcdir_ino, CT_Container)))
	    goto exit;
	}

      if (! dbfs_cursor_next (c))
	goto exit;

      if (! dbfs_cursor_inode (c, FT_Segment, & one_ino))
	goto exit;

      loc->state->src_buffer_files -= 1;
      loc->state->src_buffer_size -= one_ino.minor.ino_segment_len;

      if (! dbfs_cursor_delete (c))
	goto exit;
    }

  if (c && ! dbfs_cursor_close (c))
    goto exit;

  return TRUE;

 exit:

  if (c)
    dbfs_cursor_close (c);

  return FALSE;
}

gboolean
xdfs_reject_delta (RepoTxn         *txn,
		   XdfsLocation    *loc,
		   XdeltaControl   *control,
		   Inode           *patch_ino,
		   gboolean        *reject_delta)
{
  guint32 patch_len     = patch_ino->minor.ino_segment_len;
  guint32 construct_len = control->to_len;
  double  ratio         = (double) patch_len / (double) construct_len;
  double  threshhold    = (double) loc->state->delta_reject_percent / 100.0;

  (* reject_delta) = ratio > threshhold;

  return TRUE;
}

gboolean
xdfs_control_write (RepoTxn         *txn,
		    Inode           *inode,
		    XdeltaControl   *control,
		    Inode           *cino_out)
{
  FileHandle *control_handle;

  if (! (control_handle = dbfs_inode_open_replace_stacked (txn, inode, inode->minor.ino_stack_id, cino_out)))
    return FALSE;

  if (! xdp_control_write (control, control_handle))
    return FALSE;

  if (! handle_close (control_handle))
    return FALSE;

  handle_free (control_handle);

  return TRUE;
}

gboolean
xdfs_control_read (RepoTxn         *txn,
		   Inode           *inode,
		   XdeltaControl  **control_out)
{
  FileHandle *control_handle;

  if (! (control_handle = dbfs_inode_open_read_stacked (txn, inode, inode->minor.ino_stack_id)))
    return FALSE;

  if (! ((* control_out) = xdp_control_read (control_handle)))
    return FALSE;

  if (! handle_close (control_handle))
    return FALSE;

  handle_free (control_handle);

  return TRUE;
}

/* This function translates the Xdelta instruction TRANSLATE into an
 * XdfsInstruction coresponding to the current, local, literal file
 * set.  TRANSLATE offsets refer to the TRANSLATE_INDEX.  The output
 * is in APPEND_INST_ARRAY.
 */
void
xdfs_view_translate_copies (XdeltaInstruction *translate,
			    XdfsLocalIndex    *translate_index,
			    GArray            *append_inst_array)
{
  guint l = 0;
  guint u = translate_index->inst_len - 1;
  guint i;
  guint search_offset = translate->offset;
  guint output_start = translate->output_start;
  XdfsInstruction *inst;

  g_assert (search_offset < translate_index->length);
  g_assert (translate->length > 0);

  /* Binary search */

 again:

  i = (u+l)/2;

  inst = & translate_index->inst[i];

  if (search_offset < inst->output_start)
    {
      u = i - 1;
      goto again;
    }
  else if (search_offset >= (inst->output_start + inst->length))
    {
      l = i + 1;
      goto again;
    }
  else
    {
      guint to_copy = translate->length;

      while (to_copy > 0)
	{
	  XdfsInstruction copy;
	  guint inst_offset;
	  guint this_copy;

	  inst_offset = search_offset - inst->output_start;
	  this_copy   = MIN (to_copy, inst->length - inst_offset);

	  copy.offset = inst->offset + inst_offset;
	  copy.length = this_copy;
	  copy.inop   = inst->inop;
	  copy.output_start = output_start;

	  g_array_append_val (append_inst_array, copy);

	  output_start += this_copy;
	  search_offset += this_copy;
	  to_copy -= this_copy;
	  inst += 1;
	}
    }
}

gboolean
xdfs_view_index_delta (RepoTxn        *txn,
		       Inode          *inode,
		       XdfsLocalIndex *lip,
		       XdfsLocalIndex *src_lis)
{
  guint i;
  XdeltaControl* control;
  GArray *inst_array;

  if (! xdfs_control_read (txn, inode, & control))
    return FALSE;

  inst_array = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));

  for (i = 0; i < control->inst_len; i += 1)
    {
      XdeltaInstruction *inst = & control->inst[i];
      XdfsLocalIndex *src_li = src_lis + inst->index;

      switch (src_li->inop->minor.ino_type)
	{
	case FT_Segment:
	  {
	    XdfsInstruction xi;

	    xi.inop     = src_li->inop;
	    xi.offset   = inst->offset;
	    xi.length   = inst->length;
	    xi.output_start = inst->output_start;

	    g_array_append_val (inst_array, xi);
	  }
	  break;
	case FT_View:
	  {
	    xdfs_view_translate_copies (inst, src_li, inst_array);
	  }
	  break;
	default:
	  /* @@@ */
	  return FALSE;
	}
    }

  lip->inst = (XdfsInstruction*) inst_array->data;
  lip->inst_len = inst_array->len;
  lip->length = control->to_len;
  lip->inst_array = inst_array;

  xdp_control_free (control);

  return TRUE;
}

gboolean
xdfs_view_index_source (RepoTxn *txn, Inode* inop, XdfsLocalIndex* lip, XdfsLocalIndex* root)
{
  lip->inop = inop;

  switch (inop->minor.ino_type)
    {
    case FT_Segment:
      break;

    case FT_View:
      {
	int i;
	XdfsLocalIndex *src_lis = g_new0 (XdfsLocalIndex, MAX_XDFS_SOURCES);

	for (i = 0; i < MAX_XDFS_SOURCES; i += 1)
	  {
	    BaseName si;
	    guint16  si_be = GUINT16_TO_BE (i);
	    Inode *src_ino;

	    si.data = (void*) & si_be;
	    si.len  = sizeof (si_be);

	    src_ino = g_new (Inode, 1);

	    root->free_list = g_slist_prepend (root->free_list, src_ino);

	    if (! dbfs_read_indirect_link_stacked_named (txn, inop, inop->minor.ino_stack_id, & si, FT_IsAny, src_ino))
	      return FALSE;

	    switch (src_ino->minor.ino_type)
	      {
	      case FT_NotFound:
		goto foundallsrcs;

	      case FT_View:
	      case FT_Segment:
		if (! xdfs_view_index_source (txn, src_ino, src_lis + i, root))
		  goto exit;

		break;

	      default:
		abort ();
	      }
	  }

      foundallsrcs:

	if (! xdfs_view_index_delta (txn, inop, lip, src_lis))
	  goto exit;

	for (i = 0; i < MAX_XDFS_SOURCES; i += 1)
	  {
	    if (src_lis[i].inst)
	      g_array_free (src_lis[i].inst_array, TRUE);
	  }

	g_free (src_lis);
      }
      break;

    default:
      abort ();
      /* @@@ */
      return FALSE;
    }

  return TRUE;

 exit:

  /* @@@ cleanup */
  return FALSE;
}

gboolean
xdfs_view_begin (RepoTxn *txn,
		 Inode   *inop,
		 void    *private)
{
  XdfsLocalIndex *li = private;

  if (! xdfs_view_index_source (txn, inop, li, li))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_view_end (RepoTxn *txn,
	       Inode   *inop,
	       void    *private)
{
  XdfsLocalIndex *li = private;
  GSList *p;

  for (p = li->free_list; p; p = p->next)
    g_free (p->data);

  g_slist_free (li->free_list);
  g_array_free (li->inst_array, TRUE);

  return TRUE;
}

gboolean
xdfs_view_page_in (RepoTxn *txn,
		   Inode   *inop,
		   void    *private,
		   guint8  *pgbuf,
		   gsize    offset,
		   gsize    len)
{
  XdfsLocalIndex *li = private;
  XdfsInstruction *inst;
  guint u;
  guint l = 0;
  guint i;

  u = li->inst_len - 1;

  g_assert ((offset + len) <= li->length);

 again:
  /* Binary search */

  i = (u+l)/2;

  inst = & li->inst[i];

  if (offset < inst->output_start)
    {
      u = i - 1;
      goto again;
    }
  else if (offset >= (inst->output_start + inst->length))
    {
      l = i + 1;
      goto again;
    }
  else
    {
      gsize written = 0;

      while (len > 0)
	{
	  guint inst_offset;
	  guint this_copy;
	  FileHandle *fh;

	  inst_offset = offset - inst->output_start;
	  this_copy   = MIN (len, inst->length - inst_offset);

	  /* @@@ open/close overhead could be high, here... */

	  inst_offset += inst->offset;

	  if (! (fh = dbfs_inode_open_read (txn, inst->inop)))
	    return FALSE;

	  if (handle_seek (fh, inst_offset, HANDLE_SEEK_SET) != inst_offset)
	    return FALSE;

	  if (handle_read (fh, pgbuf + written, this_copy) != this_copy)
	    return FALSE;

	  if (! handle_close (fh))
	    return FALSE;

	  handle_free (fh);

	  written += this_copy;
	  offset  += this_copy;
	  len     -= this_copy;
	  inst    += 1;
	}
    }

  return TRUE;
}

#ifdef NO_INVERSE_CODE
gboolean
xdfs_update_inverses (RepoTxn         *txn,
		      XdeltaControl   *control)
{
  return TRUE;
}
#else
gboolean
xdfs_update_inverses (RepoTxn         *txn,
		      XdeltaControl   *control)
{
  int i;
  int src_count = xdp_control_source_count (control);
  SkipList **inverse_nonsequentials = g_new0 (SkipList*, src_count);
  GArray   **inverse_sequentials    = g_new0 (GArray*, src_count);
  Inode    **src_inodes             = g_new0 (Inode*, src_count);
  gboolean   ret = TRUE;

  /* Initialize the above arrays to contain the sources that are being
   * updated by CONTROL.  All others will be skipped.
   */
  for (i = 0; i < src_count; i += 1)
    {
      Inode *src_ino = xdp_control_source_data (control, i);
      XdfsSourceStat src_stat;

      if (! xdfs_source_stat (txn, src_ino, & src_stat))
	goto exit;

      if (src_stat.has_inverse)
	continue;

      src_inodes[i] = src_ino;

      if (xdp_control_source_sequential (control, i))
	inverse_sequentials[i] = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));
      else
	inverse_nonsequentials[i] = xdfs_skip_list_new (src_ino.minor.ino_segment_len);
    }

  /* Iterate through the instructions, updating only the data structures
   * set by the previous loop.
   */
  for (i = 0; i < control->inst_len; i += 1)
    {
      XdeltaInstruction *inst = & control->inst[i];
      guint src_index = inst->index;

      if (inverse_sequentials[i])
	{
	  XdfsInstruction xi;

	  /* For the inverse, swap output_start and offset
	   */
	  xi.offset       = inst->output_start;
	  xi.length       = inst->length;
	  xi.output_start = inst->offset;

	  g_array_append_val (inverse_sequentials[src_index], xi);
	}
      else if (inverse_nonsequentials[i])
	{
	  xdfs_skip_list_insert (inverse_nonsequentials[src_index], inst->offset, inst->length, inst);
	}
    }

  /* For the nonsequential skip lists, translate now.
   */
  for (i = 0; i < src_count; i += 1)
    {
      SkipList* sl;
      SkipListNode *sln;
      GArray* is;

      if (! (sl = inverse_nonsequentials[i]))
	continue;

      sln = xdfs_skip_list_first (sl);

      inverse_sequentials[i] = is = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));

      for (; sln; sln = xdfs_skip_list_next (sln))
	{
	  XdfsInstruction xi;
	  XdeltaInstruction *inst = xdfs_skip_list_data (sln);

	  xi.offset       = inst->output_start;
	  xi.length       = inst->length;
	  xi.output_start = inst->offset;

	  g_array_append_val (is, xi);
	}
    }

  /* Now write the new inverse records.
   */
  for (i = 0; i < src_count; i += 1)
    {
      GArray* is;

      if (! (is = inverse_sequentials[i]))
	continue;

      /* @@@ Set it: Note: haven't yet set the xi.inum field, if
       * that's what it will be...
       */
    }

  if (0)
    {
    exit:
      ret = FALSE;
    }

  /* Clean up
   */
  for (i = 0; i < src_count; i += 1)
    {
      if (inverse_nonsequentials[i])
	xdfs_skip_list_free (inverse_nonsequentials[i]);

      if (inverse_sequentials[i])
	g_array_free (inverse_sequentials[i], TRUE);
    }

  g_free (inverse_nonsequentials);
  g_free (inverse_sequentials);
  g_free (src_inodes);

  return ret;
}
#endif
