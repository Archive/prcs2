/* -*-Mode: C;-*-
 * $Id$
 * file.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "dbfs.h"
#include "getopt.h"
#include <edsiostdio.h>

/* $Format: "const char* _xdfstest_version = \"$ReleaseVersion$\"; " $ */
const char* _xdfstest_version = "0.19.0";

typedef struct _Command Command;

struct _Command {
  gchar* name;
  gboolean (* func) (RepoTxn* txn, gint argc, gchar** argv);
  gint nargs;
  gboolean create;
};

static gboolean register_command (RepoTxn* txn, gint argc, gchar** argv);
static gboolean checkin_command  (RepoTxn* txn, gint argc, gchar** argv);
static gboolean checkout_command (RepoTxn* txn, gint argc, gchar** argv);
static gboolean dump_command     (RepoTxn* txn, gint argc, gchar** argv);

static const Command commands[] =
{
  { "register", register_command, 0, TRUE },  /* register DIRECTORY */
  { "checkin",  checkin_command,  1, FALSE }, /* checkin DIRECTORY infile */
  { "checkout", checkout_command, 2, FALSE }, /* checkout DIRECTORY (NUMBER|MD5) outfile */
  { "dump",     dump_command,     0, FALSE }, /* dump DIRECTORY */
  { NULL, NULL, 0 }
};

static struct option const long_options[] =
{
  {"help",                no_argument, 0, 'h'},
  {"version",             no_argument, 0, 'v'},
  {"message",             required_argument, 0, 'm'},
  {"print-object",        no_argument, 0, 'p'},
  {"long-format",         no_argument, 0, 'l'},
  {0,0,0,0}
};

static const gchar* program_name = "prog";

static void
s_error (const gchar *format, ...)
{
  va_list args;
  va_start (args, format);
  vfprintf (stderr, format, args);
  va_end (args);
}

static void
usage ()
{
  s_error ("usage: %s COMMAND [OPTIONS] [ARG1 ...]\n", program_name);
  s_error ("use --help for more help\n");
  exit (2);
}

static void
help ()
{
  s_error ("usage: %s COMMAND [OPTIONS] [ARG1 ...]\n", program_name);
  s_error ("COMMAND is one of:\n");
  s_error ("OPTIONS are:\n");
  s_error ("  -v, --version\n");
  s_error ("  -h, --help\n");
  exit (2);
}

static void
version ()
{
  s_error ("version %s\n", _xdfstest_version);
  exit (2);
}

static gchar*
strip_leading_path (gchar* p)
{
  gchar* ls = strrchr (p, '/');

  if (ls)
    return ls + 1;

  return p;
}

static gboolean long_format = FALSE;
static const char* message = "*empty message*";
static gboolean print_object = FALSE;
static Path *xdfstest_path;

gint
main(gint argc, gchar** argv)
{
  const Command *cmd = NULL;
  gint c;
  gint longind, fatal_mask;
  Path *fs_path;
  DBFS *dbfs = NULL;
  RepoTxn *txn = NULL;

  g_assert (argc > 0 && argv[0]);

  program_name = strip_leading_path (argv[0]);

  fatal_mask = g_log_set_always_fatal (G_LOG_FATAL_MASK);
  fatal_mask |= G_LOG_LEVEL_CRITICAL;

  g_log_set_always_fatal (fatal_mask);

  if (argc < 2)
    usage ();

  for (cmd = commands; cmd->name; cmd += 1)
    if (strcmp (cmd->name, argv[1]) == 0)
      break;

  if (strcmp (argv[1], "-h") == 0 ||
      strcmp (argv[1], "--help") == 0)
    help ();

  if (strcmp (argv[1], "-v") == 0 ||
      strcmp (argv[1], "--version") == 0)
    version ();

  if (! cmd->name)
    {
      s_error ("Unrecognized command\n");
      help ();
    }

  argc -= 1;
  argv += 1;

  while ((c = getopt_long(argc,
			  argv,
			  "+lhpvm:",
			  long_options,
			  &longind)) != EOF)
    {
      switch (c)
	{
	case 'l': long_format = TRUE; break;
	case 'm': message = g_strdup (optarg); break;
	case 'h': help (); break;
	case 'p': print_object = TRUE; break;
	case 'v': version (); break;
	default:
	  s_error ("Illegal argument, use --help for help\n");
	  goto bail;
	}
    }

  argc -= optind;
  argv += optind;

  if (argc != cmd->nargs + 1)
    {
      s_error ("Wrong number of arguments\n");
      goto bail;
    }

  if (! dbfs_library_init ())
    goto bail;

  if (! (fs_path = path_from_host_string (NULL, _fs_pthn, argv[0])))
    goto bail;

  if (cmd->create)
    dbfs = dbfs_create (fs_path);
  else
    dbfs = dbfs_initialize (fs_path);

  if (! dbfs)
    goto bail;

  argc -= 1;
  argv += 1;

  if (! xdfs_library_init ())
    goto bail;

  if (! (txn = dbfs_txn_begin (dbfs, DBFS_TXN_FLAT)))
    goto bail;

  xdfstest_path = path_root (txn->alloc);

  if (! (* cmd->func) (txn, argc, argv))
    goto bail;

  if (! dbfs_txn_commit (txn))
    goto bail;

  txn = NULL;

  if (! dbfs_close (dbfs))
    goto bail;

  dbfs = NULL;

  if (! dbfs_library_close ())
    return 2;

  return 0;

 bail:

  if (txn)
    dbfs_txn_abort (txn);

  if (dbfs)
    dbfs_close (dbfs);

  dbfs_library_close ();

  return 2;
}

gboolean
register_command (RepoTxn* txn, gint argc, gchar** argv)
{
  if (! xdfs_location_create (txn, xdfstest_path, NULL))
    return FALSE;

  return TRUE;
}

gboolean
checkin_command (RepoTxn* txn, gint argc, gchar** argv)
{
  FileHandle *insert_handle;

  if (strcmp (argv[0], "-") == 0)
    insert_handle = _stdin_handle;
  else
    {
      if (! (insert_handle = handle_read_file (argv[0])))
	return FALSE;
    }

  if (! xdfs_insert_version (txn, xdfstest_path, insert_handle))
    return FALSE;

  return TRUE;
}

gboolean
checkout_command (RepoTxn* txn, gint argc, gchar** argv)
{
  guint i;
  Inode seq;
  Inode ino;
  FileHandle* src_handle;
  FileHandle* out_handle;
  gboolean res;

  if (! strtoui_checked (argv[0], & i, "Version number"))
    return FALSE;

  if (! xdfs_container_sequence (txn, xdfstest_path, & seq))
    return FALSE;

  if (! dbfs_inode_find_seqno (txn, & seq, i, DbfsNoFollowLinks, FT_CanRead, & ino))
    return FALSE;

  if (! (src_handle = dbfs_inode_open_read (txn, & ino)))
    return FALSE;

  if (strcmp (argv[1], "-") == 0)
    out_handle = _stdout_handle;
  else
    {
      if (! (out_handle = handle_write_file (argv[1])))
	return FALSE;
    }

  if (! handle_drain (src_handle, out_handle))
    return FALSE;

  if (! handle_close (src_handle))
    return FALSE;

  handle_free (src_handle);

  res = handle_close (out_handle);

  handle_free (out_handle);

  return res;
}

#if 0
static const char*
format_savings (float pat, float act)
{
  static char buf[64];

  float p;

  g_assert (pat <= act);

  if (act == 0.0)
    return "-";

  p = 100.0 * ((act - pat) / act);

  sprintf (buf, "%1.1f%%", p);

  return buf;
}
#endif

gboolean
dump_command (RepoTxn* txn, gint argc, gchar** argv)
{
  return dbfs_list (txn,
		    _stdout_handle,
		    /*(print_object ? LF_ShowSerial : 0) |*/
		    LF_ShowNamed |
		    LF_ShowStacked |
		    LF_Recursive
		    /*LF_TestSerial |
		      LF_ShowSegmentHead*/) &&
         handle_close (_stdout_handle);
}
