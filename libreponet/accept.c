/* -*-Mode: C;-*-
 * $Id: accept.c 1.9 Fri, 26 Mar 1999 03:24:51 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "reponet.h"
#include "reponetpriv.h"

#undef rsr_conn
#undef rsr_object
#undef rsr_object_type

typedef struct {
  ServerSocket     *sock;
} AcceptData;

static gboolean
protocol_accept_verify_request (Connection* conn, SerialConnectObject* startup)
{
  if (conn->protocol->major != startup->major ||
      conn->protocol->minor != startup->minor)
    {
      net_generate_intint_event (EC_NetBadProtocolVersion, startup->major, startup->minor);
      return FALSE;
    }

  /* More here later. */

  return TRUE;
}

static TaskControl
protocol_accept5 (Connection *conn, Task* task)
{
                                   /* See note in serv.ser about this cast. */
  SerialConnectObject *startup  = conn->startup_msg;
  Protocol            *protocol = conn->protocol;
  gint i;

  if (! protocol_accept_verify_request (conn, startup))
    return reponet_kill_task (task);

  for (i = 0; i < protocol->requests_len; i += 1)
    {
      Request* request = protocol->requests[i];

      if (request->request_val == startup->request)
  	{
	  ReponetServerReply reply;

	  conn->request = request;

	  g_assert (request->start.server);

	  if (request->startup_type != conn->startup_msg->arg_type)
	    {
	      net_generate_stringstringstring_event (EC_NetBadProtocolRequestStartupType,
						     request->name,
						     serializeio_generic_type_to_string (conn->startup_msg->arg_type),
						     serializeio_generic_type_to_string (request->startup_type));
	      return reponet_kill_task (task);
	    }

	  net_generate_stringstringpeer_event (EC_NetProtocolAcceptBegin, conn->protocol->name, conn->request->name, conn->peer);

	  reply = request->start.server (conn, conn->startup_msg->arg);

	  if (! reply.rsr_conn)
	    return reponet_kill_task (task);

	  return task_finish_put (conn,
				  task,
				  protocol_next,
				  serialize_connectobject (conn_sink (conn),
							   conn->request->request_val,
							   conn->protocol->major,
							   conn->protocol->minor,
							   SSF_NoFlag,
							   reply.rsr_object_type,
							   reply.rsr_object));
	}
    }

  net_generate_int_event (EC_NetBadProtocolRequest, startup->request);
  return reponet_kill_task (task);
}

static TaskControl
protocol_accept4 (Connection *conn, Task* task)
{
  return task_finish_get (conn,
			  task,
 			  protocol_accept5,
 			  ST_ConnectObject,
			  NULL,
 			  (void**) & conn->startup_msg);
}

static TaskControl
protocol_accept3 (Connection* conn, Task* task)
{
  return task_ssl_accept (conn, task, protocol_accept4);
}

static TaskControl
protocol_accept2 (Connection* conn, Task* task)
{
  gchar buf[128];

  sprintf (buf, "PRCS server-%s-%d.%d-%.50s\r\n",
	   conn->protocol->name,
	   conn->protocol->major,
	   conn->protocol->minor,
	   reponet_version);

  return task_puts (conn, task, protocol_accept3, buf);
}

static TaskControl
protocol_accept (Protocol* protocol, Task* task)
{
  AcceptData *info = task_stack_peek (task, AcceptData);
  ClientSocket   *client;
  Connection     *conn;
  Task* ntask;

  if (! (client = sock_accept (info->sock)))
    goto bail;

  conn = g_new0 (Connection, 1);

  conn->sock = client;
  conn->ssl  = reponet_ssl_new (client);
  conn->sink = reponet_ssl_sink (conn->ssl);
  conn->source = reponet_ssl_source (conn->ssl);
  conn->protocol = protocol;
  conn->protocol_sendfirst = FALSE;

  conn->peer = g_new (Peer, 1);

  conn->peer->address = sock_addr (client);
  conn->peer->port = sock_port (client);
  conn->peer->address_name = sock_addr_name (conn->peer->address);

  SSL_set_accept_state (conn->ssl);

  ntask = task_new_immediate (task->manager, conn, &protocol_accept2);

  task_kill_hook (ntask, connection_task_kill, conn);

bail:

  return task_set_accept (protocol, task, protocol_accept, info->sock);
}

gboolean
protocol_accept_init (Protocol         *protocol,
		      TaskManager      *tm)
{
  ServerSocket   *sock = sock_new_server (protocol->port);
  Task           *task;
  AcceptData *info;

  if (! sock)
    return FALSE;

  task = task_new_accept (tm, protocol, protocol_accept, sock);
  info = task_stack_push (task, AcceptData);

  info->sock = sock;

  return TRUE;
}
