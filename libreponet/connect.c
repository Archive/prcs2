/* -*-Mode: C;-*-
 * $Id: connect.c 1.13 Fri, 26 Mar 1999 03:24:51 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "reponet.h"
#include "reponetpriv.h"

static TaskControl
protocol_connect6 (Connection *conn, Task* task)
{
#ifdef REPO_DEBUG_CONNECT
  g_print ("protocol_connect6\n");
#endif

  if (conn->request->startup_type != conn->startup_msg->arg_type)
    {
      net_generate_stringstringstring_event (EC_NetBadProtocolRequestStartupType,
					     conn->request->name,
					     serializeio_generic_type_to_string (conn->startup_msg->arg_type),
					     serializeio_generic_type_to_string (conn->request->startup_type));
      return reponet_kill_task (task);
    }

  if (! conn->request->start.client (conn, conn->peer, conn->startup_msg->arg))
    return reponet_kill_task (task);

  return protocol_next (conn, task);
}

static TaskControl
protocol_connect5 (Connection *conn, Task* task)
{
#ifdef REPO_DEBUG_CONNECT
  g_print ("protocol_connect5\n");
#endif

  return task_finish_get (conn,
			  task,
 			  protocol_connect6,
 			  ST_ConnectObject,
			  NULL,
 			  (void**) & conn->startup_msg);
}

static TaskControl
protocol_connect4 (Connection *conn, Task* task)
{
#ifdef REPO_DEBUG_CONNECT
  g_print ("protocol_connect4\n");
#endif

  net_generate_stringstringpeer_event (EC_NetProtocolConnectBegin, conn->protocol->name, conn->request->name, conn->peer);

  return task_finish_put (conn,
			  task,
			  protocol_connect5,
			  serialize_connectobject (conn_sink (conn),
						   conn->request->request_val,
						   conn->protocol->major,
						   conn->protocol->minor,
						   SSF_NoFlag,
						   conn->connect_arg_type,
						   conn->connect_arg));
}

static TaskControl
protocol_connect3 (Connection *conn, Task* task)
{
#ifdef REPO_DEBUG_CONNECT
  g_print ("protocol_connect3\n");
#endif

  return task_ssl_connect (conn, task, protocol_connect4);
}

static TaskControl
protocol_connect2 (Connection* conn, Task* task)
{
  return task_gets (conn, task, & protocol_connect3, NULL);
}

TaskControl
protocol_connect_again (Connection *conn,
			Task       *task)
{
  if (! sock_connect_finish (conn->sock))
    return reponet_kill_task (task);

  if (sock_connected (conn->sock))
    return task_set_connect (conn, task, protocol_connect_again);
  else
    return task_gets (conn, task, protocol_connect3, NULL);
}

void
connection_task_kill (TaskManager* tm, void* data)
{
  Connection* conn = (Connection*) data;

  net_generate_stringstringpeer_event (EC_NetProtocolDied, conn->protocol->name, conn->request->name, conn->peer);

  if (conn->request->failure)
    conn->request->failure (tm);

  if (conn->failure)
    conn->failure (tm, conn->finish_data);

  sock_destroy (conn->sock);

  SSL_free (conn->ssl);

  conn->sink->sink_free (conn->sink);
  conn->source->source_free (conn->source);

  g_free (conn);
}

gboolean
protocol_connect_init (Peer             *peer,
		       void             *object,
		       SerialType        object_type,
		       Protocol         *protocol,
		       gint              request,
		       void*             finish_data,
		       ConnFinish        success,
		       ConnFinishFail    failure,
		       TaskManager      *tm)
{
  ClientSocket *sock;
  Connection* conn;
  Task* task;
  gint i;

  if (! (sock = sock_connect_init (peer->address, peer->port)))
    {
      failure (tm, finish_data);
      return FALSE;
    }

  conn = g_new0 (Connection, 1);

  conn->success = success;
  conn->failure = failure;
  conn->finish_data = finish_data;

  conn->sock = sock;
  conn->ssl = reponet_ssl_new (sock);
  conn->sink = reponet_ssl_sink (conn->ssl);
  conn->source = reponet_ssl_source (conn->ssl);
  conn->protocol = protocol;
  conn->protocol_sendfirst = TRUE;

  conn->peer = peer;
  conn->connect_arg = object;
  conn->connect_arg_type = object_type;

  for (i = 0; i < protocol->requests_len; i += 1)
    {
      if (protocol->requests[i]->request_val == request)
	{
	  conn->request = protocol->requests[i];
	  break;
	}
    }

  if (! conn->request)
    {
      net_generate_int_event (EC_NetBadProtocolRequest, request);
      failure (tm, finish_data);
      return FALSE;
    }

  SSL_set_connect_state (conn->ssl);

  if (! sock_connected (conn->sock))
    task = task_new_read (tm, conn, & protocol_connect_again);
  else
    task = task_new_immediate (tm, conn, & protocol_connect2);

  task_kill_hook (task, connection_task_kill, conn);

  return TRUE;
}
