/* -*-Mode: C;-*-
 * $Id: loop.c 1.6 Fri, 26 Mar 1999 03:24:51 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "reponet.h"
#include "reponetpriv.h"

#include <sys/types.h>

#if TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# if HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#include <unistd.h>

/*#define REPO_DEBUG_LOOP*/

void
reponet_exit_event_loop (TaskManager* tm)
{
  tm->exit_now = TRUE;
}

#undef TC_Kill
#undef TC_Joined
#undef TC_Blocked
#undef TC_Rescheduled
#undef TC_Done
#undef TC_Sleep
#undef TC_Consuming

static void
reponet_run_task (TaskManager* tm, Task *task)
{
  TaskControl cont;

#ifdef DEBUG_TASK_STACK
  g_print (" - running task: %s\n", task_print (task));
#endif

  task->state = TS_Running;

  cont = task->ready (task->data, task);

  g_assert (task->state != TS_Running);

  switch (cont.type)
    {
    case TC_Kill:
    case TC_Done:
    case TC_Joined:

      if (task->opposite)
	task_manager_remove (tm, task->opposite);

      task_manager_remove (tm, task);

      /* @@@ free stuff */

    case TC_Sleep:
    case TC_Blocked:
    case TC_Rescheduled:
    case TC_Consuming:
    }

#ifdef DEBUG_TASK_STACK
  g_print (" - task finished: %s (%s) at %s:%d\n", task_control_tostring (cont), task_state_tostring (task), task->current_file, task->current_line);
#endif
}

gboolean
reponet_event_loop (TaskManager* tm)
{
  gint maxfd, ready, fds, idlers, timers;
  struct timeval tv, *tvp;
  time_t start;
  Task* idle;
  GSList *tasks;
  fd_set read_fds;
  fd_set write_fds;

begin:

#ifdef DEBUG_TASK_STACK
  task_manager_print_tasks (tm);
#endif

  if (! tm->tasks)
    return TRUE;

  maxfd = 0;
  fds = 0;
  idlers = 0;
  timers = 0;
  tvp = NULL;
  start = time (NULL);
  idle = NULL;

  if (tm->exit_now)
    return FALSE;

  tv.tv_usec = 0;
  tv.tv_sec = 0;

  FD_ZERO (&read_fds);
  FD_ZERO (&write_fds);

  for (tasks = tm->tasks; tasks; tasks = tasks->next)
    {
      Task *task = tasks->data;
      fd_set *set;

      switch (task->state)
	{
	case TS_WaitImmediate:
	  reponet_run_task (tm, task);
	  goto begin;

	case TS_WaitTime:
	  if (timers == 0)
	    tv.tv_sec = task->activate - start;
	  else
	    tv.tv_sec = MIN (tv.tv_sec, task->activate - start);
	  timers += 1;
	  continue;
	case TS_WaitIdle:
	  idlers += 1;
	  idle = task;
	  continue;
	case TS_WaitRead:
	  fds += 1;
	  set = &read_fds;
	  break;
	case TS_WaitWrite:
	  fds += 1;
	  set = &write_fds;
	  break;
	case TS_WaitConsume:
	case TS_Sleeping:
	  continue;
	case TS_WaitAddr:
	case TS_Running:
	default:
	  net_generate_task_event (EC_NetIllegalTaskState, task);
	  return -1;
	}

      FD_SET (task->fd, set);
      maxfd = MAX(maxfd, task->fd);
    }

  if (tv.tv_sec < 0)
    tv.tv_sec = 0;

  if (timers > 0 || idle != NULL)
    tvp = &tv;

#ifdef REPO_DEBUG_LOOP
  if (timers > 0 || idle != NULL)
    tv.tv_sec = MIN (5, tv.tv_sec);
  else
    tv.tv_sec = 5;

  tvp = &tv;

  if (fds == 0)
    g_print ("entering wait state: %d timers %d idle\n", timers, idlers);
#endif

  ready = select (maxfd+1, &read_fds, &write_fds, NULL, tvp);

  if (tm->exit_now)
    return FALSE;

  if (ready < 0)
    {
      if (errno != EINTR)
	net_generate_errno_event (EC_NetSelectFailed);

      return -1;
    }
  else if (ready == 0)
    {
      if (idle)
	{
	  reponet_run_task (tm, idle);
	  goto begin;
	}

      start = time (NULL);

      for (tasks = tm->tasks; tasks; tasks = tasks->next)
	{
	  Task *task = tasks->data;

	  switch (task->state)
	    {
	    case TS_WaitTime:
	      if (task->activate <= start)
		{
		  reponet_run_task (tm, task);
		  goto begin;
		}
	      break;
	    default:
	      continue;
	    }
	}

#ifdef REPO_DEBUG_LOOP
      g_print ("*** 5 second timeout\n");
#endif

      goto begin;
    }

  for (tasks = tm->tasks; tasks; tasks = tasks->next)
    {
      Task *task = tasks->data;
      fd_set *set;

      switch (task->state)
	{
	case TS_WaitRead:
	  set = &read_fds;
	  break;
	case TS_WaitWrite:
	  set = &write_fds;
	  break;
	default:
	  continue;
	}

      if (FD_ISSET (task->fd, set))
	{
	  reponet_run_task (tm, task);
	  goto begin;
	}
    }

  goto begin;
}
