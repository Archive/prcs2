/* -*-Mode: C;-*-
 * $Id: protocols.c 1.8 Fri, 26 Mar 1999 03:24:51 -0800 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "reponet.h"
#include "reponetpriv.h"

/*#define REPO_DEBUG_PROTOCOL*/


TaskControl
protocol_next (Connection *conn, Task* task)
{
  Request* request = conn->request;
  gboolean position = task->protocol_position;
  gint type;
  gboolean done = position >= request->transmit_len;

  type = request->transmit[position].type;

#ifdef REPO_DEBUG_PROTOCOL
  g_print ("task %p at position %d done %d type %d parity %d\n", task, position, done, type, task->protocol_parity);
#endif

  if (conn->protocol_is_async && (done || type != ProtocolAsync))
    {
      /* Join */
      if (task->opposite)
	{
#ifdef REPO_DEBUG_PROTOCOL
	  g_print (">>joining %s into %s\n", task_print (task), task_print (task->opposite));
#endif
	  task->opposite->opposite = NULL;
	  task->opposite = NULL;

	  return reponet_join_task (task);
	}

      conn->protocol_is_async = FALSE;
    }
  else if (!conn->protocol_is_async && !done && type == ProtocolAsync)
    {
      /* Fork another task */
      Task* reader;

      conn->protocol_is_async = TRUE;
      task->protocol_is_sender = TRUE;

      reader = task_new_immediate (task->manager, conn, protocol_next);

      reader->protocol_is_sender = FALSE;
      reader->protocol_position = position;
      reader->protocol_parity = task->protocol_parity;
      reader->stack = task->stack;

      task->opposite = reader;
      reader->opposite = task;

#ifdef REPO_DEBUG_PROTOCOL
      g_print (">>forking %s with %s\n", task_print (task), task_print (reader));
#endif
    }

  if (done)
    {
      if (conn->request->success)
	conn->request->success (conn, task_manager (task));

      if (conn->success)
	conn->success (conn, task_manager (task), conn->finish_data);

      net_generate_stringstringpeer_event (EC_NetProtocolDone, conn->protocol->name, conn->request->name, conn->peer);

      sock_destroy (conn->sock);

      SSL_free (conn->ssl);

      conn->sink->sink_free (conn->sink);
      conn->source->source_free (conn->source);

      g_free (conn);

      return reponet_done_task (task);
    }

  switch (type)
    {
    case ProtocolBarrier:

      task->protocol_position += 1;

      if (request->transmit[position].send)
	return (* request->transmit[position].send) (conn, task);
      else if (request->transmit[position].recv)
	return (* request->transmit[position].recv) (conn, task);

      break;

    case ProtocolAsync:
      task->protocol_position += 1;

      if (task->protocol_is_sender)
	{
	  if (request->transmit[position].send)
	    return (* request->transmit[position].send) (conn, task);
	}
      else
	{
	  if (request->transmit[position].recv)
	    return (* request->transmit[position].recv) (conn, task);
	}

      break;

    default:
      abort ();
    }

  return protocol_next (conn, task);
}
