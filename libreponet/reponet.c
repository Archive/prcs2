/* -*-Mode: C;-*-
 * $Id: reponet.c 1.15 Mon, 03 May 1999 19:48:53 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "reponet.h"
#include "reponetpriv.h"

/* $Format: "char const * const reponet_version = \"$ReleaseVersion$\"; " $ */
char const * const reponet_version = "0.19.0"; 

gboolean
reponet_init (File* private_key)
{
  if (! net_edsio_init ())
    return FALSE;

  return reponet_ssl_init (private_key);
}

void*
conn_data_int (Connection* conn, const char* name, guint data_size)
{
  void* d;

  if (! conn->conn_data)
    conn->conn_data = g_hash_table_new (g_str_hash, g_str_equal);

  d = g_hash_table_lookup (conn->conn_data, name);

  if (! d)
    {
      d = g_malloc0 (data_size);

      g_hash_table_insert (conn->conn_data, (void*) name, d);
    }

  return d;
}

SerialSink*          conn_sink        (Connection* conn) { return conn->sink; }
SerialSource*        conn_source      (Connection* conn) { return conn->source; }
TaskManager*         task_manager     (Task* task)       { return task->manager; }

#undef TC_Kill
#undef TC_Joined
#undef TC_Done

void
task_kill_hook (Task* t, TaskKillFunc func, void* data)
{
  t->kill = func;
  t->kill_data = data;
}

TaskControl
reponet_kill_task_int (Task* task, const char* file, gint line)
{
  task->current_file = file;
  task->current_line = line;

  task->state = TS_Dead;

  if (! task->kill)
    net_generate_task_event (EC_NetTaskDeath, task);
  else
    task->kill (task->manager, task->kill_data);

  return task_control (task, TC_Kill, file, line);
}

TaskControl
reponet_join_task_int (Task* task, const char* file, gint line)
{
  task->current_file = file;
  task->current_line = line;

  task->state = TS_Dead;

  return task_control (task, TC_Joined, file, line);
}

TaskControl
reponet_done_task_int (Task* task, const char* file, gint line)
{
  task->current_file = file;
  task->current_line = line;

  task->state = TS_Dead;

  return task_control (task, TC_Done, file, line);
}

/* BIO Bridge
 */

static int  fhbio_write (BIO *h, char *buf, int num);
static int  fhbio_read  (BIO *h, char *buf, int size);
static long fhbio_ctrl  (BIO *h, int cmd, long arg1, char *arg2);
static int  fhbio_new   (BIO *h);
static int  fhbio_free  (BIO *h);
static int  fhbio_puts  (BIO *h, char *str);
static int  fhbio_gets  (BIO *h, char *str, int size);

static BIO_METHOD file_handle_bio_method = {
  (1|0x0800),
  "FileHandle",
  fhbio_write,
  fhbio_read,
  fhbio_puts,
  fhbio_gets,
  fhbio_ctrl,
  fhbio_new,
  fhbio_free
};

typedef struct _FileHandleBioCtx FileHandleBioCtx;

struct _FileHandleBioCtx
{
  FileHandle* fh;
};

BIO*
file_handle_bio_new (FileHandle *fh)
{
  BIO* bio = BIO_new (& file_handle_bio_method);
  FileHandleBioCtx *ctx = (FileHandleBioCtx*) bio->ptr;

  ctx = g_new0 (FileHandleBioCtx, 1);

  bio->init = 1;
  bio->ptr = (char *) ctx;
  bio->flags = 0;

  ctx->fh = fh;

  return bio;
}

gboolean
file_handle_bio_close (BIO *fhbio)
{
  FileHandleBioCtx *ctx;
  gboolean ret;

  ctx = (FileHandleBioCtx *)fhbio->ptr;

  ret = handle_close (ctx->fh);

  BIO_free (fhbio);

  return ret;
}

int
fhbio_write (BIO *b, char *buf, int num)
{
  FileHandleBioCtx *ctx;

  ctx = (FileHandleBioCtx *)b->ptr;

  if (! handle_write (ctx->fh, buf, num))
    return -1;

  return num;
}

int
fhbio_read (BIO *b, char *buf, int size)
{
  FileHandleBioCtx *ctx = (FileHandleBioCtx *)b->ptr;

  return handle_read (ctx->fh, buf, size);
}

static int
fhbio_puts (BIO *h, char *str)
{
  FileHandleBioCtx *ctx = (FileHandleBioCtx *)h->ptr;

  if (! handle_puts (ctx->fh, str))
    return -1;

  return strlen (str);
}

static int
fhbio_gets (BIO *h, char *str, int size)
{
  FileHandleBioCtx *ctx = (FileHandleBioCtx *)h->ptr;

  return handle_gets (ctx->fh, str, size);
}

long
fhbio_ctrl (BIO *b, int cmd, long num, char *ptr)
{
  abort ();

  return 1;
}

int
fhbio_new (BIO *h)
{
  FileHandleBioCtx *ctx;

  ctx = g_new0 (FileHandleBioCtx, 1);

  h->init = 1;
  h->ptr = (char *) ctx;
  h->flags = 0;

  return 1;
}

int
fhbio_free (BIO *a)
{
  FileHandleBioCtx *b;

  if (a == NULL)
    return 0;

  b = (FileHandleBioCtx *) a->ptr;

  g_free (b);

  a->ptr = NULL;
  a->init = 0;
  a->flags = 0;

  return 1;
}

const char*
reponet_get_conn_common_name (Connection* conn)
{
  return reponet_get_x509_common_name (SSL_get_peer_certificate (conn->ssl));
}

const char*
reponet_get_x509_common_name (X509* peer)
{
  X509_NAME       *name = X509_get_subject_name (peer);
  ASN1_OBJECT     *obj;
  X509_NAME_ENTRY *ne;
  ASN1_STRING *str;
  gint i;

  for (i = 0; i < X509_NAME_entry_count (name); i += 1)
    {
      ne  = (X509_NAME_ENTRY *) X509_NAME_get_entry (name, i);
      obj = X509_NAME_ENTRY_get_object (ne);
      str = X509_NAME_ENTRY_get_data (ne);

      if (OBJ_obj2nid (obj) == NID_commonName)
	return (const char*) str->data;
    }

  return "[Common name not supplied]";
}

const char*
eventdelivery_task_to_string (Task* task)
{
  return g_strdup ("@@@Task");
}

const char*
eventdelivery_conn_to_string (Connection* conn)
{
  return g_strdup (sock_addr_name (sock_addr (conn->sock)));
}

const char*
eventdelivery_peer_to_string (Peer* peer)
{
  if (peer->address)
    return g_strdup_printf ("%s:%s", peer->address_name, sock_addr_toa (peer->address));
  else
    return g_strdup_printf ("%s", peer->address_name);
}

const char*
eventdelivery_ssl_errors_to_string (void)
{
  long err = ERR_get_error ();

  if (err)
    return g_strdup (ERR_error_string (err, NULL));
  else
    return g_strdup ("No SSL error registered?");
}

#undef rsr_conn
#undef rsr_object
#undef rsr_object_type

ReponetServerReply
reponet_server_reply (Connection* conn, void* object, SerialType object_type)
{
  ReponetServerReply rsr;

  rsr.rsr_conn = conn;
  rsr.rsr_object = object;
  rsr.rsr_object_type = object_type;

  return rsr;
}

ReponetServerReply
reponet_server_deny  (Connection* conn)
{
  ReponetServerReply rsr;

  rsr.rsr_conn = conn;
  rsr.rsr_object = NULL;

  return rsr;
}

Task*
task_consumer_task (Task* producer)
{
  return producer->opposite;
}
