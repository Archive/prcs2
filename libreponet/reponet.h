/* -*-Mode: C;-*-
 * $Id: reponet.h 1.24 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _REPONET_H_
#define _REPONET_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <repo.h>
#include <edsio.h>
#include "sock.h"

#if HAVE_SSLEAY

  /*#define NO_FP_API -- GRR
    #define NO_STDIO*/

#include <stdio.h>

#ifdef HAVE_SSL_INCLUDE_DIR
#include <ssl/ssl.h>
#include <ssl/err.h>
#include <ssl/rand.h>
#include <ssl/pem.h>
#else
#include <ssl.h>
#include <err.h>
#include <rand.h>
#include <pem.h>
#endif


enum _SslErrorCode {
  REPONET_SSL_ERROR_NONE = SSL_ERROR_NONE,
  REPONET_SSL_ERROR_SSL = SSL_ERROR_SSL,
  REPONET_SSL_ERROR_WANT_READ = SSL_ERROR_WANT_READ,
  REPONET_SSL_ERROR_WANT_WRITE = SSL_ERROR_WANT_WRITE,
  REPONET_SSL_ERROR_WANT_X509_LOOKUP = SSL_ERROR_WANT_X509_LOOKUP,
  REPONET_SSL_ERROR_SYSCALL = SSL_ERROR_SYSCALL,
  REPONET_SSL_ERROR_ZERO_RETURN = SSL_ERROR_ZERO_RETURN,
  REPONET_SSL_ERROR_WANT_CONNECT = SSL_ERROR_WANT_CONNECT
};

typedef enum _SslErrorCode SslErrorCode;

#else
#error This library requires OpenSSL.
#endif

#define STATIC_ARRAY_LENGTH(x) (sizeof(x)/sizeof(*x))
#define STATIC_ARRAY(x) STATIC_ARRAY_LENGTH(x), x

enum _SessionStartupFlags {
  SSF_NoFlag = 0
};

typedef enum _SessionStartupFlags SessionStartupFlags;

/*#define DEBUG_TASK_STACK*/
/*#define DEBUG_TASK_STACK2*/

enum _TaskControlType {
  TC_Consuming,
  TC_Kill,
  TC_Joined,
  TC_Blocked,
  TC_Rescheduled,
  TC_Sleep,
  TC_Done
};

typedef enum _TaskControlType TaskControlType;

struct _TaskControl {
  TaskControlType type;
};

typedef struct _TaskControl TaskControl;

#define TC_Kill TC_DontUseMe
#define TC_Joined TC_DontUseMe
#define TC_Blocked TC_DontUseMe
#define TC_Rescheduled TC_DontUseMe
#define TC_Done TC_DontUseMe
#define TC_Sleep TC_DontUseMe
#define TC_Consuming TC_DontUseMe

enum _ProtocolType {
  ProtocolSync,         /* send then receive/receive then send */
  ProtocolAsync,        /* send and receive */
  ProtocolBarrier       /* resynchronize: function is called only once
			 * with send TRUE. */
};

typedef enum _ProtocolType        ProtocolType;

enum _TaskState {
  TS_WaitRead = 1,
  TS_WaitWrite,
  TS_WaitTime,
  TS_WaitAddr,
  TS_WaitIdle,
  TS_WaitImmediate,
  TS_WaitConsume,
  TS_Sleeping,
  TS_Running,
  TS_Dead
};

typedef enum _TaskState            TaskState;

typedef struct _Peer               Peer;
typedef struct _Task               Task;
typedef struct _TaskManager        TaskManager;
typedef struct _Connection         Connection;
typedef struct _Protocol           Protocol;
typedef struct _Transmition        Transmition;
typedef struct _Request            Request;
typedef struct _ReponetServerReply ReponetServerReply;

struct _Peer {
  const char   *address_name;
  int           port;
  Address      *address;
};

struct _ReponetServerReply {
  Connection *rsr_conn;
  void*       rsr_object;
  SerialType  rsr_object_type;
};

#define rsr_conn use_reponet_server_reply
#define rsr_object use_reponet_server_reply
#define rsr_object_type use_reponet_server_reply

typedef TaskControl (* ReadyFunc)        (Connection *conn,
					  Task       *task);
typedef TaskControl (* ReadyAFunc)       (Protocol   *proto,
					  Task       *task);

typedef void        (* ConnFinish)       (Connection *conn, TaskManager* tm, gpointer data);
typedef void        (* ReqFinish)        (Connection *conn, TaskManager* tm);

typedef void        (* ConnFinishFail)   (TaskManager* tm, gpointer data);
typedef void        (* ReqFinishFail)    (TaskManager* tm);

typedef void        (* TaskKillFunc)     (TaskManager* tm, void* data);

typedef ReponetServerReply (* ServerStartFunc) (Connection* conn, void* client_data);
typedef gboolean           (* ClientStartFunc) (Connection* conn, Peer* peer, void* server_data);

#include "net_edsio.h"

ReponetServerReply reponet_server_reply (Connection* conn, void* object, SerialType object_type);
ReponetServerReply reponet_server_deny  (Connection* conn);

TaskManager* task_manager_new    (void);
void         task_manager_add    (TaskManager* tm, Task* task);
void         task_manager_remove (TaskManager* tm, Task* task);

TaskManager* task_manager        (Task* task);

gboolean     task_stack_empty (Task* task);

void*        task_stack_push_int (Task* task, const char* name, const char* file, int line, guint size);
void*        task_stack_pop_int  (Task* task, const char* name, const char* file, int line, guint size);
void*        task_stack_peek_int (Task* task, const char* name, const char* file, int line, guint size);

#define task_stack_push(task,type) ((type*) task_stack_push_int (task, #type, __FILE__, __LINE__, sizeof(type)))
#define task_stack_pop(task,type)  ((type*) task_stack_pop_int (task, #type, __FILE__, __LINE__, sizeof(type)))
#define task_stack_peek(task,type) ((type*) task_stack_peek_int (task, #type, __FILE__, __LINE__, sizeof(type)))

Task* task_new_wait      (TaskManager *tm, void* data, time_t seconds, ReadyFunc ready);
Task* task_new_idle      (TaskManager *tm, void* data,                 ReadyFunc ready);
Task* task_new_read      (TaskManager *tm, Connection  *conn,  ReadyFunc   func);
Task* task_new_immediate (TaskManager *tm, Connection  *conn,  ReadyFunc   func);

void  task_kill_hook     (Task* t, TaskKillFunc func, void* data);

TaskControl task_set_wait_int      (Task   *task, void *data, time_t  seconds, ReadyFunc ready, const char* file, gint line);
TaskControl task_set_read_int      (Connection *conn, Task   *task, ReadyFunc ready, const char* file, gint line);
TaskControl task_set_write_int     (Connection *conn, Task   *task, ReadyFunc ready, const char* file, gint line);

#define task_set_wait(t,d,s,r) task_set_wait_int((t),(d),(s),(r),__FILE__,__LINE__)
#define task_set_read(c,t,r) task_set_read_int((c),(t),(r),__FILE__,__LINE__)
#define task_set_write(c,t,r) task_set_write_int((c),(t),(r),__FILE__,__LINE__)
#define task_consume(c,t) task_consume_int((c),(t),__FILE__,__LINE__)
#define task_sleep(t) task_sleep_int(t,__FILE__,__LINE__)

Task* task_consumer_task (Task* producer);

TaskControl task_sleep_int (Task* task, const char* file, gint line);
gboolean    task_wake  (Task* task);

void        task_set_immediate (Connection* conn, Task   *task, ReadyFunc ready);

typedef TaskControl (* ConsumerFunction) (Connection* conn, Task* task, ReadyFunc ready, void* data);

void           task_produce (Connection* conn, Task* task, ConsumerFunction consume, void* data);
void           task_produce_done (Connection* conn, Task* task);
TaskControl    task_consume_int (Connection* conn, Task* task, const char* file, gint line);
TaskControl    task_consume_ready (Connection* conn, Task* task);

TaskControl task_ssl_connect (Connection *conn, Task* task, ReadyFunc ready);
TaskControl task_ssl_accept  (Connection *conn, Task* task, ReadyFunc ready);

TaskControl task_finish_put (Connection *conn, Task* task, ReadyFunc ready, gboolean not_blocked);
TaskControl task_finish_get (Connection *conn, Task* task, ReadyFunc ready, guint32 acceptable, SerialType *type, void **ent);

TaskControl task_puts (Connection *conn, Task* task, ReadyFunc ready, const gchar* line);
TaskControl task_gets (Connection *conn, Task* task, ReadyFunc ready, const gchar** line);

TaskControl task_segments_put (Connection  *conn,
			       Task        *task,
			       ReadyFunc    ready,
			       FileSegment *seg1,
			       FileSegment *seg2,
			       FileSegment *seg3);
TaskControl task_segments_get (Connection  *conn,
			       Task        *task,
			       ReadyFunc    ready,
			       FileSegment *seg1,
			       FileSegment *seg2,
			       FileSegment *seg3);

#define conn_data(conn,type) ((type*)conn_data_int((conn),"default",sizeof(type)))
#define conn_named_data(conn,name,type) ((type*)conn_data_int((conn),name,sizeof(type)))
void* conn_data_int (Connection* conn, const char* name, guint data_size);

SerialSink*   conn_sink         (Connection* conn);
SerialSource* conn_source       (Connection* conn);

gboolean reponet_init (File* private_key);

gboolean protocol_accept_init  (Protocol         *protocol,
				TaskManager      *tm);

gboolean protocol_connect_init (Peer             *peer,
				void             *object,
				SerialType        object_type,
				Protocol         *protocol,
				gint              request,
				void*             finish_data,
				ConnFinish        success,
				ConnFinishFail    failure,
				TaskManager      *tm);

TaskControl protocol_next (Connection *conn, Task* task);

/* Returns the SHA1 hash of the certificate used to authenticate
 * CONN. */
const guint8* reponet_authorization_certificate_key (Connection *conn);

/* Return value of reponet_event_loop is either -1 if a critical error
 * occured, or the value of reponet_exit_event_loop() when called to
 * exit the loop. */
gboolean    reponet_event_loop      (TaskManager* tm);

/* This function is reentrant. */
void        reponet_exit_event_loop (TaskManager* tm);

/* These functions kill the task. */

TaskControl reponet_kill_task_int (Task* task, const char* file, gint line);
TaskControl reponet_join_task_int (Task* task, const char* file, gint line);
TaskControl reponet_done_task_int (Task* task, const char* file, gint line);

#define     reponet_kill_task(t) reponet_kill_task_int ((t), __FILE__, __LINE__)
#define     reponet_join_task(t) reponet_join_task_int ((t), __FILE__, __LINE__)
#define     reponet_done_task(t) reponet_done_task_int ((t), __FILE__, __LINE__)

TaskControl task_control (Task* task, TaskControlType type, const char* file, gint line);

const char* reponet_get_conn_common_name (Connection *conn);
const char* reponet_get_x509_common_name (X509       *peer);

extern char const * const reponet_version;

struct _Transmition {
  ReadyFunc    send;
  ReadyFunc    recv;
  ProtocolType type;
};

struct _Request {
  const char         *name;
  gint                request_val;
  gint                transmit_len;
  Transmition        *transmit;

  SerialType          startup_type;

  union {
    /* server_start is called on the server after the client has
     * connected.  client_data is an object of the server's
     * STARTUP_TYPE.  It returns an object which is sent back to the
     * client, or null to abort the connection. */
    ServerStartFunc server;

    /* client_start is called on the client after the client has
     * connected to peer and server has replied with server_data, which
     * is of the client's STARTUP_TYPE. */
    ClientStartFunc client;
  } start;

  ReqFinish         success;
  ReqFinishFail     failure;
};

struct _Protocol {
  const char    *name;
  guint32        port;
  File*          protocol_dir;

  gboolean      (* create) (Repository* repo, Protocol* protocol);
  gboolean      (* init)   (Repository* repo, Protocol* protocol);

  gint           major;
  gint           minor;

  gint           requests_len;
  Request      **requests;
};

/* Bridge between SSLeay's BIO and FileHandle.
 */

BIO* file_handle_bio_new  (FileHandle *fh);
gboolean file_handle_bio_close  (BIO *fhbio);

const char* eventdelivery_task_to_string (Task* task);
const char* eventdelivery_conn_to_string (Connection* conn);
const char* eventdelivery_peer_to_string (Peer* peer);

#ifdef __cplusplus
}
#endif

#endif
