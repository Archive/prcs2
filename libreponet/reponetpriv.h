/* -*-Mode: C;-*-
 * $Id: reponetpriv.h 1.11 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _REPONETPRIV_H_
#define _REPONETPRIV_H_

typedef struct _TaskStack TaskStack;

struct _TaskStack {
  TaskStack   *next;
  guint        size;
  const gchar *type;
  void        *data;
};

struct _TaskManager {
  GSList    *tasks;
  gboolean   exit_now;
  TaskStack *free_soon;
};

struct _Task {
  gint  state;

  gint  fd;
  glong activate;

  void        *data;

  TaskManager *manager;

  TaskControl (* ready) ();

  TaskKillFunc   kill;
  void*          kill_data;

  Task        *opposite;

  guint protocol_is_sender : 1;
  guint protocol_parity : 1;
  guint protocol_position;

  TaskStack *stack;

  const char *current_file;
  gint        current_line;

  GQueue *production_queue;
  GQueue *consumers;
};

struct _Connection {
  ClientSocket   *sock;
  SSL            *ssl;
  SerialSink     *sink;
  SerialSource   *source;
  Protocol       *protocol;
  Request        *request;

  Peer             *peer;

  void             *connect_arg;
  SerialType        connect_arg_type;

  SerialConnectObject *startup_msg;

  gboolean        protocol_sendfirst;
  gboolean        protocol_is_async;

  GHashTable       *conn_data;

  void*             finish_data;
  ConnFinish        success;
  ConnFinishFail    failure;
};

#ifdef DEBUG_TASK_STACK
const char* task_print (Task* task);
void task_manager_print_tasks (TaskManager* tm);
const char* task_control_tostring (TaskControl cont);
const char* task_state_tostring (Task* task);
#endif

Task* task_new_accept  (TaskManager  *tm,
			Protocol     *proto,
			ReadyAFunc    func,
			ServerSocket *sock);

TaskControl task_set_connect_int (Connection* conn, Task* task, ReadyFunc ready, const char* file, gint line);
TaskControl task_set_accept_int (Protocol* protocol, Task* task, ReadyAFunc ready, ServerSocket* sock, const char* file, gint line);

#define task_set_connect(c,t,r) task_set_connect_int((c),(t),(r),__FILE__,__LINE__)
#define task_set_accept(p,t,r,s) task_set_accept_int((p),(t),(r),(s),__FILE__,__LINE__)


int           reponet_ssl_verify_callback (int ok, X509_STORE_CTX *ctx);
SSL*          reponet_ssl_new (ClientSocket* sock);
gboolean      reponet_ssl_init (File* private_key);
SerialSink*   reponet_ssl_sink   (SSL* ssl);
SerialSource* reponet_ssl_source (SSL* ssl);
gboolean      reponet_ssl_sink_reput  (SerialSink* sink);
gint          reponet_ssl_sink_code   (SerialSink* sink);
gint          reponet_ssl_source_code (SerialSource* source);

void connection_task_kill (TaskManager* tm, void* data);

#endif
