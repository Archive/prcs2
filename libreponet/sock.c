/* -*-Mode: C;-*-
 * $Id: sock.c 1.16 Mon, 12 Apr 1999 05:57:56 -0700 jmacd $
 * sock.c:
 *
 * Copyright (C) 1997, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */


#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ctype.h>
#include <arpa/inet.h>

#include "reponet.h"
#include "reponetpriv.h"

struct _ServerSocket {
  int  fd;
};

struct _ClientSocket {
  int fd;
  int connected;
  Address* addr;
  int port;
};

struct _Address {
  struct sockaddr_in addr;
  const char* hname;
};

int
sock_fd (ClientSocket* sock)
{
  return sock->fd;
}

int
sock_serv_fd (ServerSocket* sock)
{
  return sock->fd;
}

ServerSocket*
sock_new_server (int port)
{
  ServerSocket *sock = g_new0 (ServerSocket, 1);
  struct sockaddr_in addr;
  int v = 1;

  sock->fd = socket(AF_INET, SOCK_STREAM, 0);

  if (sock->fd < 0)
    {
      g_free (sock);
      net_generate_errno_event (EC_NetSocketFailed);
      return NULL;
    }

  setsockopt (sock->fd, SOL_SOCKET, SO_REUSEADDR, (char*) &v, sizeof(v));
  fcntl (sock->fd, F_SETFL, O_NONBLOCK);

  memset (&addr, 0, sizeof(addr));
  addr.sin_family      = AF_INET;
  addr.sin_port        = g_htons(port);
  addr.sin_addr.s_addr = g_htonl(INADDR_ANY);

  if (bind (sock->fd, (struct sockaddr*) &addr, sizeof(addr)))
    {
      close (sock->fd);
      g_free (sock);
      net_generate_errno_event (EC_NetBindFailed);
      return NULL;
    }

  if (listen (sock->fd, 5))
    {
      close (sock->fd);
      g_free (sock);
      net_generate_errno_event (EC_NetListenFailed);
      return NULL;
    }

  return sock;
}

static Address*
sock_addr_bytes (struct sockaddr_in *bytes)
{
  Address* addr = g_new0 (Address, 1);
  struct hostent* hent;

  memcpy (&addr->addr, bytes, sizeof(*bytes));

  /*(defevent GetHostByAddrFailed Error ((hname string)) (herrno) HERRNO???
    "Gethostbyaddr ${HNAME} failed: ${STRERROR}")*/

  if (! (hent = gethostbyaddr ((guint8*) & addr->addr.sin_addr, sizeof (addr->addr.sin_addr), AF_INET)))
    addr->hname = g_strdup (inet_ntoa (addr->addr.sin_addr));
  else
    addr->hname = g_strdup (hent->h_name);

  return addr;
}

ClientSocket*
sock_accept (ServerSocket* sock)
{
  struct sockaddr_in addr;
  int addrlen = sizeof(addr);
  ClientSocket* csock;
  int fd;

  memset (&addr, 0, sizeof(addr));

  do
    {
      fd = accept (sock->fd, (struct sockaddr*) &addr, &addrlen);
    }
  while (fd < 0 && errno == EINTR);

  if (fd < 0)
    {
      net_generate_errno_event (EC_NetAcceptFailed);
      return NULL;
    }

  csock = g_new0 (ClientSocket, 1);
  csock->fd = fd;

  csock->addr = sock_addr_bytes (& addr);
  csock->port = addr.sin_port;

  fcntl (csock->fd, F_SETFL, O_NONBLOCK);

  return csock;
}

int
sock_connected (ClientSocket *sock)
{
  return sock->connected;
}

ClientSocket*
sock_connect_init (Address* to_addr, int port)
{
  int fd, err;
  struct sockaddr_in addr;
  ClientSocket *sock;

  fd = socket(AF_INET, SOCK_STREAM, 0);

  g_assert (to_addr);

  if (fd < 0)
    {
      close (fd);
      net_generate_errno_event (EC_NetSocketFailed);
      return NULL;
    }

  fcntl (fd, F_SETFL, O_NONBLOCK);

  memcpy (&addr, &to_addr->addr, sizeof (addr));
  addr.sin_family = AF_INET;
  addr.sin_port   = g_htons(port);
  /*addr.sin_len = sizeof (addr);*/

  err = connect(fd, (struct sockaddr *)&addr, sizeof(addr));

  if (err && errno != EINPROGRESS && errno != EINTR && errno != EALREADY)
    {
      net_generate_stringinterrno_event (EC_NetConnectFailed, to_addr->hname, port);
      return NULL;
    }

  sock = g_new0 (ClientSocket, 1);

  sock->fd = fd;
  sock->addr = to_addr;
  sock->port = port;

  if (! err)
    sock->connected = TRUE;

  return sock;
}

int sock_connect_finish (ClientSocket *sock)
{
  int val;
  struct sockaddr_in addr;

  g_assert (! sock->connected);

  memcpy (&addr.sin_addr, &sock->addr->addr, sizeof (addr.sin_addr));
  addr.sin_family = AF_INET;
  addr.sin_port   = g_htons(sock->port);

  val = connect (sock->fd, (struct sockaddr *)&addr, sizeof(addr));

  if (val < 0 && errno != EISCONN)
    {
      close (sock->fd);
      net_generate_stringinterrno_event (EC_NetConnectFailed, sock->addr->hname, sock->port);
      return FALSE;
    }

  return TRUE;
}

void
sock_destroy (ClientSocket *sock)
{
  if (sock)
    {
      shutdown (sock->fd, 2);
      close (sock->fd);
      g_free (sock);
    }
}

void
sock_destroy_server (ServerSocket *sock)
{
  if (sock)
    {
      shutdown (sock->fd, 2);
      close (sock->fd);
      g_free (sock);
    }
}

void
sock_destroy_addr (Address *a)
{
  if (a)
    {
      g_free (a);
    }
}

Address*
sock_addr_init (const char* hname)
{
  Address* addr = g_new0 (Address, 1);
  struct hostent *hent;

  hent = gethostbyname (hname);

  if (hent == NULL)
    {
      net_generate_stringerrno_event (EC_NetGetHostByNameFailed, hname);
      return NULL;
    }

  memcpy (&addr->addr.sin_addr, hent->h_addr_list[0], sizeof (addr->addr.sin_addr));
  addr->hname = g_strdup (hent->h_name);

  return addr;
}

const char*
sock_addr_toa (Address* addr)
{
  return inet_ntoa (addr->addr.sin_addr);
}

const char*
sock_addr_name (Address* addr)
{
  return addr->hname;
}

Address*
sock_addr (ClientSocket* sock)
{
  return sock->addr;
}

guint16
sock_port (ClientSocket *sock)
{
  return sock->port;
}
