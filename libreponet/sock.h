/* -*-Mode: C;-*-
 * $Id: sock.h 1.6 Fri, 26 Mar 1999 03:24:51 -0800 jmacd $
 * sock.h:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _SOCK_H_
#define _SOCK_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _ServerSocket ServerSocket;
typedef struct _ClientSocket ClientSocket;
typedef struct _Address      Address;

/* Returns a new server on port */
extern ServerSocket* sock_new_server     (int           port);

/* Accepts connections (***) */
extern ClientSocket* sock_accept         (ServerSocket *sock);

/* Initializes connect */
extern ClientSocket* sock_connect_init   (Address*    addr,
					  int         port);

/* Tests connection */
extern int           sock_connected      (ClientSocket *sock);

/* Finalizes connect (***) */
extern int           sock_connect_finish (ClientSocket *sock);

/* Selects an fd to select on for the above. */
extern int           sock_fd             (ClientSocket* sock);
extern int           sock_serv_fd        (ServerSocket* sock);

/* Destroys the above types. */
extern void          sock_destroy        (ClientSocket *sock);
extern void          sock_destroy_server (ServerSocket *sock);
extern void          sock_destroy_addr   (Address      *sock);

/* Initializes namelookup */
extern Address*      sock_addr_init      (const char* hname);
extern Address*      sock_addr           (ClientSocket *sock);
extern guint16       sock_port           (ClientSocket *sock);
extern const char*   sock_addr_toa       (Address* addr);
extern const char*   sock_addr_name      (Address* addr);

/* Once sock_addr_fd() selects for read, call this repeatedly until it
 * returns NULL to get all the lookups that have completed. (***) */
extern Address*      sock_addr_collect   (void);
extern Address*      sock_addr_timeout   (void);

#ifdef __cplusplus
}
#endif

#endif /* _SOCK_H_ */
