/* -*-Mode: C;-*-
 * $Id: sslglue.c 1.18 Sun, 18 Apr 1999 05:53:06 -0700 jmacd $
 * start.c:
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "reponet.h"
#include "reponetpriv.h"

static void
server_pkey_cb(int p,int n, char *arg)
{
  char c = '*';

  if (p == 0) c='.';
  if (p == 1) c='+';
  if (p == 2) c='*';
  if (p == 3) c='\n';

  fputc (c, stderr);
}

static EVP_PKEY*
reponet_pkey_create (File* key, guint bits)
{
  FileHandle *key_handle;
  BIO* key_bio;
  EVP_PKEY* pkey = NULL;
  int passwd_tries = 0;

  if (! (key_handle = file_open (key, HV_Replace)))
    return NULL;

  key_bio = file_handle_bio_new (key_handle);

  g_print ("No private key %s exists.  Generating a %d bit RSA private key.\n", path_to_string (_fs_repo, file_access_path (key)), bits);

  if (! (pkey = EVP_PKEY_new()))
    return NULL;

  if (! EVP_PKEY_assign_RSA (pkey, RSA_generate_key (bits, 0x10001, server_pkey_cb, NULL)))
    return NULL;

  if (pkey == NULL)
    return NULL;

 loop:

  EVP_set_pw_prompt ("Enter private key password: ");

  g_assert (EVP_get_cipherbyname ("DES-EDE3-CBC") == EVP_des_ede3_cbc());

  if (! PEM_write_bio_PrivateKey (key_bio, pkey, /* @@@ DISABLE PASSWD ENCRYPTION EVP_des_ede3_cbc()*/ NULL, NULL, 0, NULL))
    {
      if ((ERR_GET_REASON (ERR_peek_error()) == PEM_R_PROBLEMS_GETTING_PASSWORD) && (passwd_tries < 3))
	{
	  ERR_clear_error();
	  passwd_tries += 1;
	  goto loop;
	}

      return NULL;
    }

  if (! file_handle_bio_close (key_bio))
    return NULL;

  if (! repository_commit (repository_of_file (key))) /* @@@ */
    return NULL;

  return pkey;
}

static EVP_PKEY*
reponet_pkey_init (File* key)
{
  FileHandle *key_handle;
  BIO* key_bio;
  EVP_PKEY* pkey = NULL;

  if (file_is_not_type (key, FV_Regular))
    return NULL;

  if (! (key_handle = file_open (key, HV_Read)))
    return NULL;

  key_bio = file_handle_bio_new (key_handle);

  EVP_set_pw_prompt ("Enter private key password: ");

  if (! (pkey = PEM_read_bio_PrivateKey (key_bio, NULL, NULL)))
    return NULL;

  return pkey;
}

static EVP_PKEY*
reponet_find_private_key (File* private_key)
{
  if (file_is_type_noerr (private_key, FV_NotPresent))
    return reponet_pkey_create (private_key, 512);
  else
    return reponet_pkey_init (private_key);
}

/* This routine loads a PEM-encoded RSA private keypair, then generates a
 * new, self-signed X509 certificate and initializes the SSL_CTX with it.
 */
static gboolean
reponet_init_certificate (SSL_CTX *ctx, EVP_PKEY *pkey)
{
  X509 *x509;
  X509_NAME *name = NULL;
  X509_NAME_ENTRY *ne = NULL;
  X509_EXTENSION *ex = NULL;
  ASN1_OCTET_STRING *data = NULL;

  if (! (x509 = X509_new ()))
    goto bail;

  if (! X509_set_version (x509, 2)) /* Set version to V3 */
    goto bail;

  ASN1_INTEGER_set (X509_get_serialNumber (x509), 0L);

  X509_gmtime_adj (X509_get_notBefore (x509), 0);
  X509_gmtime_adj (X509_get_notAfter (x509), 3600); /* @@@ good for 1 hour? */

  X509_set_pubkey (x509, pkey);

  name = X509_NAME_new();

  ne = X509_NAME_ENTRY_create_by_NID (NULL, NID_countryName, V_ASN1_APP_CHOOSE, "US", -1);
  X509_NAME_add_entry (name, ne, 0, 0);

  X509_NAME_ENTRY_create_by_NID (&ne, NID_commonName, V_ASN1_APP_CHOOSE, "Josh MacDonald", -1);
  X509_NAME_add_entry (name, ne, 1, 0);

  X509_NAME_ENTRY_free (ne);

  X509_set_subject_name (x509, name);
  X509_set_issuer_name (x509, name);

  X509_NAME_free (name);

  data = X509v3_pack_string (NULL, V_ASN1_BIT_STRING, "\001", 1);
  ex = X509_EXTENSION_create_by_NID (NULL, NID_netscape_cert_type, 0, data);
  X509_add_ext (x509, ex, -1);

  X509v3_pack_string (&data, V_ASN1_IA5STRING, "example comment extension", -1);
  X509_EXTENSION_create_by_NID (&ex, NID_netscape_comment, 0, data);
  X509_add_ext (x509, ex, -1);

  X509v3_pack_string (&data, V_ASN1_BIT_STRING, "www.cryptsoft.com", -1);
  X509_EXTENSION_create_by_NID (&ex, NID_netscape_ssl_server_name, 0, data);
  X509_add_ext (x509,ex,-1);

  X509_EXTENSION_free (ex);

  ASN1_OCTET_STRING_free (data);

  if (! X509_sign (x509, pkey, EVP_sha1 ()))
    goto bail;

  /*X509_print_fp (stdout, x509);
    g_print ("CN=%s\n", reponet_get_x509_common_name (x509));*/

  if (SSL_CTX_use_certificate (ctx, x509) <= 0)
    goto bail;

  if (! SSL_CTX_check_private_key (ctx))
    goto bail;

  return TRUE;

 bail:

  net_generate_ssl_event (EC_NetGenericSSLFailure);
  return FALSE;
}

static int
reponet_verify_client (SSL_CTX* ctx)
{
  return TRUE;
}

static DH*
reponet_get_dh512()
{
  static const guint8 dh512_p[] = {
    0xDA,0x58,0x3C,0x16,0xD9,0x85,0x22,0x89,0xD0,0xE4,0xAF,0x75,
    0x6F,0x4C,0xCA,0x92,0xDD,0x4B,0xE5,0x33,0xB8,0x04,0xFB,0x0F,
    0xED,0x94,0xEF,0x9C,0x8A,0x44,0x03,0xED,0x57,0x46,0x50,0xD3,
    0x69,0x99,0xDB,0x29,0xD7,0x76,0x27,0x6B,0xA2,0xD3,0xD4,0x12,
    0xE2,0x18,0xF4,0xDD,0x1E,0x08,0x4C,0xF6,0xD8,0x00,0x3E,0x7C,
    0x47,0x74,0xE8,0x33,
  };

  static const guint8 dh512_g[]={
    0x02,
  };

  DH *dh=NULL;

  if ((dh=DH_new()) == NULL)
    return NULL;

  dh->p = BN_bin2bn ((guint8*)dh512_p, sizeof(dh512_p), NULL);
  dh->g = BN_bin2bn ((guint8*)dh512_g, sizeof(dh512_g), NULL);

  if ((dh->p == NULL) || (dh->g == NULL))
    return NULL;

  return dh;
}

static void
reponet_ssl_info_callback(SSL *s, int where, int ret)
{

}

static RSA*
reponet_tmp_rsa_cb(SSL *s,int export,int keylen)
{
  static RSA *rsa_tmp=NULL;

  if (rsa_tmp == NULL)
    rsa_tmp = RSA_generate_key (keylen, RSA_F4, NULL, NULL);

  return rsa_tmp;
}

const guint8*
reponet_authorization_certificate_key (Connection *conn)
{
  X509* peer = SSL_get_peer_certificate (conn->ssl);
  static guint8 peer_sha[EVP_MAX_MD_SIZE];
  gint len;

  if (! X509_digest (peer, EVP_sha1 (), peer_sha, &len))
    {
      net_generate_ssl_event (EC_NetGenericSSLFailure);
      return NULL;
    }

  g_assert (len == 20);

  return peer_sha;
}

static SSL_CTX*
reponet_ssl_ctx (void)
{
  static SSL_CTX* ctx = NULL;

  if (! ctx)
    {
      ctx = SSL_CTX_new (SSLv3_method ());

      if (ctx == NULL)
	{
	  net_generate_ssl_event (EC_NetGenericSSLFailure);
	  abort ();
	}
    }

  return ctx;
}

SSL*
reponet_ssl_new (ClientSocket* sock)
{
  SSL_CTX *ctx  = reponet_ssl_ctx ();
  SSL     *ssl  = SSL_new (ctx);
  BIO     *sbio = BIO_new_socket (sock_fd (sock), BIO_NOCLOSE);

  SSL_set_bio (ssl, sbio, sbio);

  SSL_set_verify (ssl,
		  SSL_VERIFY_PEER |
		  SSL_VERIFY_FAIL_IF_NO_PEER_CERT |
		  SSL_VERIFY_CLIENT_ONCE,
		  reponet_ssl_verify_callback);

  SSL_clear (ssl);

  return ssl;
}

gboolean
reponet_ssl_init (File* private_key)
{
  DH       *dh;
  EVP_PKEY *pkey;

  SSL_library_init ();
  SSL_load_error_strings ();

  if (! reponet_ssl_ctx ())
    return FALSE;

  SSL_CTX_set_info_callback    (reponet_ssl_ctx (), reponet_ssl_info_callback);
  SSL_CTX_set_tmp_rsa_callback (reponet_ssl_ctx (), reponet_tmp_rsa_cb);
  SSL_CTX_sess_set_cache_size  (reponet_ssl_ctx (), 128);
  SSL_CTX_set_cert_verify_cb   (reponet_ssl_ctx (), reponet_verify_client, NULL);
  SSL_CTX_set_cipher_list      (reponet_ssl_ctx (), "NULL-SHA");

  if (! (pkey = reponet_find_private_key (private_key)))
    return FALSE;

  if (SSL_CTX_use_PrivateKey (reponet_ssl_ctx (), pkey) <= 0)
    return FALSE;

  if (! (dh = reponet_get_dh512()))
    return FALSE;

  SSL_CTX_set_tmp_dh (reponet_ssl_ctx (), dh);

  DH_free (dh);

  if (! reponet_init_certificate (reponet_ssl_ctx (), pkey))
    return FALSE;

  return TRUE;
}

int
reponet_ssl_verify_callback(int ok, X509_STORE_CTX *ctx)
{
  X509 *err_cert;
  int err,depth;
  int verify_error;

  err_cert =    X509_STORE_CTX_get_current_cert (ctx);
  err=	        X509_STORE_CTX_get_error (ctx);
  depth=	X509_STORE_CTX_get_error_depth (ctx);

  if (!ok)
    {
      if (42 >= depth)
	{
	  ok=1;
	  verify_error=X509_V_OK;
	}
      else
	{
	  ok=0;
	  verify_error=X509_V_ERR_CERT_CHAIN_TOO_LONG;
	}
    }

  return ok;
}

/* SSL Sink
 */

#define HEADER_SIZE 12
#define PAYLOAD_OFFSET 8

typedef struct _SslSerialSink SslSerialSink;

struct _SslSerialSink {
  SerialSink   sink;

  SSL         *ssl;
  GByteArray  *stage;
  guint32      stage_pos;
  SslErrorCode ssl_code;
};

static gboolean   reponet_ssl_sink_type             (SerialSink* sink, SerialType type, guint size, gboolean set_allocation);
static gboolean   reponet_ssl_sink_close            (SerialSink* sink);
static gboolean   reponet_ssl_sink_write            (SerialSink* sink, const guint8 *ptr, guint32 len);
static void       reponet_ssl_sink_free             (SerialSink* sink);
static gboolean   reponet_ssl_sink_quantum          (SerialSink* sink);

gboolean
reponet_ssl_sink_quantum (SerialSink* fsink)
{
  SslSerialSink* sink = (SslSerialSink*) fsink;
  gint nwritten;
  guint32 translen = g_htonl (sink->stage->len-HEADER_SIZE);

  memcpy (sink->stage->data + PAYLOAD_OFFSET, &translen, sizeof (guint32));

  nwritten = SSL_write (sink->ssl, sink->stage->data + sink->stage_pos, sink->stage->len - sink->stage_pos);

  sink->ssl_code = SSL_get_error (sink->ssl, nwritten);

  switch (sink->ssl_code)
    {
    case REPONET_SSL_ERROR_NONE:
      break;

    case REPONET_SSL_ERROR_WANT_WRITE:
    case REPONET_SSL_ERROR_WANT_READ:
      return FALSE;

    case REPONET_SSL_ERROR_SSL:
    case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
    case REPONET_SSL_ERROR_SYSCALL:
    case REPONET_SSL_ERROR_ZERO_RETURN:
    case REPONET_SSL_ERROR_WANT_CONNECT:
      return FALSE;

    default:
      abort ();
    }

  sink->stage_pos += nwritten;

  if (sink->stage_pos == sink->stage->len)
    return TRUE;

  sink->ssl_code = REPONET_SSL_ERROR_WANT_WRITE;

  return FALSE;
}

SerialSink*
reponet_ssl_sink (SSL* ssl)
{
  SslSerialSink* it = g_new0(SslSerialSink, 1);

  serializeio_sink_init (&it->sink,
			 reponet_ssl_sink_type,
			 reponet_ssl_sink_close,
			 reponet_ssl_sink_write,
			 reponet_ssl_sink_free,
			 reponet_ssl_sink_quantum);

  it->ssl = ssl;

  return &it->sink;
}

gboolean
reponet_ssl_sink_reput (SerialSink* fsink)
{
  SslSerialSink* sink = (SslSerialSink*) fsink;

  gint nwritten;

  nwritten = SSL_write (sink->ssl, sink->stage->data + sink->stage_pos, sink->stage->len - sink->stage_pos);

  sink->ssl_code = SSL_get_error (sink->ssl, nwritten);

  switch (sink->ssl_code)
    {
    case REPONET_SSL_ERROR_NONE:
      break;

    case REPONET_SSL_ERROR_WANT_WRITE:
    case REPONET_SSL_ERROR_WANT_READ:
      return FALSE;

    case REPONET_SSL_ERROR_SSL:
    case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
    case REPONET_SSL_ERROR_SYSCALL:
    case REPONET_SSL_ERROR_ZERO_RETURN:
    case REPONET_SSL_ERROR_WANT_CONNECT:
      return FALSE;

    default:
      abort ();
    }

  sink->stage_pos += nwritten;

  if (sink->stage_pos == sink->stage->len)
    {
      g_byte_array_free (sink->stage, TRUE);
      sink->stage = NULL;
      return TRUE;
    }

  sink->ssl_code = REPONET_SSL_ERROR_WANT_WRITE;

  return FALSE;
}

gboolean
reponet_ssl_sink_type (SerialSink* fsink, SerialType type, guint size, gboolean set_allocation)
{
  SslSerialSink* sink = (SslSerialSink*) fsink;

  if (set_allocation)
    {
      if (! sink->stage)
	sink->stage = g_byte_array_new ();
      else
	g_byte_array_set_size (sink->stage, 0);

      sink->stage_pos = 0;
    }
  else
    {
      g_assert (sink->stage && sink->stage->len > 0);
    }

  if (! fsink->next_uint32 (fsink, type))
    return FALSE;

  if (set_allocation)
    {
      if (! fsink->next_uint32 (fsink, size))
	return FALSE;

      g_assert (sink->stage->len == PAYLOAD_OFFSET);

      /* THIS WILL BE REPLACED WITH PAYLOAD LEN */
      if (! fsink->next_uint32 (fsink, size))
	return FALSE;
    }

  return TRUE;
}

gboolean
reponet_ssl_sink_write (SerialSink* fsink, const guint8 *ptr, guint32 len)
{
  SslSerialSink* sink = (SslSerialSink*) fsink;

  g_byte_array_append (sink->stage, ptr, len);

  return TRUE;
}

void
reponet_ssl_sink_free (SerialSink* fsink)
{
  SslSerialSink* sink = (SslSerialSink*) fsink;

  if (sink->stage);
    g_byte_array_free (sink->stage, TRUE);

  g_free (sink);
}

gboolean
reponet_ssl_sink_close (SerialSink* fsink)
{
  return TRUE;
}

gint
reponet_ssl_sink_code (SerialSink* fsink)
{
  SslSerialSink* sink = (SslSerialSink*) fsink;

  return sink->ssl_code;
}

/* SSL source
 */

typedef struct _SslSerialSource SslSerialSource;

struct _SslSerialSource {
  SerialSource source;

  SSL* ssl;

  guint8   header_buf[HEADER_SIZE];
  gint     header_rem;

  guint8  *payload_buf;
  guint32  payload_pos;
  guint32  payload_len;
  gint     payload_rem;

  SslErrorCode ssl_code;
};

static SerialType reponet_ssl_source_type           (SerialSource* source, gboolean set_allocation);
static gboolean   reponet_ssl_source_close          (SerialSource* source);
static gboolean   reponet_ssl_source_read           (SerialSource* source, guint8 *ptr, guint32 len);
static void       reponet_ssl_source_free           (SerialSource* source);
static void       reponet_ssl_source_reset          (SerialSource* source);

SerialSource*
reponet_ssl_source (SSL* ssl)
{
  SslSerialSource* it = g_new0 (SslSerialSource, 1);

  serializeio_source_init (&it->source,
			   reponet_ssl_source_type,
			   reponet_ssl_source_close,
			   reponet_ssl_source_read,
			   reponet_ssl_source_free,
			   NULL,
			   NULL);

  it->ssl = ssl;
  it->header_rem = HEADER_SIZE;

  return &it->source;
}

void
reponet_ssl_source_reset (SerialSource* fsource)
{
  SslSerialSource* source = (SslSerialSource*) fsource;

  if (source->payload_buf)
    {
      g_free (source->payload_buf);
      source->payload_buf = NULL;
    }

  source->payload_pos = 0;
  source->payload_len = 0;
  source->payload_rem = 0;
  source->header_rem = HEADER_SIZE;
}

SerialType
reponet_ssl_source_type (SerialSource* fsource, gboolean set_allocation)
{
  SslSerialSource* source = (SslSerialSource*) fsource;
  guint32 source_type;

  if (set_allocation)
    {
      gint nread;

      g_assert (source->header_rem);

      nread = SSL_read (source->ssl, source->header_buf + HEADER_SIZE - source->header_rem, source->header_rem);

      source->ssl_code = SSL_get_error (source->ssl, nread);

      switch (source->ssl_code)
	{
	case REPONET_SSL_ERROR_NONE:
	  break;

	case REPONET_SSL_ERROR_WANT_WRITE:
	case REPONET_SSL_ERROR_WANT_READ:
	  return ST_Error;

	case REPONET_SSL_ERROR_SSL:
	case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
	case REPONET_SSL_ERROR_SYSCALL:
	case REPONET_SSL_ERROR_ZERO_RETURN:
	case REPONET_SSL_ERROR_WANT_CONNECT:
	  reponet_ssl_source_reset (fsource);
	  return ST_Error;

	default:
	  abort ();
	}

      source->header_rem -= nread;

      if (source->header_rem > 0)
	{
	  source->ssl_code = REPONET_SSL_ERROR_WANT_READ;
	  return ST_Error;
	}

      memcpy (&source_type,          source->header_buf + 0, 4);
      memcpy (&fsource->alloc_total, source->header_buf + 4, 4);
      memcpy (&source->payload_len,  source->header_buf + 8, 4);

      source_type          = g_ntohl (source_type);
      fsource->alloc_total = g_ntohl (fsource->alloc_total);
      source->payload_len  = g_ntohl (source->payload_len);

      /*g_print ("read type header %d %d %d\n", source_type, fsource->alloc_total, source->payload_len);*/

      if (source->payload_len == 0)
	/* @@@ this will foul error reporting up */
	reponet_ssl_source_reset (fsource);

      source->payload_rem = source->payload_len;
    }
  else
    {
      g_assert (! source->header_rem);

      if (! fsource->next_uint32 (fsource, & source_type))
	return ST_Error;
    }

  return source_type;
}

gboolean
reponet_ssl_source_read (SerialSource* fsource, guint8 *ptr, guint32 len)
{
  SslSerialSource* source = (SslSerialSource*) fsource;
  gint nread;

  if (source->payload_rem > 0)
    {
      if (! source->payload_buf)
	source->payload_buf = g_malloc (source->payload_len);

      nread = SSL_read (source->ssl, source->payload_buf + source->payload_len - source->payload_rem, source->payload_rem);

      source->ssl_code = SSL_get_error (source->ssl, nread);

      switch (source->ssl_code)
	{
	case REPONET_SSL_ERROR_NONE:
	  break;

	case REPONET_SSL_ERROR_WANT_WRITE:
	case REPONET_SSL_ERROR_WANT_READ:
	  return FALSE;

	case REPONET_SSL_ERROR_SSL:
	case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
	case REPONET_SSL_ERROR_SYSCALL:
	case REPONET_SSL_ERROR_ZERO_RETURN:
	case REPONET_SSL_ERROR_WANT_CONNECT:
	  reponet_ssl_source_reset (fsource);
	  return FALSE;

	default:
	  abort ();
	}

      source->payload_rem -= nread;

      if (source->payload_rem > 0)
	{
	  source->ssl_code = REPONET_SSL_ERROR_WANT_READ;
	  return FALSE;
	}
    }

  memcpy (ptr, source->payload_buf + source->payload_pos, len);

  source->payload_pos += len;

  if (source->payload_pos == source->payload_len)
    reponet_ssl_source_reset (fsource);

  return TRUE;
}

void
reponet_ssl_source_free (SerialSource* fsource)
{
  SslSerialSource* source = (SslSerialSource*) fsource;

  reponet_ssl_source_reset (fsource);

  g_free (source);
}

gboolean
reponet_ssl_source_close (SerialSource* fsource)
{
  reponet_ssl_source_reset (fsource);

  return TRUE;
}

gint
reponet_ssl_source_code (SerialSource* fsource)
{
  SslSerialSource* source = (SslSerialSource*) fsource;

  return source->ssl_code;
}
