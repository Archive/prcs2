/* -*-Mode: C;-*-
 * $Id: task.c 1.16 Mon, 03 May 1999 04:42:34 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include <unistd.h>

#include "reponet.h"
#include "reponetpriv.h"

#undef TC_Kill
#undef TC_Joined
#undef TC_Blocked
#undef TC_Rescheduled
#undef TC_Done
#undef TC_Consuming
#undef TC_Sleep

typedef struct _TaskProductionQueueEntry TaskProductionQueueEntry;

struct _TaskProductionQueueEntry {
  ConsumerFunction consume;
  void* data;
};

/* The consumer holds both queues!
 */

static void
task_allocate_queues (Task* task)
{
  if (! task->consumers)
    task->consumers = g_queue_new ();

  if (! task->production_queue)
    task->production_queue = g_queue_new ();
}

static TaskControl
task_consume_again (Connection* conn, Task* task)
{
  return task_consume (conn, task);
}

TaskControl
task_consume_done (Connection* conn, Task* task, ReadyFunc ready, void* data)
{
  return protocol_next (conn, task);
}

void
task_produce_done (Connection* conn, Task* task)
{
  task_produce (conn, task, task_consume_done, NULL);
}

void
task_produce (Connection* conn, Task* task, ConsumerFunction consume, void* data)
{
  TaskProductionQueueEntry* to_enq = g_new (TaskProductionQueueEntry, 1);

  to_enq->consume = consume;
  to_enq->data    = data;

  task_allocate_queues (task->opposite);

  g_queue_push (task->opposite->production_queue, to_enq);

  if (g_queue_get_size (task->opposite->consumers) > 0)
    {
      Task* consumer = g_queue_pop (task->opposite->consumers);

      task_set_immediate (conn, consumer, task_consume_again);
    }
}

TaskControl
task_consume_ready (Connection* conn, Task* task)
{
  return task_consume_int (conn, task, "none", 0);
}

TaskControl
task_consume_int (Connection* conn, Task* task, const char* file, gint line)
{
  task_allocate_queues (task);

  if (g_queue_get_size (task->production_queue) > 0)
    {
      TaskProductionQueueEntry* next = g_queue_pop (task->production_queue);
      TaskControl cont;

      cont = (* next->consume) (conn, task, task_consume_again, next->data);

      g_free (next);

      return cont;
    }

  g_queue_push (task->consumers, task);

  task->state = TS_WaitConsume;

  return task_control (task, TC_Consuming, file, line);
}

TaskControl
task_sleep_int (Task* task, const char* file, gint line)
{
  task->state = TS_Sleeping;

  return task_control (task, TC_Sleep, file, line);
}

gboolean
task_wake  (Task* task)
{
  task->state = TS_WaitImmediate;

  return TRUE;
}

TaskManager*
task_manager_new (void)
{
  return g_new0 (TaskManager, 1);
}

void
task_manager_add (TaskManager* tm, Task* task)
{
  tm->tasks = g_slist_prepend (tm->tasks, task);
}

void
task_manager_remove (TaskManager* tm, Task* task)
{
  tm->tasks = g_slist_remove (tm->tasks, task);
}

/*#define DEBUG_TASK_STACK2*/

#ifdef DEBUG_TASK_STACK

void
task_manager_print_tasks (TaskManager* tm)
{
  GSList *d;

  g_print ("--- task count: %d\n", g_slist_length (tm->tasks));

  for (d = tm->tasks; d; d = d->next)
    g_print (" * %s\n", task_print (d->data));
}

const char*
task_state_tostring (Task* task)
{
  switch (task->state)
    {
    case TS_WaitRead:      return "TS_WaitRead";
    case TS_WaitWrite:     return "TS_WaitWrite";
    case TS_WaitTime:      return "TS_WaitTime";
    case TS_WaitAddr:      return "TS_WaitAddr";
    case TS_WaitIdle:      return "TS_WaitIdle";
    case TS_WaitImmediate: return "TS_WaitImmediate";
    case TS_WaitConsume:   return "TS_WaitConsume";
    case TS_Sleeping:      return "TS_Sleeping";
    case TS_Running:       return "TS_Running";
    case TS_Dead:          return "TS_Dead";
    default:
      abort ();
    }
}

const char*
task_control_tostring (TaskControl cont)
{
  switch (cont.type)
    {
    case TC_Consuming: return "TC_Consuming";
    case TC_Kill: return "TC_Kill";
    case TC_Joined: return "TC_Joined";
    case TC_Blocked: return "TC_Blocked";
    case TC_Rescheduled: return "TC_Rescheduled";
    case TC_Sleep: return "TC_Sleep";
    case TC_Done: return "TC_Done";
    default:
      abort ();
    }
}

const char*
task_print (Task* task)
{
  char buf[1024];
  TaskStack* s = task->stack;

  sprintf (buf, "[%p %s ", task, task_state_tostring (task));

  for (; s; s = s->next)
    {
      strcat (buf, s->type);
      strcat (buf, " ");
    }

  strcat (buf, "]");

  return g_strdup (buf);
}
#endif

void*
task_stack_push_int (Task* task, const char* type, const char* file, int line, guint size)
{
  TaskStack* ts = g_new0 (TaskStack, 1);

#ifdef DEBUG_TASK_STACK2
  g_print ("** push %s onto %s at %s:%d\n", type, task_print (task), file, line);
#endif

  ts->next = task->stack;
  ts->data = g_malloc0 (size);
  ts->type = type;
  ts->size = size;
  task->stack = ts;

  return ts->data;
}

void*
task_stack_peek_int2 (Task* task, const char* type, const char* file, int line, guint size)
{
  g_assert (task->stack);
  g_assert (strcmp (type, task->stack->type) == 0);
  g_assert (size == task->stack->size);

  return task->stack->data;
}

void*
task_stack_peek_int (Task* task, const char* type, const char* file, int line, guint size)
{
#ifdef DEBUG_TASK_STACK2
  g_print ("-- peek %s on   %s at %s:%d\n", type, task_print (task), file, line);
#endif

  return task_stack_peek_int2 (task, type, file, line, size);
}

void*
task_stack_pop_int  (Task* task, const char* type, const char* file, int line, guint size)
{
  void* d = task_stack_peek_int2 (task, type, file, line, size);
  TaskStack* t;

#ifdef DEBUG_TASK_STACK2
  g_print ("** pop  %s from %s at %s:%d\n", type, task_print (task), file, line);
#endif

  t = task->stack;

  task->stack = t->next;

  /* intentional late free semantics.  this memory is not recovered
   * until the next entry into the event loop. */
  t->next = task->manager->free_soon;
  task->manager->free_soon = t;

  return d;
}

gboolean
task_stack_empty (Task* task)
{
  return task->stack == NULL;
}

Task*
task_new_immediate (TaskManager* tm, Connection* conn, ReadyFunc ready)
{
  Task *t = g_new0 (Task, 1);

  t->state = TS_WaitImmediate;
  t->ready = ready;
  t->data = conn;
  t->manager = tm;

  task_manager_add (tm, t);

  return t;
}

Task*
task_new_read (TaskManager* tm, Connection *conn, ReadyFunc ready)
{
  Task *t = g_new0 (Task, 1);

  t->state = TS_WaitRead;
  t->ready = ready;
  t->fd = sock_fd (conn->sock);
  t->data = conn;
  t->manager = tm;

  task_manager_add (tm, t);

  return t;
}

Task*
task_new_wait (TaskManager* tm, void* data, time_t seconds, ReadyFunc ready)
{
  Task *t = g_new0 (Task, 1);

  t->state = TS_WaitTime;
  t->activate = time (NULL) + seconds;
  t->ready = ready;
  t->data = data;
  t->manager = tm;

  task_manager_add (tm, t);

  return t;
}

Task*
task_new_idle (TaskManager* tm, void* data, ReadyFunc ready)
{
  Task *t = g_new0 (Task, 1);

  t->state = TS_WaitIdle;
  t->data = data;
  t->ready = ready;
  t->manager = tm;

  task_manager_add (tm, t);

  return t;
}

Task*
task_new_accept (TaskManager* tm, Protocol* protocol, ReadyAFunc ready, ServerSocket* sock)
{
  Task *t = g_new0 (Task, 1);

  t->state = TS_WaitRead;
  t->ready = (ReadyFunc) ready;
  t->data = protocol;
  t->fd = sock_serv_fd (sock);
  t->manager = tm;

  task_manager_add (tm, t);

  return t;
}

TaskControl
task_control (Task* task, TaskControlType type, const char* file, gint line)
{
  TaskControl cont;
  task->current_line = line;
  task->current_file = file;
  cont.type = type;
  return cont;
}

TaskControl
task_set_read_int (Connection *conn, Task* task, ReadyFunc ready, const char* file, gint line)
{
  task->state = TS_WaitRead;
  task->fd = SSL_get_fd (conn->ssl);
  task->ready = ready;
  task->data = conn;

  return task_control (task, TC_Blocked, file, line);
}

TaskControl
task_set_write_int (Connection *conn, Task* task, ReadyFunc ready, const char* file, gint line)
{
  task->state = TS_WaitWrite;
  task->fd = SSL_get_fd (conn->ssl);
  task->ready = ready;
  task->data = conn;

  return task_control (task, TC_Blocked, file, line);
}


TaskControl
task_set_accept_int (Protocol* protocol, Task* task, ReadyAFunc ready, ServerSocket* sock, const char* file, gint line)
{
  task->state = TS_WaitRead;
  task->data = protocol;
  task->ready = (ReadyFunc) ready;
  task->fd = sock_serv_fd (sock);

  return task_control (task, TC_Blocked, file, line);
}

TaskControl
task_set_connect_int (Connection* conn, Task* task, ReadyFunc ready, const char* file, gint line)
{
  task->state = TS_WaitRead;
  task->data = conn;
  task->ready = ready;
  task->fd = sock_fd (conn->sock);

  return task_control (task, TC_Blocked, file, line);
}

TaskControl
task_set_wait_int (Task* task, void *data, time_t seconds, ReadyFunc ready, const char* file, gint line)
{
  task->state = TS_WaitTime;
  task->activate = time (NULL) + seconds;
  task->ready = ready;
  task->data = data;

  return task_control (task, TC_Rescheduled, file, line);
}

void
task_set_immediate (Connection* conn, Task   *task, ReadyFunc ready)
{
  task->state = TS_WaitImmediate;
  task->data = conn;
  task->ready = ready;
}

typedef struct {
  ReadyFunc func;
} FinishPutData;

TaskControl
task_finish_put_again (Connection *conn, Task* task)
{
  FinishPutData* data = task_stack_pop (task, FinishPutData);

  return task_finish_put (conn,
			  task,
			  data->func,
			  reponet_ssl_sink_reput (conn->sink));
}

TaskControl
task_finish_put (Connection *conn,
		 Task* task,
		 ReadyFunc ready,
		 gboolean not_blocked)
{
  FinishPutData* data;

  if (not_blocked)
    return (* ready) (conn, task);

  data = task_stack_push (task, FinishPutData);
  data->func = ready;

  switch (reponet_ssl_sink_code (conn->sink))
    {
    case REPONET_SSL_ERROR_NONE:
      abort ();

    case REPONET_SSL_ERROR_WANT_WRITE:
      return task_set_write (conn, task, task_finish_put_again);

    case REPONET_SSL_ERROR_WANT_READ:
      return task_set_read (conn, task, task_finish_put_again);

    case REPONET_SSL_ERROR_SSL:
      net_generate_connssl_event (EC_NetSSLWriteFailed, conn);
      break;

    case REPONET_SSL_ERROR_SYSCALL:
      net_generate_connerrno_event (EC_NetSSLErrno, conn);
      break;

    case REPONET_SSL_ERROR_ZERO_RETURN:
    case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
    case REPONET_SSL_ERROR_WANT_CONNECT:
      net_generate_int_event (EC_NetSSLIsHardToReport, reponet_ssl_sink_code (conn->sink));
      break;

    default:
      abort ();
    }

  return reponet_kill_task (task);
}

typedef struct {
  ReadyFunc func;
  gint         get_types;
  void       **get_addr;
  SerialType  *get_type;
} FinishGetData;

TaskControl
task_finish_get_again (Connection *conn, Task* task)
{
  FinishGetData* data = task_stack_pop (task, FinishGetData);

  return task_finish_get (conn,
			  task,
			  data->func,
			  data->get_types,
			  data->get_type,
			  data->get_addr);
}

TaskControl
task_finish_get (Connection *conn,
		 Task* task,
		 ReadyFunc ready,
		 guint32 accepted_types,
		 SerialType *type,
		 void** ent_addr)
{
  SerialType unused;
  FinishGetData* data;

  gboolean got_it = serializeio_unserialize_generic_acceptable (conn->source, accepted_types, type ? type : &unused, ent_addr);

  if (got_it)
    return (* ready) (conn, task);

  data = task_stack_push (task, FinishGetData);

  data->func = ready;
  data->get_addr = ent_addr;
  data->get_types = accepted_types;
  data->get_type = type;

  switch (reponet_ssl_source_code (conn->source))
    {
    case REPONET_SSL_ERROR_NONE:
      break;

    case REPONET_SSL_ERROR_WANT_WRITE:
      return task_set_write (conn, task, task_finish_get_again);

    case REPONET_SSL_ERROR_WANT_READ:
      return task_set_read (conn, task, task_finish_get_again);

    case REPONET_SSL_ERROR_SSL:
      net_generate_connssl_event (EC_NetSSLWriteFailed, conn);
      break;

    case REPONET_SSL_ERROR_SYSCALL:
      net_generate_connerrno_event (EC_NetSSLErrno, conn);
      break;

    case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
    case REPONET_SSL_ERROR_ZERO_RETURN:
    case REPONET_SSL_ERROR_WANT_CONNECT:
      net_generate_int_event (EC_NetSSLIsHardToReport, reponet_ssl_sink_code (conn->sink));
      break;

    default:
      abort ();
    }

  return reponet_kill_task (task);
}

typedef struct {
  ReadyFunc func;
} TaskData;

TaskControl
task_ssl_connect_again (Connection *conn, Task* task)
{
  TaskData* data = task_stack_pop (task, TaskData);

  return task_ssl_connect (conn, task, data->func);
}

TaskControl
task_ssl_connect (Connection *conn, Task* task, ReadyFunc ready)
{
  gint i;
  TaskData* data = task_stack_push (task, TaskData);

  data->func = ready;

  i = SSL_connect (conn->ssl);

  if (BIO_sock_should_retry(i))
    {
      return task_set_read (conn, task, task_ssl_connect_again);
    }

  if (i <= 0)
    {
      net_generate_connssl_event (EC_NetSSLConnectFailed, conn);
      return reponet_kill_task (task);
    }

  task_stack_pop (task, TaskData);

  return (* ready) (conn, task);
}

TaskControl
task_ssl_accept_again (Connection *conn, Task* task)
{
  TaskData* data = task_stack_pop (task, TaskData);

  return task_ssl_accept (conn, task, data->func);
}

TaskControl
task_ssl_accept (Connection *conn, Task* task, ReadyFunc ready)
{
  gint i;
  TaskData* data = task_stack_push (task, TaskData);

  data->func = ready;

  i = SSL_accept (conn->ssl);

  if (BIO_sock_should_retry(i))
    {
      return task_set_read (conn, task, task_ssl_accept_again);
    }

  if (i <= 0)
    {
      net_generate_connssl_event (EC_NetSSLAcceptFailed, conn);
      return reponet_kill_task (task);
    }

  task_stack_pop (task, TaskData);

  return (* ready) (conn, task);
}

/* These happen before the connection enters SSL mode, just for
 * identification. */
TaskControl
task_puts (Connection *conn, Task* task, ReadyFunc ready, const gchar* line)
{
  gint len = strlen (line);

  if (write (sock_fd (conn->sock), line, len) != len)
    {
      net_generate_connerrno_event (EC_NetWriteFailed, conn);
      return reponet_kill_task (task);
    }

  return (* ready) (conn, task);
}

typedef struct {
  ReadyFunc func;
} GetsData;

TaskControl
task_gets_again (Connection *conn, Task* task)
{
  GetsData* data = task_stack_pop (task, GetsData);

  return task_gets (conn, task, data->func, NULL);
}

TaskControl
task_gets (Connection *conn, Task* task, ReadyFunc ready, const gchar** line)
{
  gchar buf[1];
  gint nread;
  GetsData* data = task_stack_push (task, GetsData);

  data->func = ready;

  g_assert (! line);

  while ((nread = read (sock_fd (conn->sock), buf, 1)) == 1)
    if (buf[0] == '\n')
      {
	task_stack_pop (task, GetsData);
	return (* ready) (conn, task);
      }

  if ((nread < 0 && errno != EAGAIN && errno != EINTR && errno != EWOULDBLOCK) || nread == 0)
    {
      if (nread == 0)
	net_generate_conn_event (EC_NetUnexpectedEOF, conn);
      else
	net_generate_connerrno_event (EC_NetReadFailed, conn);

      return reponet_kill_task (task);
    }
  else
    {
      return task_set_read (conn, task, task_gets_again);
    }
}

/**********************************************************************/
/*			   SEGMENT GET/PUT                            */
/**********************************************************************/

/* This code sucks badly. */

/*#define DEBUG_SEGMENTS*/

typedef struct {
  FileSegment* seg1;
  FileSegment* seg2;
  FileSegment* seg3;
  ReadyFunc seg_function;
  SerialSegmentTransferHeader *seg_transfer;
  guint seg_offset;
  guint seg_onpage;
  guint seg_psize;
  guint seg_pgno;
  const guint8 *seg_page;
  FileHandle   *seg_handle;
} TaskSegmentData;

static TaskControl
task_segments_get_data (Connection* conn, Task* task)
{
  gint to_read;
  guint8 read_buf[1<<12];
  TaskSegmentData* data = task_stack_peek (task, TaskSegmentData);

  if (! data->seg_handle)
    {
      if (! (data->seg_handle = segment_open (data->seg1,
					      HV_Replace |
					      HV_NoSeek)))
	return reponet_kill_task (task);
    }

  to_read = data->seg_transfer->data_len - data->seg_offset;

  to_read = MIN (to_read, 1<<12);

  if (to_read > 0)
    {
      gint nread, status;

      nread = SSL_read (conn->ssl, read_buf, to_read);

      status = SSL_get_error (conn->ssl, nread);

      switch (status)
	{
	case REPONET_SSL_ERROR_NONE:
	  break;

	case REPONET_SSL_ERROR_WANT_WRITE:
	  return task_set_write (conn, task, task_segments_get_data);

	case REPONET_SSL_ERROR_WANT_READ:
	  return task_set_read (conn, task, task_segments_get_data);

	case REPONET_SSL_ERROR_SSL:
	  net_generate_connssl_event (EC_NetSSLReadFailed, conn);
	  return reponet_kill_task (task);

	case REPONET_SSL_ERROR_SYSCALL:
	  net_generate_connerrno_event (EC_NetSSLErrno, conn);
	  return reponet_kill_task (task);

	case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
	case REPONET_SSL_ERROR_ZERO_RETURN:
	case REPONET_SSL_ERROR_WANT_CONNECT:
	  net_generate_int_event (EC_NetSSLIsHardToReport, reponet_ssl_sink_code (conn->sink));
	  return reponet_kill_task (task);

	default:
	  abort ();
	}

      g_assert (nread > 0);

      data->seg_offset += nread;

      if (! handle_write (data->seg_handle, read_buf, nread))
	return reponet_kill_task (task);

      return task_segments_get_data (conn, task);
    }

  if (! handle_close (data->seg_handle))
    return reponet_kill_task (task);

  task_stack_pop (task, TaskSegmentData);

  return task_segments_get (conn, task, data->seg_function,
			    data->seg2,
			    data->seg3,
			    NULL);
}

TaskControl
task_segments_get (Connection  *conn,
		   Task        *task,
		   ReadyFunc    ready,
		   FileSegment *seg1,
		   FileSegment *seg2,
		   FileSegment *seg3)
{
  TaskSegmentData* data;

  if (! seg1)
    return ready (conn, task);

  data = task_stack_push (task, TaskSegmentData);

  data->seg_offset = 0;
  data->seg_handle = NULL;
  data->seg1 = seg1;
  data->seg2 = seg2;
  data->seg3 = seg3;
  data->seg_function = ready;

  return task_finish_get (conn,
			  task,
			  task_segments_get_data,
			  ST_SegmentTransferHeader,
			  NULL,
			  (void**) &data->seg_transfer);
}

static TaskControl
task_segments_put_data (Connection* conn, Task* task)
{
  TaskSegmentData* data = task_stack_peek (task, TaskSegmentData);

enter:

  if (data->seg_offset < data->seg_onpage)
    {
      gint nwritten, status;

      nwritten = SSL_write (conn->ssl, (guint8*)(data->seg_page + data->seg_offset),
			    data->seg_onpage - data->seg_offset);

      status = SSL_get_error (conn->ssl, nwritten);

      switch (status)
	{
	case REPONET_SSL_ERROR_NONE:
	  break;

	case REPONET_SSL_ERROR_WANT_WRITE:
	  return task_set_write (conn, task, task_segments_put_data);

	case REPONET_SSL_ERROR_WANT_READ:
	  return task_set_read (conn, task, task_segments_put_data);

	case REPONET_SSL_ERROR_SSL:
	  net_generate_connssl_event (EC_NetSSLWriteFailed, conn);
	  return reponet_kill_task (task);

	case REPONET_SSL_ERROR_SYSCALL:
	  net_generate_connerrno_event (EC_NetSSLErrno, conn);
	  return reponet_kill_task (task);

	case REPONET_SSL_ERROR_WANT_X509_LOOKUP:
	case REPONET_SSL_ERROR_ZERO_RETURN:
	case REPONET_SSL_ERROR_WANT_CONNECT:
	  net_generate_int_event (EC_NetSSLIsHardToReport, reponet_ssl_sink_code (conn->sink));
	  return reponet_kill_task (task);

	default:
	  abort ();
	}

      g_assert (nwritten > 0);

      data->seg_offset += nwritten;

      goto enter;
    }

  if (data->seg_onpage == data->seg_psize)
    {
      if (! handle_unmap_page (data->seg_handle, data->seg_pgno++, &data->seg_page))
	return reponet_kill_task (task);

      data->seg_onpage = handle_map_page (data->seg_handle, data->seg_pgno, &data->seg_page);
      data->seg_offset = 0;

      if (data->seg_onpage < 0)
	return reponet_kill_task (task);

      goto enter;
    }

  if (! handle_unmap_page (data->seg_handle, data->seg_pgno++, &data->seg_page))
    return reponet_kill_task (task);

  if (! handle_close (data->seg_handle))
    return reponet_kill_task (task);

  task_stack_pop (task, TaskSegmentData);

  return task_segments_put (conn, task, data->seg_function, data->seg2, data->seg3, NULL);
}

TaskControl
task_segments_put (Connection  *conn,
		   Task        *task,
		   ReadyFunc    ready,
		   FileSegment *seg1,
		   FileSegment *seg2,
		   FileSegment *seg3)
{
  gssize s_len;
  TaskSegmentData* data;

  if (! seg1)
    return ready (conn, task);

  data = task_stack_push (task, TaskSegmentData);

  if (! (data->seg_handle = segment_open (seg1, HV_Read)))
    return reponet_kill_task (task);

  data->seg_psize = handle_pagesize (data->seg_handle);
  data->seg_onpage = 0;
  data->seg_offset = 0;
  data->seg_page = 0;
  data->seg_pgno = 0;

  data->seg_onpage = handle_map_page (data->seg_handle, data->seg_pgno, &data->seg_page);

  if (data->seg_onpage < 0)
    return reponet_kill_task (task);

  data->seg1 = seg1;
  data->seg2 = seg2;
  data->seg3 = seg3;

  data->seg_function = ready;

  if ((s_len = segment_length (seg1)) < 0)
    return reponet_kill_task (task);

  return task_finish_put (conn,
			  task,
			  task_segments_put_data,
			  serialize_segmenttransferheader (conn->sink, s_len));
}
