/* -*-Mode: C;-*-
 * $Id: log.c 1.12 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "server.h"
#include "serverpriv.h"

struct _ServerLog {
  File *dir;
  File *seq;
};

gboolean
server_log_library_init (void)
{
  return TRUE;
}

ServerLog*
server_log_init (Path* path)
{
  ServerLog* log;
  File *seq;
  File *dir;

  dir = file_initialize (srepo, path);

  if (file_is_not_type (dir, FV_Directory))
    return NULL;

  seq = file_initialize (srepo, path_append (path, "log"));

  if (file_is_not_type_noerr (seq, FV_Sequence))
    {
      if (! file_sequence_create (seq))
	return NULL;
    }

  log = g_new0 (ServerLog, 1);

  log->dir = dir;
  log->seq = seq;

  return log;
}

gboolean
server_log_create (Path* path)
{
  return file_mkdir_p (srepo, path) != NULL;
}

SerialSink*
server_log_sink (ServerLog* log)
{
  FileHandle* fh = file_open (log->seq, HV_Replace);

  return handle_sink (fh, NULL, NULL, NULL, NULL);
}

gint
server_log_point (ServerLog* log)
{
  return file_sequence_value (log->seq);
}

gboolean
server_log_print (ServerLog* log)
{
  gint i, value;

  if ((value = file_sequence_value (log->seq)) < 0)
    return FALSE;

  for (i = 0; i < value; i += 1)
    {
      File* f;
      SerialSource* src;
      SerialType     object_type;
      void          *object;

      if (! (f = file_sequence_initialize_element (log->seq, i)))
	return FALSE;

      if (file_is_not_type (f, FV_Regular))
	return FALSE;

      src = segment_source (file_default_segment (f));

      if (! serializeio_unserialize_generic (src, & object_type, & object))
	return FALSE;

      switch (object_type)
	{
	case ST_NamespaceAddLogEntry:
	  {
	    SerialNamespaceAddLogEntry* ent = object;

	    g_print ("Add namespace: %s; alias: %s\n", ent->nspath, ent->alias);
	  }
	  break;

	case ST_NamespaceInsertLogEntry:
	  {
	    SerialNamespaceInsertLogEntry* ent = object;
	    char buf[33];

	    edsio_md5_to_string (ent->digest, buf);

	    g_print ("Namespace insert: %s; md5: %s\n", ent->nspath, buf);
	  }
	  break;

	case ST_EventLogEntry:
	  {
	    SerialEventLogEntry* ent = object;
	    GenericEventDef* def = eventdelivery_event_lookup (ent->code);
	    const char* name;
	    const char* level;

	    if (! def)
	      {
		name = "*Unknown*";
		level = "*Unknown*";
	      }
	    else
	      {
		name = def->name;

		switch (def->level)
		  {
		  case EL_Information: level = "Information"; break;
		  case EL_Warning: level = "Warning"; break;
		  case EL_Error: level = "Error"; break;
		  case EL_InternalError: level = "InternalError"; break;
		  case EL_FatalError: level = "FatalError"; break;
		  default: level = "*Unknown*"; break;
		  }
	      }

	    g_print ("%s.%06d:%s:%s:%s:%d: %s\n", edsio_time_to_iso8601 (& ent->time), (ent->time.nanos / 1000), name, level, ent->file, ent->line, ent->msg);
	  }
	  break;
	default:
	  g_warning ("unknown log entry type\n");
	}
    }

  return TRUE;
}

gboolean
server_log_find (ServerLog* log, guint num, void** obj, SerialType* st)
{
  SerialSource *src;

  File* ent;

  if (! (ent = file_sequence_initialize_element (log->seq, num)))
    return FALSE;

  if (file_is_not_type (ent, FV_Regular))
    return FALSE;

  if (! (src = segment_source (file_default_segment (ent))))
    return FALSE;

  if (! serializeio_unserialize_generic (src, st, obj))
    return FALSE;

  if (! src->source_close (src))
    return FALSE;

  src->source_free (src);

  return TRUE;
}
