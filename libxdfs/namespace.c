/* -*-Mode: C;-*-
 * $Id: namespace.c 1.19 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "server.h"
#include "serverpriv.h"

/* There are 3 kinds of namespaces.  Each is a named directory which
 * contains named, aliasable sub-entries.  There is no distributed
 * consistency enforced on global name spaces, they are simply a
 * mechanism for separating different applications and for the case of
 * PRCS, projects.  Each PRCS project P will be located a namespace
 * (using UNIX-file-separators in the example):
 *
 *   /prcs/projects/UNIQUE_PROJECT_ID
 *
 * The server will maintain project aliases and a principle name
 * (alias) for the project named by UNIQUE_PROJECT_ID.  This allows
 * PRCS projects to be renamed, but since there is no global
 * consistency, renaming a project means renaming it on every server.
 *
 * A ControlledNameSpace is a type of namespace for which a single
 * machine has control.  That server is the authority responsible for
 * the files contained within the controlled directory.  A
 * ControlledNameSpace may not contain sub-directories, for now.  The
 * files are serially numbered with names "1" through "N" according to
 * the order in which they were inserted.  A controlled name space,
 * then, will be used to implement branches in PRCS.  The contents of
 * a branch are project files, named with their minor version number.
 * For example, a branch might be named:
 *
 *   /prcs-2.0/projects/UNIQUE_PROJECT_ID/UNIQUE_BRANCH_ID
 *
 * and the project files contained within are:
 *
 *   /prcs-2.0/projects/UNIQUE_PROJECT_ID/UNIQUE_BRANCH_ID/1
 *   /prcs-2.0/projects/UNIQUE_PROJECT_ID/UNIQUE_BRANCH_ID/2
 *
 * (I start with 1 because 0 has a special meaning in PRCS).  The
 * server asks application for a list of uncontrolled file versions
 * (see below) are named by each control file, and uses the information
 * to distribute the contents of each PRCS project version.
 *
 * Again, the server maintains aliases and a principle name of each
 * branch (controlled name space) entry, allowing renaming, or local
 * aliasing of branches.
 *
 * An AbsoluteNameSpace is a type of namespace which contains a
 * versioned family of (similar) files.  The application uses these
 * namespaces to collect files referenced by other (control) files in
 * the controlled name space.  These file versions may be shared
 * between control files in different branches.  A versioned file
 * might be named:
 *
 *   /prcs-2.0/families/UNIQUE_FAMILY_ID/MD5OFCONTENTS
 *
 * Files in an absolute name space are named by the MD5 of their
 * contents.  The servers are equipped to exchange and store these
 * versioned file directories efficiently and using delta transfers,
 * using the techniques described in my paper on Xdelta.
 */


static ServFileNameSpaceProperty FP_NameSpace;
Path* _nsr;

/* @@@ Need to name subdirs with a prefix to avoid name conflicts, same with
 * aliases.  LATER. */
const char*
server_namespace_path_to_nspath_int (Path* path)
{
  return path_to_string_simple (path_suffix (path, path_length (path) - 1), '/') + 1;
}

const char*
server_namespace_path_to_nspath (Path* path)
{
  return path_to_string_simple (path, '/') + 1;
}

const char*
server_namespace_nspath (NameSpace* ns)
{
  return ns->nspath;
}

AliasedIdentifier*
server_namespace_id (NameSpace *ns)
{
  return & ns->aid;
}

File*
server_namespace_xdfs_index (NameSpace *ns)
{
  return xdfs_index (ns->xdfs);
}

File*
server_namespace_xdfs_sequence (NameSpace *ns)
{
  return xdfs_sequence (ns->xdfs);
}

static gboolean
server_namespace_register (XdfsDirectory *xdfs, const guint8* md5)
{
  SerialSink* sink = server_log_sink (server_namespace_log ());
  NameSpace* ns = xdfs_user_data (xdfs);

  if (! serialize_namespaceinsertlogentry (sink, ns->nspath, 16, md5) ||
      ! sink->sink_close (sink))
    return FALSE;

  sink->sink_free (sink);

  return TRUE;
}

gboolean
server_namespace_init (void)
{
  _nsr = path_absolute ("ns");

  xdfs_listen (server_namespace_register);

  if (! edsio_new_file_namespace_property ("NS", PF_Persistent, & FP_NameSpace))
    return FALSE;

  return TRUE;
}

gboolean
server_namespace_create (void)
{
  NameSpace *ns = g_new0 (NameSpace, 1);
  File* root;

  ns->aid.alias = "";
  ns->aid.id = "";

  root = file_initialize (srepo, _nsr);

  if (! file_mkdir (root))
    return FALSE;

  return file_set_namespace (root, FP_NameSpace, ns);
}

gboolean
server_namespace_peer_log (ServerPeer* peer, SerialSyncLog* update)
{
  switch (update->entry_type)
    {
    case ST_NamespaceInsertLogEntry:
      {
	SerialNamespaceInsertLogEntry* ent = update->entry;
	NameSpace* ns;
	char md5str[33];

	if (! (ns = server_namespace_attach_nspath (ent->nspath, NULL, NSAF_Create)))
	  return FALSE;

	if (! xdfs_sync_log_peer_insert (ns->xdfs, peer, ent->digest))
	  return FALSE;

	edsio_md5_to_string (ent->digest, md5str);

	serv_generate_stringstring_event (EC_ServXdfsPeerAdd, ent->nspath, md5str);

	return TRUE;
      }
      break;
    case ST_NamespaceAddLogEntry:
      {
	SerialNamespaceAddLogEntry* ent = update->entry;
	const char* path_str = ent->nspath;
	const char* alias_str = ent->alias;
	NameSpace* ns;

	if (! (ns = server_namespace_attach_nspath (ent->nspath, alias_str, NSAF_Create)))
	  return FALSE;

	serv_generate_stringstring_event (EC_ServSyncNameSpacePeerAdd, path_str, ent->alias);

	return TRUE;
      }
      break;
    default:
      serv_generate_int_event (EC_ServSyncNameSpaceInvalidUpdate, update->entry_type);
      return FALSE;
    }
}

NameSpace*
server_namespace_attach_nspath (const char* nspath,
				const char *alias,
				gint        flags)
{
  File* dir;
  File* missing;
  NameSpace *ns;
  Path* path;
  Path* nspath_path;

  if (! (nspath_path = path_canonicalize_simple (path_root (),
						 '/',
						 nspath)))
    return NULL;

  path = path_append_path (_nsr, nspath_path);

  if (flags & NSAF_Create && ! file_mkdir_p (srepo, path_dirname (path)))
    return NULL;

  dir = file_initialize (srepo, path);

  if (flags & NSAF_Create && file_is_type_noerr (dir, FV_NotPresent))
    {
      SerialSink* sink;

      if (flags & NSAF_Unique)
	{
	  alias = path_basename (path);
	  path = path_append (path_dirname (path), server_create_unique_id ());
	  dir = file_initialize (srepo, path);
	}

      if (! alias)
	alias = "";

      if (! file_mkdir (dir))
	return NULL;

      missing = file_initialize_entry (dir, "mis");

      if (! file_sequence_create (missing))
	return NULL;

      ns = g_new0 (NameSpace, 1);

      ns->aid.id = path_basename (path);
      ns->aid.alias = alias;

      if (! file_set_namespace (dir, FP_NameSpace, ns))
	return NULL;

      if (alias[0])
	{
	  File* alias_file;
	  Path* alias_path;

	  alias_path = path_append (path_dirname (path), alias);

	  alias_file = file_initialize (srepo, alias_path);

	  if (file_is_type_noerr (alias_file, FV_NotPresent) &&
	      ! file_symlink (alias_file, path_basename (path)))
	    return NULL;
	}

      sink = server_log_sink (server_namespace_log ());

      if (! serialize_namespaceaddlogentry (sink, server_namespace_path_to_nspath_int (path), alias) ||
	  ! sink->sink_close (sink))
	return FALSE;

      sink->sink_free (sink);
    }

  missing = file_initialize_entry (dir, "mis");

  if (file_is_not_type (dir, FV_Directory))
    return NULL;

  if (file_is_not_type (missing, FV_Sequence))
    return NULL;

  if (! file_get_namespace (dir, FP_NameSpace, & ns))
    return NULL;

  ns->nspath = g_strdup (nspath);
  ns->dir = dir;
  ns->missing_seq = missing;

  if (! (ns->xdfs = xdfs_directory_init (dir, ns)))
    return NULL;

  return ns;
}
