/* -*-Mode: C;-*-
 * $Id: sconfig.c 1.7 Fri, 30 Apr 1999 01:29:46 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "server.h"
#include "serverpriv.h"

static gboolean server_config_init_peers (void);
static gboolean server_protocol_init         (Protocol* protocol);
static gboolean server_protocol_create       (Protocol* protocol);

static File* server_dir;
static Path* server_path;

static Path* events_log_path;
static Path* namespace_log_path;

static File* server_dir;
static Path* server_path;

static File* protocols_dir;
static Path* protocols_path;

static File* peers_dir;
static Path* peers_path;

static const char* _server_name;
static const char* _server_id;

static ServerLog* _server_event_log;
static ServerLog* _server_namespace_log;

static GPtrArray* server_peers;
static GHashTable* server_peers_by_id;

static ServDirStringProperty     FP_ServerId;
static ServDirUintProperty       FP_ProtocolPort;
static ServDirPeerRecordProperty FP_PeerRecord;
static ServDirStringProperty     FP_ServerName;
static ServDirUintProperty       FP_PeerSequence; /* @@@ get rid of this for a real sequence, and an index? */

const char*
server_id (void)
{
  return _server_id;
}

const char*
server_name (void)
{
  return _server_name;
}

ServerLog*
server_event_log (void)
{
  return _server_event_log;
}

ServerLog*
server_namespace_log (void)
{
  return _server_namespace_log;
}

static gboolean
sconfig_event_watch (GenericEvent* ev, GenericEventDef* def, const char* message)
{
  SerialSink* sink;
  SerialGenericTime time;

  /*if (def->level >= EL_Warning)*/
    g_print ("%s:%d: %s\n", ev->srcfile, ev->srcline, message);

  if (! (sink = server_log_sink (server_event_log ())))
    return FALSE;

  if (! edsio_time_of_day (& time))
    return FALSE;

  if (! serialize_eventlogentry (sink, ev->code, message, ev->srcfile, ev->srcline, & time))
    return FALSE;

  if (! sink->sink_close (sink))
    return FALSE;

  sink->sink_free (sink);

  return TRUE;
}

static gboolean
sconfig_init_internal (void)
{
  File* server_pkey;

  eventdelivery_event_watch_all (sconfig_event_watch);

  server_path = path_absolute ("server");
  server_dir = file_initialize (srepo, server_path);

  protocols_path = path_absolute ("protocols");
  protocols_dir = file_initialize (srepo, protocols_path);

  peers_path = path_absolute ("peers");
  peers_dir = file_initialize (srepo, peers_path);

  events_log_path = path_absolute2 ("server", "events_log");
  namespace_log_path = path_absolute2 ("server", "namespace_log");

  if (file_is_not_type (server_dir, FV_Directory))
    return FALSE;

  if (file_is_not_type (protocols_dir, FV_Directory))
    return FALSE;

  if (file_is_not_type (peers_dir, FV_Directory))
    return FALSE;

  server_pkey = file_initialize (srepo, path_append (server_path, "identity"));

  /* @@@ this just creates a key if one doesn't exist.  fix later. */
  if (! reponet_init (server_pkey))
    return FALSE;

  if (! edsio_new_dir_uint_property ("port", PF_Persistent, & FP_ProtocolPort))
    return FALSE;

  if (! edsio_new_dir_uint_property ("peer_sequence", PF_Persistent, & FP_PeerSequence))
    return FALSE;

  if (! edsio_new_dir_peerrecord_property ("record", PF_Persistent, & FP_PeerRecord))
    return FALSE;

  if (! edsio_new_dir_string_property ("server_id", PF_Persistent, & FP_ServerId))
    return FALSE;

  if (! edsio_new_dir_string_property ("server_name", PF_Persistent, & FP_ServerName))
    return FALSE;

  return TRUE;
}

gboolean
server_config_create (const char* name)
{
  gint i;

  if (! file_mkdir (file_initialize (srepo, path_absolute ("server"))))
    return FALSE;

  if (! file_mkdir (file_initialize (srepo, path_absolute ("protocols"))))
    return FALSE;

  if (! file_mkdir (file_initialize (srepo, path_absolute ("peers"))))
    return FALSE;

  if (! sconfig_init_internal ())
    return FALSE;

  if (! server_log_create (events_log_path))
    return FALSE;

  if (! server_log_create (namespace_log_path))
    return FALSE;

  if (! (_server_event_log = server_log_init (events_log_path)))
    return FALSE;

  if (! (_server_namespace_log = server_log_init (namespace_log_path)))
    return FALSE;

  if (! dir_set_uint (server_dir, FP_PeerSequence, 0))
    return FALSE;

  _server_id = server_create_unique_id ();

  serv_generate_stringstring_event (EC_ServCreated, name, _server_id);

  if (! dir_set_string (server_dir, FP_ServerId, _server_id))
    return FALSE;

  if (! dir_set_string (server_dir, FP_ServerName, name))
    return FALSE;

  for (i = 0; i < server_protocols->len; i += 1)
    {
      Protocol* proto = server_protocols->pdata[i];

      if (! server_protocol_create (proto))
	return FALSE;

      if (! proto->create (srepo, proto))
	return FALSE;
    }

  return TRUE;
}

 gboolean
server_config_init (void)
{
  gint i;

  if (! sconfig_init_internal ())
    return FALSE;

  if (! dir_get_string (server_dir, FP_ServerId, & _server_id))
    return FALSE;

  if (! dir_get_string (server_dir, FP_ServerName, & _server_name))
    return FALSE;

  if (! (_server_event_log = server_log_init (events_log_path)))
    return FALSE;

  if (! (_server_namespace_log = server_log_init (namespace_log_path)))
    return FALSE;

  for (i = 0; i < server_protocols->len; i += 1)
    {
      Protocol* proto = server_protocols->pdata[i];

      if (! server_protocol_init (proto))
	return FALSE;

      if (! proto->init (srepo, proto))
	return FALSE;
    }

  return server_config_init_peers ();
}

gboolean
server_protocol_init (Protocol* protocol)
{
  if (! protocol->port)
    return TRUE;

  protocol->protocol_dir = file_initialize (srepo, path_append (protocols_path, protocol->name));

  if (file_is_not_type (protocol->protocol_dir, FV_Directory))
    return FALSE;

  if (! dir_get_uint (protocol->protocol_dir, FP_ProtocolPort, & protocol->port))
    return FALSE;

  return TRUE;
}

gboolean
server_protocol_create (Protocol* protocol)
{
  if (! protocol->port)
    return TRUE;

  protocol->protocol_dir = file_initialize (srepo, path_append (protocols_path, protocol->name));

  if (! file_mkdir (protocol->protocol_dir))
    return FALSE;

  if (! dir_set_uint (protocol->protocol_dir, FP_ProtocolPort, protocol->port))
    return FALSE;

  return TRUE;
}

const char*
server_config_sync_setting_to_string (gint setting)
{
  static GString* str = NULL;
  gboolean no_comma = TRUE;

  if (! str)
    str = g_string_new (NULL);

  if (setting == SSS_None)
    return "none";

  g_string_truncate (str, 0);

  if (setting & SSS_Push)
    {
      g_string_append (str, "push");
      no_comma = FALSE;
    }

  if (setting & SSS_Pull)
    {
      g_string_sprintfa (str, "%spull", no_comma ? "" : ",");
      no_comma = FALSE;
    }

  if (setting & SSS_AllowPush)
    {
      g_string_sprintfa (str, "%sallowpush", no_comma ? "" : ",");
      no_comma = FALSE;
    }

  if (setting & SSS_AllowPull)
    {
      g_string_sprintfa (str, "%sallowpull", no_comma ? "" : ",");
      no_comma = FALSE;
    }

  return str->str;
}

gboolean
server_config_print (FileHandle* fh)
{
  DirectoryHandle* dh;
  Path* ent;
  gint i;

  if (! handle_printf (fh, "Server version: %s\n", server_version ()) ||
      ! handle_printf (fh, "Server name:    %s\n", server_name ()) ||
      ! handle_printf (fh, "Server id:      %s\n", server_id ()))
    return FALSE;

  /* Protocols
   */

  if (! (dh = file_opendir (protocols_dir)))
    return FALSE;

  if (! handle_printf (fh, "\n[Protocol\t\tPort]\n") ||
      ! handle_printf (fh, "-----------------------------------------------------------------------------\n"))
    return FALSE;

  while ((ent = dh_next (dh)) != NULL)
    {
      File* proto = file_initialize (srepo, ent);
      guint32 port;

      if (file_is_not_type (proto, FV_Directory))
	continue;

      if (! dir_get_uint (proto, FP_ProtocolPort, & port))
	continue;

      if (! port)
	continue;

      if (! handle_printf (fh, "%s\t\t\t%d\n", path_basename (ent), port))
	return FALSE;
    }

  if (! dh_close (dh))
    return FALSE;

  /* Peers
   */
  if (! handle_printf (fh, "\n\n[Synchronize Peer\tHost\t\tId\t\t\t\tPort\t\tSettings]\n") ||
      ! handle_printf (fh, "-----------------------------------------------------------------------------\n"))
    return FALSE;

  for (i = 0; i < server_peers->len; i += 1)
    {
      ServerPeer* peer = server_peers->pdata[i];

      if (! handle_printf (fh, "%s\t\t%s\t%s\t%d\t\t%s\n", path_basename (peer->peer_path), peer->peer_record->host, peer->peer_record->id, peer->peer_record->port, server_config_sync_setting_to_string (peer->peer_record->sync)))
	return FALSE;
    }

  return TRUE;
}

gboolean
server_config_set_port (const char* pname, guint16 port)
{
  File* protocol = file_initialize (srepo, path_append (protocols_path, pname));
  guint32 oport;

  if (file_is_not_type (protocol, FV_Directory))
    return FALSE;

  if (! dir_get_uint (protocol, FP_ProtocolPort, & oport))
    return FALSE;

  g_assert (oport != 0);

  if (! dir_set_uint (protocol, FP_ProtocolPort, port))
    return FALSE;

  serv_generate_stringintint_event (EC_ServSetPort, pname, oport, port);

  return TRUE;
}

/* Peers
 */

gboolean
server_config_init_peers (void)
{
  DirectoryHandle* dh;
  Path* peer_path;

  server_peers = g_ptr_array_new ();
  server_peers_by_id = g_hash_table_new (g_str_hash, g_str_equal);

  if (! (dh = file_opendir (peers_dir)))
    return FALSE;

  while ((peer_path = dh_next (dh)) != NULL)
    {
      File* peer_file = file_initialize (srepo, peer_path);
      SerialPeerRecord* pr;
      ServerPeer* peer;

      if (file_is_not_type (peer_file, FV_Directory))
	continue;

      if (! dir_get_peerrecord (peer_file, FP_PeerRecord, & pr))
	continue;

      pr->peer = peer = g_new0 (ServerPeer, 1);

      peer->peer.address_name = pr->host;
      peer->peer.port = pr->port;
      peer->peer_path = peer_path;
      peer->peer_dir = peer_file;
      peer->peer_record = pr;

      g_ptr_array_add (server_peers, peer);
      g_hash_table_insert (server_peers_by_id, (void*) peer->peer_record->id, peer);
    }

  if (! dh_close (dh))
    return FALSE;

  return TRUE;
}

GPtrArray*
server_config_peers (void)
{
  return server_peers;
}

gboolean
server_config_create_peer (const char* name, const char* id, const char* host, guint16 port)
{
  File* peer = file_initialize (srepo, path_append (peers_path, name));
  SerialPeerRecord* pr = g_new0 (SerialPeerRecord, 1);
  guint32 next_index;

  if (file_is_not_type (peer, FV_NotPresent))
    return FALSE;

  if (! file_mkdir (peer))
    return FALSE;

  pr->host = host;
  pr->port = port;
  pr->id = id;
  pr->sync = SSS_None;

  if (! dir_get_uint (server_dir, FP_PeerSequence, & next_index))
    return FALSE;

  pr->index = next_index ++;

  if (! dir_set_uint (server_dir, FP_PeerSequence, next_index))
    return FALSE;

  if (! dir_set_peerrecord (peer, FP_PeerRecord, pr))
    return FALSE;

  return TRUE;
}

ServerPeer*
server_config_find_peer_id (const char* id)
{
  ServerPeer* peer = g_hash_table_lookup (server_peers_by_id, id);

  if (! peer)
    serv_generate_string_event (EC_ServNoSuchPeerId, id);

  return peer;
}

ServerPeer*
server_config_find_peer_name (const char* name)
{
  File* peer_file = file_initialize (srepo, path_append (peers_path, name));
  SerialPeerRecord* pr;

  if (file_is_not_type (peer_file, FV_Directory))
    goto nope;

  if (! dir_get_peerrecord (peer_file, FP_PeerRecord, & pr))
    goto nope;

  if (! pr->peer)
    serv_generate_string_event (EC_ServNoSuchPeerName, name);

  return pr->peer;

 nope:

  serv_generate_string_event (EC_ServNoSuchPeerName, name);
  return NULL;
}

gboolean
server_config_lookup_peer_address (ServerPeer* peer)
{
  if (peer->peer.address)
    {
      sock_destroy_addr (peer->peer.address);
      peer->peer.address = NULL;
    }

  if (! (peer->peer.address = sock_addr_init (peer->peer.address_name)))
    return FALSE;

  return TRUE;
}

gboolean
server_config_bind (const char* name, const char* set)
{
  ServerPeer* peer = server_config_find_peer_name (name);

  if (! peer)
    return FALSE;

  if (strcmp (set, "push") == 0)
    peer->peer_record->sync |= SSS_Push;

  if (strcmp (set, "pull") == 0)
    peer->peer_record->sync |= SSS_Pull;

  if (strcmp (set, "allowpull") == 0)
    peer->peer_record->sync |= SSS_AllowPull;

  if (strcmp (set, "allowpush") == 0)
    peer->peer_record->sync |= SSS_AllowPush;

  if (! dir_set_peerrecord (peer->peer_dir, FP_PeerRecord, peer->peer_record))
    return FALSE;

  serv_generate_stringstring_event (EC_ServSyncBind, path_basename (peer->peer_path), server_config_sync_setting_to_string (peer->peer_record->sync));

  return TRUE;
}

gboolean
server_config_unbind (const char* name, const char* unset)
{
  ServerPeer* peer = server_config_find_peer_name (name);

  if (! peer)
    return FALSE;

  if (strcmp (unset, "push") == 0)
    peer->peer_record->sync &= ~SSS_Push;

  if (strcmp (unset, "pull") == 0)
    peer->peer_record->sync &= ~SSS_Pull;

  if (strcmp (unset, "allowpull") == 0)
    peer->peer_record->sync &= ~SSS_AllowPull;

  if (strcmp (unset, "allowpush") == 0)
    peer->peer_record->sync &= ~SSS_AllowPush;

  if (! dir_set_peerrecord (peer->peer_dir, FP_PeerRecord, peer->peer_record))
    return FALSE;

  serv_generate_stringstring_event (EC_ServSyncBind, path_basename (peer->peer_path), server_config_sync_setting_to_string (peer->peer_record->sync));

  return TRUE;
}

gboolean
server_config_peer_save (ServerPeer* peer)
{
  return dir_set_peerrecord (peer->peer_dir, FP_PeerRecord, peer->peer_record);
}
