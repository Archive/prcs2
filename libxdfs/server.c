/* -*-Mode: C;-*-
 * $Id: server.c 1.50 Mon, 03 May 1999 19:48:53 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include <stdio.h>
#include <signal.h>

#include "server.h"
#include "serverpriv.h"
#include "getopt.h"

/* $Format: "const char* _server_version = \"$ReleaseVersion$\"; " $ */
const char* _server_version = "0.19.0"; 

typedef struct _Command Command;

struct _Command {
  const char* name;
  gboolean (* func) (gint argc, gchar** argv);
  gint nargs;
  gboolean srepo_init;
  gboolean create_srepo;
  gboolean config_init;
};

static gboolean start_command           (gint argc, gchar** argv);
static gboolean create_command          (gint argc, gchar** argv);
static gboolean setport_command         (gint argc, gchar** argv);
static gboolean config_command          (gint argc, gchar** argv);
static gboolean peerconfig_command      (gint argc, gchar** argv);
static gboolean createpeer_command      (gint argc, gchar** argv);
static gboolean id_command              (gint argc, gchar** argv);
static gboolean log_command             (gint argc, gchar** argv);
static gboolean bind_command            (gint argc, gchar** argv);
static gboolean unbind_command          (gint argc, gchar** argv);
static gboolean dump_command            (gint argc, gchar** argv);
static gboolean tool_command            (gint argc, gchar** argv);

static GHashTable* commands;

static Command default_commands[] =
{
  { "create",        create_command,        2, TRUE, TRUE,  FALSE },
  { "register",      create_command,        2, TRUE, TRUE,  FALSE }, /* alias */
  { "start",         start_command,         1, TRUE, FALSE, TRUE },
  { "id",            id_command,            1, TRUE, FALSE, TRUE },
  { "config",        config_command,        1, TRUE, FALSE, TRUE },
  { "info",          config_command,        1, TRUE, FALSE, TRUE }, /* alias */
  { "dump",          dump_command,          1, TRUE, FALSE, TRUE },
  { "tool",          tool_command,          1, TRUE, FALSE, TRUE },
  { "peerconfig",    peerconfig_command,    2, TRUE, FALSE, TRUE },
  { "setport",       setport_command,       3, TRUE, FALSE, TRUE },
  { "log",           log_command,           2, TRUE, FALSE, TRUE },
  { "createpeer",    createpeer_command,    5, TRUE, FALSE, TRUE },
  { "bind",          bind_command,          3, TRUE, FALSE, TRUE },
  { "unbind",        unbind_command,        3, TRUE, FALSE, TRUE },
};

static struct option const long_options[] =
{
  {"help",                no_argument, 0, 'h'},
  {"version",             no_argument, 0, 'v'},
  {0,0,0,0}
};

static const gchar* program_name = "prog";
static TaskManager*  server_tasks           = NULL;

Repository*   srepo                  = NULL;

static Request* sync_client_protocol_requests[] =
{
  & sync_client_synchronize_request,
  & sync_client_peerconfig_request
};

static Request* sync_server_protocol_requests[] =
{
  & sync_server_synchronize_request,
  & sync_server_peerconfig_request
};

Protocol sync_client_protocol = {
  "Sync",
  0,
  NULL,
  & sync_client_protocol_create,
  & sync_client_protocol_init,
  SYNC_PROTOCOL_MAJOR_VERSION,
  SYNC_PROTOCOL_MINOR_VERSION,
  STATIC_ARRAY (sync_client_protocol_requests)
};

Protocol sync_server_protocol = {
  "Sync",
  SYNC_PROTOCOL_DEFAULT_PORT,
  NULL,
  & sync_server_protocol_create,
  & sync_server_protocol_init,
  SYNC_PROTOCOL_MAJOR_VERSION,
  SYNC_PROTOCOL_MINOR_VERSION,
  STATIC_ARRAY (sync_server_protocol_requests)
};

GPtrArray* server_protocols;

const char*
server_version (void)
{
  return _server_version;
}

static void
s_error (const gchar *format, ...)
{
  va_list args;

  va_start (args, format);

  vfprintf (stderr, format, args);

  va_end (args);
}

static void
usage ()
{
  s_error ("usage: %s COMMAND [OPTIONS] [ARG1 ...]\n", program_name);
  s_error ("use --help for more help\n");
  exit (2);
}

static void
help ()
{
  s_error ("usage: %s COMMAND [OPTIONS] [ARG1 ...]\n", program_name);
  s_error ("COMMAND is one of:\n");
  s_error ("OPTIONS are:\n");
  s_error ("  -v, --version\n");
  s_error ("  -h, --help\n");
  exit (2);
}

static void
version ()
{
  s_error ("version %s\n", server_version);
  exit (2);
}

static gchar*
strip_leading_path (gchar* p)
{
  gchar* ls = strrchr (p, '/');

  if (ls)
    return ls + 1;

  return p;
}

static void
server_handle_signal ()
{
  static int server_interrupts = 0;

  server_interrupts += 1;

  reponet_exit_event_loop (server_tasks);

  if (server_interrupts > 1)
    abort ();
}

static void
server_handle_signals (void)
{
  struct sigaction act;
  sigset_t signal_mask;

  sigfillset (&signal_mask);
  sigdelset (&signal_mask, SIGABRT);

  act.sa_handler = server_handle_signal;
  act.sa_mask = signal_mask;
  act.sa_flags = 0;

  sigaction (SIGINT, &act, NULL);

  act.sa_handler = SIG_IGN;
  act.sa_mask = signal_mask;
  act.sa_flags = 0;

  sigaction (SIGPIPE, &act, NULL);
}

static gboolean
server_srepo_init (const char* dir, gboolean create)
{
  GString *str = g_string_new ("db:");

  g_string_append (str, dir);

  if (create)
    srepo = repository_create (str->str);
  else
    srepo = repository_initialize (str->str);

  if (! srepo)
    return FALSE;

  g_string_free (str, TRUE);

  return TRUE;
}

static void
server_init (void)
{
  static gboolean once = FALSE;

  if (! once)
    {
      int i;

      once = TRUE;

      commands = g_hash_table_new (g_str_hash, g_str_equal);

      for (i = 0; i < STATIC_ARRAY_LENGTH(default_commands); i += 1)
	g_hash_table_insert (commands, (void*) default_commands[i].name, default_commands + i);

      server_protocols = g_ptr_array_new ();

      g_ptr_array_add (server_protocols, & sync_client_protocol);
      g_ptr_array_add (server_protocols, & sync_server_protocol);
    }
}

void
server_add_command (const char* name, gint (* func) (gint argc, gchar** argv), gint argc)
{
  Command* cmd = g_new (Command, 1);

  server_init ();

  cmd->name = name;
  cmd->func = func;
  cmd->nargs = argc;
  cmd->srepo_init = TRUE;
  cmd->create_srepo = FALSE;
  cmd->config_init = TRUE;

  g_hash_table_insert (commands, (void*) cmd->name, cmd);
}

void
server_add_protocol (Protocol* protocol)
{
  server_init ();

  g_ptr_array_add (server_protocols, protocol);
}

gint
server_main(gint argc, gchar** argv)
{
  const Command *cmd = NULL;
  gint c, ext = 0;
  gint longind, fatal_mask;
  const char* server_dir = NULL;

  if (! edsio_library_init ())
    exit (2);

  if (! serv_edsio_init ())
    exit (2);

  g_assert (argc > 0 && argv[0]);

  program_name = strip_leading_path (argv[0]);

  fatal_mask = g_log_set_always_fatal (G_LOG_FATAL_MASK);
  fatal_mask |= G_LOG_LEVEL_CRITICAL;

  g_log_set_always_fatal (fatal_mask);

  if (argc < 2)
    usage ();

  if (strcmp (argv[1], "-h") == 0 ||
      strcmp (argv[1], "--help") == 0)
    help ();

  if (strcmp (argv[1], "-v") == 0 ||
      strcmp (argv[1], "--version") == 0)
    version ();

  if (! (cmd = g_hash_table_lookup (commands, argv[1])))
    {
      s_error ("Unrecognized command\n");
      help ();
    }

  argc -= 1;
  argv += 1;

  while ((c = getopt_long(argc,
			  argv,
			  "+hv",
			  long_options,
			  &longind)) != EOF)
    {
      switch (c)
	{
	case 'h': help (); break;
	case 'v': version (); break;
	default:
	  s_error ("Illegal argument, use --help for help\n");
	  exit (2);
	  break;
	}
    }

  argc -= optind;
  argv += optind;

  if (cmd->nargs >= 0 && argc != cmd->nargs)
    {
      s_error ("Wrong number of arguments\n");
      exit (2);
    }

  if (! repository_system_init ())
    exit (2);

  server_handle_signals ();

  if (cmd->srepo_init)
    {
      server_dir = argv[0];

      if (! server_srepo_init (server_dir, cmd->create_srepo))
	goto bail;

      argv += 1;
      argc -= 1;
    }

  if (! server_log_library_init ())
    goto bail;

  if (! server_namespace_init ())
    goto bail;

  if (! xdfs_library_init (edsio_message_digest_md5 ()))
    goto bail;

  if (! xdfs_sync_library_init ())
    goto bail;

  if (cmd->config_init)
    {
      if (! server_config_init ())
	goto bail;
    }

  if (! (* cmd->func) (argc, argv))
    ext = 1;

  if (srepo)
    {
      if (! repository_close (srepo))
	goto bail;
    }

  if (! repository_system_close ())
    exit (2);

  return ext;

 bail:

  repository_system_close ();

  exit (2);
}

gboolean
create_command (gint argc, gchar** argv)
{
  if (! server_namespace_create ())
    return FALSE;

  if (! xdfs_sync_library_create ())
    return FALSE;

  if (! server_config_create (argv[0]))
    return FALSE;

  if (! repository_commit (srepo))
    return FALSE;

  return TRUE;
}

gboolean
start_command (gint argc, gchar** argv)
{
  gint i;
  server_tasks = task_manager_new ();

  for (i = 0; i < server_protocols->len; i += 1)
    {
      Protocol* proto = server_protocols->pdata[i];

      if (! proto->port)
	continue;

      serv_generate_string_event (EC_ServStarting, proto->name);

      if (! protocol_accept_init (proto, server_tasks))
	return FALSE;
    }

  if (! sync_client_protocol_start (server_tasks))
    return FALSE;

  return reponet_event_loop (server_tasks);
}

gboolean
setport_command (gint argc, gchar** argv)
{
  guint16 x;

  if (! strtous_checked (argv[1], & x, "Invalid port"))
    return FALSE;

  if (! server_config_set_port (argv[0], x))
    return FALSE;

  if (! repository_commit (srepo))
    return FALSE;

  return TRUE;
}

gboolean
config_command (gint argc, gchar** argv)
{
  File* t = file_initialize_temp (srepo);
  FileHandle* fh = file_open (t, HV_Replace);

  if (! server_config_print (fh))
    return FALSE;

  if (! handle_close (fh))
    return FALSE;

  if (! file_to_stdout (t))
    return FALSE;

  /* @@@ free t */

  return TRUE;
}

gboolean
createpeer_command (gint argc, gchar** argv)
{
  const char* name = argv[0];
  const char* id   = argv[1];
  const char* host = argv[2];

  guint16 port;

  if (!name[0] || !host[0] || !id[0])
    {
      serv_generate_string_event (EC_ServNoEmptyString, (!name[0]) ? "peer name" : ((!host[0]) ? "peer host" : "peer id"));
      return FALSE;
    }

  if (! strtous_checked (argv[3], & port, "Invalid port"))
    return FALSE;

  if (! server_config_create_peer (name, id, host, port))
    return FALSE;

  if (! repository_commit (srepo))
    return FALSE;

  return TRUE;
}

gboolean
id_command (gint argc, gchar** argv)
{
  g_print ("%s\n", server_id ());

  return TRUE;
}

gboolean
peerconfig_command (gint argc, gchar** argv)
{
  ServerPeer* peer;
  const char* peer_name = argv[0];
  TaskManager* tm = task_manager_new ();

  if (!peer_name[0])
    {
      serv_generate_string_event (EC_ServNoEmptyString, "peer name");
      return FALSE;
    }

  if (! (peer = server_config_find_peer_name (peer_name)))
    return FALSE;

  if (! (server_config_lookup_peer_address (peer)))
    return FALSE;

  if (! protocol_connect_init (& peer->peer,
			       sync_connect_object (peer),
			       ST_SyncConnect,
			       & sync_client_protocol,
			       SYNC_PROTOCOL_PEERCONFIG_REQUEST,
			       NULL,
			       NULL,
			       NULL,
			       tm))
    return FALSE;

  if (! reponet_event_loop (tm))
    return FALSE;

  return TRUE;
}

gboolean
log_command (gint argc, gchar** argv)
{
  const char* arg = argv[0];

  if (strcmp (arg, "events") == 0)
    return server_log_print (server_event_log ());
  else if (strcmp (arg, "namespace") == 0)
    return server_log_print (server_namespace_log ());

  s_error ("Unknown log: %s\n", arg);

  return FALSE;
}

gboolean
bind_command (gint argc, gchar** argv)
{
  if (! server_config_bind (argv[0], argv[1]))
    return FALSE;

  return TRUE;
}

gboolean
unbind_command (gint argc, gchar** argv)
{
  if (! server_config_unbind (argv[0], argv[1]))
    return FALSE;

  return TRUE;
}

gboolean
server_test_bit (const guint8 *bytes, guint32 bytes_len, guint index)
{
  guint byte = index / 8;
  guint bit = index % 8;

  if (bytes_len <= byte)
    return FALSE;

  return (bytes[byte] & (1 << bit)) != 0;
}

void
server_set_bit (const guint8 **bytes, guint32 *bytes_len, GByteArray** bytes_write, guint index)
{
  guint  byte = index / 8;
  guint  bit = index % 8;
  guint8 zero[1];
  GByteArray* array = (* bytes_write);

  zero[0] = 0;

  if (! array)
    {
      array = (* bytes_write) = g_byte_array_new ();

      g_byte_array_append (array, *bytes, *bytes_len);
    }

  while (array->len <= byte)
    g_byte_array_append (array, zero, 1);

  array->data[byte] |= (1 << bit);

  (*bytes) = array->data;
  (*bytes_len) = array->len;
}

const char*
server_create_unique_id (void)
{
  guint8 buf[NAMESPACE_UNIQUE_ID_BYTES];
  static char base64[NAMESPACE_UNIQUE_ID_BYTES*2];
  guint  base64_len = NAMESPACE_UNIQUE_ID_BYTES*2;

  RAND_bytes (buf, NAMESPACE_UNIQUE_ID_BYTES);

  if (! edsio_base64_encode_region_into (buf, NAMESPACE_UNIQUE_ID_BYTES, (guint8*) base64, &base64_len))
    abort ();

  g_assert (base64_len == (NAMESPACE_UNIQUE_ID_BYTES*4/3));

  base64[base64_len++] = 0;

  return base64;
}

gboolean
dump_command (gint argc, gchar** argv)
{
  /*| RLF_HideSegments*/
  return repository_list (srepo, RLF_Recursive | RLF_TestSerial | RLF_ShowSegmentHead | RLF_HideAutoSegments | RLF_HideSystemSegments | RLF_ListIndices);
}

gboolean
tool_command (gint argc, gchar** argv)
{
  repository_tool (srepo);
  return TRUE;
}
