/* -*-Mode: C;-*-
 * $Id: server.h 1.55 Fri, 30 Apr 1999 01:29:46 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _SERVER_H_
#define _SERVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <repo.h>
#include <reponet.h>

typedef struct _ServerPeer  ServerPeer;
typedef struct _ServerLog   ServerLog;

#include "xdfs.h"
#include "serv_edsio.h"

typedef struct _SerialNameSpace NameSpace; /* opaque */

enum _NameSpaceAttachFlags {
  NSAF_None          = 0,
  NSAF_Create        = 1 << 1,
  NSAF_Unique        = 1 << 2
};

typedef enum _NameSpaceAttachFlags NameSpaceAttachFlags;

#define NAMESPACE_UNIQUE_ID_BYTES (168/8)

/* Namespaces */

AliasedIdentifier*       server_namespace_id               (NameSpace *ns);
const char*              server_namespace_nspath           (NameSpace* ns);
const char*              server_namespace_path_to_nspath   (Path* path);

NameSpace*               server_namespace_attach_nspath    (const char *nspath,
							    const char *alias,
							    gint        flags);

File*                    server_namespace_xdfs_index       (NameSpace    *ns);

File*                    server_namespace_xdfs_sequence    (NameSpace    *ns);

/* Server main */

void server_add_command  (const char  *name,
			  gboolean   (*func) (gint argc, gchar** argv),
			  gint         argc);

void server_add_protocol (Protocol    *protocol);

gint server_main         (gint         argc,
			  gchar      **argv);

#ifdef __cplusplus
}
#endif

#endif /* _SERVER_H_ */
