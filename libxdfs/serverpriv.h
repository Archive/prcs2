/* -*-Mode: C;-*-
 * $Id: serverpriv.h 1.18 Mon, 03 May 1999 04:42:34 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _SERVERPRIV_H_
#define _SERVERPRIV_H_

#define SYNC_PROTOCOL_MAJOR_VERSION        1
#define SYNC_PROTOCOL_MINOR_VERSION        0
#define SYNC_PROTOCOL_DEFAULT_PORT         2799
#define SYNC_PROTOCOL_SYNCHRONIZE_REQUEST  1
#define SYNC_PROTOCOL_PEERCONFIG_REQUEST   2

#define SYNC_PROTOCOL_STARTUP_DELAY        1  /* seconds before server begins syncing */
#define SYNC_PROTOCOL_SYNC_PERIOD          10 /* seconds between syncs */

extern Protocol      sync_server_protocol;
extern Protocol      sync_client_protocol;
extern Request       sync_client_synchronize_request;
extern Request       sync_server_synchronize_request;
extern Request       sync_client_peerconfig_request;
extern Request       sync_server_peerconfig_request;

extern Repository*   srepo;

extern GPtrArray*    server_protocols;

typedef struct _SyncData    SyncData;

struct _SyncData {
  ServerPeer        *peer;
  SerialSyncConnect *connect;
  File              *peerconfig_temp;

  void       *msg;
  SerialType  msg_type;
};

#define SYNC_DATA(conn) conn_named_data (conn, "SYNC", SyncData)

struct _ServerPeer {
  Peer              peer;
  Path             *peer_path;
  File             *peer_dir;
  SerialPeerRecord *peer_record;
};

enum _ServerSyncSettings { /* These are set in peerrecord->sync */
  SSS_None      = 0,
  SSS_Push      = 1 << 0,
  SSS_Pull      = 1 << 1,
  SSS_AllowPush = 1 << 2,
  SSS_AllowPull = 1 << 3
};

typedef enum _ServerSyncSettings ServerSyncSettings;

/* Sync protocol
 */
gboolean    sync_client_protocol_create     (Repository* repo, Protocol* protocol);
gboolean    sync_client_protocol_init       (Repository* repo, Protocol* protocol);
gboolean    sync_client_protocol_start      (TaskManager* tm);

gboolean    sync_server_protocol_create     (Repository* repo, Protocol* protocol);
gboolean    sync_server_protocol_init       (Repository* repo, Protocol* protocol);

SerialSyncConnect* sync_connect_object (ServerPeer* peer);

/* Misc
 */
const char* server_create_unique_id (void);

/* Namespace
 */
gboolean server_namespace_init        (void);
gboolean server_namespace_create      (void);
gboolean server_namespace_peer_log    (ServerPeer* peer, SerialSyncLog* update);
const char* server_namespace_path_to_nspath_int (Path* path);

extern Path* _nsr;

/* Config
 */
gboolean server_config_init           (void);
gboolean server_config_create         (const char* name);
gboolean server_config_set_port       (const char* pname, guint16 port);
gboolean server_config_print          (FileHandle *fh);
gboolean server_config_create_peer    (const char* name, const char* id, const char* host, guint16 port);
gboolean server_config_bind           (const char* name, const char* set);
gboolean server_config_unbind         (const char* name, const char* unset);

ServerPeer* server_config_find_peer_id   (const char* id);
ServerPeer* server_config_find_peer_name (const char* name);

GPtrArray* server_config_peers (void);

gboolean server_config_peer_save (ServerPeer* peer);

gboolean server_config_lookup_peer_address (ServerPeer* peer);

ServerLog* server_event_log (void);
ServerLog* server_namespace_log (void);

/* Id
 */
const char* server_name (void);
const char* server_id (void);
const char* server_version (void);

/* XDFS synchronization
 */

gboolean       xdfs_sync_library_create       (void);
gboolean       xdfs_sync_library_init         (void);

TaskControl    xdfs_sync_server_recv_requests (Connection* conn, Task* task);
TaskControl    xdfs_sync_client_send_requests (Connection* conn, Task* task);

TaskControl    xdfs_sync_client_recv_replies  (Connection* conn, Task* task);

gboolean       xdfs_sync_log_peer_insert      (XdfsDirectory* xdfs, ServerPeer* peer, const guint8* digest);

/* Logging
 */

ServerLog*  server_log_init   (Path* path);
gboolean    server_log_create (Path* path);
SerialSink* server_log_sink   (ServerLog* log);
gboolean    server_log_print  (ServerLog* log);
gint        server_log_point  (ServerLog* log);
gboolean    server_log_find   (ServerLog* log, guint num, void** obj, SerialType* st);
gboolean    server_log_library_init (void);

/* For persistent bits
 */

gboolean server_test_bit (const guint8 *bytes,  guint32 bytes_len,  guint index);
void     server_set_bit  (const guint8 **bytes, guint32 *bytes_len, GByteArray** bytes_write, guint index);

#endif
