/* -*-Mode: C;-*-
 * $Id: sync.c 1.9 Sun, 02 May 1999 04:53:40 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "server.h"
#include "serverpriv.h"

/* Sync protocol headers
 */

static Task* _sync_client_task;

static gboolean sync_check_authorization (Connection *conn, SyncData* data);
static gboolean sync_client_start (Connection* conn, Peer* peer, void* server_data);
static ReponetServerReply sync_server_start (Connection* conn, void* client_data);
static void sync_server_success (Connection* conn, TaskManager* tm);
static void sync_client_success (Connection* conn, TaskManager* tm);
static void sync_server_failure (TaskManager* tm);
static void sync_client_failure (TaskManager* tm);

/* Client peerconfig request headers
 */

static TaskControl peerconfig_client_recv (Connection* conn, Task* task);
static TaskControl peerconfig_client_recv_a (Connection* conn, Task* task);

static Transmition sync_client_peerconfig_transmition[] =
{
  { NULL, & peerconfig_client_recv, ProtocolAsync },
};

Request sync_client_peerconfig_request =
{
  "Peerconfig Client",
  SYNC_PROTOCOL_PEERCONFIG_REQUEST,
  STATIC_ARRAY (sync_client_peerconfig_transmition),
  ST_SyncConnect,
  { (ServerStartFunc) & sync_client_start },
  & sync_client_success,
  & sync_client_failure
};

/* Server peerconfig request headers
 */

static TaskControl        peerconfig_server_send  (Connection* conn, Task* task);

static Transmition sync_server_peerconfig_transmition[] =
{
  { & peerconfig_server_send, NULL, ProtocolAsync }
};

Request sync_server_peerconfig_request =
{
  "Peerconfig Server",
  SYNC_PROTOCOL_PEERCONFIG_REQUEST,
  STATIC_ARRAY (sync_server_peerconfig_transmition),
  ST_SyncConnect,
  { & sync_server_start },
  & sync_server_success,
  & sync_server_failure
};

/* Client synchronize request headers
 */

static TaskControl synchronize_send_log (Connection* conn, Task* task);
static TaskControl synchronize_recv_log (Connection* conn, Task* task);
static TaskControl synchronize_recv_log_a (Connection* conn, Task* task);

static Transmition sync_client_synchronize_transmition[] =
{
  { & synchronize_send_log,           & synchronize_recv_log,            ProtocolAsync },
  { NULL,                             NULL,                              ProtocolBarrier },
  { & xdfs_sync_client_send_requests, & xdfs_sync_client_recv_replies,   ProtocolAsync }
};

Request sync_client_synchronize_request =
{
  "Synchronize Client",
  SYNC_PROTOCOL_SYNCHRONIZE_REQUEST,
  STATIC_ARRAY (sync_client_synchronize_transmition),
  ST_SyncConnect,
  { (ServerStartFunc) & sync_client_start },
  & sync_client_success,
  & sync_client_failure
};

/* Server synchronize request headers
 */

static Transmition sync_server_synchronize_transmition[] =
{
  { & synchronize_send_log, & synchronize_recv_log,           ProtocolAsync },
  { & task_consume_ready,   & xdfs_sync_server_recv_requests, ProtocolAsync }
};

Request sync_server_synchronize_request =
{
  "Synchronize Server",
  SYNC_PROTOCOL_SYNCHRONIZE_REQUEST,
  STATIC_ARRAY (sync_server_synchronize_transmition),
  ST_SyncConnect,
  { & sync_server_start },
  & sync_server_success,
  & sync_server_failure
};

/**********************************************************************/
/*                               General                              */
/**********************************************************************/

gboolean
sync_client_protocol_create (Repository* repo, Protocol* protocol)
{
  return TRUE;
}

gboolean
sync_client_protocol_init   (Repository* repo, Protocol* protocol)
{
  return TRUE;
}

gboolean
sync_server_protocol_create (Repository* repo, Protocol* protocol)
{
  return TRUE;
}

gboolean
sync_server_protocol_init   (Repository* repo, Protocol* protocol)
{
  return TRUE;
}

gboolean
sync_check_authorization (Connection *conn, SyncData* data)
{
  ServerPeer *peer = server_config_find_peer_id (data->connect->peerid);

  if (! peer)
    return FALSE;

  if (data->peer && data->peer != peer)
    {
      serv_generate_connstring_event (EC_ServConnectionRefused, conn, "Peer mismatch");
      return FALSE;
    }

  SYNC_DATA (conn)->peer = peer;

  return TRUE;
}

gboolean
sync_client_start (Connection* conn, Peer* peer, void* server_data)
{
  SyncData *data = SYNC_DATA (conn);

  data->connect = server_data;
  data->peer    = (ServerPeer*) peer;

  return sync_check_authorization (conn, data);
}

ReponetServerReply
sync_server_start (Connection* conn, void* client_data)
{
  SyncData* data = SYNC_DATA (conn);

  data->connect = client_data;

  if (! sync_check_authorization (conn, data))
    return reponet_server_deny (conn);

  task_sleep (_sync_client_task);

  return reponet_server_reply (conn, sync_connect_object (data->peer), ST_SyncConnect);
}

void
sync_server_success (Connection* conn, TaskManager* tm)
{
  if (! server_config_peer_save (SYNC_DATA (conn)->peer))
    reponet_exit_event_loop (tm);

  task_wake (_sync_client_task);

  if (! repository_commit (srepo))
    reponet_exit_event_loop (tm);
}

void
sync_client_success (Connection* conn, TaskManager* tm)
{
  if (! server_config_peer_save (SYNC_DATA (conn)->peer))
    reponet_exit_event_loop (tm);

  if (! repository_commit (srepo))
    reponet_exit_event_loop (tm);
}

void sync_server_failure (TaskManager* tm)
{
  task_wake (_sync_client_task);
}

void sync_client_failure (TaskManager* tm) { }

SerialSyncConnect*
sync_connect_object (ServerPeer* peer)
{
  SerialSyncConnect* sco = g_new0 (SerialSyncConnect, 1);

  sco->peerid       = server_id ();
  sco->peersynclog = peer->peer_record->synclog;

  return sco;
}

static void
sync_client_daemon_failure (TaskManager* tm, void* vpeer)
{
  ServerPeer* peer = (ServerPeer*) vpeer;

  edsio_time_of_day (& peer->peer_record->syncattempt);
}

static void
sync_client_daemon_success (Connection* conn, TaskManager* tm, void* vpeer)
{
  ServerPeer* peer = (ServerPeer*) vpeer;

  edsio_time_of_day (& peer->peer_record->synctime);
  edsio_time_of_day (& peer->peer_record->syncattempt);

  task_wake (_sync_client_task);
}

static TaskControl
sync_client_daemon (Connection* not_connected, Task* task)
{
  GPtrArray* peers = server_config_peers ();
  int i;
  SerialGenericTime t;

  edsio_time_of_day (& t);

  g_assert (task == _sync_client_task && ! not_connected);

  for (i = 0; i < peers->len; i += 1)
    {
      ServerPeer* peer = peers->pdata[i];

      if (! (peer->peer_record->sync & (SSS_Pull | SSS_Push)))
	continue;

      if (peer->peer_record->syncattempt.seconds + SYNC_PROTOCOL_SYNC_PERIOD < t.seconds)
	{
	  if (! (server_config_lookup_peer_address (peer)))
	    {
	      edsio_time_of_day (& peer->peer_record->syncattempt);
	      continue;
	    }

	  if (! protocol_connect_init (& peer->peer,
				       sync_connect_object (peer),
				       ST_SyncConnect,
				       & sync_client_protocol,
				       SYNC_PROTOCOL_SYNCHRONIZE_REQUEST,
				       peer,
				       & sync_client_daemon_success,
				       & sync_client_daemon_failure,
				       task_manager (task)))
	    {
	      edsio_time_of_day (& peer->peer_record->syncattempt);
	      continue;
	    }

	  return task_sleep (task);
	}
    }

  return task_set_wait (task, NULL, SYNC_PROTOCOL_SYNC_PERIOD, & sync_client_daemon);
}

gboolean
sync_client_protocol_start (TaskManager* tm)
{
  _sync_client_task = task_new_wait (tm, NULL, SYNC_PROTOCOL_STARTUP_DELAY, & sync_client_daemon);

  return TRUE;
}

/**********************************************************************/
/*                             Peer Config                            */
/**********************************************************************/

/* Client Peer config request items
 */

TaskControl
peerconfig_client_recv (Connection* conn, Task* task)
{
  SyncData *data = SYNC_DATA (conn);

  data->peerconfig_temp = file_initialize_temp (srepo);

  return task_segments_get (conn,
			    task,
			    peerconfig_client_recv_a,
			    file_default_segment (data->peerconfig_temp),
			    NULL,
			    NULL);
}

TaskControl
peerconfig_client_recv_a (Connection* conn, Task* task)
{
  SyncData *data = SYNC_DATA (conn);

  if (! file_to_stdout (data->peerconfig_temp))
    return reponet_kill_task (task);

  return protocol_next (conn, task);
}

/* Server Peer config request items
 */

TaskControl
peerconfig_server_send (Connection* conn, Task* task)
{
  File* t = file_initialize_temp (srepo);
  FileHandle* fh = file_open (t, HV_Replace);

  if (! server_config_print (fh))
    return reponet_kill_task (task);

  if (! handle_close (fh))
    return reponet_kill_task (task);

  /* @@@ free t */

  return task_segments_put (conn,
			    task,
			    protocol_next,
			    file_default_segment (t),
			    NULL,
			    NULL);
}

/**********************************************************************/
/*                             Synchronize                            */
/**********************************************************************/

TaskControl
synchronize_send_log (Connection* conn, Task* task)
{
  SyncData* data = SYNC_DATA (conn);
  gint value;

  if ((value = server_log_point (server_namespace_log ())) < 0)
    return reponet_kill_task (task);

  if (data->connect->peersynclog < value)
    {
      SerialType  ent_type;
      void       *ent;

      if (! server_log_find (server_namespace_log (), data->connect->peersynclog, & ent, & ent_type))
	return reponet_kill_task (task);

      /* @@@ transaction control??? */
      data->connect->peersynclog += 1;

      return task_finish_put (conn,
			      task,
			      synchronize_send_log,
			      serialize_synclog (conn_sink (conn), ent_type, ent));
    }

  return task_finish_put (conn,
			  task,
			  protocol_next,
			  serialize_synclogdone (conn_sink (conn)));
}

TaskControl
synchronize_recv_log (Connection* conn, Task* task)
{
  SyncData* data = SYNC_DATA (conn);

  return task_finish_get (conn,
			  task,
			  & synchronize_recv_log_a,
			  ST_SyncLog | ST_SyncLogDone,
			  & data->msg_type,
			  & data->msg);
}

TaskControl
synchronize_recv_log_a (Connection* conn, Task* task)
{
  SyncData* data = SYNC_DATA (conn);

  if (data->msg_type == ST_SyncLogDone)
    return protocol_next (conn, task);
  else
    {
      if (! server_namespace_peer_log (data->peer, (SerialSyncLog*) data->msg))
	return reponet_kill_task (task);

      /* @@@ transaction control??? */
      data->peer->peer_record->synclog += 1;

      return synchronize_recv_log (conn, task);
    }
}
