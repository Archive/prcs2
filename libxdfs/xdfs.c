/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: xdfs.c 1.62 Thu, 06 May 1999 00:58:18 -0700 jmacd $
 */

#include "xdfs.h"
#include "xdfspriv.h"

/*#define DEBUG_INST_ARRAY*/

#ifdef DEBUG_INST_ARRAY
#include <edsiostdio.h>
#endif

guint xdfs_search_generation;

XdfsFileXdfsFileProperty FP_XdfsFile;
XdfsFileXdfsDirectoryProperty FP_XdfsDirectory;

static void pack_xdfsfile (XdfsFile *source);
static void unpack_xdfsfile (XdfsFile *source);

static gboolean         xdfs_replace_file_with_delta (XdfsDirectory* xdfs, guint replace_sequence, guint nlit_seq);
static XdeltaGenerator* xdfs_from_segments (XdfsDirectory* xdfs, guint nlit_seq);
static gboolean         xdfs_from_segment (XdeltaGenerator* gen, File* seq, gint seq_index, gboolean is_lit);
static XdeltaSource*    xdfs_from_segment_source (File *file, gboolean is_literal);
static gboolean         xdfs_construct_index (XdfsFile *xf, gint level);
static gboolean         xdfs_construct_local_control (XdfsFile *xf, gint level);
static gboolean         xdfs_update_inverses (XdfsFile *xf);
static void             xdfs_translate_copies (GArray                *append_inst_array,
					       XdeltaInstruction     *translate,
					       ConstructionIndex     *translate_index);
static gboolean         xdfs_construct_page_in (FileSegment *seg,
						SegmentView *view,
						guint8      *pgbuf,
						gssize       page_no,
						gsize        in_len);
static gboolean         xdfs_literal_trigger   (gint32       code,
						const char  *code_name,
						File        *target_dir,
						File        *target_file,
						Path        *target_seg_name,
						FileHandle  *target_handle,
						const char  *trigger_argument,
						gint32       trigger_flags,
						gint32       trigger_id,
						gint32       trigger_parent_id,
						gint32       trigger_event_type);
static gboolean         xdfs_patch_trigger     (gint32       code,
						const char  *code_name,
						File        *target_dir,
						File        *target_file,
						Path        *target_seg_name,
						FileHandle  *target_handle,
						const char  *trigger_argument,
						gint32       trigger_flags,
						gint32       trigger_id,
						gint32       trigger_parent_id,
						gint32       trigger_event_type);

#if 0
static Path        *xdelta_checksum_index_segment_path;
#endif
static Path        *xdelta_control_segment_path;
static SegmentView *xdelta_constructed_segment_view;
static GSList      *xdfs_listeners;

const MessageDigest* xdfs_message_digest;

gboolean
xdfs_library_init (const MessageDigest *md)
{
  if (! xd_edsio_init ())
    return FALSE;

  if (! xdfs_edsio_init ())
    return FALSE;

  if (! edsio_new_file_xdfsfile_property ("XF", PF_Persistent, & FP_XdfsFile))
    return FALSE;

  if (! edsio_new_file_xdfsdirectory_property ("XDFS", PF_Persistent, & FP_XdfsDirectory))
    return FALSE;

  if (! trigger_type_register (XDFS_LITERAL_TRIGGER_TYPE, "XDFS Literal Trigger", xdfs_literal_trigger))
    return FALSE;

  if (! trigger_type_register (XDFS_PATCH_TRIGGER_TYPE, "XDFS Patch Trigger", xdfs_patch_trigger))
    return FALSE;

  /* @@@ until xdelta is generalized */
  g_assert (strcmp (md->name, "MD5") == 0);

#if 0
  xdelta_checksum_index_segment_path = path_absolute ("XdeltaIndex");
#endif

  xdelta_control_segment_path        = path_absolute ("XdeltaControl");

  xdelta_constructed_segment_view    = segment_view_register ("XdeltaConstructed", xdfs_construct_page_in);

  xdfs_message_digest = md;

  return TRUE;
}

XdfsDirectory*
xdfs_directory_init (File* dir, void* user_data)
{
  XdfsDirectory *xdfs;

  if (file_is_not_type (dir, FV_Directory))
    return NULL;

  if (! file_test_xdfsdirectory (dir, FP_XdfsDirectory))
    {
      File *index, *lit_seq, *pat_seq;

      /* Initial creation.
        */

      xdfs = g_new0 (XdfsDirectory, 1);

      xdfs->mdname = xdfs_message_digest->name;

      if (! file_set_xdfsdirectory (dir, FP_XdfsDirectory, xdfs))
	return NULL;

      index   = file_initialize_entry (dir, "index");
      pat_seq = file_initialize_entry (dir, "pat");
      lit_seq = file_initialize_entry (dir, "lit");

      if (! file_index_create (index, path_absolute (xdfs_message_digest->name), IT_Hash))
	return NULL;

      if (! file_sequence_create (lit_seq))
	return NULL;

      if (! file_sequence_create (pat_seq))
	return NULL;

      if (! file_index_link (index, dir, FALSE))
	return NULL;

      if (! auto_digest_property (dir, path_root (), xdfs_message_digest, FALSE))
	return NULL;

      if (! trigger_add (dir, XDFS_LITERAL_TRIGGER_TYPE, path_root (), TE_SegmentCreate, TF_ArgumentPrefixMask, "lit"))
	return NULL;

      if (! trigger_add (dir, XDFS_PATCH_TRIGGER_TYPE, path_root (), TE_SegmentCreate, TF_ArgumentPrefixMask, "pat"))
	return NULL;
    }

  if (! file_get_xdfsdirectory (dir, FP_XdfsDirectory, & xdfs))
    return NULL;

  if (! xdfs->dir)
    {
      xdfs->dir = dir;

      xdfs->lit_seq = file_initialize_entry (dir, "lit");
      xdfs->pat_seq = file_initialize_entry (dir, "pat");
      xdfs->index   = file_initialize_entry (dir, "index");

      if (file_is_not_type (xdfs->index, FV_Index))
	return NULL;

      if (strcmp (xdfs->mdname, xdfs_message_digest->name) != 0)
	{
	  xdfs_generate_file_event (EC_XdfsDifferentMessageDigest, dir);
	  return NULL;
	}
    }

  xdfs->user_data = user_data;

  return xdfs;
}

const char*
xdfs_storage_type_to_string (XdfsStorageType t)
{
  switch (t)
    {
    case XST_Literal:      return "literal";
    case XST_ForwardDelta: return "forward";
    case XST_ReverseDelta: return "reverse";
    case XST_Patch:        return "patch";
    case XST_NotPresent:   return "not present";
    }

  abort ();
}

void*
xdfs_user_data (XdfsDirectory* xdfs)
{
  return xdfs->user_data;
}

void
xdfs_listen (XdfsListenFunc func)
{
  xdfs_listeners = g_slist_prepend (xdfs_listeners, func);
}

File*
xdfs_index (XdfsDirectory* xdfs)
{
  return xdfs->index;
}

File*
xdfs_sequence (XdfsDirectory *xdfs)
{
  return xdfs->lit_seq;
}

gboolean
xdfs_literal_trigger (gint32       code,
		      const char  *code_name,
		      File        *target_dir,
		      File        *target_file,
		      Path        *target_seg_name,
		      FileHandle  *target_handle,
		      const char  *trigger_argument,
		      gint32       trigger_flags,
		      gint32       trigger_id,
		      gint32       trigger_parent_id,
		      gint32       trigger_event_type)
{
  XdfsDirectory *xdfs;
  GSList *listen;
  const guint8* digest;
  Path *path;
  XdfsFile *xf;
  gint point;

  if (! file_get_xdfsdirectory (target_dir, FP_XdfsDirectory, & xdfs))
    return NULL;

  path = file_access_path (target_file);

  g_assert (strncmp (path_basename (path), "lit:", 4) == 0);

  /* @@@ This is too ugly, what to do about it? */
  if (segment_is_type_noerr (file_default_segment (target_file), SV_View))
    /* This is the patch trigger creating a new view. */
    return TRUE;

  if (! (digest = file_digest (target_file, xdfs_message_digest)))
    return FALSE;

  /* Listeners
   */
  for (listen = xdfs_listeners; listen; listen = listen->next)
    {
      XdfsListenFunc func = listen->data;

      if (! func (xdfs, digest))
	return FALSE;
    }

  /* New XdfsFile record
   */
  xf = g_new0 (XdfsFile, 1);
  xf->type = XST_Literal;
  xf->inverse_sibling_name = "";
  xf->file = target_file;

  if (! file_set_xdfsfile (target_file, FP_XdfsFile, xf))
    return FALSE;

  if (! strtoui_checked (path_basename (path) + 4, & point, "Sequence index"))
    return FALSE;

  if (! xdfs_replace_file_with_delta (xdfs, xdfs->literal, point))
    return FALSE;

  /* Update dir record
   */
  xdfs->literal = point;

  if (! file_set_xdfsdirectory (xdfs->dir, FP_XdfsDirectory, xdfs))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_patch_trigger (gint32       code,
		    const char  *code_name,
		    File        *target_dir,
		    File        *target_file,
		    Path        *target_seg_name,
		    FileHandle  *target_handle,
		    const char  *trigger_argument,
		    gint32       trigger_flags,
		    gint32       trigger_id,
		    gint32       trigger_parent_id,
		    gint32       trigger_event_type)
{
  XdfsDirectory *xdfs;
  Path *path;
  XdfsFile *xf;
  File* oliteral;
  FileSegment* oseg;
  const guint8* digest;
  gssize len;

  if (! file_get_xdfsdirectory (target_dir, FP_XdfsDirectory, & xdfs))
    return NULL;

  path = file_access_path (target_file);

  g_assert (strncmp (path_basename (path), "pat:", 4) == 0);

  /* Replace the target file
   */
  if (! (oliteral = file_sequence_initialize_element (xdfs->lit_seq, xdfs->literal)))
    return FALSE;

  oseg = file_default_segment (oliteral);

  if ((len = segment_length (oseg)) < 0)
    return FALSE;

  if (! (digest = segment_digest (oseg, xdfs_message_digest)))
    return FALSE;

  if (! segment_erase (oseg, FALSE))
    return FALSE;

  if (! segment_view_create (oseg, len, xdelta_constructed_segment_view))
    return FALSE;

  if (! segment_digest_assert (oseg, xdfs_message_digest, digest))
    return FALSE;

  if (! (xf = xdfs_file (oliteral)))
    return FALSE;

  xf->type = XST_ReverseDelta;

  if (! file_set_xdfsfile (oliteral, FP_XdfsFile, xf))
    return FALSE;

  /* Set patch's xdfsfile record */
  xf = g_new0 (XdfsFile, 1);

  xf->file = target_file;
  xf->type = XST_Patch;
  xf->inverse_sibling_name = "";

  if (! file_set_xdfsfile (target_file, FP_XdfsFile, xf))
    return FALSE;

#if 0 /* @@@ */
  if (file_length (target_file) == 0)
    {
      /* @@@ can't do this because it hasn't written it yet! */
      /*if (! file_erase (target_file, FALSE))
	return FALSE;*/
    }
#endif

  return TRUE;
}

XdfsFile*
xdfs_file (File* file)
{
  XdfsFile *xf;

  if (! file_get_xdfsfile (file, FP_XdfsFile, & xf))
    return FALSE;

  if (! xf->file)
    {
      unpack_xdfsfile (xf);

      xf->file = file;
    }

  return xf;
}

XdeltaSource*
xdfs_from_segment_source (File *file, gboolean is_literal)
{
  XdeltaSource *source;
  FileHandle *stream;
  FileHandle *stream_index_in = NULL;
  FileHandle *stream_index_out = NULL;

#if 0 /* @@@ No checksum caches for now. */
  if (! is_literal)
    {
      FileSegment* index_seg;

      index_seg = file_segment (file, xdelta_checksum_index_segment_path);

      if (segment_is_type_noerr (index_seg, SV_Regular))
	{
	  if (! (stream_index_in = segment_open (index_seg, HV_Read)))
	    return NULL;
	}
      else
	{
	  if (! (stream_index_out = segment_open (index_seg, HV_Replace | HV_NoSeek)))
	    return NULL;
	}
    }
#endif

  if (file_is_not_type (file, FV_Regular))
    return NULL;

  if (! (stream = file_open (file, HV_Read)))
    return NULL;

  if (! (source = xdp_source_new (path_basename (file_access_path (file)),
				  stream,
				  stream_index_in,
				  stream_index_out)))
    return NULL;

  return source;
}

gboolean
xdfs_from_segment (XdeltaGenerator* gen, File* seq, gint seq_index, gboolean is_lit)
{
  XdeltaSource* source;
  File* file;

  if (! (file = file_sequence_initialize_element (seq, seq_index)))
    return FALSE;

  if (segment_is_not_type (file_default_segment (file), SV_Regular))
    return FALSE;

  if (! (source = xdfs_from_segment_source (file, is_lit)))
    return FALSE;

  xdp_source_add (gen, source);

  return TRUE;
}

XdeltaGenerator*
xdfs_from_segments (XdfsDirectory* xdfs, guint nlit_seq)
{
  gint i;
  gint low;
  XdeltaGenerator* gen = xdp_generator_new ();
  gint patches;

  if ((patches = file_sequence_value (xdfs->pat_seq)) < 0)
    return NULL;

  low = MAX (0, patches - XDFS_MAX_FROM_SEGS);

  if (! xdfs_from_segment (gen, xdfs->lit_seq, nlit_seq, TRUE))
    return NULL;

  for (i = patches-1; i >= low; i -= 1)
    {
      /* @@@ Stronger selection criteria, such as minimum length, etc? */

      if (! xdfs_from_segment (gen, xdfs->pat_seq, i, FALSE))
	return NULL;
    }

  return gen;
}

gboolean
xdfs_replace_file_with_delta (XdfsDirectory* xdfs, guint replace_sequence, guint nlit_seq)
{
  XdeltaGenerator* gen;
  FileHandle *ostr_in, *d_out, *c_out;
  XdfsFile *xf;
  FileSegment* control_seg;
  FileSegment* default_seg;
  File* file;

  if (file_sequence_value (xdfs->lit_seq) < 2) /* @@@ fix this */
    return TRUE;

  if (! (file = file_sequence_initialize_element (xdfs->lit_seq, replace_sequence)))
    return FALSE;

  control_seg = file_segment (file, xdelta_control_segment_path);
  default_seg = file_default_segment (file);

  if (! (xf = xdfs_file (file)))
    return NULL;

  g_assert (xf->type == XST_Literal);

  if (! (gen = xdfs_from_segments (xdfs, nlit_seq)))
    return FALSE;

  if (! (ostr_in = segment_open (default_seg, HV_Read)))
    return FALSE;

  if (! (d_out = file_open (xdfs->pat_seq, HV_Replace)))
    return FALSE;

  if (! (c_out = segment_open (control_seg, HV_Replace)))
    return FALSE;

  if (! (xf->control = xdp_generate_delta (gen, ostr_in, c_out, d_out)))
    return FALSE;

  /* Update local inverse copy info
   */

  if (! xdfs_update_inverses (xf))
    return FALSE;

  return TRUE;
}

gboolean
xdfs_sources_set (XdfsFile *xf)
{
  if (! xf->control)
    {
      FileSegment* cont_seg = file_segment (xf->file, xdelta_control_segment_path);
      FileHandle* handle;

      if (segment_is_not_type (cont_seg, SV_Regular))
	return FALSE;

      if (! (handle = segment_open (cont_seg, HV_Read)))
	return FALSE;

      if (! (xf->control = xdp_control_read (handle)))
	return FALSE;
    }

  if (xf->control->source_info_len > 0)
    {
      XdfsFile** sources = g_new (XdfsFile*, xf->control->source_info_len);
      gint i;
      File* index = file_initialize_sibling (xf->file, "index");

      if (file_is_not_type (index, FV_Index))
	return FALSE;

      for (i = 0; i < xf->control->source_info_len; i += 1)
	{
	  XdeltaSourceInfo* info = xf->control->source_info[i];
	  Path* source_path;
	  File* source;
	  XdfsFile* source_xf;

	  if (! file_index_lookup (index, info->md5, 16, & source_path))
	    return FALSE;

	  if (! source_path)
	    {
	      xdfs_generate_file_event (EC_XdfsSourceNotPresent, xf->file);
	      return FALSE;
	    }

	  source = file_initialize_sibling (xf->file, path_basename (source_path));

	  if (! (source_xf = xdfs_file (source)))
	    return FALSE;

	  sources[i] = source_xf;
	}

      xf->sources = sources;
    }

  return TRUE;
}

/* XDFS View Construction
 */

#ifdef DEBUG_INST_ARRAY
static const char*
print_xdelta_inst (void* data)
{
  XdeltaInstruction *inst = data;
  static char buf[128];

  if (inst)
    {
      sprintf (buf, "<i=%d,o=%d,l=%d,s=%d>", inst->index, inst->offset, inst->length, inst->output_start);
    }
  else
    {
      return "";
    }

  return buf;
}

static gboolean
print_inst_array (XdeltaInstruction* inst, guint inst_len)
{
  int i;

  for (i = 0; i < inst_len; i += 1)
    g_print ("pos=%d/ind=%d/off=%d/len=%d ", inst[i].output_start, inst[i].index, inst[i].offset, inst[i].length);

  g_print ("\n");

  return TRUE;
}

static gboolean
print_xdfs_inst_array (XdfsInstruction* inst, guint inst_len)
{
  int i;

  g_print ("\n");

  for (i = 0; i < inst_len; i += 1)
    {
      FileHandle* handle = file_open (inst[i].file, HV_Read);
      guint8 *buf = g_malloc (inst[i].length);

      if (handle_seek (handle, inst[i].offset, HANDLE_SEEK_SET) != inst[i].offset)
	return FALSE;

      if (handle_read (handle, buf, inst[i].length) != inst[i].length)
	return FALSE;

      handle_printf (_stderr_handle, "pos=%d/file=%s/off=%d/len=%d/\"", inst[i].output_start, path_basename (file_dest_path (inst[i].file)), inst[i].offset, inst[i].length);

#if 0
      handle_write (_stderr_handle, buf, inst[i].length);
#endif
      handle_printf (_stderr_handle, "\"\n");

      g_free (buf);
    }

  return TRUE;
}

static gboolean
print_control_inst (XdeltaControl* cont)
{
  return print_inst_array (cont->inst, cont->inst_len);
}

static gboolean
print_index_inst (ConstructionIndex* index)
{
  return print_xdfs_inst_array (index->inst, index->inst_len);
}
#endif

void
xdfs_translate_copies (GArray* append_inst_array, XdeltaInstruction *translate, ConstructionIndex* translate_index)
{
  guint l = 0;
  guint u = translate_index->inst_len - 1;
  guint i;
  guint search_offset = translate->offset;
  guint output_start = translate->output_start;
  XdfsInstruction *inst;

  g_assert (search_offset < translate_index->length);
  g_assert (translate->length > 0);

  /* Binary search */

 again:

  i = (u+l)/2;

  inst = & translate_index->inst[i];

  if (search_offset < inst->output_start)
    {
      u = i - 1;
      goto again;
    }
  else if (search_offset >= (inst->output_start + inst->length))
    {
      l = i + 1;
      goto again;
    }
  else
    {
      guint to_copy = translate->length;

      while (to_copy > 0)
	{
	  XdfsInstruction copy;
	  guint inst_offset;
	  guint this_copy;

	  inst_offset = search_offset - inst->output_start;
	  this_copy   = MIN (to_copy, inst->length - inst_offset);

	  copy.offset = inst->offset + inst_offset;
	  copy.length = this_copy;
	  copy.xf     = inst->xf;
	  copy.output_start = output_start;

	  g_array_append_val (append_inst_array, copy);

	  output_start += this_copy;
	  search_offset += this_copy;
	  to_copy -= this_copy;
	  inst += 1;
	}
    }
}

gboolean
xdfs_construct_local_control (XdfsFile *xf, gint level)
{
  guint i;
  XdeltaControl* control = xf->control;
  GArray *inst_array;

  inst_array = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));

  for (i = 0; i < control->inst_len; i += 1)
    {
      XdeltaInstruction *inst = & control->inst[i];
      XdfsFile *inst_xf;

      inst_xf = xf->sources[inst->index];

      switch (inst_xf->type)
	{
	case XST_NotPresent:

	  xdfs_generate_file_event (EC_XdfsSourceNotPresent, xf->file);
	  return FALSE;

	case XST_Patch:
	case XST_Literal:
	  {
	    XdfsInstruction xi;

	    xi.xf     = inst_xf;
	    xi.offset = inst->offset;
	    xi.length = inst->length;
	    xi.output_start = inst->output_start;

	    g_array_append_val (inst_array, xi);
	  }
	  break;

	case XST_ForwardDelta:
	case XST_ReverseDelta:
	  {
	    ConstructionIndex *inst_index = inst_xf->index;

	    g_assert (inst_index);

	    xdfs_translate_copies (inst_array, inst, inst_index);
	  }
	  break;
	}
    }

  xf->index = g_new0 (ConstructionIndex, 1);

  xf->index->inst = g_memdup (inst_array->data, inst_array->len * sizeof (XdeltaInstruction));
  xf->index->inst_len = inst_array->len;
  xf->index->length = control->to_len;

  g_array_free (inst_array, TRUE);

  return TRUE;
}

gboolean
xdfs_construct_index (XdfsFile *xf, gint level)
{
  gboolean result = FALSE;

  if (xf->search_generation != xdfs_search_generation)
    {
      xf->search_generation = xdfs_search_generation;
      xf->search_flag = 0;
    }

  if (xf->search_flag)
    {
      xdfs_generate_file_event (EC_XdfsCyclicStructure, xf->file);
      return FALSE;
    }

  xf->search_flag = 1;

  switch (xf->type)
    {
    case XST_Literal:
    case XST_Patch:
      result = TRUE;
      break;

    case XST_ForwardDelta:
    case XST_ReverseDelta:

      {
	int i;

	if (xf->index)
	  {
	    result = TRUE;
	    break;
	  }

	if (! xdfs_sources_set (xf))
	  break;

	for (i = 0; i < xf->control->source_info_len; i += 1)
	  {
	    if (! xdfs_construct_index (xf->sources[i], level + 1))
	      goto bail;
	  }

	if (! xdfs_construct_local_control (xf, level))
	  break;

	result = TRUE;
      }
      break;
    default:
      abort ();
    }

 bail:

#if 0
  if (xf->index)
    {
      g_print ("index for %s: ", path_basename (file_dest_path (xf->file)));
      if (! print_index_inst (xf->index))
	return FALSE;
    }
#endif

  return result;
}

FileHandle*
xdfs_reader_handle (XdfsFile *xf)
{
  /* @@@ YUCK */
  static GHashTable* cache = NULL;
  FileHandle *fh;

  if (! cache)
    cache = g_hash_table_new (g_direct_hash, g_direct_equal);

  fh = g_hash_table_lookup (cache, xf);

  if (! fh)
    {
      if (! (fh = file_open (xf->file, HV_Read)))
	return NULL;

      g_hash_table_insert (cache, xf, fh);
    }

  return fh;
}

gboolean
xdfs_construct_page_in (FileSegment *seg,
			SegmentView *view,
			guint8      *pgbuf,
			gssize       page_no,
			gsize        in_len)
{
  XdfsFile* xf;
  File* file = segment_file (seg);
  guint search_offset = SEGMENT_VIEW_PAGE_SIZE * page_no;
  guint u;
  guint l = 0;
  guint i;
  XdfsInstruction *inst;

  if (! (xf = xdfs_file (file)))
    return FALSE;

  g_assert (view == xdelta_constructed_segment_view);
  g_assert (segment_name (seg) == path_root ());
  g_assert (xf->type != XST_Literal);

  xdfs_search_generation += 1;

  if (! xdfs_construct_index (xf, 0))
    return FALSE;

  u = xf->index->inst_len - 1;

  g_assert (search_offset < xf->index->length);

  /* Binary search */

 again:

  i = (u+l)/2;

  inst = & xf->index->inst[i];

  if (search_offset < inst->output_start)
    {
      u = i - 1;
      goto again;
    }
  else if (search_offset >= (inst->output_start + inst->length))
    {
      l = i + 1;
      goto again;
    }
  else
    {
      guint written = 0;

      while (in_len > 0)
	{
	  guint inst_offset;
	  guint this_copy;
	  FileHandle *fh;

	  inst_offset = search_offset - inst->output_start;
	  this_copy   = MIN (in_len, inst->length - inst_offset);

	  if (! (fh = xdfs_reader_handle (inst->xf)))
	    return FALSE;

	  if (handle_seek (fh, inst->offset, HANDLE_SEEK_SET) != inst->offset)
	    return FALSE;

	  if (handle_read (fh, pgbuf + written, this_copy) != this_copy)
	    return FALSE;

	  written += this_copy;
	  search_offset += this_copy;
	  in_len -= this_copy;
	  inst += 1;
	}
    }

  return TRUE;
}

/* Inverse computations
 */

void
pack_xdfsfile (XdfsFile *source)
{
  guint i;

  if (source->inverse_list)
    {
      SkipListNode *sln = xdfs_skip_list_first (source->inverse_list);

      source->inverse_write = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));

      for (; sln; sln = xdfs_skip_list_next (sln))
	{
	  XdfsInstruction xi;
	  XdeltaInstruction *inst = xdfs_skip_list_data (sln);

	  xi.offset       = inst->output_start;
	  xi.length       = inst->length;
	  xi.output_start = inst->offset;
	  xi.xf           = source;

	  g_array_append_val (source->inverse_write, xi);
	}
    }

  g_assert (source->inverse_write);

  source->inverse = (XdfsInstruction*) source->inverse_write->data;
  source->inverse_len = source->inverse_write->len;

  if (source->inverse_sequential)
    {
      for (i = 0; i < source->inverse_len; i += 1)
	source->inverse[i].output_start = 0;
    }
}

void
unpack_xdfsfile (XdfsFile *source)
{
  guint i;
  guint output_pos = 0;

  if (source->inverse_sequential)
    {
      for (i = 0; i < source->inverse_len; i += 1)
	{
	  source->inverse[i].output_start = output_pos;
	  output_pos += source->inverse[i].length;
	}
    }
}

#define UPDATE_INVERSE 2

gboolean
xdfs_update_inverses (XdfsFile *xf)
{
  int i;

  g_assert (xf->control);

  if (! xdfs_sources_set (xf))
    return FALSE;

  for (i = 0; i < xf->control->source_info_len; i += 1)
    {
      XdfsFile *source = xf->sources[i];

      if (source->inverse_sibling_name[0] == 0)
	{
	  /* I'm only going to compute the inverse on the first
	   * update, this is because it will not accomplish anything
	   * the second time or later because patch files will always
	   * be covered and literal files only get referenced once.
	   */
	  gssize length;

	  if ((length = file_length (source->file)) < 0)
	    return FALSE;

	  g_assert (! source->inverse_write);
	  g_assert (! source->inverse_list);

	  source->inverse_sibling_name = path_basename (file_dest_path (xf->file));
	  source->misc_flags = UPDATE_INVERSE;

	  if (xf->control->source_info[i]->sequential)
	    {
	      source->inverse_sequential = TRUE;
	      source->inverse_write = g_array_new (FALSE, FALSE, sizeof (XdfsInstruction));
	    }
	  else
	    {
	      source->inverse_sequential = FALSE;
	      source->inverse_list = xdfs_skip_list_new (length);
	    }
	}
      else
	{
	  /* Do nothing. */

	  source->misc_flags = 0;
	}
    }

  for (i = 0; i < xf->control->inst_len; i += 1)
    {
      XdeltaInstruction *inst = & xf->control->inst[i];
      XdfsFile *source = xf->sources[inst->index];

      if (source->misc_flags != UPDATE_INVERSE)
	continue;

      if (source->inverse_write)
	{
	  XdfsInstruction xi;

	  xi.offset       = inst->output_start;
	  xi.length       = inst->length;
	  xi.output_start = inst->offset;
	  xi.xf           = source;

	  g_array_append_val (source->inverse_write, xi);
	}
      else
	{
	  xdfs_skip_list_insert (source->inverse_list, inst->offset, inst->length, inst);
	}
    }

  for (i = 0; i < xf->control->source_info_len; i += 1)
    {
      XdfsFile *source = xf->sources[i];

      if (source->misc_flags != UPDATE_INVERSE)
	continue;

      pack_xdfsfile (source);

      if (! file_set_xdfsfile (source->file, FP_XdfsFile, source))
	return FALSE;

      unpack_xdfsfile (source);
    }

  return TRUE;
}
