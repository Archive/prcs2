/* -*-Mode: C;-*-
 * $Id: xdfs.h 1.11 Mon, 03 May 1999 04:42:34 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _XDFS_H_
#define _XDFS_H_

#include <repo.h>
#include <xdelta.h>

typedef struct _ConstructionIndex       ConstructionIndex;
typedef struct _SkipList                SkipList;
typedef struct _SkipListNode            SkipListNode;

typedef struct _SerialXdfsDirectory     XdfsDirectory;
typedef struct _SerialXdfsFile          XdfsFile;
typedef struct _SerialXdfsInstruction   XdfsInstruction;
typedef struct _SerialXdfsInverseSource XdfsInverseSource;
typedef struct _SerialXdfsUpdate        XdfsUpdate;
typedef struct _SerialXdfsRequest       XdfsRequest;
typedef struct _SerialXdfsReply         XdfsReply;

#include "xdfs_edsio.h"

enum _XdfsStorageType {
  XST_Literal,
  XST_ForwardDelta,
  XST_ReverseDelta,
  XST_Patch,
  XST_NotPresent
};

typedef enum _XdfsStorageType XdfsStorageType;

typedef gboolean (* XdfsListenFunc) (XdfsDirectory *, const guint8* digest);

gboolean        xdfs_library_init (const MessageDigest *md);

const char*     xdfs_storage_type_to_string (XdfsStorageType t);

XdfsDirectory*  xdfs_directory_init     (File* dir, void* user_data);
void*           xdfs_user_data          (XdfsDirectory* xdfs);

void            xdfs_listen             (XdfsListenFunc func);

File*           xdfs_index              (XdfsDirectory* xdfs);
File*           xdfs_sequence           (XdfsDirectory* xdfs);

extern const MessageDigest* xdfs_message_digest;

#define XDFS_MAX_FROM_SEGS (2*6)

#define XDFS_LITERAL_TRIGGER_TYPE 123
#define XDFS_PATCH_TRIGGER_TYPE 125

#endif
