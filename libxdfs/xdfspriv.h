/* -*-Mode: C;-*-
 * $Id: xdfspriv.h 1.4 Thu, 06 May 1999 00:58:18 -0700 jmacd $
 *
 * Copyright (C) 1998, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _XDFSPRIV_H_
#define _XDFSPRIV_H_

XdfsFile* xdfs_file (File* file);

extern guint xdfs_search_generation;
extern XdfsFileXdfsFileProperty FP_XdfsFile;
extern XdfsFileXdfsDirectoryProperty FP_XdfsDirectory;

struct _ConstructionIndex {
  XdfsInstruction* inst;
  guint inst_len;
  guint length;
};

guint               xdfs_skip_list_insert (SkipList *skp,
					   guint32   insert_offset,
					   guint32   insert_length,
					   void     *data);

SkipListNode*       xdfs_skip_list_search (SkipList* skp,
					   guint32   search_offset);

SkipListNode*       xdfs_skip_list_search_nearest (SkipList* skp,
						   guint32   search_offset);

SkipList*           xdfs_skip_list_new    (guint32   length);

void                xdfs_skip_list_print  (SkipList     *skp,
					   const char* (*f) (void* data));

void*               xdfs_skip_list_data   (SkipListNode* sln);

SkipListNode*       xdfs_skip_list_first  (SkipList     *skp);
SkipListNode*       xdfs_skip_list_next   (SkipListNode *sln);
guint               xdfs_skip_list_offset (SkipListNode *sln);
guint               xdfs_skip_list_length (SkipListNode *sln);

gboolean            xdfs_sources_set (XdfsFile *xf);

FileHandle*         xdfs_reader_handle (XdfsFile *xf);

#endif
