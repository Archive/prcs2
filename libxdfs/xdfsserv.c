/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: xdfsserv.c 1.6 Fri, 30 Apr 1999 01:29:46 -0700 jmacd $
 */

#include <stdio.h>

#include "server.h"
#include "xdfs.h"

gboolean
checkin_command (gint argc, gchar** argv)
{
  Path* src_path;
  FileHandle *dest_handle, *src_handle;
  File* src_file;
  NameSpace* ns;

  if (! (ns = server_namespace_attach_nspath (argv[0], NULL, NSAF_Create)))
    return FALSE;

  if (! (dest_handle = file_open (server_namespace_xdfs_sequence (ns), HV_Replace)))
    return FALSE;

  if (! (src_path = path_canonicalize (_fs_repo, argv[1])))
    return FALSE;

  if (! (src_file = file_initialize (_fs_repo, src_path)))
    return FALSE;

  if (! (src_handle = file_open (src_file, HV_Read)))
    return FALSE;

  if (! handle_copy (src_handle, dest_handle, 0, handle_length (src_handle)))
    return FALSE;

  if (! handle_close (dest_handle))
    return FALSE;

  return TRUE;
}

gboolean
checkout_command (gint argc, gchar** argv)
{
  File* src;
  File* out;
  Path* out_path, *src_path;
  NameSpace* ns;

  if (! (ns = server_namespace_attach_nspath (argv[0], NULL, NSAF_None)))
    return FALSE;

  if (! (out_path = path_canonicalize (_fs_repo, argv[2])))
    return FALSE;

  if (! (out = file_initialize (_fs_repo, out_path)))
    return FALSE;

  if (strlen (argv[0]) == 32)
    {
      guint8 md5[16];
      File* index = server_namespace_xdfs_index (ns);

      if (! edsio_md5_from_string (md5, argv[1]))
	return FALSE;

      if (! file_index_lookup (index, md5, 16, & src_path))
	return FALSE;

      if (! src_path)
	{
	  serv_generate_filestring_event (EC_ServFileNotFoundDigest, index, argv[1]);
	  return FALSE;
	}

      src = file_initialize (repository_of_file (index), src_path);
    }
  else
    {
      guint i;

      if (! strtoui_checked (argv[1], & i, "Version number"))
	return FALSE;

      if (! (src = file_sequence_initialize_element (server_namespace_xdfs_sequence (ns), i)))
	return FALSE;
    }

  if (! file_copy (src, out))
    return FALSE;

  return TRUE;
}

int
main (int argc, char** argv)
{
  server_add_command ("checkin", checkin_command, 3);
  server_add_command ("ci",      checkin_command, 3);

  server_add_command ("checkout", checkout_command, 4);
  server_add_command ("co",       checkout_command, 4);

  return server_main (argc, argv);
}
