/* -*-Mode: C;-*-
 * PRCS - The Project Revision Control System
 * Copyright (C) 1997, 1998, 1999  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: xdfssync.c 1.13 Thu, 06 May 1999 00:58:18 -0700 jmacd $
 */

#include "server.h"
#include "serverpriv.h"
#include "xdfspriv.h"

#define DEBUG_XDFS

typedef struct {
  ReadyFunc   ready;
  GPtrArray  *family_reqs;
  ServerPeer *peer;
  FileHandle *residue_handle;

  GSList *send_segments;
  GSList *send_controls;

} XdfsFamilyReplyData;

static ServFileXdfsUpdateProperty  FP_XdfsUpdate;

static File* xdfs_sync_dir;
static File* xdfs_sync_update_index;

static TaskControl xdfs_sync_client_send_one_request    (Connection* conn, Task* task);
static TaskControl xdfs_sync_client_recv_replies_a      (Connection* conn, Task* task);
static TaskControl xdfs_sync_server_recv_requests_a     (Connection* conn, Task* task);
static TaskControl xdfs_sync_server_send_replies        (Connection* conn, Task* task, ReadyFunc ready, void* data);
static TaskControl xdfs_sync_server_send_replies_done   (Connection* conn, Task* task, ReadyFunc ready, void* data);
static TaskControl xdfs_sync_server_send_family_replies (Connection* conn, Task* task);
static TaskControl xdfs_sync_server_send_one_reply      (Connection* conn, Task* task);
static TaskControl xdfs_sync_server_send_one_literal    (Connection* conn, Task* task);

static gboolean    xdfs_initialize_search (XdfsFile *xf, XdfsFamilyReplyData *sr, gboolean request_full);

gboolean
xdfs_sync_library_init (void)
{
  if (! edsio_new_file_xdfsupdate_property ("Update", PF_Persistent, & FP_XdfsUpdate))
    return FALSE;

  xdfs_sync_dir = file_initialize (srepo, path_absolute ("xdfssync"));

  if (file_is_type_noerr (xdfs_sync_dir, FV_Directory))
    xdfs_sync_update_index = file_initialize (srepo, path_absolute2 ("xdfssync", "update"));

  return TRUE;
}

gboolean
xdfs_sync_library_create (void)
{
  if (! file_mkdir (xdfs_sync_dir))
    return FALSE;

  xdfs_sync_update_index = file_initialize (srepo, path_absolute2 ("xdfssync", "update"));

  /* @@@ Note: this index is not used for queries, only iterate, so it
   * should eventually use recno and eliminate the bogus key field. */
  if (! file_index_create (xdfs_sync_update_index, path_absolute ("Update"), IT_Hash))
    return FALSE;

  if (! file_index_link (xdfs_sync_update_index, file_initialize (srepo, _nsr), TRUE))
    return FALSE;

  return TRUE;
}

static gboolean
xdfs_sync_test_has_bit (XdfsFile* rec, guint index)
{
  return server_test_bit (rec->peerhas, rec->peerhas_len, index);
}

static void
xdfs_sync_set_has_bit (XdfsFile* rec, guint index)
{
  server_set_bit (& rec->peerhas, & rec->peerhas_len, & rec->peerhas_write, index);
}

gboolean
xdfs_sync_log_peer_insert (XdfsDirectory* xdfs, ServerPeer* peer, const guint8* digest)
{
  XdfsFile* xf;
  Path* ent_path;
  File* ent;
  NameSpace *ns = xdfs_user_data (xdfs);

  if (! file_index_lookup (xdfs_index (xdfs), digest, xdfs_message_digest->cksum_len, & ent_path))
    return FALSE;

  if (ent_path == NULL)
    {
      /* This means we don't have the file version yet.  Create a
       * stub, this involves:
       *
       *   a. Assert it's digest (even though it is not present) to be the one
       *      supplied to add it to the XDFS index.
       *   b. Set a Update token to add its dir to the requests index.
       *
       * Later,
       *
       *   c. If the file arrives, deal with moving the peer log, unset
       *      the update token.
       */

      if (! file_test_xdfsupdate (xdfs->dir, FP_XdfsUpdate))
	{
	  XdfsUpdate *xm = g_new0 (XdfsUpdate, 1);

	  RAND_bytes (xm->key, 8);

	  if (! file_set_xdfsupdate (xdfs->dir, FP_XdfsUpdate, xm))
	    return FALSE;
	}

      ent = file_sequence_initialize_next (ns->missing_seq);

      if (! file_digest_assert (ent, xdfs_message_digest, digest))
	return FALSE;
    }
  else
    {
      ent = file_initialize (srepo, ent_path);

      if (file_test_xdfsfile (ent, FP_XdfsFile))
	{
	  if (! (xf = xdfs_file (ent)))
	    return FALSE;
	}
    }

  if (! xf)
    {
      xf = g_new0 (XdfsFile, 1);

      xf->type = XST_NotPresent;
      xf->file = ent;
      xf->inverse_sibling_name = "";
    }

  xdfs_sync_set_has_bit (xf, peer->peer_record->index);

  if (! file_set_xdfsfile (ent, FP_XdfsFile, xf))
    return FALSE;

  return TRUE;
}

/* XDFS Client
 */

typedef struct {
  DirectoryHandle* update_dh;
  DirectoryHandle* family_dh;
} XdfsClientRequestData;

typedef struct {
  void       *msg;
  SerialType  msg_type;
} XdfsClientReplyData;

TaskControl
xdfs_sync_client_send_requests (Connection* conn, Task* task)
{
  XdfsClientRequestData *data = task_stack_push (task, XdfsClientRequestData);

  if (! (data->update_dh = file_index_iterate (xdfs_sync_update_index, IQF_NoIndexKey)))
    return reponet_kill_task (task);

  return xdfs_sync_client_send_one_request (conn, task);
}

TaskControl
xdfs_sync_client_send_one_request (Connection* conn, Task* task)
{
  XdfsClientRequestData* data = task_stack_peek (task, XdfsClientRequestData);
  ServerPeer* peer = SYNC_DATA (conn)->peer;

  /* This function iterates over two handles at once.
   */

 again:

  if (! data->family_dh)
    {
      Path* upath = dh_next (data->update_dh);

      if (upath)
	{
	  File* udir = file_initialize (srepo, upath);

	  if (! (data->family_dh = file_opendir_prefix (udir, "mis:")))
	    return reponet_kill_task (task);
	}
    }

  if (data->family_dh)
    {
      Path* path;

      while ((path = dh_next (data->family_dh)))
	{
	  File* ent = file_initialize (srepo, path);
	  XdfsFile* xf;
	  const guint8* digest;

	  if (! (xf = xdfs_file (ent)))
	    return reponet_kill_task (task);

	  if (! xdfs_sync_test_has_bit (xf, peer->peer_record->index))
	    continue;

	  if (! (digest = file_digest (ent, xdfs_message_digest)))
	    return reponet_kill_task (task);

#ifdef DEBUG_XDFS
	  {
	    char buf[33];
	    edsio_md5_to_string (digest, buf);
	    g_print ("request xdfs update path: %s %s\n", path_to_string (srepo, path_dirname (path)), buf);
	  }
#endif

	  return task_finish_put (conn,
				  task,
				  xdfs_sync_client_send_one_request,
				  serialize_xdfsrequest (conn_sink (conn),
							 server_namespace_path_to_nspath_int (path_dirname (path)),
							 xdfs_message_digest->cksum_len,
							 digest));
	}

      if (! dh_close (data->family_dh))
	return reponet_kill_task (task);

      data->family_dh = NULL;

      goto again;
    }

  task_stack_pop (task, XdfsClientRequestData);

  if (! dh_close (data->update_dh))
    return reponet_kill_task (task);

  return task_finish_put (conn,
			  task,
			  protocol_next,
			  serialize_xdfsrequestdone (conn_sink (conn)));
}

TaskControl
xdfs_sync_client_recv_replies (Connection* conn, Task* task)
{
  XdfsClientReplyData* data = task_stack_push (task, XdfsClientReplyData);

  return task_finish_get (conn,
			  task,
			  xdfs_sync_client_recv_replies_a,
			  ST_XdfsReply | ST_XdfsReplyDone,
			  & data->msg_type,
			  & data->msg);
}

TaskControl
xdfs_sync_client_recv_replies_a (Connection* conn, Task* task)
{
  XdfsClientReplyData* data = task_stack_pop (task, XdfsClientReplyData);

  if (data->msg_type == ST_XdfsReply)
    {
      XdfsReply* reply = data->msg;

      g_print ("received %s reply\n", reply->cont_len == 0 ? "literal" : "delta");

      /* @@@ write control */

      if (reply->cont_len == 0)
	{
	  /* @@@ choose file */
	  File* t = file_initialize_temp (srepo);

	  return task_segments_get (conn,
				    task,
				    xdfs_sync_client_recv_replies,
				    file_default_segment (t),
				    NULL,
				    NULL);
	}

      return xdfs_sync_client_recv_replies (conn, task);
    }

  return protocol_next (conn, task);
}

/* XDFS Server
 */

#define SERVER_XDFS_SYNC_DATA(conn) conn_named_data (conn, "XDFS_SYNC", XdfsServerRequestData)

typedef struct {
  void       *msg;
  SerialType  msg_type;
  GPtrArray  *family_reqs;
  const char *family_nspath;
} XdfsServerRequestData;

TaskControl
xdfs_sync_server_recv_requests (Connection* conn, Task* task)
{
  XdfsServerRequestData* data = SERVER_XDFS_SYNC_DATA (conn);

  data->msg = NULL;
  data->msg_type = 0;

  return task_finish_get (conn,
			  task,
			  xdfs_sync_server_recv_requests_a,
			  ST_XdfsRequest | ST_XdfsRequestDone,
			  & data->msg_type,
			  & data->msg);
}

TaskControl
xdfs_sync_server_recv_requests_a (Connection* conn, Task* task)
{
  XdfsServerRequestData* data = SERVER_XDFS_SYNC_DATA (conn);

  if (data->msg_type == ST_XdfsRequest)
    {
      XdfsRequest* req = data->msg;

#ifdef DEBUG_XDFS
      g_print ("req %s\n", req->nspath);
#endif

      if (! data->family_reqs)
	{
	  data->family_reqs = g_ptr_array_new ();
	  data->family_nspath = req->nspath;
	}

      if (strcmp (req->nspath, data->family_nspath) != 0)
	{
	  task_produce (conn, task, xdfs_sync_server_send_replies, data->family_reqs);

	  data->family_reqs = g_ptr_array_new ();
	  data->family_nspath = req->nspath;
	}

      g_ptr_array_add (data->family_reqs, req);

      return xdfs_sync_server_recv_requests (conn, task);
    }
  else if (data->family_reqs)
    {
      task_produce (conn, task, xdfs_sync_server_send_replies, data->family_reqs);
    }

  task_produce (conn, task, xdfs_sync_server_send_replies_done, NULL);

  task_produce_done (conn, task);

  return protocol_next (conn, task);
}

static int
req_compare (const void* va, const void* vb)
{
  XdfsRequest** a = (XdfsRequest**) va;
  XdfsRequest** b = (XdfsRequest**) vb;

  /* youngest first */

  return (*b)->order - (*a)->order;
}

TaskControl
xdfs_sync_server_send_replies_done (Connection* conn, Task* task, ReadyFunc ready, void* data)
{
  return task_finish_put (conn,
			  task,
			  ready,
			  serialize_xdfsreplydone (conn_sink (conn)));
}

enum _XdfsSyncSearchFlag {
  XSSF_None      = 0,
  XSSF_Requested = (1 << 0),
  XSSF_Have      = (1 << 1)
};

TaskControl
xdfs_sync_server_send_replies (Connection* conn, Task* task, ReadyFunc ready, void* data)
{
  GPtrArray* family_reqs = data;
  XdfsRequest* req0 = family_reqs->pdata[0];
  NameSpace *ns;
  XdfsFamilyReplyData *sr = task_stack_push (task, XdfsFamilyReplyData);
  int i;

  sr->peer = SYNC_DATA (conn)->peer;
  sr->family_reqs = family_reqs;
  sr->ready = ready;

  xdfs_search_generation += 1;

  if (! (ns = server_namespace_attach_nspath (req0->nspath, NULL, NSAF_None)))
    return reponet_kill_task (task);

  for (i = 0; i < family_reqs->len; i += 1)
    {
      XdfsRequest* req = family_reqs->pdata[i];

      File* send_file;
      Path* send_path;

      g_assert (req->xf == NULL);

      if (! file_index_lookup (server_namespace_xdfs_index (ns), req->digest, xdfs_message_digest->cksum_len, & send_path))
	return reponet_kill_task (task);

      /* @@@ Need to handle these, it is a client/server inconsistency. */
      g_assert (send_path);
      send_file = file_initialize (srepo, send_path);
      if (file_is_not_type_noerr (send_file, FV_Regular))
	abort (); /* @@@ */
      g_assert (strncmp ("lit:", path_basename (send_path), 4) == 0);
      /* @@@ Can also assert server's peer log to see if it thinks the
       * client is missing it, to take note of the inconsistency. */

      if (! strtosi_checked (path_basename (send_path) + 4, &req->order, "XDFS index"))
	return reponet_kill_task (task);

      if (! (req->xf = xdfs_file (send_file)))
	return reponet_kill_task (task);

      if (! xdfs_initialize_search (req->xf, sr, TRUE))
	return reponet_kill_task (task);
    }

  qsort (family_reqs->pdata, family_reqs->len, sizeof (void*), req_compare);

  return xdfs_sync_server_send_family_replies (conn, task);
}

gboolean
xdfs_initialize_search (XdfsFile *xf, XdfsFamilyReplyData *sr, gboolean request_full)
{
  if (xf->search_generation != xdfs_search_generation)
    {
      gssize length;

      if ((length = file_length (xf->file)) < 0)
	return NULL;

      if (! (xf->md5 = file_digest (xf->file, xdfs_message_digest)))
	return FALSE;

      xf->length = length;
      xf->sync_list = NULL;

      if (xdfs_sync_test_has_bit (xf, sr->peer->peer_record->index))
	{
	  g_assert (! request_full);

	  xf->search_flag = XSSF_Have;

	  xf->try_copy    = NULL;
	}
      else
	{
	  xf->try_copy = xdfs_skip_list_new (length);

	  if (request_full)
	    {
	      xf->search_flag = XSSF_Requested;

	      switch (xf->type)
		{
		case XST_ForwardDelta:
		case XST_ReverseDelta:

		  /* If a delta is being requested, propgate to its primary patch
		   * if it has one. */

		  if (! xdfs_sources_set (xf))
		    return FALSE;

		  if (xf->control->has_data)
		    {
		      g_assert (xf->sources[0]);

		      if (! xdfs_initialize_search (xf->sources[0], sr, TRUE))
			return FALSE;
		    }

		  /* @@@@@@@@@ Request copies for the entire thing. */
		  /* Here is where to decide that a delta will be unmodified. */
		  /* I am leaving it this way to test the new code. */
		  xdfs_skip_list_insert (xf->try_copy, 0, xf->length, NULL);
		  break;

		case XST_Patch:

		  xf->search_flag = XSSF_Have;
		  xf->try_copy    = NULL;

		  /* Add this segment to the send list */
		  sr->send_segments = g_slist_prepend (sr->send_segments, file_default_segment (xf->file));

		  break;

		case XST_Literal:

		  /* If it has no sibling, this file has to be sent. */
		  if (! xf->inverse_sibling_name[0])
		    {
		      xf->search_flag = XSSF_Have;
		      xf->try_copy    = NULL;

		      /* Add this segment to the send list */
		      sr->send_segments = g_slist_prepend (sr->send_segments, file_default_segment (xf->file));
		    }
		  else
		    {
		      /* Request copies for the entire thing. */
		      xdfs_skip_list_insert (xf->try_copy, 0, xf->length, NULL);
		    }

		  break;

		case XST_NotPresent:
		  abort ();
		}
	    }
	  else
	    {
	      xf->search_flag = XSSF_None;
	    }
	}
    }

  return TRUE;
}

static gboolean
xdfs_generate_matched_delta (XdfsFile *xf, XdfsFamilyReplyData *sr)
{
  /* Here is where the decision to send a pre-computed delta will go.
   * For now unconditionally generate a new matched delta.  */
  XdeltaControl *control;
  GArray *inst_array;
  GPtrArray *source_info;
  GHashTable *source_table;
  SkipListNode* sln;
  guint output_pos = 0;

  if (xf->search_flag == XSSF_Have)
    return TRUE;

  control = g_new0 (XdeltaControl, 1);

  memcpy (control->to_md5, xf->md5, 16);

  control->to_len = xf->length;
  control->has_data = FALSE;

  inst_array = g_array_new (FALSE, FALSE, sizeof (XdeltaInstruction));
  source_info = g_ptr_array_new ();
  source_table = g_hash_table_new (g_direct_hash, g_direct_equal);

  for (sln = xdfs_skip_list_first (xf->sync_list);
       sln;
       sln = xdfs_skip_list_next (sln))
    {
      XdeltaInstruction inst;
      XdfsInstruction *xi = xdfs_skip_list_data   (sln);
      guint length        = xdfs_skip_list_length (sln);
      guint offset        = xdfs_skip_list_offset (sln);
      guint index;
      XdeltaSourceInfo *si;

      if (! (si = g_hash_table_lookup (source_table, xi->xf)))
	{
	  si = g_new0 (XdeltaSourceInfo, 1);

	  si->name = path_basename (file_dest_path (xi->xf->file));

	  memcpy (si->md5, xi->xf->md5, 16);

	  si->len = xi->xf->length;
	  si->isdata = FALSE;
	  si->sequential = FALSE;

	  si->xdfs_index = index = source_info->len;

	  g_ptr_array_add (source_info, si);
	}
      else
	{
	  index = si->xdfs_index;
	}

      inst.index = index;
      inst.offset = xi->offset;
      inst.length = xi->length;
      inst.output_start = offset;

      g_assert (length == xi->length);
      g_assert (output_pos == inst.output_start);

      g_array_append_val (inst_array, inst);

      output_pos += length;
    }

  control->inst = (XdeltaInstruction*) inst_array->data;
  control->inst_len = inst_array->len;

  control->source_info = (XdeltaSourceInfo**) source_info->pdata;
  control->source_info_len = source_info->len;

  /* Add to the send list */
  sr->send_controls = g_slist_prepend (sr->send_controls, control);

  return TRUE;
}

static GMemChunk *xi_chunk = NULL;

static gboolean
xdfs_inverse_source_set (XdfsFile *xf, XdfsFamilyReplyData *sr)
{
  /* Set up xf->inverse_source, xf->inverse_list. */

  if (! xf->inverse_list)
    {
      guint i, inst_len = xf->inverse_len;
      File *sibling;
      XdfsInstruction *xi_array;

      g_assert (xf->inverse_sibling_name[0]);

      sibling = file_initialize_sibling (xf->file, xf->inverse_sibling_name);

      if (! (xf->inverse_source = xdfs_file (sibling)))
	return FALSE;

      g_assert (xf->inverse_source->type | (XST_ReverseDelta | XST_ForwardDelta));

      if (! xdfs_initialize_search (xf->inverse_source, sr, FALSE))
	return FALSE;

      xf->inverse_list = xdfs_skip_list_new (xf->length);

      xi_array = xf->inverse;

      for (i = 0; i < inst_len; i += 1)
	{
	  XdfsInstruction *xi = xi_array + i;

	  xi->xf = xf->inverse_source;

	  xdfs_skip_list_insert (xf->inverse_list, xi->output_start, xi->length, xi);
	}
    }

  return TRUE;
}

static gboolean
xdfs_residue_add (XdfsFile *xf, XdfsFamilyReplyData *sr, guint offset, guint length)
{
  XdfsInstruction *xi = g_chunk_new (XdfsInstruction, xi_chunk);
  FileHandle *fh;

  g_assert (xf->type & (XST_Patch | XST_Literal));

  if (! (fh = xdfs_reader_handle (xf)))
    return FALSE;

  xi->output_start = offset;
  xi->offset       = handle_length (sr->residue_handle);
  xi->length       = length;
  xi->xf           = xf;

  if (! handle_copy (fh, sr->residue_handle, offset, length))
    return FALSE;

  xdfs_skip_list_insert (xf->sync_list, xi->offset, xi->length, xi);

  return TRUE;
}

static void
xdfs_sync_translate_copies (SkipList *source_sync_list,
			    SkipList *dest_sync_list,
			    guint     source_start,
			    guint     dest_start,
			    guint     copy_len)
{
  /* translate copies from source_sync_list into dest_sync_list.  This
   * is much like the similarly named function in xdfs.c for the skip
   * list structure */

  SkipListNode *sln;

  sln = xdfs_skip_list_search (source_sync_list, source_start);

  g_assert (sln);

  for (;
       sln && copy_len > 0;
       sln = xdfs_skip_list_next (sln))
    {
      XdfsInstruction *xi = xdfs_skip_list_data (sln);
      XdfsInstruction *nxi = g_chunk_new (XdfsInstruction, xi_chunk);
      guint this_copy, diff;

      g_assert (xi->output_start <= source_start);

      diff = xi->output_start - source_start;

      this_copy = MIN (copy_len, xi->length - diff);

      nxi->offset = xi->offset + diff;
      nxi->length = this_copy;
      nxi->output_start = dest_start;
      nxi->xf = xi->xf;

      xdfs_skip_list_insert (dest_sync_list, dest_start, this_copy, nxi);

      copy_len -= this_copy;
      source_start += this_copy;
      dest_start += this_copy;
    }
}

static gboolean
xdfs_try_copies (XdfsFile *xf, SkipList *inst_list, XdfsFamilyReplyData *sr, gboolean try_phase)
{
  SkipListNode *sln;

  for (sln = xdfs_skip_list_first (xf->try_copy);
       sln;
       sln = xdfs_skip_list_next (sln))
    {
      guint try_copy_start  = xdfs_skip_list_offset (sln);
      guint try_copy_length = xdfs_skip_list_length (sln);

      SkipListNode *iln;

      for (iln = xdfs_skip_list_search_nearest (inst_list, try_copy_start);
	   try_copy_length > 0 && iln;
	   iln = xdfs_skip_list_next (iln))
	{
	  guint inst_start  = xdfs_skip_list_offset (iln);
	  guint inst_length = xdfs_skip_list_length (iln);
	  XdfsInstruction *xi = xdfs_skip_list_data (iln);
	  guint diff, copy_len;

	  if (try_copy_start < inst_start)
	    {
	      diff = MIN (inst_start - try_copy_start, try_copy_length);

	      if (try_phase && ! xdfs_residue_add (xf, sr, try_copy_start, diff))
		return FALSE;

	      try_copy_start += diff;
	      try_copy_length -= diff;
	    }

	  if (try_copy_start < inst_start)
	    break;

	  diff = try_copy_start - inst_start;

	  if (diff > 0)
	    {
	      inst_start += diff;
	      inst_length -= diff;
	    }

	  g_assert (inst_length > 0);

	  copy_len = MIN (inst_length, try_copy_length);

	  /* add interval to source try_copy */
	  if (try_phase)
	    {
	      xdfs_skip_list_insert (xi->xf->try_copy, xi->offset, copy_len, NULL);
	    }
	  else
	    {
	      xdfs_sync_translate_copies (xi->xf->sync_list, xf->sync_list, xi->offset, inst_start, copy_len);
	    }

	  try_copy_start += diff;
	  try_copy_length -= diff;
	}

      if (try_phase && ! xdfs_residue_add (xf, sr, try_copy_start, try_copy_length))
	return FALSE;
    }

  return TRUE;
}

static gboolean
xdfs_mark_residue (XdfsFile *xf, XdfsFamilyReplyData *sr)
{
  SkipList *inst_list;
  XdfsFile **all_sources;
  guint      all_sources_len;
  XdfsFile  *inverse_source;

  /* This is a post-order traversal, sync_list is the mark. */
  if (xf->sync_list)
    return TRUE;

  xf->sync_list = xdfs_skip_list_new (xf->length);

  if (! xi_chunk)
    xi_chunk = g_mem_chunk_create (XdfsInstruction, 1024, G_ALLOC_ONLY);

  if (xf->search_flag == XSSF_Have)
    {
      /* if they have it, there is no residue, compute a trivial
       * sync_list, return. */
      XdfsInstruction *xi = g_chunk_new (XdfsInstruction, xi_chunk);

      xi->offset = 0;
      xi->length = xf->length;
      xi->output_start = 0;
      xi->xf = xf;

      xdfs_skip_list_insert (xf->sync_list, 0, xf->length, xf);

      return TRUE;
    }

  /* Now do type-specific initializataion of this file's copy
   * instructions */
  switch (xf->type)
    {
    case XST_ForwardDelta:
    case XST_ReverseDelta:
      {
	XdeltaControl *control = xf->control;
	XdeltaInstruction *inst_array = control->inst;

	guint i, inst_len = control->inst_len;

	if (! xdfs_sources_set (xf))
	  return FALSE;

	all_sources = xf->sources;
	all_sources_len = xf->control->source_info_len;

	if (xf->control->has_data)
	  {
	    /* If a delta and has_data, make recursive call on patch. */
	    /* This call results in a base-case of the recursion,
	     * which is the patch data's inverse, or this delta.
	     * After completing this final level of recursion, xf's
	     * try_copy list is complete. */

	    g_assert (xf->sources[0]);

	    if (! xdfs_mark_residue (xf->sources[0], sr))
	      return FALSE;
	  }

	inst_list = xdfs_skip_list_new (xf->length);

	for (i = 0; i < inst_len; i += 1)
	  {
	    XdeltaInstruction *inst = inst_array + i;
	    XdfsInstruction *xi = g_chunk_new (XdfsInstruction, xi_chunk);

	    xi->output_start = inst->output_start;
	    xi->length       = inst->length;
	    xi->offset       = inst->offset;
	    xi->xf           = xf->sources[inst->index];

	    xdfs_skip_list_insert (inst_list, xi->output_start, xi->length, xi);
	  }
      }
      break;

    case XST_Patch:
    case XST_Literal:

      {
	if (! xdfs_inverse_source_set (xf, sr))
	  return FALSE;

	inverse_source = xf->inverse_source;

	all_sources = & xf->inverse_source;
	all_sources_len = 1;

	inst_list = xf->inverse_list;
      }

      break;

    default:
      break;
    }

  /* At this point, all requested intervals have been entered into
   * xf->try_copy, and all available copy intervals are in
   * inst_list. */

  /* Now, iterate over the try_copy and inst_list, filling source
   * try_copies and residue with the overlap and differences,
   * respectively.  NULL data fields are filled in now, as only the
   * intervals are used to build the sync_list. */

  g_assert (xf->try_copy);

  if (! xdfs_try_copies (xf, inst_list, sr, TRUE))
    return FALSE;

  /* Now recurse on the literal source, if there is one. */
  if (inverse_source && ! xdfs_mark_residue (inverse_source, sr))
    return FALSE;

  /* Iterate over the self try_copy list again, filling in holes in in
   * sync_list using the sources' sync_lists (now computed) */

  if (! xdfs_try_copies (xf, inst_list, sr, FALSE))
    return FALSE;

  /* Return from this function means xf->sync_list is a interval tree
   * matched to the peer. */

  return TRUE;
}

static gboolean
xdfs_compute_residue (XdfsFile *xf, XdfsFamilyReplyData *sr)
{
  if (! xdfs_initialize_search (xf, sr, FALSE))
    return FALSE;

  if (! xdfs_mark_residue (xf, sr))
    return FALSE;

  return TRUE;
}

TaskControl
xdfs_sync_server_send_family_replies (Connection* conn, Task* task)
{
  XdfsFamilyReplyData *sr = task_stack_peek (task, XdfsFamilyReplyData);
  guint i;
  FileHandle *fh;
  File* t = file_initialize_temp (srepo);

  if (! (fh = file_open (t, HV_Replace)))
    return reponet_kill_task (task);

  handle_digest_compute (fh, xdfs_message_digest);

  sr->residue_handle = fh;

  for (i = 0; i < sr->family_reqs->len; i += 1)
    {
      if (! xdfs_compute_residue (sr->family_reqs->pdata[i], sr))
	return reponet_kill_task (task);
    }

  if (! handle_close (fh))
    return reponet_kill_task (task);

  for (i = 0; i < sr->family_reqs->len; i += 1)
    {
      if (! xdfs_generate_matched_delta (sr->family_reqs->pdata[i], sr))
	return reponet_kill_task (task);
    }

  return xdfs_sync_server_send_one_reply (conn, task);
}

TaskControl
xdfs_sync_server_send_one_reply (Connection* conn, Task* task)
{
  XdfsFamilyReplyData *sr = task_stack_peek (task, XdfsFamilyReplyData);

  if (sr->send_segments)
    {
      return task_finish_put (conn,
			      task,
			      xdfs_sync_server_send_one_literal,
			      serialize_xdfsreply (conn_sink (conn), 0, NULL));
    }

  if (sr->send_controls)
    {
      XdeltaControl *cont = sr->send_controls->data;

      sr->send_controls = sr->send_controls->next;

      return task_finish_put (conn,
			      task,
			      xdfs_sync_server_send_one_reply,
			      serialize_xdfsreply (conn_sink (conn), 1, &cont));

    }

  task_stack_pop (task, XdfsFamilyReplyData);

  return sr->ready (conn, task);
}

TaskControl
xdfs_sync_server_send_one_literal (Connection* conn, Task* task)
{
  XdfsFamilyReplyData *sr = task_stack_peek (task, XdfsFamilyReplyData);
  FileSegment *seg = sr->send_segments->data;

  /* @@@ leaky */
  sr->send_segments = sr->send_segments->next;

  return task_segments_put (conn,
			    task,
			    xdfs_sync_server_send_one_reply,
			    seg,
			    NULL,
			    NULL);
}
